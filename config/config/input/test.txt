layers:
- data:
  - type: max.stage.layer.CollisionLayer, max.stage
    layer_id: max.vision.ax.COLLISION_LAYER
  - type: max.vision.stage.layer.AXLabelLayer, max.vision
    data:
    - Name: Bedroom
      type: max.vision.stage.component.label.AXLabel, max.vision
    - Name: Dining Room
      type: max.vision.stage.component.label.AXLabel, max.vision
    - Name: Library
      type: max.vision.stage.component.label.AXLabel, max.vision
    - Name: Grandfather Clock
      Menu: false
      type: max.vision.stage.component.label.AXLabel, max.vision
    layer_id: max.vision.ax.LABEL_LAYER
  - type: max.vision.stage.layer.AXLabelLayer, max.vision
    layer_id: max.vision.ax.BOOKMARK_LAYER
  type: max.stage.layer.ContainerLayer, max.stage
  layer_id: max.vision.ax.ROOT
type: max.stage.controller.StageController, max.stage

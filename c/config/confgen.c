/* Copyright 2020 Roger Merryfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * file: config/confgen.c
 * description:
 *   Configuration header generating program.
 * author(s):
 *   William
 *
 **/

/* TODO: add an intepreter level on top of the lex */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define BUFBYTES  8192
#define KEYBYTES  256
#define RESPBYTES 512

static FILE *fconf = NULL;

static char key[KEYBYTES];
static char resp[RESPBYTES];
static char buf[BUFBYTES];

#include "confr.c"
#include "../src/bits/squote.inl"

/* TODO resolve fixup for new squote.inl */
#define _mxker_fsquote _fsquote

static void write_conf_value(char const *k, int quoted, FILE *f)
{
  if (quoted)
    _mxker_fsquote(f, k);
  else
    fprintf(f, "%s", k);
}

#define KEYWORD_IF  0x1
#define KEYWORD_FI  0x2
#define KEYWORD_NEG 0x4
static int write_conf(char const *k, int quoted, FILE *f, int show)
{
  char const *vargv[] = { "~", NULL, NULL };
  long offset = 0;
  int kind = 0;

  /* check for keywords first */
  if (k[0] == 'i' && k[1] == 'f')
  {
    offset = 2;
    while (k[offset] == ' ')
      ++offset;
    if (k[offset] == '@')
    {
      offset += 1;
      kind = KEYWORD_IF;
    }
    else if (k[offset] == '~' && k[offset+1] == '@')
    {
      offset += 2;
      kind = KEYWORD_IF|KEYWORD_NEG;
    }
    else
    {
      fprintf(stderr, "%s%s\n", "readconf: malformed if ", k);
      return -2;
    }
  }
  if (strcmp("@fi", k) == 0)
  {
    kind = KEYWORD_FI;
  }
  /*fprintf(stderr, "kind: 0x%03x, parsed: '%s', offset: '%s', first3c: '%c%c%c'\n", kind, k, k+offset, k[0], k[1], k[2]);*/

  vargv[1] = k + offset;

  if (kind != KEYWORD_FI)
  {
    fseek(fconf, 0, SEEK_SET);
    if (_mxker_confread(2, (char **)vargv, fconf, stderr, resp, RESPBYTES - 1) != 0)
    {
      fprintf(stderr, "%s%s\n", "readconf: can't find key ", k);
      return -1;
    }
    resp[RESPBYTES - 1] = '\0';

    if (kind & KEYWORD_IF)
    {
      /*fprintf(f, "! confgen: %s", k);*/
      if ((resp[0] == '\0') || (strcmp(resp, "0") == 0) || (strcmp(resp, "0x0") == 0))
        return kind; /* return failed if */
      else
        return kind | 0x800; /* return okay if */
    }
    else
      write_conf_value(resp, quoted, f);
  }
  /* else
    fprintf(f, "! confgen: %s", k);*/
  return kind;
}

int main(int argc, char *argv[])
{
  FILE *cf = NULL;
  FILE *hf = NULL;
  char *cp = NULL;
  char *cpr = NULL;
  char *kp = NULL;
  long nsubst = 0;
  long nsubstbad = 0;
  int eatnewline = 0;
  int state = 0;
  int ferr = 0;
  int ret = 0;
  char c = '\0';

  int ifstack[256] = {0};
  int *pifs = &ifstack[1]; /* push 0 for hidden, push 1 for show */
  ifstack[0] = 1;
  
  if (argc < 4)
  {
    fprintf(stderr, "%s\n", "not enough arguments.");
    return 1;
  }

  memset(key,  0, KEYBYTES);
  memset(buf,  0, BUFBYTES);
  memset(resp, 0, RESPBYTES);

  cf = fopen(argv[2], "r");
  if (cf == NULL)
  {
    fprintf(stderr, "%s%s\n", "cannot open input file ", argv[2]);
    return 1;
  }

  hf = fopen(argv[3], "w");
  if (hf == NULL)
  {
    fprintf(stderr, "%s%s\n", "cannot open output file ", argv[3]);
    fclose(cf);
    return 1;
  }
  
  fconf = fopen(argv[1], "r");
  if (fconf == NULL)
  {
    fprintf(stderr, "%s%s\n", "cannot open config file ", argv[1]);
    fclose(cf);
    fclose(hf);
    return 1;
  }

READMORE:
  state = 0;
  memset(buf, 0, BUFBYTES);
  if (fgets(buf, BUFBYTES-1, cf) == NULL)
  {
    ferr = ferror(stdin);
    goto HALT;
  }
  cp = &buf[0];

DELTA:
  state = 0;
  while (1)
  {
    c = *cp++;
    if (c == '\r')
    {
      /* TODO: handle windows cr nl madness */
      if (!eatnewline)
      {
        if (pifs[-1])
          fprintf(hf, "%c", '\r');
      }
    }
    else if (c == '\n')
    {
      if (eatnewline == 0)
      {
        if (pifs[-1])
          fprintf(hf, "%c", '\n');
      }
      else
        eatnewline = 0;
    }
    else if (c != '{' && c != '\0')
    {
      if (pifs[-1])
        fprintf(hf, "%c", c);
    }
    else if (c == '\0')
      goto READMORE;
    else
    {
      memset(key, 0, KEYBYTES);
      kp = &key[0];
      *kp++ = '{';
      break;
    }
  }
  state = 1;
  c = *kp++ = *cp++;
  if (c == '"') state |= 0x8;
  else if (c == '$')
  {
    state |= 0x2;
    state |= (kp - key) << 20;
  }
  if (~state & 0x2)
  {
    c = *kp++ = *cp++;
    if (c == '$')
    {
      state |= 0x2;
      state |= (kp - key) << 20;
    }
  }
  if (~state & 0x2)
  {
    if (pifs[-1])
      fprintf(hf, "%s", key);
    goto DELTA;
  }
  while (1)
  {
    c = *cp++;
    if (c == '$')
    {
      state &= ~0x1;
      ret = write_conf(&key[state >> 20], (state & 0x8) != 0, hf, pifs[-1]);
      switch (ret)
      {
      case 0x000:
        ++nsubst;
        break;
      case 0x001:
        /* if failed */
        pifs[0] = 0;
        ++pifs;
        eatnewline = 1;
        break;
      case 0x801:
        /* if ok */
        pifs[0] = pifs[-1];
        ++pifs;
        eatnewline = 1;
        break;
      case 0x005:
        /* neg if ok */
        pifs[0] = pifs[-1];
        ++pifs;
        eatnewline = 1;
        break;
      case 0x805:
        /* neg if failed */
        pifs[0] = 0;
        ++pifs;
        eatnewline = 1;
        break;
      case 0x002:
        /* fi */
        if (pifs - 1 == &ifstack[0])
        {
          fprintf(stderr, "%s\n", "readconf: fatal: fi outside if block");
          goto HALT;
        }
        --pifs;
        eatnewline = 1;
        break;
      default:
        ++nsubstbad;
        break;
      };
      break;
    }
    else if (c == '\0')
    {
      if (pifs[-1])
      {
        fprintf(stderr, "KEY: %s", key);
        fprintf(hf, "%s", key);
      }
      goto READMORE;
    }
    else
      *kp++ = c;
  }
  while (1)
  {
    c = *cp++;
    if (c == '}')
      goto DELTA;
    else if (c == '\0')
      goto READMORE;
  }

HALT:
  if (state != 0)
    fprintf(stderr, "%s%08x\n", "Parsing automata halted in non-zero state! ", state);

  if (ferr != 0)
    fprintf(stderr, "%s%d%s\n", "Fatal error: File I/O error ", ferr, "\n");
  else if (state != 0)
    fprintf(stderr, "%s\n", "Fatal error: Input malformed.");

  fclose(cf);
  fclose(hf);
  fclose(fconf);

  if (ferr == 0 && state == 0)
    fprintf(stderr, "%s%s%s%s\n", "mxker conf ", argv[2], " → ", argv[3]);
  fprintf(stderr, "    %ld substitution(s) ok\n", nsubst);
  fprintf(stderr, "    %ld substitution(s) failed\n", nsubstbad);

  if (nsubstbad > 0)
    return 1; /* halt the build */
  else
    return 0;
}


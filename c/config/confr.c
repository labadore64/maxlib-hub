/* Copyright 2020 Roger Merryfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * file: config/confr.c
 * description:
 *   Configuration scraping subprogram.
 * author(s):
 *   William
 *
 **/

#define _MXKER_CONFR_LINEBYTES 512
#define _MXKER_CONFR_KEYBYTES 256

static char _mkker_confr_buf[_MXKER_CONFR_LINEBYTES];
static char _mkker_confr_key[_MXKER_CONFR_KEYBYTES];

static int _mxker_confread(int argc, char *argv[], FILE *finput, FILE *foutput, char *buf, size_t nbuf)
{
  int r, blen, klen, firstnonspace, iscomment;
  char *pk, *pb, **ppargv;
  char *ptmp;

  memset(_mkker_confr_buf, 0, _MXKER_CONFR_LINEBYTES);
  memset(_mkker_confr_key, 0, _MXKER_CONFR_KEYBYTES);

  if (argc < 2)
    return 1;

  r = 1;
  ppargv = &argv[0];

  while (argc-- > 1)
  {
    ++ppargv;

    strncpy(_mkker_confr_key, *ppargv, _MXKER_CONFR_KEYBYTES-1);
    klen = 0;
    ptmp = _mkker_confr_key;
    while (klen < _MXKER_CONFR_KEYBYTES-1 && *ptmp++ != '\0')
      klen = ptmp - _mkker_confr_key;
    /* non c99 compliant */
    /* klen = strnlen(_mkker_confr_key, _MXKER_CONFR_KEYBYTES-1); */

    r = 1;
    while (1)
    {
      if (fgets(_mkker_confr_buf, _MXKER_CONFR_LINEBYTES-1, finput) == NULL)
        break;
      
      _mkker_confr_buf[_MXKER_CONFR_LINEBYTES-1] = '\0';
      
      pb = &_mkker_confr_buf[0];
      while (*pb != '\0') ++pb;
      if (pb-&_mkker_confr_buf[0] > 0 && pb[-1] == '\n') pb[-1] = '\0';
      pb = &_mkker_confr_buf[0];

      firstnonspace = -1;
      iscomment = 0;
      while (*pb++ != '\0')
      {
        /* skip comment (!) line */
        if (pb[-1] != ' ' && firstnonspace < 0)
        {
          firstnonspace = pb - &_mkker_confr_buf[0] - 1;
          if (pb[-1] == '!') /* it's a comment */
          {
            iscomment = 1;
            break;
          }
        }
        if (*pb == '=')
          break;
      }

      /* skip comment */
      if (iscomment)
        continue;
      /* a key cannot be empty */
      if (firstnonspace < 0)
        continue;

      /* gobble whitespace */
      blen = pb - &_mkker_confr_buf[firstnonspace];
      if (blen == klen)
      {
        *pb = '\0';
        if (strncmp(_mkker_confr_key, _mkker_confr_buf+firstnonspace, klen) == 0)
        {
          if (nbuf > 0 && buf != NULL)
          {
            snprintf(buf, nbuf, "%s", &pb[1]);
          }
          else
          {
            fprintf(foutput, "%s", &pb[1]);
          }
          r = 0;
          break;
        }
      }
    }
  }

  return r;
}


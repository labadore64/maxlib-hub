/* Copyright 2020 Roger Merryfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * file: config/terminfo_collect.c
 * description:
 *   UNIX terminfo collector program
 * author(s):
 *   William
 * version:
 *   terminfo-collect-2
 *
 **/

/* probe the terminal's control codes using tput */

#include <unistd.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>

/* C quoted string printer */
#include "../src/bits/squote.inl"

struct spair_s
{
  char const *sleft;
  char const *sright;
};

struct spair_s terminals[] = {
  { .sleft="xterm",         .sright="xterm"         },
  { .sleft="rxvt-256color", .sright="rxvt_256color" },
  { .sleft="rxvt-unicode",  .sright="rxvt_unicode"  },
  { .sleft="linux",         .sright="linux"         },
  { .sleft="Eterm",         .sright="eterm"         },
  { .sleft="screen",        .sright="screen"        },
  { 0 },
};

struct spair_s keys[] = {
  { .sleft="F1",              .sright="kf1"    },
	{ .sleft="F2",              .sright="kf2"    },
	{ .sleft="F3",              .sright="kf3"    },
	{ .sleft="F4",              .sright="kf4"    },
	{ .sleft="F5",              .sright="kf5"    },
	{ .sleft="F6",              .sright="kf6"    },
	{ .sleft="F7",              .sright="kf7"    },
	{ .sleft="F8",              .sright="kf8"    },
	{ .sleft="F9",              .sright="kf9"    },
	{ .sleft="F10",             .sright="kf10"   },
	{ .sleft="F11",             .sright="kf11"   },
	{ .sleft="F12",             .sright="kf12"   },
	{ .sleft="INSERT",          .sright="kich1"  },
	{ .sleft="DELETE",          .sright="kdch1"  },
	{ .sleft="HOME",            .sright="khome"  },
	{ .sleft="END",             .sright="kend"   },
	{ .sleft="PGUP",            .sright="kpp"    },
	{ .sleft="PGDN",            .sright="knp"    },
	{ .sleft="KEY_ARROW_UP",    .sright="kcuu1"  },
	{ .sleft="KEY_ARROW_DOWN",  .sright="kcud1"  },
	{ .sleft="KEY_ARROW_LEFT",  .sright="kcub1"  },
	{ .sleft="KEY_ARROW_RIGHT", .sright="kcuf1"  },
  { 0 },
};

struct spair_s funcs[] = {
  { .sleft="MXTERMINFO_ENTER_CA",     .sright="smcup" },
	{ .sleft="MXTERMINFO_EXIT_CA",      .sright="rmcup" },
	{ .sleft="MXTERMINFO_SHOW_CURSOR",  .sright="cnorm" },
	{ .sleft="MXTERMINFO_HIDE_CURSOR",  .sright="civis" },
	{ .sleft="MXTERMINFO_CLEAR_SCREEN", .sright="clear" },
	{ .sleft="MXTERMINFO_SGR0",         .sright="sgr0"  },
	{ .sleft="MXTERMINFO_UNDERLINE",    .sright="smul"  },
	{ .sleft="MXTERMINFO_BOLD",         .sright="bold"  },
	{ .sleft="MXTERMINFO_BLINK",        .sright="blink" },
	{ .sleft="MXTERMINFO_REVERSE",      .sright="rev"   },
	{ .sleft="MXTERMINFO_ENTER_KEYPAD", .sright="smkx"  },
	{ .sleft="MXTERMINFO_EXIT_KEYPAD",  .sright="rmkx"  },
  { .sleft="MXTERMINFO_ENTER_MOUSE",  .sright=NULL    },
  { .sleft="MXTERMINFO_EXIT_MOUSE",   .sright=NULL    },
  { 0 },
};

/* >= 0 if output of tput read successfully, < 0 on error */
static ssize_t tput(char const *sterm, char const *sname, char *ppipebuf, size_t npipebuf)
{
  ssize_t nread = 0, ntotalread = 0;
  FILE *fproc = NULL;
  static char cmdbuf[256];
  int c;

  memset(cmdbuf, 0, sizeof(cmdbuf));
  strcat(cmdbuf, "tput -T");
  strcat(cmdbuf, sterm);
  strcat(cmdbuf, " ");
  strcat(cmdbuf, sname);

  fproc = popen(cmdbuf, "r");
  if (fproc == NULL)
  {
    perror("bad command");
    return -1;
  }

  while ((c = fgetc(fproc)) != EOF)
    ppipebuf[ntotalread++] = (char)c;

  return ntotalread;
}

static int tputescape(char const *sterm, char const *sname)
{
  char tputbuf[256];
  ssize_t nread = 0;
  char *ps = NULL;
  int c = 0;

  memset(tputbuf, 0, sizeof(tputbuf));
  nread = tput(sterm, sname, tputbuf, sizeof(tputbuf));

  if (nread < 0)
    return nread;

  ps = &tputbuf[0];
  _fsquote(stdout, tputbuf);

  return 0;
}

/* no re-entry */
static void do_term(char const *pterm, char const *pnick)
{
  struct spair_s *pk = NULL;

  printf("/* %s (%s) */\n", pterm, pnick);
  printf("static char const *_mxterminfo_%s_keys[] = {\n", pnick);
  
  for (pk = keys; pk->sleft != NULL && pk->sright != NULL; ++pk)
  {
    printf("  ");
    tputescape(pterm, pk->sright);
    printf("%s%s%s\n", ", /* ", pk->sleft, " */");
  }

  printf("%s", "  NULL,\n};\n");
  printf("static char const *_mxterminfo_%s_funcs[] = {\n", pnick);
  for (pk = funcs; pk->sleft != NULL && pk->sright != NULL; ++pk)
  {
    printf("  ");
    if (strcmp(pk->sright, "sgr") == 0)
      printf("%s", "\"\\033[3\%d;4\%dm\"");
    else if (strcmp(pk->sright, "cup") == 0)
      printf("%s", "\"\\033[\%d;\%dH\"");
    else
      tputescape(pterm, pk->sright);
    printf("%s%s%s\n", ", /* ", pk->sleft, " */");
  }
  for (; pk->sleft != NULL; ++pk)
  {
    if (strcmp(pk->sleft, "MXTERMINFO_ENTER_MOUSE") == 0)
      printf("%s/* %s */\n", "  "
          "\"\\033[?1000h\\033[?1002h\\033[?1015h\\033[?1006h\""
          ", ", pk->sleft);
    else if (strcmp(pk->sleft, "MXTERMINFO_EXIT_MOUSE") == 0)
      printf("%s/* %s */\n", "  "
          "\"\\033[?1006l\\033[?1015l\\033[?1002l\\033[?1000l\""
          ", ", pk->sleft);
    else
      break;
  } 
  printf("%s", "  NULL,\n};\n\n");
}

int main()
{
  register int i;
  struct spair_s *pk = NULL;

  for (pk = terminals; pk->sleft != NULL && pk->sright != NULL; ++pk)
    do_term(pk->sleft, pk->sright);

  printf("%s", "struct mxterm_term_s\n{\n"
               "  char const *sname;\n"
               "  char const *const *pskeys;\n"
               "  char const *const *psfunc;\n"
               "  unsigned long long nname;\n"
               "};\n\n"
               "static struct mxterm_term_s _mxterminfo_terms[] = {\n");

  for (pk = terminals; pk->sleft != NULL && pk->sright != NULL; ++pk)
  {
    printf("  { .nname=%luULL", (unsigned long)strlen(pk->sleft));
    printf("%s", ", .sname=");
    _fsquote(stdout, pk->sleft);
    printf(
        ", .pskeys=_mxterminfo_%s_keys, "
        ".psfunc=_mxterminfo_%s_funcs },\n", pk->sright, pk->sright);
  }

  printf("%s", "  { 0 },\n"
               "};\n\n"
               "enum\n"
               "{\n");

  for (i = 0, pk = funcs; pk->sleft != NULL; ++pk, ++i)
  {
    printf("  %s = %d,\n", pk->sleft, i);
  }

  printf("  MXTERMINFO_FUNC_COUNT = %d%s", i, ",\n};\n");
  
  return 0;
}


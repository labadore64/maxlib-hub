/* Copyright 2020 Roger Merryfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * file: config/conf.c
 * description:
 *   Configuration program.
 * author(s):
 *   William
 *
 **/

/* TODO : we need implementations of strnlen and strncmp as well as popen
 *        in order to avoid some of these things breaking in the future. */

#if !defined(_MXKER_CONF_NO_STANDALONE)
#include "conf_incl.inl"
#include "../mxker.conf.h"
#endif
#include "../src/bits/crc8.inl"

/* This is the family that is regarded as our own. */
static char const *_magicfamily = "roger";
#define MAGICFAMILYBYTES 5

static char timebuf[80];
static time_t now;

/* Write out the time of configuration. */
unsigned long print_time()
{
  struct tm ts;

  time(&now);
  ts = *localtime(&now);
  memset(timebuf, 0, sizeof(timebuf));
  strftime(timebuf, sizeof(timebuf), "%a %Y-%m-%d %H:%M:%S %Z", &ts);

  fprintf(stderr, "%s%s\n", "mxker configured at ", timebuf);

  printf("%s", "\n"
               "! Configuration time\n");
  printf(      "    mxker.version.time.epoch=%lu\n", now);
  printf(      "    mxker.version.time.string=%s\n", timebuf);

  return 0;
}

/* Write out version info. */
unsigned long print_version()
{
  unsigned long vuser, checksum;

  /* Determine if this is a user configuration or a developer configuration. */
  vuser = (strncmp(_magicfamily, version_family, MAGICFAMILYBYTES) != 0);

  /* Construct the checksum of the version information. */
  checksum = 0xFF & now;
  checksum = _mxker_crc8s(checksum, version_family);
  checksum = _mxker_crc8s(checksum, timebuf);
  checksum = _mxker_crc8_16(checksum, version_major);
  checksum = _mxker_crc8_16(checksum, version_minor);
  checksum = _mxker_crc8_24(checksum, version_patch);
  
  /* Write out the version info in hexadecimal. */
  char buf[20];
  memset(buf, 0, sizeof(buf));
  snprintf(buf, 20, "%04x%04x%06x%02x",
      (unsigned int)version_major, (unsigned int)version_minor,
      (unsigned int)version_patch, (unsigned int)checksum);

  printf("%s", "\n"
               "! Version info\n");
  printf("%s", "  ! String encoding\n");

  /* version info */
  printf("%s", "    ! Semantic\n");
  printf(      "        mxker.version.string.major=%lu\n", version_major);
  printf(      "        mxker.version.string.minor=%lu\n", version_minor);
  printf(      "        mxker.version.string.patch=%lu\n", version_patch);
  printf("%s", "    ! Variant\n");
  printf(      "        mxker.version.string.family=%s\n", version_family);
  printf("%s", "    ! Canonical version string\n");
  printf(      "        mxker.version.string=%lu.%lu.%lu (%s)\n",
      version_major, version_minor, version_patch, version_family);

  printf("%s", "  ! Big endian hex encoding\n");
  printf("%s", "    ! Semantic\n");
  printf(      "        mxker.version.hex.major=%04x\n",
      (unsigned int)version_major);
  printf(      "        mxker.version.hex.minor=%04x\n",
      (unsigned int)version_minor);
  printf(      "        mxker.version.hex.patch=%06x\n",
      (unsigned int)version_patch);
  printf("%s", "    ! Configuration checksum\n");
  printf(      "        mxker.version.hex.crc8=%02x\n",
      (unsigned int)checksum);
  printf("%s", "    ! Canonical version encoding\n");
  printf(      "        mxker.version.hex.64=%s\n", &buf[0]);
  printf("%s", "    ! 32-bit low word\n");
  printf(      "        mxker.version.hex.lo32=%s\n", &buf[0]+8);
  buf[8] = '\0';
  printf("%s", "    ! 32-bit high word\n");
  printf(      "        mxker.version.hex.hi32=%s\n", &buf[0]);
  printf("%s", "    ! is this a user variant or a canonical variant?\n");
  printf(      "        mxker.version.user=%lu\n", vuser);

  printf("%s", "\n"
               "! Library names\n");

  printf("%s", "  ! Release build\n");

  /* release unix libname */
  printf("%s", "    ! ld libname\n");
  printf(      "        mxker.libname.unix.release=mxker%s\n", version_family);

  /* release soname */
  printf("%s", "    ! ldd soname\n");
  printf(      "        mxker.soname.release.base=libmxker%s.so\n",
      version_family);
  printf(      "        mxker.soname.release.major=libmxker%s.so.%lu\n",
      version_family, version_major);
  printf(      "        mxker.soname.release.minor=libmxker%s.so.%lu.%lu\n",
      version_family, version_major, version_minor);
  printf(      "        mxker.soname.release.full=libmxker%s.so.%lu.%lu.%lu\n",
      version_family, version_major, version_minor, version_patch);

  /* release DLL name */
  printf("%s", "    ! win32 native and mono (dllmap) library name\n");
  printf(      "        mxker.dllname.release.base=mxker-%s.dll\n",
      version_family);
  printf(      "        mxker.dllname.release.major=mxker-%s-%lu.dll\n",
      version_family, version_major);
  printf(      "        mxker.dllname.release.minor=mxker-%s-%lu.%lu.dll\n",
      version_family, version_major, version_minor);
  printf(      "        mxker.dllname.release.full=mxker-%s-%lu.%lu.%lu.dll\n",
      version_family, version_major, version_minor, version_patch);

  printf("%s", "  ! Debug build\n");

  /* debug unix libname */
  printf("%s", "    ! ld libname\n");
  printf(      "        mxker.libname.unix.debug=mxker%sd\n", version_family);

  /* debug soname */
  printf("%s", "    ! ldd soname\n");
  printf(      "        mxker.soname.debug.base=libmxker%sd.so\n",
      version_family);
  printf(      "        mxker.soname.debug.major=libmxker%sd.so.%lu\n",
      version_family, version_major);
  printf(      "        mxker.soname.debug.minor=libmxker%sd.so.%lu.%lu\n",
      version_family, version_major, version_minor);
  printf(      "        mxker.soname.debug.full=libmxker%sd.so.%lu.%lu.%lu\n",
      version_family, version_major, version_minor, version_patch);

  /* debug DLL name */
  printf("%s", "    ! win32 native and mono (dllmap) library name\n");
  printf(      "        mxker.dllname.debug.base=mxkerd-%s.dll\n",
      version_family);
  printf(      "        mxker.dllname.debug.major=mxkerd-%s-%lu.dll\n",
      version_family, version_major);
  printf(      "        mxker.dllname.debug.minor=mxkerd-%s-%lu.%lu.dll\n",
      version_family, version_major, version_minor);
  printf(      "        mxker.dllname.debug.full=mxkerd-%s-%lu.%lu.%lu.dll\n",
      version_family, version_major, version_minor, version_patch);

  return 0;
}

unsigned long print_mem()
{
  /* determine if the allocator supports separate arenas */
  unsigned long striated =
    (mem_heap && mem_blockcycler) |
    (mem_heap && mem_bigstacks) |
    (mem_blockcycler && mem_bigstacks);

  printf("%s", "\n"
               "! Memory configuration\n");

  /* memory info */
  printf(      "    mxker.mem.service=%lu\n", mem_service);
  printf(      "    mxker.mem.heap=%lu\n", mem_heap);
  printf(      "    mxker.mem.blockcycler=%lu\n", mem_blockcycler);
  printf(      "    mxker.mem.bigstacks=%lu\n", mem_bigstacks);
  printf(      "    mxker.mem.paranoid=%lu\n", mem_paranoid);
  printf(      "    mxker.mem.guard=%lu\n", mem_guard);
  printf(      "    mxker.mem.striated=%lu\n", striated);

  return 0;
}

/* C type size defines */
#define sc  ((unsigned long)cc_size_bytes_char)
#define ss  ((unsigned long)cc_size_bytes_short)
#define si  ((unsigned long)cc_size_bytes_int)
#define sl  ((unsigned long)cc_size_bytes_long)
#define sll ((unsigned long)cc_size_bytes_llong)

#define sf  ((unsigned long)cc_size_bytes_float)
#define sfd ((unsigned long)cc_size_bytes_double)
#define sfl ((unsigned long)cc_size_bytes_ldouble)

/* C# type size defines */
#define cssb ((unsigned long)1)
#define csss ((unsigned long)csc_size_bytes_short)
#define cssi ((unsigned long)csc_size_bytes_int)
#define cssl ((unsigned long)csc_size_bytes_long)

#define cssf ((unsigned long)csc_size_bytes_float)
#define cssd ((unsigned long)csc_size_bytes_double)

#define css0 ((unsigned long)csc_size_bytes_bool)

/* C/mxker type names and type IDs */
char const *const /* C integer names (without signed and unsigned keywords) */
  intnames[]      = {"char",  "short",  "int",   "long",   "long long", NULL};
char const *const /* mxker mapped unsigned integer names */
  intua[]         = {"uchar", "ushort", "uword", "ulword", "ullword",   NULL};
char const *const /* mxker mapped unsigned integer IDs   */
  intutids[]      = {"1",     "2",      "3",     "4",      "5",         NULL};
char const *const /* mxker mapped signed integer names   */
  intsa[]         = {"schar", "sshort", "sword", "slword", "sllword",   NULL};
char const *const /* mxker mapped signed integer IDs     */
  intstids[]      = {"6",     "7",      "8",     "9",      "A",         NULL};

char const *const /* C floating point names */
  floatnames[]    = {"float", "double", "long double", NULL};
char const *const /* mxker mapped floating point names */
  floata[]        = {"float", "lfloat", "llfloat",     NULL};
char const *const /* mxker mapped floating point IDs   */
  floattids[]     = {"B",     "C",      "D",           NULL};

/* C/mxker type sizes (filled in at runtime) */
unsigned long intsizes[6]   = {0}; /* C integer type sizes */
unsigned long floatsizes[4] = {0}; /* C floating point type sizes */

/* C# type names */
char const *const /* C# signed integer type names   */
  cssintnames[]   = {"sbyte", "short",  "int",  "long",  NULL};
char const *const /* C# unsigned integer type names */
  csuintnames[]   = {"byte",  "ushort", "uint", "ulong", NULL};
char const *const /* C# floating point type names   */
  csfloatnames[]  = {"float", "double", NULL};

char const *csboolname = "bool";

/* C# type sizes (filled in at runtime) */
unsigned long csintsizes[5]   = {0}; /* C# integer type sizes */
unsigned long csfloatsizes[3] = {0}; /* C# floating point type sizes */
unsigned long csboolsize;            /* C# boolean type size */

/* mxker integer sizes */
static unsigned long const cc_intsizes[] = {
  1,
  4,
  8,
  12,
  16,
  20,
  24,
  28,
  32,
  36,
  40,
  44,
  48,
  52,
  56,
  60,
  64,
  80,
  96,
  112,
  128,
  0,
};

/* mxker float sizes */
static unsigned long const cc_floatsizes[] = {
  8,
  16,
  32,
  64,
  80,
  128,
  0,
};

/* requested type allocations; "auto" by default (see config file) */
char const *cc_intsizes_of[22] = {0};
char const *cc_intsizes_of_fast[22] = {0};
char const *cc_floatsizes_of[7] = {0};
char const *cc_floatsizes_of_fast[7] = {0};

/* float and int binding tables */
unsigned long csc_least_int_binding_table[22] = {0};
unsigned long csc_fast_int_binding_table[22] = {0};
unsigned long csc_least_float_binding_table[7] = {0};
unsigned long csc_fast_float_binding_table[7] = {0};

void _print_or_none(char const *k, char const *p)
{
  if (strncmp(p, "none", 4) != 0)
    printf("%s=%s\n", k, p);
  else
    printf("%s=\n", k);
}

unsigned long print_cenv()
{
  int i, j, k, largest_int, largest_float;
  char const *const *p;

  intsizes[0] = sc;
  intsizes[1] = ss;
  intsizes[2] = si;
  intsizes[3] = sl;
  intsizes[4] = sll;
  intsizes[5] = 0;

  floatsizes[0] = sf;
  floatsizes[1] = sfd;
  floatsizes[2] = sfl;
  floatsizes[3] = 0;

  cc_intsizes_of[0]  = cc_bit_least;
  cc_intsizes_of[1]  = cc_int4_least;
  cc_intsizes_of[2]  = cc_int8_least;
  cc_intsizes_of[3]  = cc_int12_least;
  cc_intsizes_of[4]  = cc_int16_least;
  cc_intsizes_of[5]  = cc_int20_least;
  cc_intsizes_of[6]  = cc_int24_least;
  cc_intsizes_of[7]  = cc_int28_least;
  cc_intsizes_of[8]  = cc_int32_least;
  cc_intsizes_of[9]  = cc_int36_least;
  cc_intsizes_of[10] = cc_int40_least;
  cc_intsizes_of[11] = cc_int44_least;
  cc_intsizes_of[12] = cc_int48_least;
  cc_intsizes_of[13] = cc_int52_least;
  cc_intsizes_of[14] = cc_int56_least;
  cc_intsizes_of[15] = cc_int60_least;
  cc_intsizes_of[16] = cc_int64_least;
  cc_intsizes_of[17] = cc_int80_least;
  cc_intsizes_of[18] = cc_int96_least;
  cc_intsizes_of[19] = cc_int112_least;
  cc_intsizes_of[20] = cc_int128_least;
  cc_intsizes_of[21] = NULL;

  cc_intsizes_of_fast[0]  = cc_bit_fast;
  cc_intsizes_of_fast[1]  = cc_int4_fast;
  cc_intsizes_of_fast[2]  = cc_int8_fast;
  cc_intsizes_of_fast[3]  = cc_int12_fast;
  cc_intsizes_of_fast[4]  = cc_int16_fast;
  cc_intsizes_of_fast[5]  = cc_int20_fast;
  cc_intsizes_of_fast[6]  = cc_int24_fast;
  cc_intsizes_of_fast[7]  = cc_int28_fast;
  cc_intsizes_of_fast[8]  = cc_int32_fast;
  cc_intsizes_of_fast[9]  = cc_int36_fast;
  cc_intsizes_of_fast[10] = cc_int40_fast;
  cc_intsizes_of_fast[11] = cc_int44_fast;
  cc_intsizes_of_fast[12] = cc_int48_fast;
  cc_intsizes_of_fast[13] = cc_int52_fast;
  cc_intsizes_of_fast[14] = cc_int56_fast;
  cc_intsizes_of_fast[15] = cc_int60_fast;
  cc_intsizes_of_fast[16] = cc_int64_fast;
  cc_intsizes_of_fast[17] = cc_int80_fast;
  cc_intsizes_of_fast[18] = cc_int96_fast;
  cc_intsizes_of_fast[19] = cc_int112_fast;
  cc_intsizes_of_fast[20] = cc_int128_fast;
  cc_intsizes_of_fast[21] = NULL;

  cc_floatsizes_of[0] = cc_binary8_least;
  cc_floatsizes_of[1] = cc_binary16_least;
  cc_floatsizes_of[2] = cc_binary32_least;
  cc_floatsizes_of[3] = cc_binary64_least;
  cc_floatsizes_of[4] = cc_binary80_least;
  cc_floatsizes_of[5] = cc_binary128_least;
  cc_floatsizes_of[6] = NULL;

  cc_floatsizes_of_fast[0] = cc_binary8_fast;
  cc_floatsizes_of_fast[1] = cc_binary16_fast;
  cc_floatsizes_of_fast[2] = cc_binary32_fast;
  cc_floatsizes_of_fast[3] = cc_binary64_fast;
  cc_floatsizes_of_fast[4] = cc_binary80_fast;
  cc_floatsizes_of_fast[5] = cc_binary128_fast;
  cc_floatsizes_of_fast[6] = NULL;

  printf("%s", "\n"
               "! C environment configuration\n");

  printf(      "    mxker.cc.service=%lu\n", cc_service);
  printf(      "    mxker.cc.allowinject=%lu\n", cc_allowinject);
  printf(      "    mxker.cc.program=%s\n", cc_program);
  printf(      "    mxker.cc.arch=%s\n", cc_arch);

  printf("%s", "  ! Extra flags");

  /* compat extra preprocessor flags (all) */
  printf("%s", "\n"
               "      mxker.cc.extra cppflags=");
  for (p = &cc_extra_cppflags[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&cc_extra_cppflags[0]?"":" ", *p);

  /* extra preprocessor flags (all+release) */
  printf("%s", "\n"
               "      mxker.cc.extra cppflags.release=");
  for (p = &cc_extra_cppflags[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&cc_extra_cppflags[0]?"":" ", *p);
  if (cc_extra_cppflags_r[0] != NULL) printf("%c", ' ');
  for (p = &cc_extra_cppflags_r[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&cc_extra_cppflags_r[0]?"":" ", *p);

  /* extra preprocessor flags (all+debug) */
  printf("%s", "\n"
               "      mxker.cc.extra cppflags.debug=");
  for (p = &cc_extra_cppflags[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&cc_extra_cppflags[0]?"":" ", *p);
  if (cc_extra_cppflags_d[0] != NULL) printf("%c", ' ');
  for (p = &cc_extra_cppflags_d[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&cc_extra_cppflags_d[0]?"":" ", *p);

  /* compat extra compiler flags (all) */
  printf("%s", "\n"
               "      mxker.cc.extra cflags=");
  for (p = &cc_extra_cflags[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&cc_extra_cflags[0]?"":" ", *p);

  /* extra compiler flags (all+release) */
  printf("%s", "\n"
               "      mxker.cc.extra cflags.release=");
  for (p = &cc_extra_cflags[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&cc_extra_cflags[0]?"":" ", *p);
  if (cc_extra_cflags_r[0] != NULL) printf("%c", ' ');
  for (p = &cc_extra_cflags_r[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&cc_extra_cflags_r[0]?"":" ", *p);

  /* extra compiler flags (all+debug) */
  printf("%s", "\n"
               "      mxker.cc.extra cflags.debug=");
  for (p = &cc_extra_cflags[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&cc_extra_cflags[0]?"":" ", *p);
  if (cc_extra_cflags_d[0] != NULL) printf("%c", ' ');
  for (p = &cc_extra_cflags_d[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&cc_extra_cflags_d[0]?"":" ", *p);

  /* compat extra linker flags (all) */
  printf("%s", "\n"
               "      mxker.cc.extra ldflags=");
  for (p = &cc_extra_ldflags[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&cc_extra_ldflags[0]?"":" ", *p);

  /* extra linker flags (all+release) */
  printf("%s", "\n"
               "      mxker.cc.extra ldflags.release=");
  for (p = &cc_extra_ldflags[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&cc_extra_ldflags[0]?"":" ", *p);
  if (cc_extra_ldflags_r[0] != NULL) printf("%c", ' ');
  for (p = &cc_extra_ldflags_r[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&cc_extra_ldflags_r[0]?"":" ", *p);

  /* extra linker flags (all+debug) */
  printf("%s", "\n"
               "      mxker.cc.extra ldflags.debug=");
  for (p = &cc_extra_ldflags[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&cc_extra_ldflags[0]?"":" ", *p);
  if (cc_extra_ldflags_d[0] != NULL) printf("%c", ' ');
  for (p = &cc_extra_ldflags_d[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&cc_extra_ldflags_d[0]?"":" ", *p);

  if (csc_bindings == 0)
  {
    /* TODO Make sure there aren't any more needed stubs. */
    printf("%s", "\n  ! No C# bindings, prevent makefile generation from "
                       "failing with these stubs.");
    printf("%s", "\n      mxker.csc.bindings=");
    printf("%s", "\n      mxker.csc.program=");
    printf("%s", "\n      mxker.csc.extra flags=");
    printf("%s", "\n      mxker.csc.extra flags.release=");
    printf("%s", "\n      mxker.csc.extra flags.debug=");
  }

  printf("%s", "\n"
               "  ! Native arithmetic type widths (in bits).\n");

  printf(      "      mxker.cc.width.char=%lu\n",    cc_size_bits_char*sc);
  printf(      "      mxker.cc.width.short=%lu\n",   cc_size_bits_char*ss);
  printf(      "      mxker.cc.width.word=%lu\n",    cc_size_bits_char*si);
  printf(      "      mxker.cc.width.lword=%lu\n",   cc_size_bits_char*sl);
  printf(      "      mxker.cc.width.llword=%lu\n",  cc_size_bits_char*sll);
  printf(      "      mxker.cc.width.float=%lu\n",   cc_size_bits_char*sf);
  printf(      "      mxker.cc.width.lfloat=%lu\n",  cc_size_bits_char*sfd);
  printf(      "      mxker.cc.width.llfloat=%lu\n", cc_size_bits_char*sfl);

  printf("%s", "  ! Native arithmetic type sizes (in bytes).\n");

  printf(      "      mxker.cc.size.char=%lu\n",     sc);
  printf(      "      mxker.cc.size.short=%lu\n",    ss);
  printf(      "      mxker.cc.size.word=%lu\n",     si);
  printf(      "      mxker.cc.size.lword=%lu\n",    sl);
  printf(      "      mxker.cc.size.llword=%lu\n",   sll);
  printf(      "      mxker.cc.size.float=%lu\n",    sf);
  printf(      "      mxker.cc.size.lfloat=%lu\n",   sfd);
  printf(      "      mxker.cc.size.llfloat=%lu\n",  sfl);

  printf("%s", "  ! Base arithmetic type mapping\n");

  j = 0; /* borrow j to keep track of size */
  largest_int = 0;
  for (i = 0; intnames[i] != NULL; ++i)
  {
    printf(      "    ! Native C %s mapping\n", intnames[i]);
    printf("%s", "      ! Type names\n");
    printf(      "          mxker.cc.type.%s=unsigned %s\n", intua[i],
        intnames[i]);
    printf(      "          mxker.cc.type.%s=  signed %s\n", intsa[i],
        intnames[i]);
    printf("%s", "      ! Base type IDs\n");
    printf(      "          mxker.cc.tid.%s=%s\n", intua[i], intutids[i]);
    printf(      "          mxker.cc.tid.%s=%s\n", intsa[i], intstids[i]);
    if (intsizes[i] > j)
    {
      largest_int = i;
      j = intsizes[i];
    }
  }
  k = 0; /* borrow k to keep track of size */
  largest_float = 0;
  for (i = 0; floatnames[i] != NULL; ++i)
  {
    printf(      "    ! Native C %s mapping\n", floatnames[i]);
    printf("%s", "      ! Type names\n");
    printf(      "          mxker.cc.type.%s=%s\n", floata[i], floatnames[i]);
    printf("%s", "      ! Base type IDs\n");
    printf(      "          mxker.cc.tid.%s=%s\n", floata[i], floattids[i]);
    if (floatsizes[i] > k)
    {
      largest_float = i;
      k = floatsizes[i];
    }
  }

  printf("%s", "  ! Constrained arithmetic type mapping\n");

  printf("%s", "    ! Smallest constrained boolean flag types\n");
  /* HARD DEPRECATION */
  /* printf("mxker.cc.least.int.1=%s\n", intnames[0]); */
  printf("%s", "      ! Native type names\n");
  printf(      "          mxker.cc.least.uint.1=unsigned %s\n", intnames[0]);
  printf(      "          mxker.cc.least.sint.1=signed %s\n", intnames[0]);
  printf("%s", "      ! Base type IDs\n");
  printf(      "          mxker.cc.tid.least.uint.1=%s\n", intutids[0]);
  printf(      "          mxker.cc.tid.least.sint.1=%s\n", intstids[0]);
  printf("%s", "      ! Size in bytes\n");
  printf(      "          mxker.cc.size.least.uint.1=%lu\n", intsizes[0]);
  printf(      "          mxker.cc.size.least.sint.1=%lu\n", intsizes[0]);
  printf("%s", "      ! Size in bits\n");
  printf(      "          mxker.cc.width.least.uint.1=%lu\n",
      intsizes[0]*cc_size_bits_char);
  printf(      "          mxker.cc.width.least.sint.1=%lu\n",
      intsizes[0]*cc_size_bits_char);

  printf("%s", "    ! Fast constrained boolean flag types\n");
  /* HARD DEPRECATION */
  /* printf("mxker.cc.fast.int.1=%s\n", intnames[2]); */
  printf("%s", "      ! Native type names\n");
  printf(      "          mxker.cc.fast.uint.1=unsigned %s\n", intnames[2]);
  printf(      "          mxker.cc.fast.sint.1=signed %s\n", intnames[2]);
  printf("%s", "      ! Base type IDs\n");
  printf(      "          mxker.cc.tid.fast.uint.1=%s\n", intutids[2]);
  printf(      "          mxker.cc.tid.fast.sint.1=%s\n", intstids[2]);
  printf("%s", "      ! Size in bytes\n");
  printf(      "          mxker.cc.size.least.uint.1=%lu\n", intsizes[2]);
  printf(      "          mxker.cc.size.least.sint.1=%lu\n", intsizes[2]);
  printf("%s", "      ! Size in bits\n");
  printf(      "          mxker.cc.width.least.uint.1=%lu\n",
      intsizes[2]*cc_size_bits_char);
  printf(      "          mxker.cc.width.least.sint.1=%lu\n",
      intsizes[2]*cc_size_bits_char);

  printf("%s", "    ! Largest constrained integer types\n");
  /* HARD DEPRECATION */
  /* printf("mxker.cc.type.largest.int=%s\n", intnames[largest_int]); */
  printf("%s", "      ! Native type names\n");
  printf(      "          mxker.cc.type.largest.sint=signed %s\n",
      intnames[largest_int]);
  printf(      "          mxker.cc.type.largest.uint=unsigned %s\n",
      intnames[largest_int]);
  printf("%s", "      ! Base type IDs\n");
  printf(      "          mxker.cc.tid.largest.uint=%s\n",
      intutids[largest_int]);
  printf(      "          mxker.cc.tid.largest.sint=%s\n",
      intstids[largest_int]);
  printf("%s", "      ! Size in bytes\n");
  printf(      "          mxker.cc.size.largest.uint=%lu\n",
      intsizes[largest_int]);
  printf(      "          mxker.cc.size.largest.sint=%lu\n",
      intsizes[largest_int]);
  printf("%s", "      ! Size in bits\n");
  printf(      "          mxker.cc.width.largest.uint=%lu\n",
      intsizes[largest_int]*cc_size_bits_char);
  printf(      "          mxker.cc.width.largest.sint=%lu\n",
      intsizes[largest_int]*cc_size_bits_char);

  printf("%s", "    ! Largest constrained float type\n");
  printf(      "        mxker.cc.type.largest.float=%s\n",
      floatnames[largest_float]);
  printf("%s", "      ! Base type ID\n");
  printf(      "          mxker.cc.tid.largest.float=%s\n",
      floattids[largest_float]);
  printf("%s", "      ! Size in bytes\n");
  printf(      "          mxker.cc.size.largest.float=%lu\n",
      floatsizes[largest_float]);
  printf("%s", "      ! Size in bits\n");
  printf(      "          mxker.cc.width.largest.float=%lu\n",
      floatsizes[largest_float]*cc_size_bits_char);

  printf("%s",
      "    ! Smallest constrained explicitly bounded integer types\n");

  i = 1;
  while (cc_intsizes_of[i] != NULL)
  {
    printf("      ! Smallest integer types of at least %lu bits.\n",
        cc_intsizes[i]);
    if (strncmp(cc_intsizes_of[i], "auto", 4) != 0)
    {
      fprintf(stderr, "%s",
          "explicit configuration of arithmetic types not yet supported!\n");
      exit(1);
      printf("mxker.cc.least.int.%lu=%s\n", cc_intsizes[i], cc_intsizes_of[i]);
      /* FIXME : Add type ID deduction and validation so that explicit
       * configuration of arithmetic types can be supported. */
      ++i;
      continue;
    }
    j = 0;
    while (1)
    {
      if (intsizes[j]*cc_size_bits_char >= i*4)
      {
        printf("%s", "        ! Native type names\n");
        printf(      "            mxker.cc.least.sint.%lu=signed %s\n",
            cc_intsizes[i], intnames[j]);
        printf(      "            mxker.cc.least.uint.%lu=unsigned %s\n",
            cc_intsizes[i], intnames[j]);
        printf("%s", "        ! Base type IDs\n");
        printf(      "            mxker.cc.tid.least.uint.%lu=%s\n",
            cc_intsizes[i], intutids[j]);
        printf(      "            mxker.cc.tid.least.sint.%lu=%s\n",
            cc_intsizes[i], intstids[j]);

        /* C# int binding table entry */
        csc_least_int_binding_table[i] = intsizes[j];
        break;
      }
      if (intsizes[++j] == 0)
      {
        printf("%s", "        ! Native type names (N/A)\n");
        printf(      "            mxker.cc.least.sint.%lu=%s\n",
            cc_intsizes[i], "void");
        printf(      "            mxker.cc.least.uint.%lu=%s\n",
            cc_intsizes[i], "void");
        printf("%s", "        ! Base type IDs (N/A)\n");
        printf(      "            mxker.cc.tid.least.uint.%lu=%s\n",
            cc_intsizes[i], "0");
        printf(      "            mxker.cc.tid.least.sint.%lu=%s\n",
            cc_intsizes[i], "0");

        /* C# int binding table entry */
        csc_least_int_binding_table[i] = 0;
        break;
      }
    }
    ++i;
  }

  printf("%s",
      "    ! Smallest constrained explicitly bounded floating point types\n");

  i = 0;
  while (cc_floatsizes_of[i] != NULL)
  {
    printf("      ! Smallest floating point type of at least %lu bits.\n",
        cc_floatsizes[i]);
    if (strncmp(cc_floatsizes_of[i], "auto", 4) != 0)
    {
      fprintf(stderr, "%s",
          "explicit configuration of arithmetic types not yet supported!\n");
      exit(1);
      printf("mxker.cc.least.float.%lu=%s\n", cc_floatsizes[i],
          cc_floatsizes_of[i]);
      /* FIXME : Add type ID deduction and validation so that explicit
       * configuration of arithmetic types can be supported. */
      ++i;
      continue;
    }
    j = 0;
    while (1)
    {
      if (floatsizes[j]*cc_size_bits_char >= cc_floatsizes[i])
      {
        printf("%s", "        ! Native type name\n");
        printf(      "            mxker.cc.least.float.%lu=%s\n",
            cc_floatsizes[i], floatnames[j]);
        printf("%s", "        ! Base type ID\n");
        printf(      "            mxker.cc.tid.least.float.%lu=%s\n",
            cc_floatsizes[i], floattids[j]);

        /* C# float binding table entry */
        csc_least_float_binding_table[i] = floatsizes[j];
        break;
      }
      if (floatsizes[++j] == 0)
      {
        printf("%s", "        ! Native type name (N/A)\n");
        printf(      "            mxker.cc.least.float.%lu=%s\n",
            cc_floatsizes[i], "int");
        printf("%s", "        ! Base type ID (N/A)\n");
        printf(      "            mxker.cc.tid.least.float.%lu=%s\n",
            cc_floatsizes[i], "0");

        /* C# float binding table entry */
        csc_least_float_binding_table[i] = 0;
        break;
      }
    }
    ++i;
  }

  printf("%s", "    ! Fast constrained explicitly bounded integer types\n");

  i = 1;
  while (cc_intsizes_of_fast[i] != NULL)
  {
    printf("      ! Fast integer types of at least %lu bits.\n",
        cc_intsizes[i]);
    if (strncmp(cc_intsizes_of_fast[i], "auto", 4) != 0)
    {
      fprintf(stderr, "%s",
          "explicit configuration of arithmetic types not yet supported!\n");
      exit(1);
      printf("mxker.cc.fast.int.%lu=%s\n", cc_intsizes[i],
          cc_intsizes_of_fast[i]);
      /* FIXME : Add type ID deduction and validation so that explicit
       * configuration of arithmetic types can be supported. */
      ++i;
      continue;
    }
    j = 2;
    while (1)
    {
      if (intsizes[j]*cc_size_bits_char >= i*4)
      {
        printf("%s", "        ! Native type names\n");
        printf(      "            mxker.cc.fast.sint.%lu=signed %s\n",
            cc_intsizes[i], intnames[j]);
        printf(      "            mxker.cc.fast.uint.%lu=unsigned %s\n",
            cc_intsizes[i], intnames[j]);
        printf("%s", "        ! Base type IDs\n");
        printf(      "            mxker.cc.tid.fast.uint.%lu=%s\n",
            cc_intsizes[i], intutids[j]);
        printf(      "            mxker.cc.tid.fast.sint.%lu=%s\n",
            cc_intsizes[i], intstids[j]);

        /* C# int binding table entry */
        csc_fast_int_binding_table[i] = intsizes[j];
        break;
      }
      if (intsizes[++j] == 0)
      {
        printf("%s", "        ! Native type names (N/A)\n");
        printf(      "            mxker.cc.fast.sint.%lu=%s\n",
            cc_intsizes[i], "void");
        printf(      "            mxker.cc.fast.uint.%lu=%s\n",
            cc_intsizes[i], "void");
        printf("%s", "        ! Base type IDs (N/A)\n");
        printf(      "            mxker.cc.tid.fast.uint.%lu=%s\n",
            cc_intsizes[i], "0");
        printf(      "            mxker.cc.tid.fast.sint.%lu=%s\n",
            cc_intsizes[i], "0");

        /* C# int binding table entry */
        csc_fast_int_binding_table[i] = 0;
        break;
      }
    }
    ++i;
  }

  printf("%s",
      "    ! Fast constrained explicitly bounded floating point types\n");

  i = 0;
  while (cc_floatsizes_of_fast[i] != NULL)
  {
    printf("      ! Fast floating point type of at least %lu bits.\n",
        cc_floatsizes[i]);
    if (strncmp(cc_floatsizes_of_fast[i], "auto", 4) != 0)
    {
      fprintf(stderr, "%s",
          "explicit configuration of arithmetic types not yet supported!\n");
      exit(1);
      printf("mxker.cc.fast.float.%lu=%s\n", cc_floatsizes[i],
          cc_floatsizes_of_fast[i]);
      /* FIXME : Add type ID deduction and validation so that explicit
       * configuration of arithmetic types can be supported. */
      ++i;
      continue;
    }
    j = 0;
    while (1)
    {
      if (floatsizes[j]*cc_size_bits_char >= cc_floatsizes[i])
      {
        printf("%s", "        ! Native type name\n");
        printf(      "            mxker.cc.fast.float.%lu=%s\n",
            cc_floatsizes[i], floatnames[j]);
        printf("%s", "        ! Base type ID\n");
        printf(      "            mxker.cc.tid.fast.float.%lu=%s\n",
            cc_floatsizes[i], floattids[j]);

        /* C# float binding table entry */
        csc_fast_float_binding_table[i] = floatsizes[j];
        break;
      }
      if (floatsizes[++j] == 0)
      {
        printf("%s", "        ! Native type name (N/A)\n");
        printf(      "            mxker.cc.fast.float.%lu=%s\n",
            cc_floatsizes[i], "int");
        printf("%s", "        ! Base type ID (N/A)\n");
        printf(      "            mxker.cc.tid.fast.float.%lu=%s\n",
            cc_floatsizes[i], "0");

        /* C# float binding table entry */
        csc_fast_float_binding_table[i] = 0;
        break;
      }
    }
    ++i;
  }

  printf("%s",   "  ! Intrinsic specifications\n");
  _print_or_none("      mxker.cc.intrinsic.complex", cc_intrinsic_complex);
  _print_or_none("      mxker.cc.intrinsic.atomics", cc_intrinsic_atomics);
  _print_or_none("      mxker.cc.intrinsic.visibility",
      cc_intrinsic_visibility);
  _print_or_none("      mxker.cc.intrinsic.alignment", cc_intrinsic_alignment);

  return 0;
}

unsigned long print_csenv()
{
  char const *const *p;
  int i, j, firstof, foundit;

  csintsizes[0] = cssb;
  csintsizes[1] = csss;
  csintsizes[2] = cssi;
  csintsizes[3] = cssl;
  csintsizes[4] = 0;

  csfloatsizes[0] = cssf;
  csfloatsizes[1] = cssd;
  csfloatsizes[2] = 0;

  csboolsize = css0;

  printf("%s", "\n"
               "! C# (.NET/Mono) environment configuration\n");

  printf("%s", "    mxker.csc.bindings=yes\n");
  printf("%s%s", "    mxker.csc.program=", csc_program);

  /* compat C# extra flags (all) */
  printf("\n%s", "    mxker.csc.extra flags=");
  for (p = &csc_extra_flags[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&csc_extra_flags[0]?"":" ", *p);

  /* C# extra flags (all+release) */
  printf("\n%s", "    mxker.csc.extra flags.release=");
  for (p = &csc_extra_flags[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&csc_extra_flags[0]?"":" ", *p);
  if (csc_extra_flags_r[0] != NULL) printf("%c", ' ');
  for (p = &csc_extra_flags_r[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&csc_extra_flags_r[0]?"":" ", *p);

  /* C# extra flags (all+debug) */
  printf("\n%s", "    mxker.csc.extra flags.debug=");
  for (p = &csc_extra_flags[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&csc_extra_flags[0]?"":" ", *p);
  if (csc_extra_flags_d[0] != NULL) printf("%c", ' ');
  for (p = &csc_extra_flags_d[0]; *p != NULL; ++p)
    printf("%s'%s'", p==&csc_extra_flags_d[0]?"":" ", *p);

  printf("\n%s", "  ! .NET/Mono arithmetic type widths (in bits).\n");

  printf("      mxker.csc.width.sbyte=%lu\n",   csc_size_bits_byte*cssb);
  printf("      mxker.csc.width.byte=%lu\n",    csc_size_bits_byte*cssb);
  printf("      mxker.csc.width.char=%lu\n",    csc_size_bits_byte*csss);
  printf("      mxker.csc.width.short=%lu\n",   csc_size_bits_byte*csss);
  printf("      mxker.csc.width.ushort=%lu\n",  csc_size_bits_byte*csss);
  printf("      mxker.csc.width.int=%lu\n",     csc_size_bits_byte*cssi);
  printf("      mxker.csc.width.uint=%lu\n",    csc_size_bits_byte*cssi);
  printf("      mxker.csc.width.long=%lu\n",    csc_size_bits_byte*cssl);
  printf("      mxker.csc.width.ulong=%lu\n",   csc_size_bits_byte*cssl);
  printf("      mxker.csc.width.float=%lu\n",   csc_size_bits_byte*cssf);
  printf("      mxker.csc.width.double=%lu\n",  csc_size_bits_byte*cssd);
  printf("      mxker.csc.width.bool=%lu\n",    csc_size_bits_byte*css0);

  printf("%s", "  ! .NET/Mono arithmetic type widths (in bytes).\n");

  printf("      mxker.csc.size.sbyte=%lu\n",    cssb);
  printf("      mxker.csc.size.byte=%lu\n",     cssb);
  printf("      mxker.csc.size.char=%lu\n",     csss);
  printf("      mxker.csc.size.short=%lu\n",    csss);
  printf("      mxker.csc.size.ushort=%lu\n",   csss);
  printf("      mxker.csc.size.int=%lu\n",      cssi);
  printf("      mxker.csc.size.uint=%lu\n",     cssi);
  printf("      mxker.csc.size.long=%lu\n",     cssl);
  printf("      mxker.csc.size.ulong=%lu\n",    cssl);
  printf("      mxker.csc.size.float=%lu\n",    cssf);
  printf("      mxker.csc.size.double=%lu\n",   cssd);
  printf("      mxker.csc.size.bool=%lu\n",     css0);

  printf("%s", "  ! .NET/Mono arithmetic type mapping\n");
  printf("%s", "    ! Native integer type mapping\n");

  /* ints - match C# types to C builtin types */
  for (i = 0; csintsizes[i] != 0; ++i)
  {
    foundit = 0;
    for (j = 0; intsizes[j] != 0; ++j)
    {
      if (csc_size_bits_byte*csintsizes[i] == cc_size_bits_char*intsizes[j])
      {
        printf("      ! Native %lu-bit integer mapping\n",
            cc_size_bits_char*intsizes[j]);
        printf("          mxker.csc.ctype.%s=%s\n", intsa[j], cssintnames[i]);
        printf("          mxker.csc.ctype.%s=%s\n", intua[j], csuintnames[i]);
        foundit = 1;
        break;
      }
    }
    if (!foundit)
    {
      printf("      ! Native %lu-bit integer mapping (N/A)\n",
          cc_size_bits_char*intsizes[j]);
      printf("          mxker.csc.ctype.%s=%s\n", intsa[j], "IntPtr");
      printf("          mxker.csc.ctype.%s=%s\n", intua[j], "IntPtr");
    }
  }

  printf("%s", "    ! Native floating point type mapping\n");

  /* floats - match C# types to C builtin types */
  for (i = 0; csfloatsizes[i] != 0; ++i)
  {
    foundit = 0;
    for (j = 0; floatsizes[j] != 0; ++j)
    {
      if (csc_size_bits_byte*csfloatsizes[i]
          == cc_size_bits_char*floatsizes[j])
      {
        printf("      ! Native %lu-bit floating point mapping\n",
            cc_size_bits_char*floatsizes[j]);
        printf("          mxker.csc.ctype.%s=%s\n", floata[j],
            csfloatnames[i]);
        foundit = 1;
        break;
      }
    }
    if (!foundit)
    {
      printf("      ! Native %lu-bit floating point mapping (N/A)\n",
          cc_size_bits_char*floatsizes[j]);
      printf("          mxker.csc.ctype.%s=%s\n", floata[j], "IntPtr");
    }
  }

  printf("%s", "    ! Constrained integer type correspondence mapping\n");

  /* ints - match C types to C# builtin types */
  for (i = 0; csintsizes[i] != 0; ++i)
  {
    printf("      ! Mappings for %s and %s\n", cssintnames[i], csuintnames[i]);
    printf("%s", "        ! Type ID mapping\n");
    /* type id table for signed ints */
    printf(      "            mxker.csc.tidlist.%s={ ", cssintnames[i]);
    firstof = 1;
    for (j = 0; intsizes[j] != 0; ++j)
    {
      if (csintsizes[i] == intsizes[j])
      {
        if (firstof)
          firstof = 0;
        else
          printf("%s", ", ");
        printf("0x%s", intutids[j]);
      }
    }
    printf("%s", " }\n");
    
    /* type id table for unsigned ints */
    printf(      "            mxker.csc.tidlist.%s={ ", csuintnames[i]);
    firstof = 1;
    for (j = 0; intsizes[j] != 0; ++j)
    {
      if (csintsizes[i] == intsizes[j])
      {
        if (firstof)
          firstof = 0;
        else
          printf("%s", ", ");
        printf("0x%s", intstids[j]);
      }
    }
    printf("%s", " }\n");

    /* c type binding entries */
    printf("%s", "        ! Native type binding targets\n");
    for (j = 1; cc_intsizes_of[j] != NULL; ++j)
    {
      if (csc_size_bits_byte*csintsizes[i]
          == cc_size_bits_char*csc_least_int_binding_table[j])
      {
        printf("          ! Bindings to smallest native integers of "
                           "at least %lu bits.\n", cc_intsizes[j]);
        /* I'm a naughty girl and I'm assuming signed ints are default but in
         * C# land that should be okay. */
        /* HARD DEPRECATION */
        /* printf("mxker.csc.least.int.%lu=%s\n",
            (unsigned long)(j*4), cssintnames[i]); */
        printf("              mxker.csc.least.sint.%lu=%s\n",
            (unsigned long)(j*4), cssintnames[i]);
        printf("              mxker.csc.least.uint.%lu=%s\n",
            (unsigned long)(j*4), csuintnames[i]);
      }
      if (csc_size_bits_byte*csintsizes[i]
          == cc_size_bits_char*csc_fast_int_binding_table[j])
      {
        printf("          ! Bindings to fast native integers of "
                           "at least %lu bits.\n", cc_intsizes[j]);
        /* I'm a naughty girl and I'm assuming signed ints are default but in
         * C# land that should be okay. */
        /* HARD DEPRECATION */
        /* printf("mxker.csc.fast.int.%lu=%s\n",
            (unsigned long)(j*4), cssintnames[i]); */
        printf("              mxker.csc.fast.sint.%lu=%s\n",
            (unsigned long)(j*4), cssintnames[i]);
        printf("              mxker.csc.fast.uint.%lu=%s\n",
            (unsigned long)(j*4), csuintnames[i]);
      }
    }
  }

  printf("%s", "    ! Constrained floating point type correspondence "
                     "mapping\n");

  /* floats - match C types to C# builtin types */
  for (i = 0; csfloatsizes[i] != 0; ++i)
  {
    printf("      ! Mappings for %s\n", csfloatnames[i]);
    printf("%s", "        ! Type ID mapping\n");
    /* type id table for floats */
    printf(      "            mxker.csc.tidlist.%s={ ", csfloatnames[i]);
    firstof = 1;
    for (j = 0; floatsizes[j] != 0; ++j)
    {
      if (csfloatsizes[i] == floatsizes[j])
      {
        if (firstof)
          firstof = 0;
        else
          printf("%s", ", ");
        printf("0x%s", floattids[j]);
      }
    }
    printf("%s", " }\n");

    /* c type binding entries */
    printf("%s", "        ! Native type binding targets\n");
    for (j = 1; cc_floatsizes_of[j] != NULL; ++j)
    {
      if (csfloatsizes[i] == csc_least_float_binding_table[j])
      {
        printf("          ! Binding to smallest native floating point of "
                           "at least %lu bits.\n", cc_floatsizes[j]);
        printf("              mxker.csc.least.float.%lu=%s\n",
            cc_floatsizes[j], csfloatnames[i]);
      }
      if (csfloatsizes[i] == csc_fast_float_binding_table[j])
      {
        printf("          ! Binding to fast native floating point of "
                           "at least %lu bits.\n", cc_floatsizes[j]);
        printf("              mxker.csc.fast.float.%lu=%s\n",
            cc_floatsizes[j], csfloatnames[i]);
      }
    }
  }

  return 0;
}

unsigned long print_uid()
{
  unsigned long modwidth, usrtidwidth, systidwidth;

  printf("%s", "\n"
               "! Unique ID format specification\n");
  printf("%s", "  ! Field widths\n");
  printf(      "      mxker.uid.width=%lu\n",
      uid_unique_width+uid_type_width+uid_property_width);
  printf(      "      mxker.uid.width.unique=%lu\n", uid_unique_width);
  printf(      "      mxker.uid.width.type=%lu\n", uid_type_width);
  printf(      "      mxker.uid.width.property=%lu\n", uid_property_width);

  printf("%s", "  ! Complex property type support "
                   "(0=none, 1=a+jb, 2=a+ia+jb+kc)\n");
  printf(      "      mxker.uid.complexmode=%lu\n", uid_complexmode);

  switch (uid_complexmode)
  {
  case 0:
    modwidth = 0;
    break;
  case 1:
    modwidth = 1;
    break;
  case 2:
    /* fall through */
  default:
    modwidth = 2;
    break;
  };

  usrtidwidth = uid_type_width - 1;     /* make room for the user flag */
  systidwidth = usrtidwidth - modwidth; /* make room for the modifier */

  printf("%s", "  ! Unique ID field offsets\n");
  printf(      "      mxker.uid.offset.unique=%lu\n",
      uid_property_width+uid_type_width);
  printf(      "      mxker.uid.offset.type=%lu\n", uid_property_width);
  printf(      "      mxker.uid.offset.property=%lu\n", 0UL);

  printf("%s", "  ! Type ID subfield offsets (see Type ID format "
                   "specification)\n");
  printf(      "      mxker.uid.offset.type.sysflag=%lu\n",
      usrtidwidth+uid_property_width);
  printf(      "      mxker.uid.offset.type.mod=%lu\n",
      systidwidth+uid_property_width);
  printf(      "      mxker.uid.offset.type.index=%lu\n", uid_property_width);

  printf("%s", "\n"
               "! Type ID format specification\n");
  printf("%s", "  ! Field widths\n");
  printf(      "      mxker.tid.width=%lu\n", uid_type_width);
  printf(      "      mxker.tid.width.sysflag=%lu\n", 1UL);
  printf(      "      mxker.tid.width.mod=%lu\n", modwidth);
  printf(      "      mxker.tid.width.sys=%lu\n", systidwidth);
  printf(      "      mxker.tid.width.usr=%lu\n", usrtidwidth);

  printf("%s", "  ! Field offsets\n");
  printf(      "      mxker.tid.offset.sysflag=%lu\n", usrtidwidth);
  printf(      "      mxker.tid.offset.mod=%lu\n", systidwidth);
  printf(      "      mxker.tid.offset.index=%lu\n", 0UL);

  return 0;
}

unsigned long print_blas()
{
  printf("%s", "\n"
               "! Basic Linear Algebra Subprograms configuration\n");
  printf(      "    mxker.blas.service=%lu\n", blas_service);
  printf(      "    mxker.blas.level=%lu\n", blas_level);
  printf(      "    mxker.blas.arrays=%lu\n", blas_enablearrays);

  return 0;
}

unsigned long print_lua()
{
  printf("%s", "\n"
               "! Lua bindings configuration\n");
  printf(      "    mxker.lua.service=%lu\n", lua_service);
  printf(      "    mxker.lua.enablejit=%lu\n", lua_enablejit);
  printf(      "    mxker.lua.allowinject=%lu\n", lua_allowinject);

  return 0;
}

unsigned long print_concurrency()
{
  printf("%s", "\n"
               "! Concurrency configuration\n");
  printf(      "    mxker.concurrency.runlevelfences=%lu\n", con_runlevelfences);
  printf(      "    mxker.concurrency.interprocess=%lu\n", con_interprocess);
  printf(      "    mxker.concurrency.atomicbus=%lu\n", con_atomic_bus);

  return 0;
}

int main()
{
  printf("%s", "!"
"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^!"
"\n!"
" This is an mxker intermediate configurator file. It is generated by way of !"
"\n!"
" compiling and executing config/conf.out.c after it is generated by         !"
"\n!"
" combining config/conf.c with a prefix block which includes the header file !"
"\n!"
" containing the kernel configuration, usually in version/.                  !"
"\n!"
"vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv!"
"\n"
);

  (void)print_time();
  (void)print_version();
  (void)print_cenv();
  if (csc_bindings)
    (void)print_csenv();
  (void)print_uid();
  (void)print_mem();
  (void)print_blas();
  (void)print_lua();
  (void)print_concurrency();

  return 0;
}




import math

sizes = range(4, 65, 4)

def de_bruijn(k, n: int) -> str:
    ab = list(map(str, range(k)))
    a = [0] * k * n
    seq = []

    def db(t, p):
        if t > n:
            if n % p == 0:
                seq.extend(a[1:p + 1])
        else:
            a[t] = a[t - p]
            db(t + 1, p)
            for j in range(a[t - p] + 1, k):
                a[t] = j
                db(t + 1, t)
    db(1, 1)
    return ''.join(ab[i] for i in seq)

for sz in sizes:
    n = int(math.ceil(math.log2(sz)))
    s = de_bruijn(2, n)[::-1]
    print(f'  {int(s, 2):16X} ; {sz:2} → {n} → {s}')


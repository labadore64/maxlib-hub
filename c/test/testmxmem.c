#include "mxker.h"
#include "private/memconf.h"

#include <sys/fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

void dump_spec(char const *sname, struct mxmem_allocator_spec_s *pspec)
{
  fprintf(stdout, "%s.puser →  %016lX\n", sname, (mxker_uaddr_t)pspec->puser);
  fprintf(stdout, "%s.errorcode →  %d\n", sname, pspec->errorcode);
  fprintf(stdout, "%s.pfn_alloc →  %016lX\n", sname, (mxker_uaddr_t)pspec->pfn_alloc);
  fprintf(stdout, "%s.pfn_dealloc →  %016lX\n", sname, (mxker_uaddr_t)pspec->pfn_dealloc);
  fprintf(stdout, "%s.pfn_aligned_alloc →  %016lX\n", sname, (mxker_uaddr_t)pspec->pfn_aligned_alloc);
  fprintf(stdout, "%s.pfn_aligned_dealloc →  %016lX\n\n", sname, (mxker_uaddr_t)pspec->pfn_aligned_dealloc);
}

/* disable optimization to test segmentation. check with valgrind */
int main()
{
  struct mxmem_allocator_spec_s m_alloc_spec = { 0 };

  size_t nsucceedaalloc = 0;
  size_t nsucceedadealloc = 0;

  size_t naalloc = 0;
  size_t nadealloc = 0;

  mxker_uaddr_t uiq, uio, nalloc;
  unsigned long aligned_actual, aligned_required, aligned_request;

  void *pstack[2048];
  void **ppstack = &pstack[0];

  void *p;
  int tno = 0;
  int tcount = 1024*1024;

  int n, sh, al, bad = 0;

  int rfd;
  ssize_t rrr;
  unsigned long long rnums[2];

  if ((rfd = open("/dev/urandom", O_RDONLY)) < 0)
  {
    fprintf(stderr, "%s\n", "cannot open entropy device, abort");
    return 1;
  }

  dump_spec("m_alloc_spec", &m_alloc_spec);
  fprintf(stdout, "mxmem_init_allocator_spec(&m_alloc_spec) →  %d\n\n",
      mxmem_init_allocator_spec(&m_alloc_spec));

  dump_spec("m_alloc_spec", &m_alloc_spec);

  for (tno = 0; tno < tcount; ++tno)
  {
    if ((rrr = read(rfd, (char *)&rnums[0], sizeof(rnums))) < 0)
    {
      fprintf(stderr, "%s\n", "cannot read entropy device, abort");
      return 1;
    }
    sh = 1 + (rnums[0] % 10);
    al = 1 << sh; /* alignment */
    n = 1 + (rnums[1] % 4096*8);

    p = mxmem_alloc_aligned(&m_alloc_spec, n, al);
    ++naalloc;
    if (NULL != p)
    {
      ++nsucceedaalloc;
      if (ppstack - &pstack[0] <= 2000)
        *ppstack++ = p;
    }

CONTINUE_TEST:
    if (ppstack - &pstack[0] >= 2000)
      goto FLUSH_STACK;
  }

FLUSH_STACK:
  while (ppstack - &pstack[0] > 0)
  {
    p = *(--ppstack);

    uiq              = ((mxker_uaddr_t *)p)[MXMEM_BASEADDRPOS];
    uio              = ((mxker_uaddr_t *)p)[MXMEM_ADDROFFPOS];
    n                = ((mxker_uaddr_t *)p)[MXMEM_CHARCOUNTPOS];
    nalloc           = ((mxker_uaddr_t *)p)[MXMEM_NALLOCPOS];
    aligned_required = ((mxker_uaddr_t *)p)[MXMEM_NALIGNPOS];
    aligned_request  = ((mxker_uaddr_t *)p)[MXMEM_RALIGNPOS];

    aligned_actual = 1;
    while (((aligned_actual - 1) & (mxker_uaddr_t)p) == 0)
      aligned_actual <<= 1;
    aligned_actual >>= 1;

    bad = 0;
    ++nadealloc;
    if (aligned_actual < aligned_required)
      bad = 1;
    if (uiq + uio != (mxker_uaddr_t)p)
      bad = 1;

    if (bad == 0)
    {
      /* check segmentation */
      memset(p, 0x00, n);
      memset(p, 0x5C, n);
      memset(p, 0xFF, n);
      memset(p, 0xA5, n);

      mxmem_dealloc_aligned(&m_alloc_spec, p, n, al);
      ++nsucceedadealloc;
    }
    
    if (bad || (tno % 32768 == 0))
    {
      fprintf(stderr, "%s [%lu/%lu = %03.2Lf%%]\n", bad ? "failure" : "okay",
          (unsigned long)tno, (unsigned long)tcount, (100.*((long double)tno/(long double)tcount)));
      fprintf(stderr, "  p=%016lX n=%d\n", (unsigned long)p, n);
      fprintf(stderr,
          "  %s %016lX\n"
          "  %s %lu\n"
          "  %s %lu\n"
          "  %s %lu\n"
          "  %s %lu\n"
          "  %s %lu\n",
        "baseaddr        ", (unsigned long)uiq,
        "addroff         ", (unsigned long)uio,
        "nalloc          ", (unsigned long)nalloc,
        "aligned request ", (unsigned long)aligned_request,
        "aligned required", (unsigned long)aligned_required,
        "aligned actual  ", (unsigned long)aligned_actual);
    }
  }

  if (tno < tcount)
    goto CONTINUE_TEST;

  close(rfd);

  fprintf(stdout, "%s = %s/%s (%s%%)\n",
      "test name       ", "succeeded"     , "total"   , "percent");
  fprintf(stdout, "%s = %ld/%ld (%03.2Lf%%)\n",
      "aligned allocs  ", nsucceedaalloc  , naalloc   ,
      (100.*((long double)nsucceedaalloc/(long double)naalloc)));
  fprintf(stdout, "%s = %ld/%ld (%03.2Lf%%)\n\n",
      "aligned deallocs", nsucceedadealloc, nadealloc ,
      (100.*((long double)nsucceedadealloc/(long double)nadealloc)));

  fprintf(stdout, "%s\n", "on completion:");
  dump_spec("m_alloc_spec", &m_alloc_spec);

  return 0;
}

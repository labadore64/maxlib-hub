# Copyright 2020 Roger Merryfield
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# file: Makefile.in
# description:
#   The Makefile for building mxker.
# author(s):
#   William
# kernel:
#   {$mxker.version.string$}
# when:
#   {$mxker.version.time.string$}
# .

# project dirs
BINDIR := bin
LIBDIR := lib
OBJDIR := obj
INCDIR := include

# make unit identifier
ECHO = @echo "MLM(mxker) "

# dependency dirs
DPRDIR := $(OBJDIR)/.deps_r
DPDDIR := $(OBJDIR)/.deps_d

# dependency flags
DPRFLAGS = -MT $@ -MMD -MP -MF $(DPRDIR)/$*.d
DPDFLAGS = -MT $@ -MMD -MP -MF $(DPDDIR)/$*.d

{$if@mxker.csc.bindings$}
CSFLAGS = /unsafe {$mxker.csc.extra flags$}
{$@fi$}

# sources
LIBSRC = src/mxker_core.c
MXKERTESTSRC = test/testmxker.c
MXMEMTESTSRC = test/testmxmem.c

# public generated headers
PUBLICLIBHEADERS  = include/mxker.h           \
		    include/mxker/lib.h       \
                    include/mxker/ver.h       \
                    include/mxker/typ.h       \
                    include/mxker/arr.h       \
                    include/mxker/blas.h      \
                    include/mxker/calc.h      \
                    include/mxker/ifc.h       \
                    include/mxker/dir.h       \
                    include/mxker/mem.h       \
	            include/mxker/ds.h        \
                    include/mxker/sz.h	      \
		    include/mxker/utf8.h

# private generated headers
PRIVATELIBHEADERS = include/private/memconf.h

{$if@mxker.csc.bindings$}
# C# test source templates
CSHARPTESTSOURCES        = ffi/csharp/test/testver.cs  \
                           ffi/csharp/test/testmem.cs  \
                           ffi/csharp/test/testarr.cs

# C# bindings configured sources
CSHARPBINDINGCONFSOURCES = ffi/csharp/MxTyp.cs

# C# bindings sources
CSHARPBINDINGSOURCES     = $(CSHARPBINDINGCONFSOURCES) \
                           ffi/csharp/MxBlit.cs        \
                           ffi/csharp/MxAtc.cs

# C# Mono assembly configuration templates (needed for dllmap specifications)
CSHARPCONFIGS            = ffi/csharp/mxker.dllmap.release.config \
                           ffi/csharp/mxker.dllmap.debug.config

# All C# files
CSHARPCONFFILES = $(CSHARPTESTSOURCES) $(CSHARPCONFIGS) $(CSHARPBINDINGCONFSOURCES)
{$@fi$}
{$if~@mxker.csc.bindings$}
# No C# sources
{$@fi$}

# generated headers
LIBHEADERS       = $(PUBLICLIBHEADERS) $(PRIVATELIBHEADERS)

# C release objects
LIBOBJ           = $(LIBSRC:%.c=$(OBJDIR)/%.o)
MXKERTESTOBJ     = $(MXKERTESTSRC:%.c=$(OBJDIR)/%.o)
MXMEMTESTOBJ     = $(MXMEMTESTSRC:%.c=$(OBJDIR)/%.o)

# C debug objects
LIBOBJ_D         = $(LIBSRC:%.c=$(OBJDIR)/%_dbg.o)
MXKERTESTOBJ_D   = $(MXKERTESTSRC:%.c=$(OBJDIR)/%_dbg.o)
MXMEMTESTOBJ_D   = $(MXMEMTESTSRC:%.c=$(OBJDIR)/%_dbg.o)

{$if@mxker.csc.bindings$}
# C# test assemblies
CSHARPTESTS      = $(CSHARPTESTSOURCES:%.cs=%.exe)
CSHARPTESTS_D    = $(CSHARPTESTSOURCES:%.cs=%d.exe)
CSHARPTESTS_PDBS = $(CSHARPTESTSOURCES:%.cs=%d.pdb)
{$@fi$}
{$if~@mxker.csc.bindings$}
# No C# test assemblies
{$@fi$}

# C sources
SRCS = $(LIBSRC) $(TESTSRC)

# mxker release targets
KERNELSOFULL       = $(LIBDIR)/{$mxker.soname.release.full$}
KERNELSOMINOR      = $(LIBDIR)/{$mxker.soname.release.minor$}
KERNELSOMAJOR      = $(LIBDIR)/{$mxker.soname.release.major$}
KERNELSOBASE       = $(LIBDIR)/{$mxker.soname.release.base$}

# mxker debug targets
KERNELSOFULL_D     = $(LIBDIR)/{$mxker.soname.debug.full$}
KERNELSOMINOR_D    = $(LIBDIR)/{$mxker.soname.debug.minor$}
KERNELSOMAJOR_D    = $(LIBDIR)/{$mxker.soname.debug.major$}
KERNELSOBASE_D     = $(LIBDIR)/{$mxker.soname.debug.base$}

# mxker release test targets
KERNELTESTBIN      = $(BINDIR)/testmxker
MEMTESTBIN         = $(BINDIR)/testmxmem

# mxker debug test targets
KERNELTESTBIN_D    = $(BINDIR)/testmxkerd
MEMTESTBIN_D       = $(BINDIR)/testmxmemd

# release symbol table output
KERNELSYMTABBASE   = {$mxker.soname.release.full$}.{$mxker.version.time.epoch$}
KERNELSYMTABNAME   = $(KERNELSYMTABBASE)-{$mxker.version.hex.crc8$}.symtab
KERNELSYMTAB       = tools/$(KERNELSYMTABNAME)

# debug symbol table output
KERNELSYMTABBASE_D = {$mxker.soname.debug.full$}.{$mxker.version.time.epoch$}
KERNELSYMTABNAME_D = $(KERNELSYMTABBASE_D)-{$mxker.version.hex.crc8$}.symtab
KERNELSYMTAB_D     = tools/$(KERNELSYMTABNAME_D)

# some programs
RM  = @rm -f
LNS = ln -s
RMR = $(RM) -r # recursive rm
CAT = @cat
ECHOP = @echo

# bootstrap configurations
CC = {$mxker.cc.program$}
{$if@mxker.csc.bindings$}
CSC = scripts/posix/nogates.sh {$mxker.csc.program$}
{$@fi$}
TARGET_ARCH = -march={$mxker.cc.arch$} -mtune={$mxker.cc.arch$}

# compile command form
COMPILE.c = $(CC) $(DEPFLAGS) $(CFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c

# C# compile command form
COMPILE.cs = $(CSC) $(CSFLAGS)

# configurator command form
CONFIGURATOR = @config/confgen config/mxker.conf

kernel-release: DEPFLAGS = $(DPRFLAGS)
kernel-release: CPPFLAGS = -D MXKER_BUILD {$mxker.cc.extra cppflags.release$}
kernel-release: CFLAGS = -fPIC -fvisibility=hidden -I./include {$mxker.cc.extra cflags.release$}
kernel-release: LDLIBS = -pthread
kernel-release: LDFLAGS = -shared -fPIC -fvisibility=hidden -Wl,-soname,{$mxker.soname.release.full$} {$mxker.cc.extra ldflags.release$}
kerneltest-release: DEPFLAGS = $(DPRFLAGS)
kerneltest-release: CFLAGS = -Wall -Wextra -g -Og -I./include
kerneltest-release: LDLIBS = -l{$mxker.libname.unix.release$} -pthread
kerneltest-release: LDFLAGS = -L./lib -Wl,-rpath,./lib:../lib

kernel-debug: DEPFLAGS = $(DPDFLAGS)
kernel-debug: CPPFLAGS = -D MXKER_BUILD {$mxker.cc.extra cppflags.debug$}
kernel-debug: CFLAGS = -fPIC -fvisibility=hidden -I./include {$mxker.cc.extra cflags.debug$}
kernel-debug: LDLIBS = -pthread
kernel-debug: LDFLAGS = -shared -fPIC -fvisibility=hidden -Wl,-soname,{$mxker.soname.debug.full$} {$mxker.cc.extra ldflags.debug$}
kerneltest-debug: DEPFLAGS = $(DPDFLAGS)
kerneltest-debug: CFLAGS = -Wall -Wextra -g -I./include
kerneltest-debug: LDLIBS = -l{$mxker.libname.unix.debug$} -pthread
kerneltest-debug: LDFLAGS = -L./lib -Wl,-rpath,./lib:../lib

# build everything
.PHONY: all
all: kernel kerneltest

# build all release targets
.PHONY: all-release
all-release: kernel-release kerneltest-release

# build all debug targets
.PHONY: all-debug
all-debug: kernel-debug kerneltest-debug

# configure sources and headers
.PHONY: conf
conf: $(LIBHEADERS) $(LIBSRC) 

# configure C# bindings
.PHONY: conf-cs
{$if@mxker.csc.bindings$}
conf-cs: $(CSHARPCONFIGS) $(CSHARPBINDINGSOURCES)
{$@fi$}
{$if~@mxker.csc.bindings$}
conf-cs:
	$(ECHO) "No C# bindings to configure; skipping."
{$@fi$}

# build all kernel targets
.PHONY: kernel
kernel: kernel-debug kernel-release

# build the release kernel target
.PHONY: kernel-release
kernel-release: $(KERNELSOBASE)

# build the debug kernel target
.PHONY: kernel-debug
kernel-debug: $(KERNELSOBASE_D)

# dump all the kernel symbol tables
.PHONY: kernel-symtab
kernel-symtab: kernel-symtab-debug kernel-symtab-release

# dump the release kernel symbol table
.PHONY: kernel-symtab-release
kernel-symtab-release: $(KERNELSYMTAB)

# dump the debug kernel symbol table
.PHONY: kernel-symtab-debug
kernel-symtab-debug: $(KERNELSYMTAB_D)

# make sure the symbol tables generated together agree on the time (for
# organizational purposes).
SYMTABDATE = $(shell date)

# debug kernel test targets; conditionally C# targets if bindings will be
# generated
KERNELTEST_DEBUG_DEPS = \
  $(KERNELTESTBIN_D)    \
  $(MEMTESTBIN_D){$if@mxker.csc.bindings$} $(CSHARPTESTS_D){$@fi$}

# release kernel test targets; conditionally C# targets if bindings will be
# generated
KERNELTEST_RELEASE_DEPS = \
  $(KERNELTESTBIN)        \
  $(MEMTESTBIN){$if@mxker.csc.bindings$} $(CSHARPTESTS){$@fi$}

# release symbol table target
$(KERNELSYMTAB): kernel-release conf
	$(ECHO) "Configuration:" > "$@"
	@cat config/mxker.conf | grep 'mxker.version' >> "$@"
	$(ECHO) "Symbol table [$(KERNELSOFULL); $(SYMTABDATE)]:" >> "$@"
	@nm "$(KERNELSOFULL)" >> "$@"
	$(ECHO) "Tabulated symbols for release mxker library in $@ ."

# debug symbol table target
$(KERNELSYMTAB_D): kernel-debug conf
	$(ECHO) "Configuration:" > "$@"
	@cat config/mxker.conf | grep 'mxker.version' >> "$@"
	$(ECHO) "Symbol table [$(KERNELSOFULL_D); $(SYMTABDATE)]:" >> "$@"
	@nm "$(KERNELSOFULL_D)" >> "$@"
	$(ECHO) "Tabulated symbols for debug mxker library in $@ ."

# build all C# bindings
.PHONY: kernel-cs
kernel-cs: kernel-cs-debug kernel-cs-release

{$if@mxker.csc.bindings$}
# build C# debug bindings
.PHONY: kernel-cs-debug
kernel-cs-debug: conf-cs kernel-debug
	$(ECHO) "Placeholder for C# debug bindings generator"

# build C# release bindings
.PHONY: kernel-cs-release
kernel-cs-release: conf-cs kernel-release
	$(ECHO) "Placeholder for C# release bindings generator"
{$@fi$}
{$if~@mxker.csc.bindings$}
# build C# debug bindings
.PHONY: kernel-cs-debug
kernel-cs-debug: conf-cs kernel-debug
	$(ECHO) "No C# bindings; skipping debug bindings generation"

# build C# release bindings
.PHONY: kernel-cs-release
kernel-cs-release: conf-cs kernel-release
	$(ECHO) "No C# bindings; skipping release bindings generation"
{$@fi$}

# build all the test programs
.PHONY: kerneltest
kerneltest: kerneltest-debug kerneltest-release

# build the debug test programs
.PHONY: kerneltest-debug
kerneltest-debug: $(KERNELTEST_DEBUG_DEPS)

# build the release test programs
.PHONY: kerneltest-release
kerneltest-release: $(KERNELTEST_RELEASE_DEPS)

# release kernel test target
$(KERNELTESTBIN): $(MXKERTESTOBJ) | kernel-release
	$(ECHO) " CC $@ [$(words $^) object(s); $(LDLIBS)]"
	@$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS)

# release mxmem test target
$(MEMTESTBIN): $(MXMEMTESTOBJ) | kernel-release
	$(ECHO) " CC $@ [$(words $^) object(s); $(LDLIBS)]"
	@$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS) 

# debug kernel test target
$(KERNELTESTBIN_D): $(MXKERTESTOBJ_D) | kernel-debug
	$(ECHO) " CC $@ [$(words $^) object(s); $(LDLIBS)]"
	@$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS) 

# debug mxmem test target
$(MEMTESTBIN_D): $(MXMEMTESTOBJ_D) | kernel-debug
	$(ECHO) " CC $@ [$(words $^) object(s); $(LDLIBS)]"
	@$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS) 

# release kernel library target
$(KERNELSOFULL): $(LIBOBJ) | conf $(LIBHEADERS)
	$(ECHO) " CC $@ [$(words $^) object(s); $(LDLIBS)]"
	@$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS) 
	$(ECHO) "Stripping symbol table of $(@F) for release."
	@strip --strip-unneeded $@

# release kernel library minor version symbolic link target
$(KERNELSOMINOR): $(KERNELSOFULL)
	$(RM) $(LIBDIR)/{$mxker.soname.release.minor$}
	@cd $(LIBDIR) && $(LNS) {$mxker.soname.release.full$} {$mxker.soname.release.minor$}

# release kernel library major version symbolic link target
$(KERNELSOMAJOR): $(KERNELSOMINOR)
	$(RM) $(LIBDIR)/{$mxker.soname.release.major$}
	@cd $(LIBDIR) && $(LNS) {$mxker.soname.release.minor$} {$mxker.soname.release.major$}

# release kernel library base symbolic link target
$(KERNELSOBASE): $(KERNELSOMAJOR)
	$(RM) $(LIBDIR)/{$mxker.soname.release.base$}
	@cd $(LIBDIR) && $(LNS) {$mxker.soname.release.major$} {$mxker.soname.release.base$}

# debug kernel library target
$(KERNELSOFULL_D): $(LIBOBJ_D) | conf $(LIBHEADERS)
	$(ECHO) " CC $@ [$(words $^) object(s); $(LDLIBS)]"
	@$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS) 

# debug kernel library minor version symbolic link target
$(KERNELSOMINOR_D): $(KERNELSOFULL_D)
	$(RM) $(LIBDIR)/{$mxker.soname.debug.minor$}
	@cd $(LIBDIR) && $(LNS) {$mxker.soname.debug.full$} {$mxker.soname.debug.minor$}

# debug kernel library major version symbolic link target
$(KERNELSOMAJOR_D): $(KERNELSOMINOR_D)
	$(RM) $(LIBDIR)/{$mxker.soname.debug.major$}
	@cd $(LIBDIR) && $(LNS) {$mxker.soname.debug.minor$} {$mxker.soname.debug.major$}

# debug kernel library base symbolic link target
$(KERNELSOBASE_D): $(KERNELSOMAJOR_D)
	$(RM) $(LIBDIR)/{$mxker.soname.debug.base$}
	@cd $(LIBDIR) && $(LNS) {$mxker.soname.debug.major$} {$mxker.soname.debug.base$}

# release compilation rule
$(OBJDIR)/%.o : %.c $(DPRDIR)/%.d | conf $(LIBHEADERS) $(DPRDIR) $(OBJDIR)
	@mkdir -p $(@D)
	$(ECHO) " CC $@ [$<]"
	@$(COMPILE.c) $(OUTPUT_OPTION) $<

# debug compilation rule
$(OBJDIR)/%_dbg.o : %.c $(DPDDIR)/%.d | conf $(LIBHEADERS) $(DPDDIR) $(OBJDIR)
	@mkdir -p $(@D)
	$(ECHO) " CC $@ [$<]"
	@$(COMPILE.c) $(OUTPUT_OPTION) $<

{$if@mxker.csc.bindings$}
# C# release assembly compilation rule
%.exe : %.cs | kernel-release conf-cs
	$(ECHO) "CSC $@ [$<; exe ← $(@D)/mxker.dllmap.release.config]"
	@$(COMPILE.cs) /target:exe /appconfig:$(@D)/../mxker.dllmap.release.config /out:$@ $<

# C# debug assembly compilation rule
%d.exe : %.cs | kernel-debug conf-cs
	$(ECHO) "CSC $@ [$<; exe,pdb ← $(@D)/mxker.dllmap.debug.config]"
	@$(COMPILE.cs) /target:exe /appconfig:$(@D)/../mxker.dllmap.debug.config /out:$@ $<

{$@fi$}
# configurator rule .h
%.h : %.h.in config/mxker.conf
	$(CONFIGURATOR) $< $@

# configurator rule .c
%.c : %.c.in config/mxker.conf
	$(CONFIGURATOR) $< $@

# configurator rule .cs
%.cs : %.cs.in config/mxker.conf
	$(CONFIGURATOR) $< $@

# configurator rule .config (Mono assembly configuration)
%.config : %.config.in config/mxker.conf
	$(CONFIGURATOR) $< $@

# make sure the tree in the dependency dir exists
$(DPRDIR)/%.d : ; @mkdir -p $(@D)
$(DPDDIR)/%.d : ; @mkdir -p $(@D)

# missing directory rules
$(OBJDIR): ; @mkdir -p $@
$(DPRDIR): ; @mkdir -p $@
$(DPDDIR): ; @mkdir -p $@

# cleanup target
.PHONY: clean
clean:
	$(ECHO) "Cleaning mxker..."
	$(RM) $(KERNELTESTBIN) $(MEMTESTBIN)
	$(ECHO) "Deleted release test programs."
	$(RM) $(KERNELTESTBIN_D) $(MEMTESTBIN_D)
	$(ECHO) "Deleted debug test programs."
	$(RM) $(KERNELSOFULL)
	$(RM) $(KERNELSOMINOR)
	$(RM) $(KERNELSOMAJOR)
	$(RM) $(KERNELSOBASE)
	$(ECHO) "Deleted release kernel object."
	$(RM) $(KERNELSOFULL_D)
	$(RM) $(KERNELSOMINOR_D)
	$(RM) $(KERNELSOMAJOR_D)
	$(RM) $(KERNELSOBASE_D)
	$(ECHO) "Deleted debug kernel object."
	$(RM) $(LIBHEADERS)
	$(ECHO) "Deleted configured headers."
	@$(RMR) $(DPRDIR)
	$(ECHO) "Deleted release dependency directory."
	@$(RMR) $(DPDDIR)
	$(ECHO) "Deleted debug dependency directory."
	@$(RMR) $(OBJDIR)
	$(ECHO) "Deleted object directory."
{$if@mxker.csc.bindings$}
	$(RM) $(CSHARPCONFFILES)
	$(ECHO) "Deleted configured C# test files."
	$(RM) $(CSHARPTESTS)
	$(ECHO) "Deleted release C# test programs."
	$(RM) $(CSHARPTESTS_D)
	$(ECHO) "Deleted debug C# test programs."
	$(RM) $(CSHARPTESTS_PDBS)
	$(ECHO) "Deleted debug C# test program debug databases."
{$@fi$}
	$(ECHO) "Finished cleaning."

.PHONY: deepclean
deepclean: clean
	$(ECHO) "Cleaning bootstrap files..."
	$(RM) config/mxker.conf
	$(RM) config/conf.out.c
	$(RM) config/conf config/confread config/confgen
	$(RM) config/terminfo_collect
	$(RM) src/bits/terminfo.inl 
	$(ECHO) "Deleted configurator intermediate files."
	$(RM) version/.dir*
	$(ECHO) "Deleted version directory listings."
	$(RM) Makefile
	$(ECHO) "Deleted configurator output Makefile."
	$(ECHO) "Ready for release, bootstrap required."

null :=
space := ${null} ${null}
star := *
${space} := ${space}

define \n


endef

GITIGNORE_CONTENT = \
    c/$(KERNELTESTBIN) c/$(MEMTESTBIN) \
    c/$(KERNELTESTBIN_D) c/$(MEMTESTBIN_D) \
{$if@mxker.csc.bindings$}
    $(patsubst %,c/%,$(CSHARPCONFFILES)) \
    $(patsubst %,c/%,$(CSHARPTESTS)) \
    $(patsubst %,c/%,$(CSHARPTESTS_D)) \
    *.pdb \
{$@fi$}
    c/$(KERNELSOBASE)${star} \
    c/$(KERNELSOBASE_D)${star} \
    $(patsubst %,c/%,$(LIBHEADERS)) \
    c/$(DPRDIR) c/$(DPDDIR) c/$(OBJDIR) \
    c/config/mxker.conf c/config/conf.out.c c/config/conf c/config/confread \
    c/config/confgen c/config/terminfo_collect \
    terminfo.inl \
    .dir${star} \
    c/tools/gitignore \
    c/Makefile \
    *.symtab

# TODO : implement clean targets using this method so we don't have
#        redundant copies of the build files

MXKER_GITIGNORE_LINES = $(subst ${ },${\n},$(GITIGNORE_CONTENT))
export MXKER_GITIGNORE_LINES

# gitignore maintenance target
.PHONY: gitignore
gitignore:
	$(ECHOP) '# Automatically generated .gitignore section from buildfiles.' > tools/gitignore
	$(ECHOP) '# Run "make gitignore" in the "c" directory assuming already bootstrapped to' >> tools/gitignore
	$(ECHOP) '# regenerate.' >> tools/gitignore
	$(ECHOP) "$${MXKER_GITIGNORE_LINES}" >> tools/gitignore
	$(CAT) tools/gitignore.in >> tools/gitignore
	$(ECHO) "Wrote tools/gitignore, use this to update your .gitignore file."

# dependency files
DPDFILES := $(SRCS:%.c=$(DPDDIR)/%.d)
DPRFILES := $(SRCS:%.c=$(DPRDIR)/%.d)

# generate release dependencies
$(DPRFILES):
include $(wildcard $(DPRFILES))

# generate debug dependencies
$(DPDFILES):
include $(wildcard $(DPDFILES))

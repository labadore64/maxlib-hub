#ifndef _MXSTR_UTF8_IMPLEMENTATION_
#define _MXSTR_UTF8_IMPLEMENTATION_

/********************/
/* Unicode Routines */
/********************/

static inline int _utf8_len(int i)
{
  static int const tab[32] = {
    3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,5,5,5,5,6,6,1,1,
  };
  if (i < 192) return 1;
  if (i < 224) return 2;
  /* look ma, no hands! */
  return tab[i - 224];
}

static inline int _utf8_slen(mxker_schar_t c)
{
  int const ci = c;
	return _utf8_len(ci + ((ci >= 0)<<8)-1);
}

static mxker_fast_u32_t const _utf8_mask[6] = {
	0x7F,
	0x1F,
	0x0F,
	0x07,
	0x03,
	0x01
};

/* mxstr_utf8_clen
 * params:
 *   mxker_schar_t c  - the first byte of a UTF-8 code point
 * return:
 *   mxker_fast_u32_t - the length of the UTF-8 encoding of a code point
 *                      beginning with c
 * */
mxker_fast_u32_t mxstr_utf8_clen(mxker_schar_t c)
{
	return (mxker_fast_u32_t)_utf8_slen(c);
}

/* mxstr_utf8_c2u
 * params:
 *   mxker_fast_u32_t *pout - code point output address
 *   mxker_schar_t    *pc   - char array input address
 * return:
 *   mxker_fast_u32_t       - length of the decoded code point
 * */
mxker_fast_u32_t mxstr_utf8_c2u(mxker_fast_u32_t *pout, const mxker_schar_t *pc)
{
	int i, len;
  mxker_fast_u32_t result;

	if (*pc == 0)
		return MXSTR_EOF; /* 0xFFFFFFFF */

  len = _utf8_slen(*pc);
	result = (mxker_fast_u32_t)pc[0] & _utf8_mask[len-1];

	for (i = 1; i < len; ++i)
  {
		result <<= 6;
		result |= pc[i] & 0x3F;
	}

	*pout = result;
	return (mxker_fast_u32_t)len;
}

/* mxstr_utf8_c2u
 * params:
 *   mxker_fast_u32_t *pout - char array output address
 *   mxker_schar_t     u    - code point
 * return:
 *   mxker_fast_u32_t       - length of the encoded code point
 * */
mxker_fast_u32_t mxstr_utf8_u2c(mxker_schar_t *pout, mxker_fast_u32_t u)
{
  int i, len;
  mxker_fast_u32_t first;

	if (u < 0x80)
  {
		first = 0;
		len = 1;
	}
  else if (u < 0x800)
  {
		first = 0xC0;
		len = 2;
	}
  else if (u < 0x10000)
  {
		first = 0xE0;
		len = 3;
	}
  else if (u < 0x200000)
  {
		first = 0xF0;
		len = 4;
	}
  else if (u < 0x4000000)
  {
		first = 0xF8;
		len = 5;
	}
  else
  {
		first = 0xFC;
		len = 6;
	}

	for (i = len - 1; i > 0; --i)
  {
		pout[i] = (u & 0x3F) | 0x80;
		u >>= 6;
	}
	pout[0] = u | first;

	return (mxker_fast_u32_t)len;
}

#endif


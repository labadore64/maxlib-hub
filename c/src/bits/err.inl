#ifndef _MXERR_IMPLEMENTATION_
#define _MXERR_IMPLEMENTATION_

#include <stdint.h>
/* error implementation */

enum
{
  MXERRVALUE_STR  = 1,
  MXERRVALUE_SINT = 2,
  MXERRVALUE_UINT = 3,
  MXERRVALUE_PTR  = 4
};

struct mxerr_spec_s
{
  char const *name;
  void const *v;
  int what;
};

static inline void mxerr_sstr(char const *name, char const *v)
{
  fprintf(stderr, "  (%s) %s\n", name, v);
}

static inline void mxerr_sint(char const *name, intptr_t v)
{
  fprintf(stderr, "  (%s) %lld\n", name, (long long)v);
}

static inline void mxerr_uint(char const *name, uintptr_t v)
{
  fprintf(stderr, "  (%s) %llu\n", name, (unsigned long long)v);
}

static inline void mxerr_addr(char const *name, void const *v)
{
  if (sizeof(uintptr_t) > 4)
    fprintf(stderr, "  (%s) %016lX\n", name, (unsigned long)v);
  else
    fprintf(stderr, "  (%s) %08lX\n", name, (unsigned long)v);
}

/* no return */
static void mxerr_panic(char const *s, int exitcode, struct mxerr_spec_s *pspec)
{
  fprintf(stderr, "FATAL %s\n", s);

  if (NULL != pspec)
  {
    /* dump related error values */
    while (pspec->name)
    {
      switch (pspec->what)
      {
      case MXERRVALUE_STR:
        mxerr_sstr(pspec->name, (char const *)pspec->v);
        break;
      case MXERRVALUE_SINT:
        mxerr_sint(pspec->name, (intptr_t)pspec->v);
        break;
      case MXERRVALUE_UINT:
        mxerr_uint(pspec->name, (uintptr_t)pspec->v);
        break;
      case MXERRVALUE_PTR:
        /* fall through */
      default:
        mxerr_addr(pspec->name, pspec->v);
        break;
      };
      ++pspec; /* next value dump */
    }
  }
  exit(exitcode);
}

#endif


#ifndef _MXKER_CRC8_IMPLEMENTATION
#define _MXKER_CRC8_IMPLEMENTATION

/* Copyright 2020 Roger Merryfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * file: src/bits/crc8.inl
 * description:
 *   CRC-8 checksum subprograms.
 * author(s):
 *   William
 *
 **/

#define _MXKER_CRC8POLYNOMIAL (0x1070U<<3)

/* this code makes the naughty assumption that the machine uses two's
 * compliment integer storage, though it will nearly always be right this is
 * still bad practice. */

static inline unsigned int _mxker_crc8(unsigned int crc, unsigned int d)
{
  /* int i; */ /* unrolled */
  unsigned int data = ((0xFF & crc) ^ (0xFF & d)) << 8;

  /* unrolled */
  /* for (i = 0; i < 8; ++i)
  { */
    data ^= _MXKER_CRC8POLYNOMIAL & -((data & 0x8000) != 0);
    data <<= 1;
    data ^= _MXKER_CRC8POLYNOMIAL & -((data & 0x8000) != 0);
    data <<= 1;
    data ^= _MXKER_CRC8POLYNOMIAL & -((data & 0x8000) != 0);
    data <<= 1;
    data ^= _MXKER_CRC8POLYNOMIAL & -((data & 0x8000) != 0);
    data <<= 1;
    data ^= _MXKER_CRC8POLYNOMIAL & -((data & 0x8000) != 0);
    data <<= 1;
    data ^= _MXKER_CRC8POLYNOMIAL & -((data & 0x8000) != 0);
    data <<= 1;
    data ^= _MXKER_CRC8POLYNOMIAL & -((data & 0x8000) != 0);
    data <<= 1;
    data ^= _MXKER_CRC8POLYNOMIAL & -((data & 0x8000) != 0);
    data <<= 1;
  /* } */
  return 0xFF & (data >> 8);
}

static inline unsigned int _mxker_crc8s(unsigned int crc, char const *data)
{
  while (*data != '\0')
    crc = _mxker_crc8(crc, *data++);
  return crc;
}

static inline unsigned int
_mxker_crc8n(unsigned int crc, char const *data, long len)
{
  while (len-- > 0)
    crc = _mxker_crc8(crc, *data++);
  return crc;
}

static inline unsigned int _mxker_crc8_16(unsigned int crc, unsigned long data)
{
  crc = _mxker_crc8(crc, data & 0xFF);
  crc = _mxker_crc8(crc, (data >> 8) & 0xFF);
  return crc;
}

static inline unsigned int _mxker_crc8_24(unsigned int crc, unsigned long data)
{
  crc = _mxker_crc8_16(crc, data);
  crc = _mxker_crc8(crc, (data >> 16) & 0xFF);
  return crc;
}

static inline unsigned int _mxker_crc8_32(unsigned int crc, unsigned long data)
{
  crc = _mxker_crc8_16(crc, data);
  crc = _mxker_crc8_16(crc, data >> 16);
  return crc;
}

static inline unsigned int _mxker_crc8_48(unsigned int crc, unsigned long data)
{
  crc = _mxker_crc8_24(crc, data);
  crc = _mxker_crc8_24(crc, data >> 24);
  return crc;
}

static inline unsigned int _mxker_crc8_64(unsigned int crc, unsigned long data)
{
  crc = _mxker_crc8_32(crc, data);
  crc = _mxker_crc8_32(crc, data >> 32);
  return crc;
}

#undef _MXKER_CRC8POLYNOMIAL

#endif


#ifndef _MXMEM_IMPLEMENTATION_
#define _MXMEM_IMPLEMENTATION_

/* Copyright 2020 Roger Merryfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * file: src/bits/mem.inl
 * description:
 *   allocator context manipulation and aligned allocator wrapper subprograms.
 * author(s):
 *   William
 *
 **/

#include "mxker/typ.h"
#include "mxker/mem.h"
#include "private/memconf.h"

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>
#include <limits.h>
#include <string.h>

#include "err.inl"
#include "pow2.inl"
#include "crc8.inl"

/* memory interface bits */

#define _MXMEM_UINTPTRWIDTH  MXKER_LARGEST_UINT_BITS
#define _MXMEM_UINTPTRSIZE   MXKER_LARGEST_UINT_BYTES

#define _MXMEM_UINTPTRWIDTH2 (_MXMEM_UINTPTRWIDTH>>1)

#define _MXMEM_MINALIGNSHIFT   2
#define _MXMEM_MINALIGN        (1<<_MXMEM_MINALIGNSHIFT) /* 4 */
#define _MXMEM_MAXALIGN        65536

/* workaround in case the uintptr is the largest integer type */
#define _MXMEM_UINTPTRHALFMASK ((1ULL<<_MXMEM_UINTPTRWIDTH2)-1ULL)
#define _MXMEM_UINTPTRMASK     (_MXMEM_UINTPTRHALFMASK |   \
                                (_MXMEM_UINTPTRHALFMASK << \
                                 (_MXMEM_UINTPTRWIDTH2)    \
                                )                          \
                               )

#if defined(MXMEM_GUARDED)
#define _MXMEM_GUARDWORD       (0xA5A5C3C35A5A3C3CULL & _MXMEM_UINTPTRMASK)
#define _MXMEM_GUARDWORD2      (0x5A5A3C3CA5A5C3C3ULL & _MXMEM_UINTPTRMASK)
#else
#define _MXMEM_GUARDWORD       0ULL
#define _MXMEM_GUARDWORD2      0ULL
#endif

/* for storing offset info in builtin aligned allocators */
#define _mxmem_metadatacount ((size_t const)8)
#define _mxmem_metadatasize  ((size_t const)(_MXMEM_UINTPTRSIZE * _mxmem_metadatacount))
#define _mxmem_parity_0      ((size_t const)MXMEM_PARITY_0)     /* first parity field, holds guard word or zero. */
#define _mxmem_baseaddrpos   ((size_t const)MXMEM_BASEADDRPOS)  /* base address position */
#define _mxmem_addroffpos    ((size_t const)MXMEM_ADDROFFPOS)   /* address offset position */
#define _mxmem_charcountpos  ((size_t const)MXMEM_CHARCOUNTPOS) /* available byte count position */
#define _mxmem_parity_1      ((size_t const)MXMEM_PARITY_1)     /* second parity field, holds crc8 repeated eightfold, second guard word, or zero. */
#define _mxmem_nallocpos     ((size_t const)MXMEM_NALLOCPOS)    /* byte count position */
#define _mxmem_nalignpos     ((size_t const)MXMEM_NALIGNPOS)    /* actual alignment position */
#define _mxmem_ralignpos     ((size_t const)MXMEM_RALIGNPOS)    /* requested alignment position */

mxker_fast_u16_t mxmem_conf()
{
  return (mxker_fast_u16_t)MXMEM_CONFFLAGS;
}
                                           
/* wrap the malloc function of the builtin malloc. */
void *mxmem_malloc_wrapper(struct mxmem_allocator_spec_s *ps, size_t n)
{
  void *p;

  (void)ps;              /* explicitly ignore */
  p = (void *)malloc(n); /* malloc heap allocate */
  return p;
}

/* wrap the free function of the builtin malloc, which does not require a
 * size specification to free memory. */
void mxmem_free_wrapper(struct mxmem_allocator_spec_s *ps, void *p, size_t n)
{
  (void)ps; /* explicitly ignore */
  (void)n;  /* explicitly ignore */
  free(p);  /* malloc heap deallocate */
}

#if defined(__cplusplus)
/* wrap the new function of the builtin c++ heap. */
void *mxmem_cxx_new_wrapper(struct mxmem_allocator_spec_s *ps, size_t n)
{
  char *p;

  (void)ps;                           /* explicitly ignore */
  p = new char[n];                    /* c++ heap allocate character array */
  return reinterpret_cast<void *>(p); /* cast to void address */
}

/* wrap the delete function of the builtin c++ heap, which does not require a
 * size specification to free memory. */
void mxmem_cxx_delete_wrapper(struct mxmem_allocator_spec_s *ps, void *p, size_t n)
{
  char *pc;

  (void)ps;                         /* explicitly ignore */
  (void)n;                          /* explicitly ignore */
  pc = reinterpret_cast<char *>(p); /* cast to character address */
  delete[] pc;                      /* c++ heap deallocate character array */
}
#endif

size_t mxmem_buitin_max_align()
{
  return (size_t)_MXMEM_MAXALIGN;
}

static inline mxker_uaddr_t _mxmem_compute_parity(mxker_uaddr_t *q)
{
#if defined(MXMEM_PARANOID)
  register unsigned int crc;
  register mxker_uaddr_t smear;

  /* use the checksum because we're paranoid about memory management today */
  crc = ((mxker_uaddr_t *)q)[_mxmem_parity_0];

  crc = _mxker_crc8_64(crc, ((mxker_uaddr_t *)q)[_mxmem_baseaddrpos]);
  crc = _mxker_crc8_64(crc, ((mxker_uaddr_t *)q)[_mxmem_addroffpos]);
  crc = _mxker_crc8_64(crc, ((mxker_uaddr_t *)q)[_mxmem_charcountpos]);

#if defined(MXMEM_GUARDED)
  crc = _mxker_crc8_64(crc, (mxker_uaddr_t)_MXMEM_GUARDWORD2);
#else
  crc = _mxker_crc8_64(crc, (mxker_uaddr_t)0);
#endif

  crc = _mxker_crc8_64(crc, ((mxker_uaddr_t *)q)[_mxmem_nallocpos]);
  crc = _mxker_crc8_64(crc, ((mxker_uaddr_t *)q)[_mxmem_nalignpos]);
  crc = _mxker_crc8_64(crc, ((mxker_uaddr_t *)q)[_mxmem_ralignpos]);

  smear = crc;
  smear |= smear << 8;
  smear |= smear << 16;
  smear |= smear << 32;
  return smear;
#else
  return _MXMEM_GUARDWORD2;
#endif
}

static void *_mxmem_builtin_aligned_alloc(
    struct mxmem_allocator_spec_s *pspec, size_t n, size_t align)
{
  void          *p, *q;
  mxker_uaddr_t  uip_base, uip_rgn, uip_off;
  unsigned long  nalloc, nalign;
  unsigned long  alignmask;

  if (pspec == NULL)
  {
    struct mxerr_spec_s errspec[] = {
      { .name="_MXMEM_MAXALIGN", .what=MXERRVALUE_UINT,
        .v=(void const *)((mxker_uaddr_t)_MXMEM_MAXALIGN) },
      { .name="pspec",           .what=MXERRVALUE_PTR,
        .v=(void const *)pspec },
      { .name="n",               .what=MXERRVALUE_UINT,
        .v=(void const *)((mxker_uaddr_t)n) },
      { .name="align",           .what=MXERRVALUE_UINT,
        .v=(void const *)((mxker_uaddr_t)align) },
      { 0 },
    };
    mxerr_panic(
        "allocator spec address cannot be NULL.", 12, &errspec[0]);
  }

  if (align > _MXMEM_MAXALIGN)
  {
    struct mxerr_spec_s errspec[] = {
      { .name="_MXMEM_MAXALIGN", .what=MXERRVALUE_UINT,
        .v=(void const *)((mxker_uaddr_t)_MXMEM_MAXALIGN) },
      { .name="pspec",           .what=MXERRVALUE_PTR,
        .v=(void const *)pspec },
      { .name="n",               .what=MXERRVALUE_UINT,
        .v=(void const *)((mxker_uaddr_t)n) },
      { .name="align",           .what=MXERRVALUE_UINT,
        .v=(void const *)((mxker_uaddr_t)align) },
      { 0 },
    };
    mxerr_panic(
        "alignment cannot be greater than _MXMEM_MAXALIGN!", 12, &errspec[0]);
  }

  /* calculate the alignment to be the smallest power of two equal to or
   * greater than the requested alignment, and force alignment of at least
   * _MXMEM_MINALIGN. because this value is a power of two, only one bit is
   * set. note that we further factor in the size of a single pointer to
   * store the base address for the deallocator. */
  nalign = 1UL << _mxatc_log2_64(
      align < _MXMEM_MINALIGN ? _MXMEM_MINALIGN : align);
  
  /* the alignment mask is the alignment minus one, which consists of all the
   * bits less significant than the bit set to specify alignment being set.
   * for example:
   *   alignment: 10000 
   *   alignmask: 01111 */
  alignmask = nalign - 1;

  /* allocate the number of bytes requested plus the size of the alignment
   * and the size of the base address storage */
  nalloc = n + nalign + _mxmem_metadatasize;

  /* call the user allocator */
  p = pspec->pfn_alloc(pspec, nalloc);

  /* make sure the allocation was successful before processing */
  if (NULL == p)
    return NULL;

  memset(p, 0, nalloc); /* clear memory */

  /* convert the pointer to an int the size of an address */
  uip_base = (mxker_uaddr_t)p + _mxmem_metadatasize;
  /* move the offset */
  uip_rgn  = uip_base + nalign;     /* save maximal offset to uip_rgn */
  uip_rgn &= ~alignmask;            /* save the alignment to uip_rgn */
  uip_off = uip_rgn - (mxker_uaddr_t)p; /* record the alignment offset */

  q = (void *)uip_rgn;

  ((mxker_uaddr_t *)q)[_mxmem_parity_0] = _MXMEM_GUARDWORD;

  ((mxker_uaddr_t *)q)[_mxmem_baseaddrpos] = (mxker_uaddr_t)p;
  ((mxker_uaddr_t *)q)[_mxmem_addroffpos] = uip_off;
  ((mxker_uaddr_t *)q)[_mxmem_charcountpos] = (mxker_uaddr_t)n;

  ((mxker_uaddr_t *)q)[_mxmem_nallocpos] = (mxker_uaddr_t)nalloc;
  ((mxker_uaddr_t *)q)[_mxmem_nalignpos] = (mxker_uaddr_t)nalign;
  ((mxker_uaddr_t *)q)[_mxmem_ralignpos] = (mxker_uaddr_t)align;

  ((mxker_uaddr_t *)q)[_mxmem_parity_1] =
    (mxker_uaddr_t)_mxmem_compute_parity(q);
  
  return q;
}

static void _mxmem_builtin_aligned_dealloc(
    struct mxmem_allocator_spec_s *pspec, void *p, size_t n, size_t align)
{
  mxker_uaddr_t            guardsrc;
  mxker_uaddr_t paritycal, paritysrc;
  mxker_uaddr_t uiq, uio, nalloc, nalign, ralign, nreported;
  void *q;

  if (NULL == p)
  {
    pspec->errorcode = MXMEM_EBADARG;
    return;
  }

  uiq       = ((mxker_uaddr_t *)p)[_mxmem_baseaddrpos];
  uio       = ((mxker_uaddr_t *)p)[_mxmem_addroffpos];
  nreported = ((mxker_uaddr_t *)p)[_mxmem_charcountpos];
  nalloc    = ((mxker_uaddr_t *)p)[_mxmem_nallocpos];
  nalign    = ((mxker_uaddr_t *)p)[_mxmem_nalignpos];
  ralign    = ((mxker_uaddr_t *)p)[_mxmem_ralignpos];

#if defined(MXMEM_PARANOID) || defined(MXMEM_GUARDED)
  guardsrc = ((mxker_uaddr_t *)p)[_mxmem_parity_0];

  paritycal = _mxmem_compute_parity(p);
  paritysrc = ((mxker_uaddr_t *)p)[_mxmem_parity_1];

  if (paritycal != paritysrc)
  {
    struct mxerr_spec_s errspec[] = {
      { .name="_MXMEM_GUARDWORD", .what=MXERRVALUE_UINT,
        .v=(void const *)_MXMEM_GUARDWORD },
      { .name="pspec",     .what=MXERRVALUE_PTR,  .v=(void const *)pspec },
      { .name="p",         .what=MXERRVALUE_PTR,  .v=(void const *)p },
      { .name="guard",     .what=MXERRVALUE_UINT, .v=(void const *)guardsrc },
      { .name="n",         .what=MXERRVALUE_UINT, .v=(void const *)n },
      { .name="align",     .what=MXERRVALUE_UINT, .v=(void const *)align },
      { .name="baseaddr",  .what=MXERRVALUE_PTR,  .v=(void const *)uiq },
      { .name="addroff",   .what=MXERRVALUE_SINT, .v=(void const *)uio },
      { .name="charcount", .what=MXERRVALUE_UINT, .v=(void const *)nreported },
      { .name="realparity",.what=MXERRVALUE_UINT, .v=(void const *)paritycal },
      { .name="storparity",.what=MXERRVALUE_UINT, .v=(void const *)paritysrc },
      { .name="nalloc",    .what=MXERRVALUE_UINT, .v=(void const *)nalloc },
      { .name="nalign",    .what=MXERRVALUE_UINT, .v=(void const *)nalign },
      { .name="ralign",    .what=MXERRVALUE_UINT, .v=(void const *)ralign },
      { 0 },
    };
    pspec->errorcode = MXMEM_ECORRUPT;
    mxerr_panic("mismatch between computed parity and stored parity",
        12, &errspec[0]);
  }
#endif

#if defined(MXMEM_GUARDED)
  if (_MXMEM_GUARDWORD != guardsrc)
  {
    struct mxerr_spec_s errspec[] = {
      { .name="_MXMEM_GUARDWORD", .what=MXERRVALUE_UINT,
        .v=(void const *)_MXMEM_GUARDWORD },
      { .name="pspec",     .what=MXERRVALUE_PTR,  .v=(void const *)pspec },
      { .name="p",         .what=MXERRVALUE_PTR,  .v=(void const *)p },
      { .name="guard",     .what=MXERRVALUE_UINT, .v=(void const *)guardsrc },
      { .name="n",         .what=MXERRVALUE_UINT, .v=(void const *)n },
      { .name="align",     .what=MXERRVALUE_UINT, .v=(void const *)align },
      { .name="baseaddr",  .what=MXERRVALUE_PTR,  .v=(void const *)uiq },
      { .name="addroff",   .what=MXERRVALUE_SINT, .v=(void const *)uio },
      { .name="charcount", .what=MXERRVALUE_UINT, .v=(void const *)nreported },
      { .name="realparity",.what=MXERRVALUE_UINT, .v=(void const *)paritycal },
      { .name="storparity",.what=MXERRVALUE_UINT, .v=(void const *)paritysrc },
      { .name="nalloc",    .what=MXERRVALUE_UINT, .v=(void const *)nalloc },
      { .name="nalign",    .what=MXERRVALUE_UINT, .v=(void const *)nalign },
      { .name="ralign",    .what=MXERRVALUE_UINT, .v=(void const *)ralign },
      { 0 },
    };
    pspec->errorcode = MXMEM_ECORRUPT;
    mxerr_panic("mismatch between check guard word and stored guard word",
        12, &errspec[0]);
  }

  if (uiq + uio != (mxker_uaddr_t)p)
  {
    struct mxerr_spec_s errspec[] = {
      { .name="_MXMEM_GUARDWORD", .what=MXERRVALUE_UINT,
        .v=(void const *)_MXMEM_GUARDWORD },
      { .name="pspec",     .what=MXERRVALUE_PTR,  .v=(void const *)pspec },
      { .name="p",         .what=MXERRVALUE_PTR,  .v=(void const *)p },
      { .name="guard",     .what=MXERRVALUE_UINT, .v=(void const *)guardsrc },
      { .name="n",         .what=MXERRVALUE_UINT, .v=(void const *)n },
      { .name="align",     .what=MXERRVALUE_UINT, .v=(void const *)align },
      { .name="baseaddr",  .what=MXERRVALUE_PTR,  .v=(void const *)uiq },
      { .name="addroff",   .what=MXERRVALUE_SINT, .v=(void const *)uio },
      { .name="charcount", .what=MXERRVALUE_UINT, .v=(void const *)nreported },
      { .name="realparity",.what=MXERRVALUE_UINT, .v=(void const *)paritycal },
      { .name="parity",    .what=MXERRVALUE_UINT, .v=(void const *)paritysrc },
      { .name="nalloc",    .what=MXERRVALUE_UINT, .v=(void const *)nalloc },
      { .name="nalign",    .what=MXERRVALUE_UINT, .v=(void const *)nalign },
      { .name="ralign",    .what=MXERRVALUE_UINT, .v=(void const *)ralign },
      { 0 },
    };
    pspec->errorcode = MXMEM_ECORRUPT;
    mxerr_panic("mismatch between base address offset sum and aligned pointer",
        12, &errspec[0]);
  }

  if (nreported != (mxker_uaddr_t)n)
  {
    struct mxerr_spec_s errspec[] = {
      { .name="_MXMEM_GUARDWORD", .what=MXERRVALUE_UINT,
        .v=(void const *)_MXMEM_GUARDWORD },
      { .name="pspec",     .what=MXERRVALUE_PTR,  .v=(void const *)pspec },
      { .name="p",         .what=MXERRVALUE_PTR,  .v=(void const *)p },
      { .name="guard",     .what=MXERRVALUE_UINT, .v=(void const *)guardsrc },
      { .name="n",         .what=MXERRVALUE_UINT, .v=(void const *)n },
      { .name="align",     .what=MXERRVALUE_UINT, .v=(void const *)align },
      { .name="baseaddr",  .what=MXERRVALUE_PTR,  .v=(void const *)uiq },
      { .name="addroff",   .what=MXERRVALUE_SINT, .v=(void const *)uio },
      { .name="charcount", .what=MXERRVALUE_UINT, .v=(void const *)nreported },
      { .name="realparity",.what=MXERRVALUE_UINT, .v=(void const *)paritycal },
      { .name="parity",    .what=MXERRVALUE_UINT, .v=(void const *)paritysrc },
      { .name="nalloc",    .what=MXERRVALUE_UINT, .v=(void const *)nalloc },
      { .name="nalign",    .what=MXERRVALUE_UINT, .v=(void const *)nalign },
      { .name="ralign",    .what=MXERRVALUE_UINT, .v=(void const *)ralign },
      { 0 },
    };
    pspec->errorcode = MXMEM_ECORRUPT;
    mxerr_panic("mismatch between requested deallocation amount (n) and "
                "charcount",
        13, &errspec[0]);
  }
#endif

  q = (void *)uiq;

  pspec->pfn_dealloc(pspec, q, nalloc);
}

/* initialize allocator spec */
int mxmem_create_allocator_spec(
    struct mxmem_allocator_spec_s       *pout,
    struct mxmem_allocator_spec_s const *pspec)
{
  int f_nonnull;

  if (NULL == pout)
    return -1;
  if (NULL != pspec)
  {
    f_nonnull =  (NULL != pspec->pfn_alloc)                  /* 0x1 */
              | ((NULL != pspec->pfn_dealloc) << 1)          /* 0x2 */
              | ((NULL != pspec->pfn_aligned_alloc) << 2)    /* 0x4 */
              | ((NULL != pspec->pfn_aligned_dealloc) << 3); /* 0x8 */
  }
  else
    f_nonnull = 0;

  memset(pout, 0, sizeof(struct mxmem_allocator_spec_s));

  pout->errorcode = MXMEM_EMALFORMED;
  pout->puser = pspec->puser;

  switch (f_nonnull)
  {
  case 0x0: /* nothing set */
#if defined(__cplusplus)
    pout->pfn_alloc = &mxmem_cxx_new_wrapper;
    pout->pfn_dealloc = &mxmem_cxx_delete_wrapper;
#else
    pout->pfn_alloc = &mxmem_malloc_wrapper;
    pout->pfn_dealloc = &mxmem_free_wrapper;
#endif
    break;
  case 0x3: /* set: alloc, dealloc */
    pout->pfn_alloc = pspec->pfn_alloc;
    pout->pfn_dealloc = pspec->pfn_dealloc;
    break;
  case 0xF: /* set: alloc, dealloc, aligned_alloc, aligned_dealloc */
    pout->pfn_alloc = pspec->pfn_alloc;
    pout->pfn_dealloc = pspec->pfn_dealloc;
    pout->pfn_aligned_alloc = pspec->pfn_aligned_alloc;
    pout->pfn_aligned_dealloc = pspec->pfn_aligned_dealloc;
    pout->errorcode = MXMEM_EOK;
    break;
  default: /* all other cases malformed */
    break;
  };

  if (pout->errorcode != MXMEM_EOK)
  {
    /* set up builtin aligned allocators */
    pout->pfn_aligned_alloc = &_mxmem_builtin_aligned_alloc;
    pout->pfn_aligned_dealloc = &_mxmem_builtin_aligned_dealloc;
    pout->errorcode = MXMEM_EOK;
  }

  return pout->errorcode;
}

/* initialize allocator spec inplace */
int mxmem_init_allocator_spec(
    struct mxmem_allocator_spec_s *pspec)
{
  int result;
  struct mxmem_allocator_spec_s spec_out;

  if ((result = mxmem_create_allocator_spec(&spec_out, pspec)) == MXMEM_EOK)
    memcpy(pspec, &spec_out, sizeof(spec_out));

  return result;
}

#undef _MXMEM_UINTPTRWIDTH
#undef _MXMEM_UINTPTRSIZE
#undef _MXMEM_UINTPTRWIDTH2

#undef _MXMEM_MINALIGNSHIFT
#undef _MXMEM_MINALIGN
#undef _MXMEM_MAXALIGN

#undef _MXMEM_UINTPTRHALFMASK
#undef _MXMEM_UINTPTRMASK

#undef _MXMEM_GUARDWORD 
#undef _MXMEM_GUARDWORD2

#endif


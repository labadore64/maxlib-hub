#ifndef _MXATC_POW2_IMPLEMENTATION_
#define _MXATC_POW2_IMPLEMENTATION_

#include "mxker/typ.h"

/* power of two arithmetic */

/* debrujin log2 table */
static mxker_fast_u64_t const _mxatc_log2tab64[64] =
{
  63,  0, 58,  1, 59, 47, 53,  2,
  60, 39, 48, 27, 54, 33, 42,  3,
  61, 51, 37, 40, 49, 18, 28, 20,
  55, 30, 34, 11, 43, 14, 22,  4,
  62, 57, 46, 52, 38, 26, 32, 41,
  50, 36, 17, 19, 29, 10, 13, 21,
  56, 45, 25, 31, 35, 16,  9, 12,
  44, 24, 15,  8, 23,  7,  6,  5,
};

/* debrujin log2 table */
static mxker_fast_u32_t const _mxatc_log2tab32[64] =
{
   0,  9,  1, 10, 13, 21,  2, 29,
  11, 14, 16, 18, 22, 25,  3, 30,
   8, 12, 20, 28, 15, 17, 24,  7,
  19, 27, 23,  6, 26,  5,  4, 31,
};

/* calculate integer log2 of u64 */
static inline mxker_fast_u64_t _mxatc_log2_64(mxker_fast_u64_t n)
{
  n |= n >> 1;
  n |= n >> 2;
  n |= n >> 4;
  n |= n >> 8;
  n |= n >> 16;
  n |= n >> 32;
  /* debrujin product magick */
  return _mxatc_log2tab64[((mxker_fast_u64_t)((n - (n >> 1))*(mxker_fast_u64_t)0x07EDD5E59A4E28C2)) >> 58];
}

/* calculate integer log2 of u32 */
static inline mxker_fast_u32_t _mxatc_log2_32(mxker_fast_u32_t n)
{
  n |= n >> 1;
  n |= n >> 2;
  n |= n >> 4;
  n |= n >> 8;
  n |= n >> 16;
  /* debrujin product magick */
  return _mxatc_log2tab32[(n*(mxker_fast_u32_t)0x07C4ACDD) >> 27];
}

/* calculate integer log2 of u16 */
static inline mxker_fast_u16_t _mxatc_log2_16(mxker_fast_u16_t n)
{
  return (mxker_fast_u16_t)_mxatc_log2_32((mxker_fast_u32_t)n);
}

/* calculate next power of two of unsigned long (unless already a power of two)
 * */
static inline mxker_fast_u64_t _mxatc_pow2_64(mxker_fast_u64_t n)
{
  n -= 1;
  n |= n >> 1;
  n |= n >> 2;
  n |= n >> 4;
  n |= n >> 8;
  n |= n >> 16;
  n |= n >> 32;
  n += 1;
  return n;
}
/* calculate next power of two of unsigned long (unless already a power of two)
 * */
static inline mxker_fast_u32_t _mxatc_pow2_32(mxker_fast_u32_t n)
{
  n -= 1;
  n |= n >> 1;
  n |= n >> 2;
  n |= n >> 4;
  n |= n >> 8;
  n |= n >> 16;
  n += 1;
  return n;
}
/* calculate next power of two of unsigned long (unless already a power of two)
 * */
static inline mxker_fast_u16_t _mxatc_pow2_16(mxker_fast_u16_t n)
{
  n -= 1;
  n |= n >> 1;
  n |= n >> 2;
  n |= n >> 4;
  n |= n >> 8;
  n += 1;
  return n;
}

#endif


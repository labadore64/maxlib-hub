/* Copyright 2020 Roger Merryfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * file: src/bits/squote.inl
 * description:
 *   C string literal encoding subprogram.
 * author(s):
 *   William
 * version:
 *   squote-inline-1
 *
 **/

/* implementation of C string literal encoder. */
static void _fsquote(FILE *f, char const *s)
{
  register int c;
  char const *k = s;
  fprintf(f, "%c", '"');
  while ((c = (int)*k++) != 0)
  {
    switch (c)
    {
    case '\'':
      fprintf(f, "%c", c);
      break;
    case '"':
      fprintf(f, "%s", "\\\"");
      break;
    case '\a':
      fprintf(f, "%s", "\\a");
      break;
    case '\b':
      fprintf(f, "%s", "\\b");
      break;
    case '\f':
      fprintf(f, "%s", "\\f");
      break;
    case '\n':
      fprintf(f, "%s", "\\n");
      break;
    case '\r':
      fprintf(f, "%s", "\\r");
      break;
    case '\t':
      fprintf(f, "%s", "\\t");
      break;
    case '\v':
      fprintf(f, "%s", "\\v");
      break;
    case '\\':
      fprintf(f, "%s", "\\\\");
      break;
    case 27:
      fprintf(f, "%s", "\\033");
      break;
    default:
      if (isprint(c) && c >= 32 && c <= 127)
        fprintf(f, "%c", c);
      else
        fprintf(f, "%s%02x%s", "\" \"\\x", c, "\" \"");
      break;
    };
  }
  fprintf(f, "%c", '"');
}


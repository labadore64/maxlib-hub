// This is the MaxKernel.Blit class.
//
// MxBlit.cs was automatically generated at
//
//   2020-05-05 02:11:30.232640 (UTC)
//
// by scripts/python/genblit_csharp.py, which is a part of the Max Library
// Machine utility scripts. It provides (relatively) fast blitting routines for
// C# as a convenience for interfacing with the max kernel (mxker), and these
// also support punning during blits. Concrete methods are about twice as fast
// as generics on both Mono and .net, and this is owed to the fact that unlike
// C# generic methods, C# concrete methods can be compiled to native code. In
// particular, C# generics are this way because their mechanism is not to
// generate a set of concrete instances as is done for templates by C++
// compilers, but adjust implementation at runtime directly from the IL code.
//
// Since the entire purpose of these routines is to offer real time blitting
// performance, C# generics are unacceptable, and so they are implemented in
// generic form as formatting templates in a python program which is used to
// regenerate the file if they are changed for any reason. The bootstrap
// process is opinionated against invoking any languages other than C and
// shell scripts (only as a stop gap until the entire process is written in
// C), so this is done by the developer in the course of maintenance.

using System;

namespace MaxKernel {
    public static class Blit {
        ///////////////////////////////////////////////////////
        // Punning To Circumvent Type Conversion Limitations //
        ///////////////////////////////////////////////////////

        public static double PunLongDouble(long li)
            { unsafe { return *((double *)&li); } }

        public static long PunDoubleLong(double d)
            { unsafe { return *((long *)&d); } }

        public static float PunIntFloat(int i)
            { unsafe { return *((float *)&i); } }

        public static int PunFloatInt(float f)
            { unsafe { return *((int *)&f); } }

        public static int PunBoolInt(bool b)
            { unsafe { return *((int *)&b); } }

        public static bool PunIntBool(int i)
            { unsafe { return *((bool *)&i); } }

        ///////////////////////////////////////
        // Pointer-Pointer Blitting Routines //
        ///////////////////////////////////////

        unsafe public static void BlitBoolPtrBoolPtr(bool *pdst, bool *psrc, int n)
            { while (n-- > 0) *pdst++ = *psrc++; }

        unsafe public static void BlitBoolPtrIntPtr(int *pdst, bool *psrc, int n)
            { while (n-- > 0) *pdst++ = *(int *)psrc++; }

        unsafe public static void BlitBoolPtrUIntPtr(uint *pdst, bool *psrc, int n)
            { while (n-- > 0) *pdst++ = *(uint *)psrc++; }

        unsafe public static void BlitBoolPtrFloatPtr(float *pdst, bool *psrc, int n)
            { while (n-- > 0) *pdst++ = *(float *)psrc++; }

        unsafe public static void BlitBytePtrBytePtr(byte *pdst, byte *psrc, int n)
            { while (n-- > 0) *pdst++ = *psrc++; }

        unsafe public static void BlitBytePtrSBytePtr(sbyte *pdst, byte *psrc, int n)
            { while (n-- > 0) *pdst++ = *(sbyte *)psrc++; }

        unsafe public static void BlitSBytePtrBytePtr(byte *pdst, sbyte *psrc, int n)
            { while (n-- > 0) *pdst++ = *(byte *)psrc++; }

        unsafe public static void BlitSBytePtrSBytePtr(sbyte *pdst, sbyte *psrc, int n)
            { while (n-- > 0) *pdst++ = *psrc++; }

        unsafe public static void BlitCharPtrCharPtr(char *pdst, char *psrc, int n)
            { while (n-- > 0) *pdst++ = *psrc++; }

        unsafe public static void BlitCharPtrShortPtr(short *pdst, char *psrc, int n)
            { while (n-- > 0) *pdst++ = *(short *)psrc++; }

        unsafe public static void BlitCharPtrUShortPtr(ushort *pdst, char *psrc, int n)
            { while (n-- > 0) *pdst++ = *(ushort *)psrc++; }

        unsafe public static void BlitShortPtrCharPtr(char *pdst, short *psrc, int n)
            { while (n-- > 0) *pdst++ = *(char *)psrc++; }

        unsafe public static void BlitShortPtrShortPtr(short *pdst, short *psrc, int n)
            { while (n-- > 0) *pdst++ = *psrc++; }

        unsafe public static void BlitShortPtrUShortPtr(ushort *pdst, short *psrc, int n)
            { while (n-- > 0) *pdst++ = *(ushort *)psrc++; }

        unsafe public static void BlitUShortPtrCharPtr(char *pdst, ushort *psrc, int n)
            { while (n-- > 0) *pdst++ = *(char *)psrc++; }

        unsafe public static void BlitUShortPtrShortPtr(short *pdst, ushort *psrc, int n)
            { while (n-- > 0) *pdst++ = *(short *)psrc++; }

        unsafe public static void BlitUShortPtrUShortPtr(ushort *pdst, ushort *psrc, int n)
            { while (n-- > 0) *pdst++ = *psrc++; }

        unsafe public static void BlitIntPtrBoolPtr(bool *pdst, int *psrc, int n)
            { while (n-- > 0) *pdst++ = *(bool *)psrc++; }

        unsafe public static void BlitIntPtrIntPtr(int *pdst, int *psrc, int n)
            { while (n-- > 0) *pdst++ = *psrc++; }

        unsafe public static void BlitIntPtrUIntPtr(uint *pdst, int *psrc, int n)
            { while (n-- > 0) *pdst++ = *(uint *)psrc++; }

        unsafe public static void BlitIntPtrFloatPtr(float *pdst, int *psrc, int n)
            { while (n-- > 0) *pdst++ = *(float *)psrc++; }

        unsafe public static void BlitUIntPtrBoolPtr(bool *pdst, uint *psrc, int n)
            { while (n-- > 0) *pdst++ = *(bool *)psrc++; }

        unsafe public static void BlitUIntPtrIntPtr(int *pdst, uint *psrc, int n)
            { while (n-- > 0) *pdst++ = *(int *)psrc++; }

        unsafe public static void BlitUIntPtrUIntPtr(uint *pdst, uint *psrc, int n)
            { while (n-- > 0) *pdst++ = *psrc++; }

        unsafe public static void BlitUIntPtrFloatPtr(float *pdst, uint *psrc, int n)
            { while (n-- > 0) *pdst++ = *(float *)psrc++; }

        unsafe public static void BlitLongPtrLongPtr(long *pdst, long *psrc, int n)
            { while (n-- > 0) *pdst++ = *psrc++; }

        unsafe public static void BlitLongPtrULongPtr(ulong *pdst, long *psrc, int n)
            { while (n-- > 0) *pdst++ = *(ulong *)psrc++; }

        unsafe public static void BlitLongPtrDoublePtr(double *pdst, long *psrc, int n)
            { while (n-- > 0) *pdst++ = *(double *)psrc++; }

        unsafe public static void BlitULongPtrLongPtr(long *pdst, ulong *psrc, int n)
            { while (n-- > 0) *pdst++ = *(long *)psrc++; }

        unsafe public static void BlitULongPtrULongPtr(ulong *pdst, ulong *psrc, int n)
            { while (n-- > 0) *pdst++ = *psrc++; }

        unsafe public static void BlitULongPtrDoublePtr(double *pdst, ulong *psrc, int n)
            { while (n-- > 0) *pdst++ = *(double *)psrc++; }

        unsafe public static void BlitFloatPtrBoolPtr(bool *pdst, float *psrc, int n)
            { while (n-- > 0) *pdst++ = *(bool *)psrc++; }

        unsafe public static void BlitFloatPtrIntPtr(int *pdst, float *psrc, int n)
            { while (n-- > 0) *pdst++ = *(int *)psrc++; }

        unsafe public static void BlitFloatPtrUIntPtr(uint *pdst, float *psrc, int n)
            { while (n-- > 0) *pdst++ = *(uint *)psrc++; }

        unsafe public static void BlitFloatPtrFloatPtr(float *pdst, float *psrc, int n)
            { while (n-- > 0) *pdst++ = *psrc++; }

        unsafe public static void BlitDoublePtrLongPtr(long *pdst, double *psrc, int n)
            { while (n-- > 0) *pdst++ = *(long *)psrc++; }

        unsafe public static void BlitDoublePtrULongPtr(ulong *pdst, double *psrc, int n)
            { while (n-- > 0) *pdst++ = *(ulong *)psrc++; }

        unsafe public static void BlitDoublePtrDoublePtr(double *pdst, double *psrc, int n)
            { while (n-- > 0) *pdst++ = *psrc++; }

        ///////////////////////////////////////
        // Address-Address Blitting Routines //
        ///////////////////////////////////////

        unsafe public static void BlitBoolAddrBoolAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            bool *pdst = (bool *)dstaddr;
            bool *psrc = (bool *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitBoolAddrIntAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            int *pdst = (int *)dstaddr;
            bool *psrc = (bool *)srcaddr;
            while (n-- > 0) *pdst++ = *(int *)psrc++;
        }

        unsafe public static void BlitBoolAddrUIntAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            uint *pdst = (uint *)dstaddr;
            bool *psrc = (bool *)srcaddr;
            while (n-- > 0) *pdst++ = *(uint *)psrc++;
        }

        unsafe public static void BlitBoolAddrFloatAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            float *pdst = (float *)dstaddr;
            bool *psrc = (bool *)srcaddr;
            while (n-- > 0) *pdst++ = *(float *)psrc++;
        }

        unsafe public static void BlitByteAddrByteAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            byte *pdst = (byte *)dstaddr;
            byte *psrc = (byte *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitByteAddrSByteAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            sbyte *pdst = (sbyte *)dstaddr;
            byte *psrc = (byte *)srcaddr;
            while (n-- > 0) *pdst++ = *(sbyte *)psrc++;
        }

        unsafe public static void BlitSByteAddrByteAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            byte *pdst = (byte *)dstaddr;
            sbyte *psrc = (sbyte *)srcaddr;
            while (n-- > 0) *pdst++ = *(byte *)psrc++;
        }

        unsafe public static void BlitSByteAddrSByteAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            sbyte *pdst = (sbyte *)dstaddr;
            sbyte *psrc = (sbyte *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitCharAddrCharAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            char *pdst = (char *)dstaddr;
            char *psrc = (char *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitCharAddrShortAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            short *pdst = (short *)dstaddr;
            char *psrc = (char *)srcaddr;
            while (n-- > 0) *pdst++ = *(short *)psrc++;
        }

        unsafe public static void BlitCharAddrUShortAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            ushort *pdst = (ushort *)dstaddr;
            char *psrc = (char *)srcaddr;
            while (n-- > 0) *pdst++ = *(ushort *)psrc++;
        }

        unsafe public static void BlitShortAddrCharAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            char *pdst = (char *)dstaddr;
            short *psrc = (short *)srcaddr;
            while (n-- > 0) *pdst++ = *(char *)psrc++;
        }

        unsafe public static void BlitShortAddrShortAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            short *pdst = (short *)dstaddr;
            short *psrc = (short *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitShortAddrUShortAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            ushort *pdst = (ushort *)dstaddr;
            short *psrc = (short *)srcaddr;
            while (n-- > 0) *pdst++ = *(ushort *)psrc++;
        }

        unsafe public static void BlitUShortAddrCharAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            char *pdst = (char *)dstaddr;
            ushort *psrc = (ushort *)srcaddr;
            while (n-- > 0) *pdst++ = *(char *)psrc++;
        }

        unsafe public static void BlitUShortAddrShortAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            short *pdst = (short *)dstaddr;
            ushort *psrc = (ushort *)srcaddr;
            while (n-- > 0) *pdst++ = *(short *)psrc++;
        }

        unsafe public static void BlitUShortAddrUShortAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            ushort *pdst = (ushort *)dstaddr;
            ushort *psrc = (ushort *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitIntAddrBoolAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            bool *pdst = (bool *)dstaddr;
            int *psrc = (int *)srcaddr;
            while (n-- > 0) *pdst++ = *(bool *)psrc++;
        }

        unsafe public static void BlitIntAddrIntAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            int *pdst = (int *)dstaddr;
            int *psrc = (int *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitIntAddrUIntAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            uint *pdst = (uint *)dstaddr;
            int *psrc = (int *)srcaddr;
            while (n-- > 0) *pdst++ = *(uint *)psrc++;
        }

        unsafe public static void BlitIntAddrFloatAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            float *pdst = (float *)dstaddr;
            int *psrc = (int *)srcaddr;
            while (n-- > 0) *pdst++ = *(float *)psrc++;
        }

        unsafe public static void BlitUIntAddrBoolAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            bool *pdst = (bool *)dstaddr;
            uint *psrc = (uint *)srcaddr;
            while (n-- > 0) *pdst++ = *(bool *)psrc++;
        }

        unsafe public static void BlitUIntAddrIntAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            int *pdst = (int *)dstaddr;
            uint *psrc = (uint *)srcaddr;
            while (n-- > 0) *pdst++ = *(int *)psrc++;
        }

        unsafe public static void BlitUIntAddrUIntAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            uint *pdst = (uint *)dstaddr;
            uint *psrc = (uint *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitUIntAddrFloatAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            float *pdst = (float *)dstaddr;
            uint *psrc = (uint *)srcaddr;
            while (n-- > 0) *pdst++ = *(float *)psrc++;
        }

        unsafe public static void BlitLongAddrLongAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            long *pdst = (long *)dstaddr;
            long *psrc = (long *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitLongAddrULongAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            ulong *pdst = (ulong *)dstaddr;
            long *psrc = (long *)srcaddr;
            while (n-- > 0) *pdst++ = *(ulong *)psrc++;
        }

        unsafe public static void BlitLongAddrDoubleAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            double *pdst = (double *)dstaddr;
            long *psrc = (long *)srcaddr;
            while (n-- > 0) *pdst++ = *(double *)psrc++;
        }

        unsafe public static void BlitULongAddrLongAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            long *pdst = (long *)dstaddr;
            ulong *psrc = (ulong *)srcaddr;
            while (n-- > 0) *pdst++ = *(long *)psrc++;
        }

        unsafe public static void BlitULongAddrULongAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            ulong *pdst = (ulong *)dstaddr;
            ulong *psrc = (ulong *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitULongAddrDoubleAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            double *pdst = (double *)dstaddr;
            ulong *psrc = (ulong *)srcaddr;
            while (n-- > 0) *pdst++ = *(double *)psrc++;
        }

        unsafe public static void BlitFloatAddrBoolAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            bool *pdst = (bool *)dstaddr;
            float *psrc = (float *)srcaddr;
            while (n-- > 0) *pdst++ = *(bool *)psrc++;
        }

        unsafe public static void BlitFloatAddrIntAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            int *pdst = (int *)dstaddr;
            float *psrc = (float *)srcaddr;
            while (n-- > 0) *pdst++ = *(int *)psrc++;
        }

        unsafe public static void BlitFloatAddrUIntAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            uint *pdst = (uint *)dstaddr;
            float *psrc = (float *)srcaddr;
            while (n-- > 0) *pdst++ = *(uint *)psrc++;
        }

        unsafe public static void BlitFloatAddrFloatAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            float *pdst = (float *)dstaddr;
            float *psrc = (float *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitDoubleAddrLongAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            long *pdst = (long *)dstaddr;
            double *psrc = (double *)srcaddr;
            while (n-- > 0) *pdst++ = *(long *)psrc++;
        }

        unsafe public static void BlitDoubleAddrULongAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            ulong *pdst = (ulong *)dstaddr;
            double *psrc = (double *)srcaddr;
            while (n-- > 0) *pdst++ = *(ulong *)psrc++;
        }

        unsafe public static void BlitDoubleAddrDoubleAddr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {
            double *pdst = (double *)dstaddr;
            double *psrc = (double *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        ///////////////////////////////////
        // Array-Array Blitting Routines //
        ///////////////////////////////////

        unsafe public static void BlitBoolArrBoolArr(bool[] dst, bool[] src, int n)
        {
            fixed (bool *fdst = &dst[0])
            {
                fixed (bool *pfsrc = &src[0])
                {
                    bool *pdst = pfdst;
                    bool *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitBoolArrBoolArr(bool[] dst, int idst, bool[] src, int n)
        {
            fixed (bool *fdst = &dst[0])
            {
                fixed (bool *pfsrc = &src[0])
                {
                    bool *pdst = pfdst + idst;
                    bool *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitBoolArrBoolArr(bool[] dst, bool[] src, int isrc, int n)
        {
            fixed (bool *fdst = &dst[0])
            {
                fixed (bool *pfsrc = &src[0])
                {
                    bool *pdst = pfdst;
                    bool *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitBoolArrBoolArr(bool[] dst, int idst, bool[] src, int isrc, int n)
        {
            fixed (bool *fdst = &dst[0])
            {
                fixed (bool *pfsrc = &src[0])
                {
                    bool *pdst = pfdst + idst;
                    bool *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitBoolArrIntArr(int[] dst, bool[] src, int n)
        {
            fixed (int *fdst = &dst[0])
            {
                fixed (bool *pfsrc = &src[0])
                {
                    int *pdst = pfdst;
                    bool *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(int *)psrc++;
                }
            }
        }

        unsafe public static void BlitBoolArrIntArr(int[] dst, int idst, bool[] src, int n)
        {
            fixed (int *fdst = &dst[0])
            {
                fixed (bool *pfsrc = &src[0])
                {
                    int *pdst = pfdst + idst;
                    bool *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(int *)psrc++;
                }
            }
        }

        unsafe public static void BlitBoolArrIntArr(int[] dst, bool[] src, int isrc, int n)
        {
            fixed (int *fdst = &dst[0])
            {
                fixed (bool *pfsrc = &src[0])
                {
                    int *pdst = pfdst;
                    bool *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(int *)psrc++;
                }
            }
        }

        unsafe public static void BlitBoolArrIntArr(int[] dst, int idst, bool[] src, int isrc, int n)
        {
            fixed (int *fdst = &dst[0])
            {
                fixed (bool *pfsrc = &src[0])
                {
                    int *pdst = pfdst + idst;
                    bool *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(int *)psrc++;
                }
            }
        }

        unsafe public static void BlitBoolArrUIntArr(uint[] dst, bool[] src, int n)
        {
            fixed (uint *fdst = &dst[0])
            {
                fixed (bool *pfsrc = &src[0])
                {
                    uint *pdst = pfdst;
                    bool *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(uint *)psrc++;
                }
            }
        }

        unsafe public static void BlitBoolArrUIntArr(uint[] dst, int idst, bool[] src, int n)
        {
            fixed (uint *fdst = &dst[0])
            {
                fixed (bool *pfsrc = &src[0])
                {
                    uint *pdst = pfdst + idst;
                    bool *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(uint *)psrc++;
                }
            }
        }

        unsafe public static void BlitBoolArrUIntArr(uint[] dst, bool[] src, int isrc, int n)
        {
            fixed (uint *fdst = &dst[0])
            {
                fixed (bool *pfsrc = &src[0])
                {
                    uint *pdst = pfdst;
                    bool *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(uint *)psrc++;
                }
            }
        }

        unsafe public static void BlitBoolArrUIntArr(uint[] dst, int idst, bool[] src, int isrc, int n)
        {
            fixed (uint *fdst = &dst[0])
            {
                fixed (bool *pfsrc = &src[0])
                {
                    uint *pdst = pfdst + idst;
                    bool *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(uint *)psrc++;
                }
            }
        }

        unsafe public static void BlitBoolArrFloatArr(float[] dst, bool[] src, int n)
        {
            fixed (float *fdst = &dst[0])
            {
                fixed (bool *pfsrc = &src[0])
                {
                    float *pdst = pfdst;
                    bool *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(float *)psrc++;
                }
            }
        }

        unsafe public static void BlitBoolArrFloatArr(float[] dst, int idst, bool[] src, int n)
        {
            fixed (float *fdst = &dst[0])
            {
                fixed (bool *pfsrc = &src[0])
                {
                    float *pdst = pfdst + idst;
                    bool *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(float *)psrc++;
                }
            }
        }

        unsafe public static void BlitBoolArrFloatArr(float[] dst, bool[] src, int isrc, int n)
        {
            fixed (float *fdst = &dst[0])
            {
                fixed (bool *pfsrc = &src[0])
                {
                    float *pdst = pfdst;
                    bool *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(float *)psrc++;
                }
            }
        }

        unsafe public static void BlitBoolArrFloatArr(float[] dst, int idst, bool[] src, int isrc, int n)
        {
            fixed (float *fdst = &dst[0])
            {
                fixed (bool *pfsrc = &src[0])
                {
                    float *pdst = pfdst + idst;
                    bool *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(float *)psrc++;
                }
            }
        }

        unsafe public static void BlitByteArrByteArr(byte[] dst, byte[] src, int n)
        {
            fixed (byte *fdst = &dst[0])
            {
                fixed (byte *pfsrc = &src[0])
                {
                    byte *pdst = pfdst;
                    byte *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitByteArrByteArr(byte[] dst, int idst, byte[] src, int n)
        {
            fixed (byte *fdst = &dst[0])
            {
                fixed (byte *pfsrc = &src[0])
                {
                    byte *pdst = pfdst + idst;
                    byte *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitByteArrByteArr(byte[] dst, byte[] src, int isrc, int n)
        {
            fixed (byte *fdst = &dst[0])
            {
                fixed (byte *pfsrc = &src[0])
                {
                    byte *pdst = pfdst;
                    byte *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitByteArrByteArr(byte[] dst, int idst, byte[] src, int isrc, int n)
        {
            fixed (byte *fdst = &dst[0])
            {
                fixed (byte *pfsrc = &src[0])
                {
                    byte *pdst = pfdst + idst;
                    byte *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitByteArrSByteArr(sbyte[] dst, byte[] src, int n)
        {
            fixed (sbyte *fdst = &dst[0])
            {
                fixed (byte *pfsrc = &src[0])
                {
                    sbyte *pdst = pfdst;
                    byte *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(sbyte *)psrc++;
                }
            }
        }

        unsafe public static void BlitByteArrSByteArr(sbyte[] dst, int idst, byte[] src, int n)
        {
            fixed (sbyte *fdst = &dst[0])
            {
                fixed (byte *pfsrc = &src[0])
                {
                    sbyte *pdst = pfdst + idst;
                    byte *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(sbyte *)psrc++;
                }
            }
        }

        unsafe public static void BlitByteArrSByteArr(sbyte[] dst, byte[] src, int isrc, int n)
        {
            fixed (sbyte *fdst = &dst[0])
            {
                fixed (byte *pfsrc = &src[0])
                {
                    sbyte *pdst = pfdst;
                    byte *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(sbyte *)psrc++;
                }
            }
        }

        unsafe public static void BlitByteArrSByteArr(sbyte[] dst, int idst, byte[] src, int isrc, int n)
        {
            fixed (sbyte *fdst = &dst[0])
            {
                fixed (byte *pfsrc = &src[0])
                {
                    sbyte *pdst = pfdst + idst;
                    byte *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(sbyte *)psrc++;
                }
            }
        }

        unsafe public static void BlitSByteArrByteArr(byte[] dst, sbyte[] src, int n)
        {
            fixed (byte *fdst = &dst[0])
            {
                fixed (sbyte *pfsrc = &src[0])
                {
                    byte *pdst = pfdst;
                    sbyte *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(byte *)psrc++;
                }
            }
        }

        unsafe public static void BlitSByteArrByteArr(byte[] dst, int idst, sbyte[] src, int n)
        {
            fixed (byte *fdst = &dst[0])
            {
                fixed (sbyte *pfsrc = &src[0])
                {
                    byte *pdst = pfdst + idst;
                    sbyte *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(byte *)psrc++;
                }
            }
        }

        unsafe public static void BlitSByteArrByteArr(byte[] dst, sbyte[] src, int isrc, int n)
        {
            fixed (byte *fdst = &dst[0])
            {
                fixed (sbyte *pfsrc = &src[0])
                {
                    byte *pdst = pfdst;
                    sbyte *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(byte *)psrc++;
                }
            }
        }

        unsafe public static void BlitSByteArrByteArr(byte[] dst, int idst, sbyte[] src, int isrc, int n)
        {
            fixed (byte *fdst = &dst[0])
            {
                fixed (sbyte *pfsrc = &src[0])
                {
                    byte *pdst = pfdst + idst;
                    sbyte *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(byte *)psrc++;
                }
            }
        }

        unsafe public static void BlitSByteArrSByteArr(sbyte[] dst, sbyte[] src, int n)
        {
            fixed (sbyte *fdst = &dst[0])
            {
                fixed (sbyte *pfsrc = &src[0])
                {
                    sbyte *pdst = pfdst;
                    sbyte *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitSByteArrSByteArr(sbyte[] dst, int idst, sbyte[] src, int n)
        {
            fixed (sbyte *fdst = &dst[0])
            {
                fixed (sbyte *pfsrc = &src[0])
                {
                    sbyte *pdst = pfdst + idst;
                    sbyte *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitSByteArrSByteArr(sbyte[] dst, sbyte[] src, int isrc, int n)
        {
            fixed (sbyte *fdst = &dst[0])
            {
                fixed (sbyte *pfsrc = &src[0])
                {
                    sbyte *pdst = pfdst;
                    sbyte *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitSByteArrSByteArr(sbyte[] dst, int idst, sbyte[] src, int isrc, int n)
        {
            fixed (sbyte *fdst = &dst[0])
            {
                fixed (sbyte *pfsrc = &src[0])
                {
                    sbyte *pdst = pfdst + idst;
                    sbyte *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitCharArrCharArr(char[] dst, char[] src, int n)
        {
            fixed (char *fdst = &dst[0])
            {
                fixed (char *pfsrc = &src[0])
                {
                    char *pdst = pfdst;
                    char *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitCharArrCharArr(char[] dst, int idst, char[] src, int n)
        {
            fixed (char *fdst = &dst[0])
            {
                fixed (char *pfsrc = &src[0])
                {
                    char *pdst = pfdst + idst;
                    char *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitCharArrCharArr(char[] dst, char[] src, int isrc, int n)
        {
            fixed (char *fdst = &dst[0])
            {
                fixed (char *pfsrc = &src[0])
                {
                    char *pdst = pfdst;
                    char *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitCharArrCharArr(char[] dst, int idst, char[] src, int isrc, int n)
        {
            fixed (char *fdst = &dst[0])
            {
                fixed (char *pfsrc = &src[0])
                {
                    char *pdst = pfdst + idst;
                    char *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitCharArrShortArr(short[] dst, char[] src, int n)
        {
            fixed (short *fdst = &dst[0])
            {
                fixed (char *pfsrc = &src[0])
                {
                    short *pdst = pfdst;
                    char *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(short *)psrc++;
                }
            }
        }

        unsafe public static void BlitCharArrShortArr(short[] dst, int idst, char[] src, int n)
        {
            fixed (short *fdst = &dst[0])
            {
                fixed (char *pfsrc = &src[0])
                {
                    short *pdst = pfdst + idst;
                    char *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(short *)psrc++;
                }
            }
        }

        unsafe public static void BlitCharArrShortArr(short[] dst, char[] src, int isrc, int n)
        {
            fixed (short *fdst = &dst[0])
            {
                fixed (char *pfsrc = &src[0])
                {
                    short *pdst = pfdst;
                    char *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(short *)psrc++;
                }
            }
        }

        unsafe public static void BlitCharArrShortArr(short[] dst, int idst, char[] src, int isrc, int n)
        {
            fixed (short *fdst = &dst[0])
            {
                fixed (char *pfsrc = &src[0])
                {
                    short *pdst = pfdst + idst;
                    char *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(short *)psrc++;
                }
            }
        }

        unsafe public static void BlitCharArrUShortArr(ushort[] dst, char[] src, int n)
        {
            fixed (ushort *fdst = &dst[0])
            {
                fixed (char *pfsrc = &src[0])
                {
                    ushort *pdst = pfdst;
                    char *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(ushort *)psrc++;
                }
            }
        }

        unsafe public static void BlitCharArrUShortArr(ushort[] dst, int idst, char[] src, int n)
        {
            fixed (ushort *fdst = &dst[0])
            {
                fixed (char *pfsrc = &src[0])
                {
                    ushort *pdst = pfdst + idst;
                    char *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(ushort *)psrc++;
                }
            }
        }

        unsafe public static void BlitCharArrUShortArr(ushort[] dst, char[] src, int isrc, int n)
        {
            fixed (ushort *fdst = &dst[0])
            {
                fixed (char *pfsrc = &src[0])
                {
                    ushort *pdst = pfdst;
                    char *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(ushort *)psrc++;
                }
            }
        }

        unsafe public static void BlitCharArrUShortArr(ushort[] dst, int idst, char[] src, int isrc, int n)
        {
            fixed (ushort *fdst = &dst[0])
            {
                fixed (char *pfsrc = &src[0])
                {
                    ushort *pdst = pfdst + idst;
                    char *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(ushort *)psrc++;
                }
            }
        }

        unsafe public static void BlitShortArrCharArr(char[] dst, short[] src, int n)
        {
            fixed (char *fdst = &dst[0])
            {
                fixed (short *pfsrc = &src[0])
                {
                    char *pdst = pfdst;
                    short *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(char *)psrc++;
                }
            }
        }

        unsafe public static void BlitShortArrCharArr(char[] dst, int idst, short[] src, int n)
        {
            fixed (char *fdst = &dst[0])
            {
                fixed (short *pfsrc = &src[0])
                {
                    char *pdst = pfdst + idst;
                    short *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(char *)psrc++;
                }
            }
        }

        unsafe public static void BlitShortArrCharArr(char[] dst, short[] src, int isrc, int n)
        {
            fixed (char *fdst = &dst[0])
            {
                fixed (short *pfsrc = &src[0])
                {
                    char *pdst = pfdst;
                    short *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(char *)psrc++;
                }
            }
        }

        unsafe public static void BlitShortArrCharArr(char[] dst, int idst, short[] src, int isrc, int n)
        {
            fixed (char *fdst = &dst[0])
            {
                fixed (short *pfsrc = &src[0])
                {
                    char *pdst = pfdst + idst;
                    short *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(char *)psrc++;
                }
            }
        }

        unsafe public static void BlitShortArrShortArr(short[] dst, short[] src, int n)
        {
            fixed (short *fdst = &dst[0])
            {
                fixed (short *pfsrc = &src[0])
                {
                    short *pdst = pfdst;
                    short *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitShortArrShortArr(short[] dst, int idst, short[] src, int n)
        {
            fixed (short *fdst = &dst[0])
            {
                fixed (short *pfsrc = &src[0])
                {
                    short *pdst = pfdst + idst;
                    short *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitShortArrShortArr(short[] dst, short[] src, int isrc, int n)
        {
            fixed (short *fdst = &dst[0])
            {
                fixed (short *pfsrc = &src[0])
                {
                    short *pdst = pfdst;
                    short *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitShortArrShortArr(short[] dst, int idst, short[] src, int isrc, int n)
        {
            fixed (short *fdst = &dst[0])
            {
                fixed (short *pfsrc = &src[0])
                {
                    short *pdst = pfdst + idst;
                    short *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitShortArrUShortArr(ushort[] dst, short[] src, int n)
        {
            fixed (ushort *fdst = &dst[0])
            {
                fixed (short *pfsrc = &src[0])
                {
                    ushort *pdst = pfdst;
                    short *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(ushort *)psrc++;
                }
            }
        }

        unsafe public static void BlitShortArrUShortArr(ushort[] dst, int idst, short[] src, int n)
        {
            fixed (ushort *fdst = &dst[0])
            {
                fixed (short *pfsrc = &src[0])
                {
                    ushort *pdst = pfdst + idst;
                    short *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(ushort *)psrc++;
                }
            }
        }

        unsafe public static void BlitShortArrUShortArr(ushort[] dst, short[] src, int isrc, int n)
        {
            fixed (ushort *fdst = &dst[0])
            {
                fixed (short *pfsrc = &src[0])
                {
                    ushort *pdst = pfdst;
                    short *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(ushort *)psrc++;
                }
            }
        }

        unsafe public static void BlitShortArrUShortArr(ushort[] dst, int idst, short[] src, int isrc, int n)
        {
            fixed (ushort *fdst = &dst[0])
            {
                fixed (short *pfsrc = &src[0])
                {
                    ushort *pdst = pfdst + idst;
                    short *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(ushort *)psrc++;
                }
            }
        }

        unsafe public static void BlitUShortArrCharArr(char[] dst, ushort[] src, int n)
        {
            fixed (char *fdst = &dst[0])
            {
                fixed (ushort *pfsrc = &src[0])
                {
                    char *pdst = pfdst;
                    ushort *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(char *)psrc++;
                }
            }
        }

        unsafe public static void BlitUShortArrCharArr(char[] dst, int idst, ushort[] src, int n)
        {
            fixed (char *fdst = &dst[0])
            {
                fixed (ushort *pfsrc = &src[0])
                {
                    char *pdst = pfdst + idst;
                    ushort *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(char *)psrc++;
                }
            }
        }

        unsafe public static void BlitUShortArrCharArr(char[] dst, ushort[] src, int isrc, int n)
        {
            fixed (char *fdst = &dst[0])
            {
                fixed (ushort *pfsrc = &src[0])
                {
                    char *pdst = pfdst;
                    ushort *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(char *)psrc++;
                }
            }
        }

        unsafe public static void BlitUShortArrCharArr(char[] dst, int idst, ushort[] src, int isrc, int n)
        {
            fixed (char *fdst = &dst[0])
            {
                fixed (ushort *pfsrc = &src[0])
                {
                    char *pdst = pfdst + idst;
                    ushort *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(char *)psrc++;
                }
            }
        }

        unsafe public static void BlitUShortArrShortArr(short[] dst, ushort[] src, int n)
        {
            fixed (short *fdst = &dst[0])
            {
                fixed (ushort *pfsrc = &src[0])
                {
                    short *pdst = pfdst;
                    ushort *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(short *)psrc++;
                }
            }
        }

        unsafe public static void BlitUShortArrShortArr(short[] dst, int idst, ushort[] src, int n)
        {
            fixed (short *fdst = &dst[0])
            {
                fixed (ushort *pfsrc = &src[0])
                {
                    short *pdst = pfdst + idst;
                    ushort *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(short *)psrc++;
                }
            }
        }

        unsafe public static void BlitUShortArrShortArr(short[] dst, ushort[] src, int isrc, int n)
        {
            fixed (short *fdst = &dst[0])
            {
                fixed (ushort *pfsrc = &src[0])
                {
                    short *pdst = pfdst;
                    ushort *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(short *)psrc++;
                }
            }
        }

        unsafe public static void BlitUShortArrShortArr(short[] dst, int idst, ushort[] src, int isrc, int n)
        {
            fixed (short *fdst = &dst[0])
            {
                fixed (ushort *pfsrc = &src[0])
                {
                    short *pdst = pfdst + idst;
                    ushort *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(short *)psrc++;
                }
            }
        }

        unsafe public static void BlitUShortArrUShortArr(ushort[] dst, ushort[] src, int n)
        {
            fixed (ushort *fdst = &dst[0])
            {
                fixed (ushort *pfsrc = &src[0])
                {
                    ushort *pdst = pfdst;
                    ushort *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitUShortArrUShortArr(ushort[] dst, int idst, ushort[] src, int n)
        {
            fixed (ushort *fdst = &dst[0])
            {
                fixed (ushort *pfsrc = &src[0])
                {
                    ushort *pdst = pfdst + idst;
                    ushort *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitUShortArrUShortArr(ushort[] dst, ushort[] src, int isrc, int n)
        {
            fixed (ushort *fdst = &dst[0])
            {
                fixed (ushort *pfsrc = &src[0])
                {
                    ushort *pdst = pfdst;
                    ushort *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitUShortArrUShortArr(ushort[] dst, int idst, ushort[] src, int isrc, int n)
        {
            fixed (ushort *fdst = &dst[0])
            {
                fixed (ushort *pfsrc = &src[0])
                {
                    ushort *pdst = pfdst + idst;
                    ushort *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitIntArrBoolArr(bool[] dst, int[] src, int n)
        {
            fixed (bool *fdst = &dst[0])
            {
                fixed (int *pfsrc = &src[0])
                {
                    bool *pdst = pfdst;
                    int *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(bool *)psrc++;
                }
            }
        }

        unsafe public static void BlitIntArrBoolArr(bool[] dst, int idst, int[] src, int n)
        {
            fixed (bool *fdst = &dst[0])
            {
                fixed (int *pfsrc = &src[0])
                {
                    bool *pdst = pfdst + idst;
                    int *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(bool *)psrc++;
                }
            }
        }

        unsafe public static void BlitIntArrBoolArr(bool[] dst, int[] src, int isrc, int n)
        {
            fixed (bool *fdst = &dst[0])
            {
                fixed (int *pfsrc = &src[0])
                {
                    bool *pdst = pfdst;
                    int *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(bool *)psrc++;
                }
            }
        }

        unsafe public static void BlitIntArrBoolArr(bool[] dst, int idst, int[] src, int isrc, int n)
        {
            fixed (bool *fdst = &dst[0])
            {
                fixed (int *pfsrc = &src[0])
                {
                    bool *pdst = pfdst + idst;
                    int *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(bool *)psrc++;
                }
            }
        }

        unsafe public static void BlitIntArrIntArr(int[] dst, int[] src, int n)
        {
            fixed (int *fdst = &dst[0])
            {
                fixed (int *pfsrc = &src[0])
                {
                    int *pdst = pfdst;
                    int *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitIntArrIntArr(int[] dst, int idst, int[] src, int n)
        {
            fixed (int *fdst = &dst[0])
            {
                fixed (int *pfsrc = &src[0])
                {
                    int *pdst = pfdst + idst;
                    int *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitIntArrIntArr(int[] dst, int[] src, int isrc, int n)
        {
            fixed (int *fdst = &dst[0])
            {
                fixed (int *pfsrc = &src[0])
                {
                    int *pdst = pfdst;
                    int *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitIntArrIntArr(int[] dst, int idst, int[] src, int isrc, int n)
        {
            fixed (int *fdst = &dst[0])
            {
                fixed (int *pfsrc = &src[0])
                {
                    int *pdst = pfdst + idst;
                    int *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitIntArrUIntArr(uint[] dst, int[] src, int n)
        {
            fixed (uint *fdst = &dst[0])
            {
                fixed (int *pfsrc = &src[0])
                {
                    uint *pdst = pfdst;
                    int *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(uint *)psrc++;
                }
            }
        }

        unsafe public static void BlitIntArrUIntArr(uint[] dst, int idst, int[] src, int n)
        {
            fixed (uint *fdst = &dst[0])
            {
                fixed (int *pfsrc = &src[0])
                {
                    uint *pdst = pfdst + idst;
                    int *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(uint *)psrc++;
                }
            }
        }

        unsafe public static void BlitIntArrUIntArr(uint[] dst, int[] src, int isrc, int n)
        {
            fixed (uint *fdst = &dst[0])
            {
                fixed (int *pfsrc = &src[0])
                {
                    uint *pdst = pfdst;
                    int *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(uint *)psrc++;
                }
            }
        }

        unsafe public static void BlitIntArrUIntArr(uint[] dst, int idst, int[] src, int isrc, int n)
        {
            fixed (uint *fdst = &dst[0])
            {
                fixed (int *pfsrc = &src[0])
                {
                    uint *pdst = pfdst + idst;
                    int *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(uint *)psrc++;
                }
            }
        }

        unsafe public static void BlitIntArrFloatArr(float[] dst, int[] src, int n)
        {
            fixed (float *fdst = &dst[0])
            {
                fixed (int *pfsrc = &src[0])
                {
                    float *pdst = pfdst;
                    int *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(float *)psrc++;
                }
            }
        }

        unsafe public static void BlitIntArrFloatArr(float[] dst, int idst, int[] src, int n)
        {
            fixed (float *fdst = &dst[0])
            {
                fixed (int *pfsrc = &src[0])
                {
                    float *pdst = pfdst + idst;
                    int *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(float *)psrc++;
                }
            }
        }

        unsafe public static void BlitIntArrFloatArr(float[] dst, int[] src, int isrc, int n)
        {
            fixed (float *fdst = &dst[0])
            {
                fixed (int *pfsrc = &src[0])
                {
                    float *pdst = pfdst;
                    int *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(float *)psrc++;
                }
            }
        }

        unsafe public static void BlitIntArrFloatArr(float[] dst, int idst, int[] src, int isrc, int n)
        {
            fixed (float *fdst = &dst[0])
            {
                fixed (int *pfsrc = &src[0])
                {
                    float *pdst = pfdst + idst;
                    int *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(float *)psrc++;
                }
            }
        }

        unsafe public static void BlitUIntArrBoolArr(bool[] dst, uint[] src, int n)
        {
            fixed (bool *fdst = &dst[0])
            {
                fixed (uint *pfsrc = &src[0])
                {
                    bool *pdst = pfdst;
                    uint *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(bool *)psrc++;
                }
            }
        }

        unsafe public static void BlitUIntArrBoolArr(bool[] dst, int idst, uint[] src, int n)
        {
            fixed (bool *fdst = &dst[0])
            {
                fixed (uint *pfsrc = &src[0])
                {
                    bool *pdst = pfdst + idst;
                    uint *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(bool *)psrc++;
                }
            }
        }

        unsafe public static void BlitUIntArrBoolArr(bool[] dst, uint[] src, int isrc, int n)
        {
            fixed (bool *fdst = &dst[0])
            {
                fixed (uint *pfsrc = &src[0])
                {
                    bool *pdst = pfdst;
                    uint *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(bool *)psrc++;
                }
            }
        }

        unsafe public static void BlitUIntArrBoolArr(bool[] dst, int idst, uint[] src, int isrc, int n)
        {
            fixed (bool *fdst = &dst[0])
            {
                fixed (uint *pfsrc = &src[0])
                {
                    bool *pdst = pfdst + idst;
                    uint *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(bool *)psrc++;
                }
            }
        }

        unsafe public static void BlitUIntArrIntArr(int[] dst, uint[] src, int n)
        {
            fixed (int *fdst = &dst[0])
            {
                fixed (uint *pfsrc = &src[0])
                {
                    int *pdst = pfdst;
                    uint *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(int *)psrc++;
                }
            }
        }

        unsafe public static void BlitUIntArrIntArr(int[] dst, int idst, uint[] src, int n)
        {
            fixed (int *fdst = &dst[0])
            {
                fixed (uint *pfsrc = &src[0])
                {
                    int *pdst = pfdst + idst;
                    uint *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(int *)psrc++;
                }
            }
        }

        unsafe public static void BlitUIntArrIntArr(int[] dst, uint[] src, int isrc, int n)
        {
            fixed (int *fdst = &dst[0])
            {
                fixed (uint *pfsrc = &src[0])
                {
                    int *pdst = pfdst;
                    uint *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(int *)psrc++;
                }
            }
        }

        unsafe public static void BlitUIntArrIntArr(int[] dst, int idst, uint[] src, int isrc, int n)
        {
            fixed (int *fdst = &dst[0])
            {
                fixed (uint *pfsrc = &src[0])
                {
                    int *pdst = pfdst + idst;
                    uint *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(int *)psrc++;
                }
            }
        }

        unsafe public static void BlitUIntArrUIntArr(uint[] dst, uint[] src, int n)
        {
            fixed (uint *fdst = &dst[0])
            {
                fixed (uint *pfsrc = &src[0])
                {
                    uint *pdst = pfdst;
                    uint *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitUIntArrUIntArr(uint[] dst, int idst, uint[] src, int n)
        {
            fixed (uint *fdst = &dst[0])
            {
                fixed (uint *pfsrc = &src[0])
                {
                    uint *pdst = pfdst + idst;
                    uint *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitUIntArrUIntArr(uint[] dst, uint[] src, int isrc, int n)
        {
            fixed (uint *fdst = &dst[0])
            {
                fixed (uint *pfsrc = &src[0])
                {
                    uint *pdst = pfdst;
                    uint *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitUIntArrUIntArr(uint[] dst, int idst, uint[] src, int isrc, int n)
        {
            fixed (uint *fdst = &dst[0])
            {
                fixed (uint *pfsrc = &src[0])
                {
                    uint *pdst = pfdst + idst;
                    uint *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitUIntArrFloatArr(float[] dst, uint[] src, int n)
        {
            fixed (float *fdst = &dst[0])
            {
                fixed (uint *pfsrc = &src[0])
                {
                    float *pdst = pfdst;
                    uint *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(float *)psrc++;
                }
            }
        }

        unsafe public static void BlitUIntArrFloatArr(float[] dst, int idst, uint[] src, int n)
        {
            fixed (float *fdst = &dst[0])
            {
                fixed (uint *pfsrc = &src[0])
                {
                    float *pdst = pfdst + idst;
                    uint *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(float *)psrc++;
                }
            }
        }

        unsafe public static void BlitUIntArrFloatArr(float[] dst, uint[] src, int isrc, int n)
        {
            fixed (float *fdst = &dst[0])
            {
                fixed (uint *pfsrc = &src[0])
                {
                    float *pdst = pfdst;
                    uint *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(float *)psrc++;
                }
            }
        }

        unsafe public static void BlitUIntArrFloatArr(float[] dst, int idst, uint[] src, int isrc, int n)
        {
            fixed (float *fdst = &dst[0])
            {
                fixed (uint *pfsrc = &src[0])
                {
                    float *pdst = pfdst + idst;
                    uint *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(float *)psrc++;
                }
            }
        }

        unsafe public static void BlitLongArrLongArr(long[] dst, long[] src, int n)
        {
            fixed (long *fdst = &dst[0])
            {
                fixed (long *pfsrc = &src[0])
                {
                    long *pdst = pfdst;
                    long *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitLongArrLongArr(long[] dst, int idst, long[] src, int n)
        {
            fixed (long *fdst = &dst[0])
            {
                fixed (long *pfsrc = &src[0])
                {
                    long *pdst = pfdst + idst;
                    long *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitLongArrLongArr(long[] dst, long[] src, int isrc, int n)
        {
            fixed (long *fdst = &dst[0])
            {
                fixed (long *pfsrc = &src[0])
                {
                    long *pdst = pfdst;
                    long *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitLongArrLongArr(long[] dst, int idst, long[] src, int isrc, int n)
        {
            fixed (long *fdst = &dst[0])
            {
                fixed (long *pfsrc = &src[0])
                {
                    long *pdst = pfdst + idst;
                    long *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitLongArrULongArr(ulong[] dst, long[] src, int n)
        {
            fixed (ulong *fdst = &dst[0])
            {
                fixed (long *pfsrc = &src[0])
                {
                    ulong *pdst = pfdst;
                    long *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(ulong *)psrc++;
                }
            }
        }

        unsafe public static void BlitLongArrULongArr(ulong[] dst, int idst, long[] src, int n)
        {
            fixed (ulong *fdst = &dst[0])
            {
                fixed (long *pfsrc = &src[0])
                {
                    ulong *pdst = pfdst + idst;
                    long *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(ulong *)psrc++;
                }
            }
        }

        unsafe public static void BlitLongArrULongArr(ulong[] dst, long[] src, int isrc, int n)
        {
            fixed (ulong *fdst = &dst[0])
            {
                fixed (long *pfsrc = &src[0])
                {
                    ulong *pdst = pfdst;
                    long *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(ulong *)psrc++;
                }
            }
        }

        unsafe public static void BlitLongArrULongArr(ulong[] dst, int idst, long[] src, int isrc, int n)
        {
            fixed (ulong *fdst = &dst[0])
            {
                fixed (long *pfsrc = &src[0])
                {
                    ulong *pdst = pfdst + idst;
                    long *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(ulong *)psrc++;
                }
            }
        }

        unsafe public static void BlitLongArrDoubleArr(double[] dst, long[] src, int n)
        {
            fixed (double *fdst = &dst[0])
            {
                fixed (long *pfsrc = &src[0])
                {
                    double *pdst = pfdst;
                    long *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(double *)psrc++;
                }
            }
        }

        unsafe public static void BlitLongArrDoubleArr(double[] dst, int idst, long[] src, int n)
        {
            fixed (double *fdst = &dst[0])
            {
                fixed (long *pfsrc = &src[0])
                {
                    double *pdst = pfdst + idst;
                    long *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(double *)psrc++;
                }
            }
        }

        unsafe public static void BlitLongArrDoubleArr(double[] dst, long[] src, int isrc, int n)
        {
            fixed (double *fdst = &dst[0])
            {
                fixed (long *pfsrc = &src[0])
                {
                    double *pdst = pfdst;
                    long *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(double *)psrc++;
                }
            }
        }

        unsafe public static void BlitLongArrDoubleArr(double[] dst, int idst, long[] src, int isrc, int n)
        {
            fixed (double *fdst = &dst[0])
            {
                fixed (long *pfsrc = &src[0])
                {
                    double *pdst = pfdst + idst;
                    long *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(double *)psrc++;
                }
            }
        }

        unsafe public static void BlitULongArrLongArr(long[] dst, ulong[] src, int n)
        {
            fixed (long *fdst = &dst[0])
            {
                fixed (ulong *pfsrc = &src[0])
                {
                    long *pdst = pfdst;
                    ulong *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(long *)psrc++;
                }
            }
        }

        unsafe public static void BlitULongArrLongArr(long[] dst, int idst, ulong[] src, int n)
        {
            fixed (long *fdst = &dst[0])
            {
                fixed (ulong *pfsrc = &src[0])
                {
                    long *pdst = pfdst + idst;
                    ulong *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(long *)psrc++;
                }
            }
        }

        unsafe public static void BlitULongArrLongArr(long[] dst, ulong[] src, int isrc, int n)
        {
            fixed (long *fdst = &dst[0])
            {
                fixed (ulong *pfsrc = &src[0])
                {
                    long *pdst = pfdst;
                    ulong *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(long *)psrc++;
                }
            }
        }

        unsafe public static void BlitULongArrLongArr(long[] dst, int idst, ulong[] src, int isrc, int n)
        {
            fixed (long *fdst = &dst[0])
            {
                fixed (ulong *pfsrc = &src[0])
                {
                    long *pdst = pfdst + idst;
                    ulong *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(long *)psrc++;
                }
            }
        }

        unsafe public static void BlitULongArrULongArr(ulong[] dst, ulong[] src, int n)
        {
            fixed (ulong *fdst = &dst[0])
            {
                fixed (ulong *pfsrc = &src[0])
                {
                    ulong *pdst = pfdst;
                    ulong *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitULongArrULongArr(ulong[] dst, int idst, ulong[] src, int n)
        {
            fixed (ulong *fdst = &dst[0])
            {
                fixed (ulong *pfsrc = &src[0])
                {
                    ulong *pdst = pfdst + idst;
                    ulong *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitULongArrULongArr(ulong[] dst, ulong[] src, int isrc, int n)
        {
            fixed (ulong *fdst = &dst[0])
            {
                fixed (ulong *pfsrc = &src[0])
                {
                    ulong *pdst = pfdst;
                    ulong *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitULongArrULongArr(ulong[] dst, int idst, ulong[] src, int isrc, int n)
        {
            fixed (ulong *fdst = &dst[0])
            {
                fixed (ulong *pfsrc = &src[0])
                {
                    ulong *pdst = pfdst + idst;
                    ulong *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitULongArrDoubleArr(double[] dst, ulong[] src, int n)
        {
            fixed (double *fdst = &dst[0])
            {
                fixed (ulong *pfsrc = &src[0])
                {
                    double *pdst = pfdst;
                    ulong *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(double *)psrc++;
                }
            }
        }

        unsafe public static void BlitULongArrDoubleArr(double[] dst, int idst, ulong[] src, int n)
        {
            fixed (double *fdst = &dst[0])
            {
                fixed (ulong *pfsrc = &src[0])
                {
                    double *pdst = pfdst + idst;
                    ulong *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(double *)psrc++;
                }
            }
        }

        unsafe public static void BlitULongArrDoubleArr(double[] dst, ulong[] src, int isrc, int n)
        {
            fixed (double *fdst = &dst[0])
            {
                fixed (ulong *pfsrc = &src[0])
                {
                    double *pdst = pfdst;
                    ulong *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(double *)psrc++;
                }
            }
        }

        unsafe public static void BlitULongArrDoubleArr(double[] dst, int idst, ulong[] src, int isrc, int n)
        {
            fixed (double *fdst = &dst[0])
            {
                fixed (ulong *pfsrc = &src[0])
                {
                    double *pdst = pfdst + idst;
                    ulong *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(double *)psrc++;
                }
            }
        }

        unsafe public static void BlitFloatArrBoolArr(bool[] dst, float[] src, int n)
        {
            fixed (bool *fdst = &dst[0])
            {
                fixed (float *pfsrc = &src[0])
                {
                    bool *pdst = pfdst;
                    float *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(bool *)psrc++;
                }
            }
        }

        unsafe public static void BlitFloatArrBoolArr(bool[] dst, int idst, float[] src, int n)
        {
            fixed (bool *fdst = &dst[0])
            {
                fixed (float *pfsrc = &src[0])
                {
                    bool *pdst = pfdst + idst;
                    float *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(bool *)psrc++;
                }
            }
        }

        unsafe public static void BlitFloatArrBoolArr(bool[] dst, float[] src, int isrc, int n)
        {
            fixed (bool *fdst = &dst[0])
            {
                fixed (float *pfsrc = &src[0])
                {
                    bool *pdst = pfdst;
                    float *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(bool *)psrc++;
                }
            }
        }

        unsafe public static void BlitFloatArrBoolArr(bool[] dst, int idst, float[] src, int isrc, int n)
        {
            fixed (bool *fdst = &dst[0])
            {
                fixed (float *pfsrc = &src[0])
                {
                    bool *pdst = pfdst + idst;
                    float *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(bool *)psrc++;
                }
            }
        }

        unsafe public static void BlitFloatArrIntArr(int[] dst, float[] src, int n)
        {
            fixed (int *fdst = &dst[0])
            {
                fixed (float *pfsrc = &src[0])
                {
                    int *pdst = pfdst;
                    float *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(int *)psrc++;
                }
            }
        }

        unsafe public static void BlitFloatArrIntArr(int[] dst, int idst, float[] src, int n)
        {
            fixed (int *fdst = &dst[0])
            {
                fixed (float *pfsrc = &src[0])
                {
                    int *pdst = pfdst + idst;
                    float *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(int *)psrc++;
                }
            }
        }

        unsafe public static void BlitFloatArrIntArr(int[] dst, float[] src, int isrc, int n)
        {
            fixed (int *fdst = &dst[0])
            {
                fixed (float *pfsrc = &src[0])
                {
                    int *pdst = pfdst;
                    float *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(int *)psrc++;
                }
            }
        }

        unsafe public static void BlitFloatArrIntArr(int[] dst, int idst, float[] src, int isrc, int n)
        {
            fixed (int *fdst = &dst[0])
            {
                fixed (float *pfsrc = &src[0])
                {
                    int *pdst = pfdst + idst;
                    float *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(int *)psrc++;
                }
            }
        }

        unsafe public static void BlitFloatArrUIntArr(uint[] dst, float[] src, int n)
        {
            fixed (uint *fdst = &dst[0])
            {
                fixed (float *pfsrc = &src[0])
                {
                    uint *pdst = pfdst;
                    float *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(uint *)psrc++;
                }
            }
        }

        unsafe public static void BlitFloatArrUIntArr(uint[] dst, int idst, float[] src, int n)
        {
            fixed (uint *fdst = &dst[0])
            {
                fixed (float *pfsrc = &src[0])
                {
                    uint *pdst = pfdst + idst;
                    float *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(uint *)psrc++;
                }
            }
        }

        unsafe public static void BlitFloatArrUIntArr(uint[] dst, float[] src, int isrc, int n)
        {
            fixed (uint *fdst = &dst[0])
            {
                fixed (float *pfsrc = &src[0])
                {
                    uint *pdst = pfdst;
                    float *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(uint *)psrc++;
                }
            }
        }

        unsafe public static void BlitFloatArrUIntArr(uint[] dst, int idst, float[] src, int isrc, int n)
        {
            fixed (uint *fdst = &dst[0])
            {
                fixed (float *pfsrc = &src[0])
                {
                    uint *pdst = pfdst + idst;
                    float *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(uint *)psrc++;
                }
            }
        }

        unsafe public static void BlitFloatArrFloatArr(float[] dst, float[] src, int n)
        {
            fixed (float *fdst = &dst[0])
            {
                fixed (float *pfsrc = &src[0])
                {
                    float *pdst = pfdst;
                    float *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitFloatArrFloatArr(float[] dst, int idst, float[] src, int n)
        {
            fixed (float *fdst = &dst[0])
            {
                fixed (float *pfsrc = &src[0])
                {
                    float *pdst = pfdst + idst;
                    float *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitFloatArrFloatArr(float[] dst, float[] src, int isrc, int n)
        {
            fixed (float *fdst = &dst[0])
            {
                fixed (float *pfsrc = &src[0])
                {
                    float *pdst = pfdst;
                    float *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitFloatArrFloatArr(float[] dst, int idst, float[] src, int isrc, int n)
        {
            fixed (float *fdst = &dst[0])
            {
                fixed (float *pfsrc = &src[0])
                {
                    float *pdst = pfdst + idst;
                    float *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitDoubleArrLongArr(long[] dst, double[] src, int n)
        {
            fixed (long *fdst = &dst[0])
            {
                fixed (double *pfsrc = &src[0])
                {
                    long *pdst = pfdst;
                    double *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(long *)psrc++;
                }
            }
        }

        unsafe public static void BlitDoubleArrLongArr(long[] dst, int idst, double[] src, int n)
        {
            fixed (long *fdst = &dst[0])
            {
                fixed (double *pfsrc = &src[0])
                {
                    long *pdst = pfdst + idst;
                    double *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(long *)psrc++;
                }
            }
        }

        unsafe public static void BlitDoubleArrLongArr(long[] dst, double[] src, int isrc, int n)
        {
            fixed (long *fdst = &dst[0])
            {
                fixed (double *pfsrc = &src[0])
                {
                    long *pdst = pfdst;
                    double *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(long *)psrc++;
                }
            }
        }

        unsafe public static void BlitDoubleArrLongArr(long[] dst, int idst, double[] src, int isrc, int n)
        {
            fixed (long *fdst = &dst[0])
            {
                fixed (double *pfsrc = &src[0])
                {
                    long *pdst = pfdst + idst;
                    double *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(long *)psrc++;
                }
            }
        }

        unsafe public static void BlitDoubleArrULongArr(ulong[] dst, double[] src, int n)
        {
            fixed (ulong *fdst = &dst[0])
            {
                fixed (double *pfsrc = &src[0])
                {
                    ulong *pdst = pfdst;
                    double *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(ulong *)psrc++;
                }
            }
        }

        unsafe public static void BlitDoubleArrULongArr(ulong[] dst, int idst, double[] src, int n)
        {
            fixed (ulong *fdst = &dst[0])
            {
                fixed (double *pfsrc = &src[0])
                {
                    ulong *pdst = pfdst + idst;
                    double *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *(ulong *)psrc++;
                }
            }
        }

        unsafe public static void BlitDoubleArrULongArr(ulong[] dst, double[] src, int isrc, int n)
        {
            fixed (ulong *fdst = &dst[0])
            {
                fixed (double *pfsrc = &src[0])
                {
                    ulong *pdst = pfdst;
                    double *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(ulong *)psrc++;
                }
            }
        }

        unsafe public static void BlitDoubleArrULongArr(ulong[] dst, int idst, double[] src, int isrc, int n)
        {
            fixed (ulong *fdst = &dst[0])
            {
                fixed (double *pfsrc = &src[0])
                {
                    ulong *pdst = pfdst + idst;
                    double *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *(ulong *)psrc++;
                }
            }
        }

        unsafe public static void BlitDoubleArrDoubleArr(double[] dst, double[] src, int n)
        {
            fixed (double *fdst = &dst[0])
            {
                fixed (double *pfsrc = &src[0])
                {
                    double *pdst = pfdst;
                    double *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitDoubleArrDoubleArr(double[] dst, int idst, double[] src, int n)
        {
            fixed (double *fdst = &dst[0])
            {
                fixed (double *pfsrc = &src[0])
                {
                    double *pdst = pfdst + idst;
                    double *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitDoubleArrDoubleArr(double[] dst, double[] src, int isrc, int n)
        {
            fixed (double *fdst = &dst[0])
            {
                fixed (double *pfsrc = &src[0])
                {
                    double *pdst = pfdst;
                    double *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        unsafe public static void BlitDoubleArrDoubleArr(double[] dst, int idst, double[] src, int isrc, int n)
        {
            fixed (double *fdst = &dst[0])
            {
                fixed (double *pfsrc = &src[0])
                {
                    double *pdst = pfdst + idst;
                    double *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *psrc++;
                }
            }
        }

        ///////////////////////////////////////
        // Address-Pointer Blitting Routines //
        ///////////////////////////////////////

        unsafe public static void BlitBoolAddrBoolPtr(bool *pdst, IntPtr srcaddr, int n)
        {
            bool *psrc = (bool *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitBoolPtrBoolAddr(IntPtr dstaddr, bool *psrc, int n)
        {
            bool *pdst = (bool *)dstaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitBoolAddrIntPtr(int *pdst, IntPtr srcaddr, int n)
        {
            bool *psrc = (bool *)srcaddr;
            while (n-- > 0) *pdst++ = *(int *)psrc++;
        }

        unsafe public static void BlitBoolPtrIntAddr(IntPtr dstaddr, bool *psrc, int n)
        {
            int *pdst = (int *)dstaddr;
            while (n-- > 0) *pdst++ = *(int *)psrc++;
        }

        unsafe public static void BlitBoolAddrUIntPtr(uint *pdst, IntPtr srcaddr, int n)
        {
            bool *psrc = (bool *)srcaddr;
            while (n-- > 0) *pdst++ = *(uint *)psrc++;
        }

        unsafe public static void BlitBoolPtrUIntAddr(IntPtr dstaddr, bool *psrc, int n)
        {
            uint *pdst = (uint *)dstaddr;
            while (n-- > 0) *pdst++ = *(uint *)psrc++;
        }

        unsafe public static void BlitBoolAddrFloatPtr(float *pdst, IntPtr srcaddr, int n)
        {
            bool *psrc = (bool *)srcaddr;
            while (n-- > 0) *pdst++ = *(float *)psrc++;
        }

        unsafe public static void BlitBoolPtrFloatAddr(IntPtr dstaddr, bool *psrc, int n)
        {
            float *pdst = (float *)dstaddr;
            while (n-- > 0) *pdst++ = *(float *)psrc++;
        }

        unsafe public static void BlitByteAddrBytePtr(byte *pdst, IntPtr srcaddr, int n)
        {
            byte *psrc = (byte *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitBytePtrByteAddr(IntPtr dstaddr, byte *psrc, int n)
        {
            byte *pdst = (byte *)dstaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitByteAddrSBytePtr(sbyte *pdst, IntPtr srcaddr, int n)
        {
            byte *psrc = (byte *)srcaddr;
            while (n-- > 0) *pdst++ = *(sbyte *)psrc++;
        }

        unsafe public static void BlitBytePtrSByteAddr(IntPtr dstaddr, byte *psrc, int n)
        {
            sbyte *pdst = (sbyte *)dstaddr;
            while (n-- > 0) *pdst++ = *(sbyte *)psrc++;
        }

        unsafe public static void BlitSByteAddrBytePtr(byte *pdst, IntPtr srcaddr, int n)
        {
            sbyte *psrc = (sbyte *)srcaddr;
            while (n-- > 0) *pdst++ = *(byte *)psrc++;
        }

        unsafe public static void BlitSBytePtrByteAddr(IntPtr dstaddr, sbyte *psrc, int n)
        {
            byte *pdst = (byte *)dstaddr;
            while (n-- > 0) *pdst++ = *(byte *)psrc++;
        }

        unsafe public static void BlitSByteAddrSBytePtr(sbyte *pdst, IntPtr srcaddr, int n)
        {
            sbyte *psrc = (sbyte *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitSBytePtrSByteAddr(IntPtr dstaddr, sbyte *psrc, int n)
        {
            sbyte *pdst = (sbyte *)dstaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitCharAddrCharPtr(char *pdst, IntPtr srcaddr, int n)
        {
            char *psrc = (char *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitCharPtrCharAddr(IntPtr dstaddr, char *psrc, int n)
        {
            char *pdst = (char *)dstaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitCharAddrShortPtr(short *pdst, IntPtr srcaddr, int n)
        {
            char *psrc = (char *)srcaddr;
            while (n-- > 0) *pdst++ = *(short *)psrc++;
        }

        unsafe public static void BlitCharPtrShortAddr(IntPtr dstaddr, char *psrc, int n)
        {
            short *pdst = (short *)dstaddr;
            while (n-- > 0) *pdst++ = *(short *)psrc++;
        }

        unsafe public static void BlitCharAddrUShortPtr(ushort *pdst, IntPtr srcaddr, int n)
        {
            char *psrc = (char *)srcaddr;
            while (n-- > 0) *pdst++ = *(ushort *)psrc++;
        }

        unsafe public static void BlitCharPtrUShortAddr(IntPtr dstaddr, char *psrc, int n)
        {
            ushort *pdst = (ushort *)dstaddr;
            while (n-- > 0) *pdst++ = *(ushort *)psrc++;
        }

        unsafe public static void BlitShortAddrCharPtr(char *pdst, IntPtr srcaddr, int n)
        {
            short *psrc = (short *)srcaddr;
            while (n-- > 0) *pdst++ = *(char *)psrc++;
        }

        unsafe public static void BlitShortPtrCharAddr(IntPtr dstaddr, short *psrc, int n)
        {
            char *pdst = (char *)dstaddr;
            while (n-- > 0) *pdst++ = *(char *)psrc++;
        }

        unsafe public static void BlitShortAddrShortPtr(short *pdst, IntPtr srcaddr, int n)
        {
            short *psrc = (short *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitShortPtrShortAddr(IntPtr dstaddr, short *psrc, int n)
        {
            short *pdst = (short *)dstaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitShortAddrUShortPtr(ushort *pdst, IntPtr srcaddr, int n)
        {
            short *psrc = (short *)srcaddr;
            while (n-- > 0) *pdst++ = *(ushort *)psrc++;
        }

        unsafe public static void BlitShortPtrUShortAddr(IntPtr dstaddr, short *psrc, int n)
        {
            ushort *pdst = (ushort *)dstaddr;
            while (n-- > 0) *pdst++ = *(ushort *)psrc++;
        }

        unsafe public static void BlitUShortAddrCharPtr(char *pdst, IntPtr srcaddr, int n)
        {
            ushort *psrc = (ushort *)srcaddr;
            while (n-- > 0) *pdst++ = *(char *)psrc++;
        }

        unsafe public static void BlitUShortPtrCharAddr(IntPtr dstaddr, ushort *psrc, int n)
        {
            char *pdst = (char *)dstaddr;
            while (n-- > 0) *pdst++ = *(char *)psrc++;
        }

        unsafe public static void BlitUShortAddrShortPtr(short *pdst, IntPtr srcaddr, int n)
        {
            ushort *psrc = (ushort *)srcaddr;
            while (n-- > 0) *pdst++ = *(short *)psrc++;
        }

        unsafe public static void BlitUShortPtrShortAddr(IntPtr dstaddr, ushort *psrc, int n)
        {
            short *pdst = (short *)dstaddr;
            while (n-- > 0) *pdst++ = *(short *)psrc++;
        }

        unsafe public static void BlitUShortAddrUShortPtr(ushort *pdst, IntPtr srcaddr, int n)
        {
            ushort *psrc = (ushort *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitUShortPtrUShortAddr(IntPtr dstaddr, ushort *psrc, int n)
        {
            ushort *pdst = (ushort *)dstaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitIntAddrBoolPtr(bool *pdst, IntPtr srcaddr, int n)
        {
            int *psrc = (int *)srcaddr;
            while (n-- > 0) *pdst++ = *(bool *)psrc++;
        }

        unsafe public static void BlitIntPtrBoolAddr(IntPtr dstaddr, int *psrc, int n)
        {
            bool *pdst = (bool *)dstaddr;
            while (n-- > 0) *pdst++ = *(bool *)psrc++;
        }

        unsafe public static void BlitIntAddrIntPtr(int *pdst, IntPtr srcaddr, int n)
        {
            int *psrc = (int *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitIntPtrIntAddr(IntPtr dstaddr, int *psrc, int n)
        {
            int *pdst = (int *)dstaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitIntAddrUIntPtr(uint *pdst, IntPtr srcaddr, int n)
        {
            int *psrc = (int *)srcaddr;
            while (n-- > 0) *pdst++ = *(uint *)psrc++;
        }

        unsafe public static void BlitIntPtrUIntAddr(IntPtr dstaddr, int *psrc, int n)
        {
            uint *pdst = (uint *)dstaddr;
            while (n-- > 0) *pdst++ = *(uint *)psrc++;
        }

        unsafe public static void BlitIntAddrFloatPtr(float *pdst, IntPtr srcaddr, int n)
        {
            int *psrc = (int *)srcaddr;
            while (n-- > 0) *pdst++ = *(float *)psrc++;
        }

        unsafe public static void BlitIntPtrFloatAddr(IntPtr dstaddr, int *psrc, int n)
        {
            float *pdst = (float *)dstaddr;
            while (n-- > 0) *pdst++ = *(float *)psrc++;
        }

        unsafe public static void BlitUIntAddrBoolPtr(bool *pdst, IntPtr srcaddr, int n)
        {
            uint *psrc = (uint *)srcaddr;
            while (n-- > 0) *pdst++ = *(bool *)psrc++;
        }

        unsafe public static void BlitUIntPtrBoolAddr(IntPtr dstaddr, uint *psrc, int n)
        {
            bool *pdst = (bool *)dstaddr;
            while (n-- > 0) *pdst++ = *(bool *)psrc++;
        }

        unsafe public static void BlitUIntAddrIntPtr(int *pdst, IntPtr srcaddr, int n)
        {
            uint *psrc = (uint *)srcaddr;
            while (n-- > 0) *pdst++ = *(int *)psrc++;
        }

        unsafe public static void BlitUIntPtrIntAddr(IntPtr dstaddr, uint *psrc, int n)
        {
            int *pdst = (int *)dstaddr;
            while (n-- > 0) *pdst++ = *(int *)psrc++;
        }

        unsafe public static void BlitUIntAddrUIntPtr(uint *pdst, IntPtr srcaddr, int n)
        {
            uint *psrc = (uint *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitUIntPtrUIntAddr(IntPtr dstaddr, uint *psrc, int n)
        {
            uint *pdst = (uint *)dstaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitUIntAddrFloatPtr(float *pdst, IntPtr srcaddr, int n)
        {
            uint *psrc = (uint *)srcaddr;
            while (n-- > 0) *pdst++ = *(float *)psrc++;
        }

        unsafe public static void BlitUIntPtrFloatAddr(IntPtr dstaddr, uint *psrc, int n)
        {
            float *pdst = (float *)dstaddr;
            while (n-- > 0) *pdst++ = *(float *)psrc++;
        }

        unsafe public static void BlitLongAddrLongPtr(long *pdst, IntPtr srcaddr, int n)
        {
            long *psrc = (long *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitLongPtrLongAddr(IntPtr dstaddr, long *psrc, int n)
        {
            long *pdst = (long *)dstaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitLongAddrULongPtr(ulong *pdst, IntPtr srcaddr, int n)
        {
            long *psrc = (long *)srcaddr;
            while (n-- > 0) *pdst++ = *(ulong *)psrc++;
        }

        unsafe public static void BlitLongPtrULongAddr(IntPtr dstaddr, long *psrc, int n)
        {
            ulong *pdst = (ulong *)dstaddr;
            while (n-- > 0) *pdst++ = *(ulong *)psrc++;
        }

        unsafe public static void BlitLongAddrDoublePtr(double *pdst, IntPtr srcaddr, int n)
        {
            long *psrc = (long *)srcaddr;
            while (n-- > 0) *pdst++ = *(double *)psrc++;
        }

        unsafe public static void BlitLongPtrDoubleAddr(IntPtr dstaddr, long *psrc, int n)
        {
            double *pdst = (double *)dstaddr;
            while (n-- > 0) *pdst++ = *(double *)psrc++;
        }

        unsafe public static void BlitULongAddrLongPtr(long *pdst, IntPtr srcaddr, int n)
        {
            ulong *psrc = (ulong *)srcaddr;
            while (n-- > 0) *pdst++ = *(long *)psrc++;
        }

        unsafe public static void BlitULongPtrLongAddr(IntPtr dstaddr, ulong *psrc, int n)
        {
            long *pdst = (long *)dstaddr;
            while (n-- > 0) *pdst++ = *(long *)psrc++;
        }

        unsafe public static void BlitULongAddrULongPtr(ulong *pdst, IntPtr srcaddr, int n)
        {
            ulong *psrc = (ulong *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitULongPtrULongAddr(IntPtr dstaddr, ulong *psrc, int n)
        {
            ulong *pdst = (ulong *)dstaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitULongAddrDoublePtr(double *pdst, IntPtr srcaddr, int n)
        {
            ulong *psrc = (ulong *)srcaddr;
            while (n-- > 0) *pdst++ = *(double *)psrc++;
        }

        unsafe public static void BlitULongPtrDoubleAddr(IntPtr dstaddr, ulong *psrc, int n)
        {
            double *pdst = (double *)dstaddr;
            while (n-- > 0) *pdst++ = *(double *)psrc++;
        }

        unsafe public static void BlitFloatAddrBoolPtr(bool *pdst, IntPtr srcaddr, int n)
        {
            float *psrc = (float *)srcaddr;
            while (n-- > 0) *pdst++ = *(bool *)psrc++;
        }

        unsafe public static void BlitFloatPtrBoolAddr(IntPtr dstaddr, float *psrc, int n)
        {
            bool *pdst = (bool *)dstaddr;
            while (n-- > 0) *pdst++ = *(bool *)psrc++;
        }

        unsafe public static void BlitFloatAddrIntPtr(int *pdst, IntPtr srcaddr, int n)
        {
            float *psrc = (float *)srcaddr;
            while (n-- > 0) *pdst++ = *(int *)psrc++;
        }

        unsafe public static void BlitFloatPtrIntAddr(IntPtr dstaddr, float *psrc, int n)
        {
            int *pdst = (int *)dstaddr;
            while (n-- > 0) *pdst++ = *(int *)psrc++;
        }

        unsafe public static void BlitFloatAddrUIntPtr(uint *pdst, IntPtr srcaddr, int n)
        {
            float *psrc = (float *)srcaddr;
            while (n-- > 0) *pdst++ = *(uint *)psrc++;
        }

        unsafe public static void BlitFloatPtrUIntAddr(IntPtr dstaddr, float *psrc, int n)
        {
            uint *pdst = (uint *)dstaddr;
            while (n-- > 0) *pdst++ = *(uint *)psrc++;
        }

        unsafe public static void BlitFloatAddrFloatPtr(float *pdst, IntPtr srcaddr, int n)
        {
            float *psrc = (float *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitFloatPtrFloatAddr(IntPtr dstaddr, float *psrc, int n)
        {
            float *pdst = (float *)dstaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitDoubleAddrLongPtr(long *pdst, IntPtr srcaddr, int n)
        {
            double *psrc = (double *)srcaddr;
            while (n-- > 0) *pdst++ = *(long *)psrc++;
        }

        unsafe public static void BlitDoublePtrLongAddr(IntPtr dstaddr, double *psrc, int n)
        {
            long *pdst = (long *)dstaddr;
            while (n-- > 0) *pdst++ = *(long *)psrc++;
        }

        unsafe public static void BlitDoubleAddrULongPtr(ulong *pdst, IntPtr srcaddr, int n)
        {
            double *psrc = (double *)srcaddr;
            while (n-- > 0) *pdst++ = *(ulong *)psrc++;
        }

        unsafe public static void BlitDoublePtrULongAddr(IntPtr dstaddr, double *psrc, int n)
        {
            ulong *pdst = (ulong *)dstaddr;
            while (n-- > 0) *pdst++ = *(ulong *)psrc++;
        }

        unsafe public static void BlitDoubleAddrDoublePtr(double *pdst, IntPtr srcaddr, int n)
        {
            double *psrc = (double *)srcaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        unsafe public static void BlitDoublePtrDoubleAddr(IntPtr dstaddr, double *psrc, int n)
        {
            double *pdst = (double *)dstaddr;
            while (n-- > 0) *pdst++ = *psrc++;
        }

        /////////////////////////////////////
        // Array-Pointer Blitting Routines //
        /////////////////////////////////////

        unsafe public static void BlitBoolArrBoolPtr(bool *pdst, bool[] src, int n)
        {
            fixed (bool *pfsrc = &src[0])
            {
                bool *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitBoolArrBoolPtr(bool *pdst, bool[] src, int isrc, int n)
        {
            fixed (bool *pfsrc = &src[0])
            {
                bool *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitBoolPtrBoolArr(bool[] dst, bool *psrc, int n)
        {
            fixed (bool *pfdst = &dst[0])
            {
                bool *pdst = pfdst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitBoolPtrBoolArr(bool[] dst, int idst, bool *psrc, int n)
        {
            fixed (bool *pfdst = &dst[0])
            {
                bool *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitBoolArrIntPtr(int *pdst, bool[] src, int n)
        {
            fixed (bool *pfsrc = &src[0])
            {
                bool *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitBoolArrIntPtr(int *pdst, bool[] src, int isrc, int n)
        {
            fixed (bool *pfsrc = &src[0])
            {
                bool *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitBoolPtrIntArr(int[] dst, bool *psrc, int n)
        {
            fixed (int *pfdst = &dst[0])
            {
                int *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitBoolPtrIntArr(int[] dst, int idst, bool *psrc, int n)
        {
            fixed (int *pfdst = &dst[0])
            {
                int *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitBoolArrUIntPtr(uint *pdst, bool[] src, int n)
        {
            fixed (bool *pfsrc = &src[0])
            {
                bool *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitBoolArrUIntPtr(uint *pdst, bool[] src, int isrc, int n)
        {
            fixed (bool *pfsrc = &src[0])
            {
                bool *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitBoolPtrUIntArr(uint[] dst, bool *psrc, int n)
        {
            fixed (uint *pfdst = &dst[0])
            {
                uint *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitBoolPtrUIntArr(uint[] dst, int idst, bool *psrc, int n)
        {
            fixed (uint *pfdst = &dst[0])
            {
                uint *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitBoolArrFloatPtr(float *pdst, bool[] src, int n)
        {
            fixed (bool *pfsrc = &src[0])
            {
                bool *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitBoolArrFloatPtr(float *pdst, bool[] src, int isrc, int n)
        {
            fixed (bool *pfsrc = &src[0])
            {
                bool *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitBoolPtrFloatArr(float[] dst, bool *psrc, int n)
        {
            fixed (float *pfdst = &dst[0])
            {
                float *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitBoolPtrFloatArr(float[] dst, int idst, bool *psrc, int n)
        {
            fixed (float *pfdst = &dst[0])
            {
                float *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitByteArrBytePtr(byte *pdst, byte[] src, int n)
        {
            fixed (byte *pfsrc = &src[0])
            {
                byte *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitByteArrBytePtr(byte *pdst, byte[] src, int isrc, int n)
        {
            fixed (byte *pfsrc = &src[0])
            {
                byte *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitBytePtrByteArr(byte[] dst, byte *psrc, int n)
        {
            fixed (byte *pfdst = &dst[0])
            {
                byte *pdst = pfdst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitBytePtrByteArr(byte[] dst, int idst, byte *psrc, int n)
        {
            fixed (byte *pfdst = &dst[0])
            {
                byte *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitByteArrSBytePtr(sbyte *pdst, byte[] src, int n)
        {
            fixed (byte *pfsrc = &src[0])
            {
                byte *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(sbyte *)psrc++;
            }
        }

        unsafe public static void BlitByteArrSBytePtr(sbyte *pdst, byte[] src, int isrc, int n)
        {
            fixed (byte *pfsrc = &src[0])
            {
                byte *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(sbyte *)psrc++;
            }
        }

        unsafe public static void BlitBytePtrSByteArr(sbyte[] dst, byte *psrc, int n)
        {
            fixed (sbyte *pfdst = &dst[0])
            {
                sbyte *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(sbyte *)psrc++;
            }
        }

        unsafe public static void BlitBytePtrSByteArr(sbyte[] dst, int idst, byte *psrc, int n)
        {
            fixed (sbyte *pfdst = &dst[0])
            {
                sbyte *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(sbyte *)psrc++;
            }
        }

        unsafe public static void BlitSByteArrBytePtr(byte *pdst, sbyte[] src, int n)
        {
            fixed (sbyte *pfsrc = &src[0])
            {
                sbyte *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(byte *)psrc++;
            }
        }

        unsafe public static void BlitSByteArrBytePtr(byte *pdst, sbyte[] src, int isrc, int n)
        {
            fixed (sbyte *pfsrc = &src[0])
            {
                sbyte *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(byte *)psrc++;
            }
        }

        unsafe public static void BlitSBytePtrByteArr(byte[] dst, sbyte *psrc, int n)
        {
            fixed (byte *pfdst = &dst[0])
            {
                byte *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(byte *)psrc++;
            }
        }

        unsafe public static void BlitSBytePtrByteArr(byte[] dst, int idst, sbyte *psrc, int n)
        {
            fixed (byte *pfdst = &dst[0])
            {
                byte *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(byte *)psrc++;
            }
        }

        unsafe public static void BlitSByteArrSBytePtr(sbyte *pdst, sbyte[] src, int n)
        {
            fixed (sbyte *pfsrc = &src[0])
            {
                sbyte *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitSByteArrSBytePtr(sbyte *pdst, sbyte[] src, int isrc, int n)
        {
            fixed (sbyte *pfsrc = &src[0])
            {
                sbyte *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitSBytePtrSByteArr(sbyte[] dst, sbyte *psrc, int n)
        {
            fixed (sbyte *pfdst = &dst[0])
            {
                sbyte *pdst = pfdst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitSBytePtrSByteArr(sbyte[] dst, int idst, sbyte *psrc, int n)
        {
            fixed (sbyte *pfdst = &dst[0])
            {
                sbyte *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitCharArrCharPtr(char *pdst, char[] src, int n)
        {
            fixed (char *pfsrc = &src[0])
            {
                char *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitCharArrCharPtr(char *pdst, char[] src, int isrc, int n)
        {
            fixed (char *pfsrc = &src[0])
            {
                char *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitCharPtrCharArr(char[] dst, char *psrc, int n)
        {
            fixed (char *pfdst = &dst[0])
            {
                char *pdst = pfdst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitCharPtrCharArr(char[] dst, int idst, char *psrc, int n)
        {
            fixed (char *pfdst = &dst[0])
            {
                char *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitCharArrShortPtr(short *pdst, char[] src, int n)
        {
            fixed (char *pfsrc = &src[0])
            {
                char *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(short *)psrc++;
            }
        }

        unsafe public static void BlitCharArrShortPtr(short *pdst, char[] src, int isrc, int n)
        {
            fixed (char *pfsrc = &src[0])
            {
                char *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(short *)psrc++;
            }
        }

        unsafe public static void BlitCharPtrShortArr(short[] dst, char *psrc, int n)
        {
            fixed (short *pfdst = &dst[0])
            {
                short *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(short *)psrc++;
            }
        }

        unsafe public static void BlitCharPtrShortArr(short[] dst, int idst, char *psrc, int n)
        {
            fixed (short *pfdst = &dst[0])
            {
                short *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(short *)psrc++;
            }
        }

        unsafe public static void BlitCharArrUShortPtr(ushort *pdst, char[] src, int n)
        {
            fixed (char *pfsrc = &src[0])
            {
                char *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(ushort *)psrc++;
            }
        }

        unsafe public static void BlitCharArrUShortPtr(ushort *pdst, char[] src, int isrc, int n)
        {
            fixed (char *pfsrc = &src[0])
            {
                char *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(ushort *)psrc++;
            }
        }

        unsafe public static void BlitCharPtrUShortArr(ushort[] dst, char *psrc, int n)
        {
            fixed (ushort *pfdst = &dst[0])
            {
                ushort *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(ushort *)psrc++;
            }
        }

        unsafe public static void BlitCharPtrUShortArr(ushort[] dst, int idst, char *psrc, int n)
        {
            fixed (ushort *pfdst = &dst[0])
            {
                ushort *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(ushort *)psrc++;
            }
        }

        unsafe public static void BlitShortArrCharPtr(char *pdst, short[] src, int n)
        {
            fixed (short *pfsrc = &src[0])
            {
                short *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(char *)psrc++;
            }
        }

        unsafe public static void BlitShortArrCharPtr(char *pdst, short[] src, int isrc, int n)
        {
            fixed (short *pfsrc = &src[0])
            {
                short *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(char *)psrc++;
            }
        }

        unsafe public static void BlitShortPtrCharArr(char[] dst, short *psrc, int n)
        {
            fixed (char *pfdst = &dst[0])
            {
                char *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(char *)psrc++;
            }
        }

        unsafe public static void BlitShortPtrCharArr(char[] dst, int idst, short *psrc, int n)
        {
            fixed (char *pfdst = &dst[0])
            {
                char *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(char *)psrc++;
            }
        }

        unsafe public static void BlitShortArrShortPtr(short *pdst, short[] src, int n)
        {
            fixed (short *pfsrc = &src[0])
            {
                short *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitShortArrShortPtr(short *pdst, short[] src, int isrc, int n)
        {
            fixed (short *pfsrc = &src[0])
            {
                short *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitShortPtrShortArr(short[] dst, short *psrc, int n)
        {
            fixed (short *pfdst = &dst[0])
            {
                short *pdst = pfdst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitShortPtrShortArr(short[] dst, int idst, short *psrc, int n)
        {
            fixed (short *pfdst = &dst[0])
            {
                short *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitShortArrUShortPtr(ushort *pdst, short[] src, int n)
        {
            fixed (short *pfsrc = &src[0])
            {
                short *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(ushort *)psrc++;
            }
        }

        unsafe public static void BlitShortArrUShortPtr(ushort *pdst, short[] src, int isrc, int n)
        {
            fixed (short *pfsrc = &src[0])
            {
                short *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(ushort *)psrc++;
            }
        }

        unsafe public static void BlitShortPtrUShortArr(ushort[] dst, short *psrc, int n)
        {
            fixed (ushort *pfdst = &dst[0])
            {
                ushort *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(ushort *)psrc++;
            }
        }

        unsafe public static void BlitShortPtrUShortArr(ushort[] dst, int idst, short *psrc, int n)
        {
            fixed (ushort *pfdst = &dst[0])
            {
                ushort *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(ushort *)psrc++;
            }
        }

        unsafe public static void BlitUShortArrCharPtr(char *pdst, ushort[] src, int n)
        {
            fixed (ushort *pfsrc = &src[0])
            {
                ushort *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(char *)psrc++;
            }
        }

        unsafe public static void BlitUShortArrCharPtr(char *pdst, ushort[] src, int isrc, int n)
        {
            fixed (ushort *pfsrc = &src[0])
            {
                ushort *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(char *)psrc++;
            }
        }

        unsafe public static void BlitUShortPtrCharArr(char[] dst, ushort *psrc, int n)
        {
            fixed (char *pfdst = &dst[0])
            {
                char *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(char *)psrc++;
            }
        }

        unsafe public static void BlitUShortPtrCharArr(char[] dst, int idst, ushort *psrc, int n)
        {
            fixed (char *pfdst = &dst[0])
            {
                char *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(char *)psrc++;
            }
        }

        unsafe public static void BlitUShortArrShortPtr(short *pdst, ushort[] src, int n)
        {
            fixed (ushort *pfsrc = &src[0])
            {
                ushort *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(short *)psrc++;
            }
        }

        unsafe public static void BlitUShortArrShortPtr(short *pdst, ushort[] src, int isrc, int n)
        {
            fixed (ushort *pfsrc = &src[0])
            {
                ushort *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(short *)psrc++;
            }
        }

        unsafe public static void BlitUShortPtrShortArr(short[] dst, ushort *psrc, int n)
        {
            fixed (short *pfdst = &dst[0])
            {
                short *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(short *)psrc++;
            }
        }

        unsafe public static void BlitUShortPtrShortArr(short[] dst, int idst, ushort *psrc, int n)
        {
            fixed (short *pfdst = &dst[0])
            {
                short *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(short *)psrc++;
            }
        }

        unsafe public static void BlitUShortArrUShortPtr(ushort *pdst, ushort[] src, int n)
        {
            fixed (ushort *pfsrc = &src[0])
            {
                ushort *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitUShortArrUShortPtr(ushort *pdst, ushort[] src, int isrc, int n)
        {
            fixed (ushort *pfsrc = &src[0])
            {
                ushort *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitUShortPtrUShortArr(ushort[] dst, ushort *psrc, int n)
        {
            fixed (ushort *pfdst = &dst[0])
            {
                ushort *pdst = pfdst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitUShortPtrUShortArr(ushort[] dst, int idst, ushort *psrc, int n)
        {
            fixed (ushort *pfdst = &dst[0])
            {
                ushort *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitIntArrBoolPtr(bool *pdst, int[] src, int n)
        {
            fixed (int *pfsrc = &src[0])
            {
                int *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitIntArrBoolPtr(bool *pdst, int[] src, int isrc, int n)
        {
            fixed (int *pfsrc = &src[0])
            {
                int *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitIntPtrBoolArr(bool[] dst, int *psrc, int n)
        {
            fixed (bool *pfdst = &dst[0])
            {
                bool *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitIntPtrBoolArr(bool[] dst, int idst, int *psrc, int n)
        {
            fixed (bool *pfdst = &dst[0])
            {
                bool *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitIntArrIntPtr(int *pdst, int[] src, int n)
        {
            fixed (int *pfsrc = &src[0])
            {
                int *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitIntArrIntPtr(int *pdst, int[] src, int isrc, int n)
        {
            fixed (int *pfsrc = &src[0])
            {
                int *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitIntPtrIntArr(int[] dst, int *psrc, int n)
        {
            fixed (int *pfdst = &dst[0])
            {
                int *pdst = pfdst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitIntPtrIntArr(int[] dst, int idst, int *psrc, int n)
        {
            fixed (int *pfdst = &dst[0])
            {
                int *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitIntArrUIntPtr(uint *pdst, int[] src, int n)
        {
            fixed (int *pfsrc = &src[0])
            {
                int *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitIntArrUIntPtr(uint *pdst, int[] src, int isrc, int n)
        {
            fixed (int *pfsrc = &src[0])
            {
                int *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitIntPtrUIntArr(uint[] dst, int *psrc, int n)
        {
            fixed (uint *pfdst = &dst[0])
            {
                uint *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitIntPtrUIntArr(uint[] dst, int idst, int *psrc, int n)
        {
            fixed (uint *pfdst = &dst[0])
            {
                uint *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitIntArrFloatPtr(float *pdst, int[] src, int n)
        {
            fixed (int *pfsrc = &src[0])
            {
                int *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitIntArrFloatPtr(float *pdst, int[] src, int isrc, int n)
        {
            fixed (int *pfsrc = &src[0])
            {
                int *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitIntPtrFloatArr(float[] dst, int *psrc, int n)
        {
            fixed (float *pfdst = &dst[0])
            {
                float *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitIntPtrFloatArr(float[] dst, int idst, int *psrc, int n)
        {
            fixed (float *pfdst = &dst[0])
            {
                float *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitUIntArrBoolPtr(bool *pdst, uint[] src, int n)
        {
            fixed (uint *pfsrc = &src[0])
            {
                uint *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitUIntArrBoolPtr(bool *pdst, uint[] src, int isrc, int n)
        {
            fixed (uint *pfsrc = &src[0])
            {
                uint *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitUIntPtrBoolArr(bool[] dst, uint *psrc, int n)
        {
            fixed (bool *pfdst = &dst[0])
            {
                bool *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitUIntPtrBoolArr(bool[] dst, int idst, uint *psrc, int n)
        {
            fixed (bool *pfdst = &dst[0])
            {
                bool *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitUIntArrIntPtr(int *pdst, uint[] src, int n)
        {
            fixed (uint *pfsrc = &src[0])
            {
                uint *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitUIntArrIntPtr(int *pdst, uint[] src, int isrc, int n)
        {
            fixed (uint *pfsrc = &src[0])
            {
                uint *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitUIntPtrIntArr(int[] dst, uint *psrc, int n)
        {
            fixed (int *pfdst = &dst[0])
            {
                int *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitUIntPtrIntArr(int[] dst, int idst, uint *psrc, int n)
        {
            fixed (int *pfdst = &dst[0])
            {
                int *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitUIntArrUIntPtr(uint *pdst, uint[] src, int n)
        {
            fixed (uint *pfsrc = &src[0])
            {
                uint *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitUIntArrUIntPtr(uint *pdst, uint[] src, int isrc, int n)
        {
            fixed (uint *pfsrc = &src[0])
            {
                uint *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitUIntPtrUIntArr(uint[] dst, uint *psrc, int n)
        {
            fixed (uint *pfdst = &dst[0])
            {
                uint *pdst = pfdst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitUIntPtrUIntArr(uint[] dst, int idst, uint *psrc, int n)
        {
            fixed (uint *pfdst = &dst[0])
            {
                uint *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitUIntArrFloatPtr(float *pdst, uint[] src, int n)
        {
            fixed (uint *pfsrc = &src[0])
            {
                uint *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitUIntArrFloatPtr(float *pdst, uint[] src, int isrc, int n)
        {
            fixed (uint *pfsrc = &src[0])
            {
                uint *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitUIntPtrFloatArr(float[] dst, uint *psrc, int n)
        {
            fixed (float *pfdst = &dst[0])
            {
                float *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitUIntPtrFloatArr(float[] dst, int idst, uint *psrc, int n)
        {
            fixed (float *pfdst = &dst[0])
            {
                float *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitLongArrLongPtr(long *pdst, long[] src, int n)
        {
            fixed (long *pfsrc = &src[0])
            {
                long *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitLongArrLongPtr(long *pdst, long[] src, int isrc, int n)
        {
            fixed (long *pfsrc = &src[0])
            {
                long *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitLongPtrLongArr(long[] dst, long *psrc, int n)
        {
            fixed (long *pfdst = &dst[0])
            {
                long *pdst = pfdst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitLongPtrLongArr(long[] dst, int idst, long *psrc, int n)
        {
            fixed (long *pfdst = &dst[0])
            {
                long *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitLongArrULongPtr(ulong *pdst, long[] src, int n)
        {
            fixed (long *pfsrc = &src[0])
            {
                long *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(ulong *)psrc++;
            }
        }

        unsafe public static void BlitLongArrULongPtr(ulong *pdst, long[] src, int isrc, int n)
        {
            fixed (long *pfsrc = &src[0])
            {
                long *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(ulong *)psrc++;
            }
        }

        unsafe public static void BlitLongPtrULongArr(ulong[] dst, long *psrc, int n)
        {
            fixed (ulong *pfdst = &dst[0])
            {
                ulong *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(ulong *)psrc++;
            }
        }

        unsafe public static void BlitLongPtrULongArr(ulong[] dst, int idst, long *psrc, int n)
        {
            fixed (ulong *pfdst = &dst[0])
            {
                ulong *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(ulong *)psrc++;
            }
        }

        unsafe public static void BlitLongArrDoublePtr(double *pdst, long[] src, int n)
        {
            fixed (long *pfsrc = &src[0])
            {
                long *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(double *)psrc++;
            }
        }

        unsafe public static void BlitLongArrDoublePtr(double *pdst, long[] src, int isrc, int n)
        {
            fixed (long *pfsrc = &src[0])
            {
                long *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(double *)psrc++;
            }
        }

        unsafe public static void BlitLongPtrDoubleArr(double[] dst, long *psrc, int n)
        {
            fixed (double *pfdst = &dst[0])
            {
                double *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(double *)psrc++;
            }
        }

        unsafe public static void BlitLongPtrDoubleArr(double[] dst, int idst, long *psrc, int n)
        {
            fixed (double *pfdst = &dst[0])
            {
                double *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(double *)psrc++;
            }
        }

        unsafe public static void BlitULongArrLongPtr(long *pdst, ulong[] src, int n)
        {
            fixed (ulong *pfsrc = &src[0])
            {
                ulong *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(long *)psrc++;
            }
        }

        unsafe public static void BlitULongArrLongPtr(long *pdst, ulong[] src, int isrc, int n)
        {
            fixed (ulong *pfsrc = &src[0])
            {
                ulong *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(long *)psrc++;
            }
        }

        unsafe public static void BlitULongPtrLongArr(long[] dst, ulong *psrc, int n)
        {
            fixed (long *pfdst = &dst[0])
            {
                long *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(long *)psrc++;
            }
        }

        unsafe public static void BlitULongPtrLongArr(long[] dst, int idst, ulong *psrc, int n)
        {
            fixed (long *pfdst = &dst[0])
            {
                long *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(long *)psrc++;
            }
        }

        unsafe public static void BlitULongArrULongPtr(ulong *pdst, ulong[] src, int n)
        {
            fixed (ulong *pfsrc = &src[0])
            {
                ulong *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitULongArrULongPtr(ulong *pdst, ulong[] src, int isrc, int n)
        {
            fixed (ulong *pfsrc = &src[0])
            {
                ulong *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitULongPtrULongArr(ulong[] dst, ulong *psrc, int n)
        {
            fixed (ulong *pfdst = &dst[0])
            {
                ulong *pdst = pfdst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitULongPtrULongArr(ulong[] dst, int idst, ulong *psrc, int n)
        {
            fixed (ulong *pfdst = &dst[0])
            {
                ulong *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitULongArrDoublePtr(double *pdst, ulong[] src, int n)
        {
            fixed (ulong *pfsrc = &src[0])
            {
                ulong *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(double *)psrc++;
            }
        }

        unsafe public static void BlitULongArrDoublePtr(double *pdst, ulong[] src, int isrc, int n)
        {
            fixed (ulong *pfsrc = &src[0])
            {
                ulong *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(double *)psrc++;
            }
        }

        unsafe public static void BlitULongPtrDoubleArr(double[] dst, ulong *psrc, int n)
        {
            fixed (double *pfdst = &dst[0])
            {
                double *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(double *)psrc++;
            }
        }

        unsafe public static void BlitULongPtrDoubleArr(double[] dst, int idst, ulong *psrc, int n)
        {
            fixed (double *pfdst = &dst[0])
            {
                double *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(double *)psrc++;
            }
        }

        unsafe public static void BlitFloatArrBoolPtr(bool *pdst, float[] src, int n)
        {
            fixed (float *pfsrc = &src[0])
            {
                float *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitFloatArrBoolPtr(bool *pdst, float[] src, int isrc, int n)
        {
            fixed (float *pfsrc = &src[0])
            {
                float *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitFloatPtrBoolArr(bool[] dst, float *psrc, int n)
        {
            fixed (bool *pfdst = &dst[0])
            {
                bool *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitFloatPtrBoolArr(bool[] dst, int idst, float *psrc, int n)
        {
            fixed (bool *pfdst = &dst[0])
            {
                bool *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitFloatArrIntPtr(int *pdst, float[] src, int n)
        {
            fixed (float *pfsrc = &src[0])
            {
                float *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitFloatArrIntPtr(int *pdst, float[] src, int isrc, int n)
        {
            fixed (float *pfsrc = &src[0])
            {
                float *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitFloatPtrIntArr(int[] dst, float *psrc, int n)
        {
            fixed (int *pfdst = &dst[0])
            {
                int *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitFloatPtrIntArr(int[] dst, int idst, float *psrc, int n)
        {
            fixed (int *pfdst = &dst[0])
            {
                int *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitFloatArrUIntPtr(uint *pdst, float[] src, int n)
        {
            fixed (float *pfsrc = &src[0])
            {
                float *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitFloatArrUIntPtr(uint *pdst, float[] src, int isrc, int n)
        {
            fixed (float *pfsrc = &src[0])
            {
                float *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitFloatPtrUIntArr(uint[] dst, float *psrc, int n)
        {
            fixed (uint *pfdst = &dst[0])
            {
                uint *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitFloatPtrUIntArr(uint[] dst, int idst, float *psrc, int n)
        {
            fixed (uint *pfdst = &dst[0])
            {
                uint *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitFloatArrFloatPtr(float *pdst, float[] src, int n)
        {
            fixed (float *pfsrc = &src[0])
            {
                float *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitFloatArrFloatPtr(float *pdst, float[] src, int isrc, int n)
        {
            fixed (float *pfsrc = &src[0])
            {
                float *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitFloatPtrFloatArr(float[] dst, float *psrc, int n)
        {
            fixed (float *pfdst = &dst[0])
            {
                float *pdst = pfdst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitFloatPtrFloatArr(float[] dst, int idst, float *psrc, int n)
        {
            fixed (float *pfdst = &dst[0])
            {
                float *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitDoubleArrLongPtr(long *pdst, double[] src, int n)
        {
            fixed (double *pfsrc = &src[0])
            {
                double *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(long *)psrc++;
            }
        }

        unsafe public static void BlitDoubleArrLongPtr(long *pdst, double[] src, int isrc, int n)
        {
            fixed (double *pfsrc = &src[0])
            {
                double *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(long *)psrc++;
            }
        }

        unsafe public static void BlitDoublePtrLongArr(long[] dst, double *psrc, int n)
        {
            fixed (long *pfdst = &dst[0])
            {
                long *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(long *)psrc++;
            }
        }

        unsafe public static void BlitDoublePtrLongArr(long[] dst, int idst, double *psrc, int n)
        {
            fixed (long *pfdst = &dst[0])
            {
                long *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(long *)psrc++;
            }
        }

        unsafe public static void BlitDoubleArrULongPtr(ulong *pdst, double[] src, int n)
        {
            fixed (double *pfsrc = &src[0])
            {
                double *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(ulong *)psrc++;
            }
        }

        unsafe public static void BlitDoubleArrULongPtr(ulong *pdst, double[] src, int isrc, int n)
        {
            fixed (double *pfsrc = &src[0])
            {
                double *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(ulong *)psrc++;
            }
        }

        unsafe public static void BlitDoublePtrULongArr(ulong[] dst, double *psrc, int n)
        {
            fixed (ulong *pfdst = &dst[0])
            {
                ulong *pdst = pfdst;
                while (n-- > 0) *pdst++ = *(ulong *)psrc++;
            }
        }

        unsafe public static void BlitDoublePtrULongArr(ulong[] dst, int idst, double *psrc, int n)
        {
            fixed (ulong *pfdst = &dst[0])
            {
                ulong *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *(ulong *)psrc++;
            }
        }

        unsafe public static void BlitDoubleArrDoublePtr(double *pdst, double[] src, int n)
        {
            fixed (double *pfsrc = &src[0])
            {
                double *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitDoubleArrDoublePtr(double *pdst, double[] src, int isrc, int n)
        {
            fixed (double *pfsrc = &src[0])
            {
                double *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitDoublePtrDoubleArr(double[] dst, double *psrc, int n)
        {
            fixed (double *pfdst = &dst[0])
            {
                double *pdst = pfdst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitDoublePtrDoubleArr(double[] dst, int idst, double *psrc, int n)
        {
            fixed (double *pfdst = &dst[0])
            {
                double *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        /////////////////////////////////////
        // Array-Address Blitting Routines //
        /////////////////////////////////////

        unsafe public static void BlitBoolArrBoolPtr(IntPtr dstaddr, bool[] src, int n)
        {
            fixed (bool *pfsrc = &src[0])
            {
                bool *pdst = (bool *)dstaddr;
                bool *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitBoolArrBoolPtr(IntPtr dstaddr, bool[] src, int isrc, int n)
        {
            fixed (bool *pfsrc = &src[0])
            {
                bool *pdst = (bool *)dstaddr;
                bool *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitBoolPtrBoolArr(bool[] dst, IntPtr srcaddr, int n)
        {
            fixed (bool *pfdst = &dst[0])
            {
                bool *pdst = pfdst;
                bool *psrc = (bool *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitBoolPtrBoolArr(bool[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (bool *pfdst = &dst[0])
            {
                bool *pdst = pfdst + idst;
                bool *psrc = (bool *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitBoolArrIntPtr(IntPtr dstaddr, bool[] src, int n)
        {
            fixed (bool *pfsrc = &src[0])
            {
                int *pdst = (int *)dstaddr;
                bool *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitBoolArrIntPtr(IntPtr dstaddr, bool[] src, int isrc, int n)
        {
            fixed (bool *pfsrc = &src[0])
            {
                int *pdst = (int *)dstaddr;
                bool *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitBoolPtrIntArr(int[] dst, IntPtr srcaddr, int n)
        {
            fixed (int *pfdst = &dst[0])
            {
                int *pdst = pfdst;
                bool *psrc = (bool *)srcaddr;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitBoolPtrIntArr(int[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (int *pfdst = &dst[0])
            {
                int *pdst = pfdst + idst;
                bool *psrc = (bool *)srcaddr;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitBoolArrUIntPtr(IntPtr dstaddr, bool[] src, int n)
        {
            fixed (bool *pfsrc = &src[0])
            {
                uint *pdst = (uint *)dstaddr;
                bool *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitBoolArrUIntPtr(IntPtr dstaddr, bool[] src, int isrc, int n)
        {
            fixed (bool *pfsrc = &src[0])
            {
                uint *pdst = (uint *)dstaddr;
                bool *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitBoolPtrUIntArr(uint[] dst, IntPtr srcaddr, int n)
        {
            fixed (uint *pfdst = &dst[0])
            {
                uint *pdst = pfdst;
                bool *psrc = (bool *)srcaddr;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitBoolPtrUIntArr(uint[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (uint *pfdst = &dst[0])
            {
                uint *pdst = pfdst + idst;
                bool *psrc = (bool *)srcaddr;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitBoolArrFloatPtr(IntPtr dstaddr, bool[] src, int n)
        {
            fixed (bool *pfsrc = &src[0])
            {
                float *pdst = (float *)dstaddr;
                bool *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitBoolArrFloatPtr(IntPtr dstaddr, bool[] src, int isrc, int n)
        {
            fixed (bool *pfsrc = &src[0])
            {
                float *pdst = (float *)dstaddr;
                bool *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitBoolPtrFloatArr(float[] dst, IntPtr srcaddr, int n)
        {
            fixed (float *pfdst = &dst[0])
            {
                float *pdst = pfdst;
                bool *psrc = (bool *)srcaddr;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitBoolPtrFloatArr(float[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (float *pfdst = &dst[0])
            {
                float *pdst = pfdst + idst;
                bool *psrc = (bool *)srcaddr;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitByteArrBytePtr(IntPtr dstaddr, byte[] src, int n)
        {
            fixed (byte *pfsrc = &src[0])
            {
                byte *pdst = (byte *)dstaddr;
                byte *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitByteArrBytePtr(IntPtr dstaddr, byte[] src, int isrc, int n)
        {
            fixed (byte *pfsrc = &src[0])
            {
                byte *pdst = (byte *)dstaddr;
                byte *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitBytePtrByteArr(byte[] dst, IntPtr srcaddr, int n)
        {
            fixed (byte *pfdst = &dst[0])
            {
                byte *pdst = pfdst;
                byte *psrc = (byte *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitBytePtrByteArr(byte[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (byte *pfdst = &dst[0])
            {
                byte *pdst = pfdst + idst;
                byte *psrc = (byte *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitByteArrSBytePtr(IntPtr dstaddr, byte[] src, int n)
        {
            fixed (byte *pfsrc = &src[0])
            {
                sbyte *pdst = (sbyte *)dstaddr;
                byte *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(sbyte *)psrc++;
            }
        }

        unsafe public static void BlitByteArrSBytePtr(IntPtr dstaddr, byte[] src, int isrc, int n)
        {
            fixed (byte *pfsrc = &src[0])
            {
                sbyte *pdst = (sbyte *)dstaddr;
                byte *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(sbyte *)psrc++;
            }
        }

        unsafe public static void BlitBytePtrSByteArr(sbyte[] dst, IntPtr srcaddr, int n)
        {
            fixed (sbyte *pfdst = &dst[0])
            {
                sbyte *pdst = pfdst;
                byte *psrc = (byte *)srcaddr;
                while (n-- > 0) *pdst++ = *(sbyte *)psrc++;
            }
        }

        unsafe public static void BlitBytePtrSByteArr(sbyte[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (sbyte *pfdst = &dst[0])
            {
                sbyte *pdst = pfdst + idst;
                byte *psrc = (byte *)srcaddr;
                while (n-- > 0) *pdst++ = *(sbyte *)psrc++;
            }
        }

        unsafe public static void BlitSByteArrBytePtr(IntPtr dstaddr, sbyte[] src, int n)
        {
            fixed (sbyte *pfsrc = &src[0])
            {
                byte *pdst = (byte *)dstaddr;
                sbyte *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(byte *)psrc++;
            }
        }

        unsafe public static void BlitSByteArrBytePtr(IntPtr dstaddr, sbyte[] src, int isrc, int n)
        {
            fixed (sbyte *pfsrc = &src[0])
            {
                byte *pdst = (byte *)dstaddr;
                sbyte *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(byte *)psrc++;
            }
        }

        unsafe public static void BlitSBytePtrByteArr(byte[] dst, IntPtr srcaddr, int n)
        {
            fixed (byte *pfdst = &dst[0])
            {
                byte *pdst = pfdst;
                sbyte *psrc = (sbyte *)srcaddr;
                while (n-- > 0) *pdst++ = *(byte *)psrc++;
            }
        }

        unsafe public static void BlitSBytePtrByteArr(byte[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (byte *pfdst = &dst[0])
            {
                byte *pdst = pfdst + idst;
                sbyte *psrc = (sbyte *)srcaddr;
                while (n-- > 0) *pdst++ = *(byte *)psrc++;
            }
        }

        unsafe public static void BlitSByteArrSBytePtr(IntPtr dstaddr, sbyte[] src, int n)
        {
            fixed (sbyte *pfsrc = &src[0])
            {
                sbyte *pdst = (sbyte *)dstaddr;
                sbyte *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitSByteArrSBytePtr(IntPtr dstaddr, sbyte[] src, int isrc, int n)
        {
            fixed (sbyte *pfsrc = &src[0])
            {
                sbyte *pdst = (sbyte *)dstaddr;
                sbyte *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitSBytePtrSByteArr(sbyte[] dst, IntPtr srcaddr, int n)
        {
            fixed (sbyte *pfdst = &dst[0])
            {
                sbyte *pdst = pfdst;
                sbyte *psrc = (sbyte *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitSBytePtrSByteArr(sbyte[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (sbyte *pfdst = &dst[0])
            {
                sbyte *pdst = pfdst + idst;
                sbyte *psrc = (sbyte *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitCharArrCharPtr(IntPtr dstaddr, char[] src, int n)
        {
            fixed (char *pfsrc = &src[0])
            {
                char *pdst = (char *)dstaddr;
                char *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitCharArrCharPtr(IntPtr dstaddr, char[] src, int isrc, int n)
        {
            fixed (char *pfsrc = &src[0])
            {
                char *pdst = (char *)dstaddr;
                char *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitCharPtrCharArr(char[] dst, IntPtr srcaddr, int n)
        {
            fixed (char *pfdst = &dst[0])
            {
                char *pdst = pfdst;
                char *psrc = (char *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitCharPtrCharArr(char[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (char *pfdst = &dst[0])
            {
                char *pdst = pfdst + idst;
                char *psrc = (char *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitCharArrShortPtr(IntPtr dstaddr, char[] src, int n)
        {
            fixed (char *pfsrc = &src[0])
            {
                short *pdst = (short *)dstaddr;
                char *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(short *)psrc++;
            }
        }

        unsafe public static void BlitCharArrShortPtr(IntPtr dstaddr, char[] src, int isrc, int n)
        {
            fixed (char *pfsrc = &src[0])
            {
                short *pdst = (short *)dstaddr;
                char *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(short *)psrc++;
            }
        }

        unsafe public static void BlitCharPtrShortArr(short[] dst, IntPtr srcaddr, int n)
        {
            fixed (short *pfdst = &dst[0])
            {
                short *pdst = pfdst;
                char *psrc = (char *)srcaddr;
                while (n-- > 0) *pdst++ = *(short *)psrc++;
            }
        }

        unsafe public static void BlitCharPtrShortArr(short[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (short *pfdst = &dst[0])
            {
                short *pdst = pfdst + idst;
                char *psrc = (char *)srcaddr;
                while (n-- > 0) *pdst++ = *(short *)psrc++;
            }
        }

        unsafe public static void BlitCharArrUShortPtr(IntPtr dstaddr, char[] src, int n)
        {
            fixed (char *pfsrc = &src[0])
            {
                ushort *pdst = (ushort *)dstaddr;
                char *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(ushort *)psrc++;
            }
        }

        unsafe public static void BlitCharArrUShortPtr(IntPtr dstaddr, char[] src, int isrc, int n)
        {
            fixed (char *pfsrc = &src[0])
            {
                ushort *pdst = (ushort *)dstaddr;
                char *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(ushort *)psrc++;
            }
        }

        unsafe public static void BlitCharPtrUShortArr(ushort[] dst, IntPtr srcaddr, int n)
        {
            fixed (ushort *pfdst = &dst[0])
            {
                ushort *pdst = pfdst;
                char *psrc = (char *)srcaddr;
                while (n-- > 0) *pdst++ = *(ushort *)psrc++;
            }
        }

        unsafe public static void BlitCharPtrUShortArr(ushort[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (ushort *pfdst = &dst[0])
            {
                ushort *pdst = pfdst + idst;
                char *psrc = (char *)srcaddr;
                while (n-- > 0) *pdst++ = *(ushort *)psrc++;
            }
        }

        unsafe public static void BlitShortArrCharPtr(IntPtr dstaddr, short[] src, int n)
        {
            fixed (short *pfsrc = &src[0])
            {
                char *pdst = (char *)dstaddr;
                short *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(char *)psrc++;
            }
        }

        unsafe public static void BlitShortArrCharPtr(IntPtr dstaddr, short[] src, int isrc, int n)
        {
            fixed (short *pfsrc = &src[0])
            {
                char *pdst = (char *)dstaddr;
                short *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(char *)psrc++;
            }
        }

        unsafe public static void BlitShortPtrCharArr(char[] dst, IntPtr srcaddr, int n)
        {
            fixed (char *pfdst = &dst[0])
            {
                char *pdst = pfdst;
                short *psrc = (short *)srcaddr;
                while (n-- > 0) *pdst++ = *(char *)psrc++;
            }
        }

        unsafe public static void BlitShortPtrCharArr(char[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (char *pfdst = &dst[0])
            {
                char *pdst = pfdst + idst;
                short *psrc = (short *)srcaddr;
                while (n-- > 0) *pdst++ = *(char *)psrc++;
            }
        }

        unsafe public static void BlitShortArrShortPtr(IntPtr dstaddr, short[] src, int n)
        {
            fixed (short *pfsrc = &src[0])
            {
                short *pdst = (short *)dstaddr;
                short *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitShortArrShortPtr(IntPtr dstaddr, short[] src, int isrc, int n)
        {
            fixed (short *pfsrc = &src[0])
            {
                short *pdst = (short *)dstaddr;
                short *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitShortPtrShortArr(short[] dst, IntPtr srcaddr, int n)
        {
            fixed (short *pfdst = &dst[0])
            {
                short *pdst = pfdst;
                short *psrc = (short *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitShortPtrShortArr(short[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (short *pfdst = &dst[0])
            {
                short *pdst = pfdst + idst;
                short *psrc = (short *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitShortArrUShortPtr(IntPtr dstaddr, short[] src, int n)
        {
            fixed (short *pfsrc = &src[0])
            {
                ushort *pdst = (ushort *)dstaddr;
                short *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(ushort *)psrc++;
            }
        }

        unsafe public static void BlitShortArrUShortPtr(IntPtr dstaddr, short[] src, int isrc, int n)
        {
            fixed (short *pfsrc = &src[0])
            {
                ushort *pdst = (ushort *)dstaddr;
                short *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(ushort *)psrc++;
            }
        }

        unsafe public static void BlitShortPtrUShortArr(ushort[] dst, IntPtr srcaddr, int n)
        {
            fixed (ushort *pfdst = &dst[0])
            {
                ushort *pdst = pfdst;
                short *psrc = (short *)srcaddr;
                while (n-- > 0) *pdst++ = *(ushort *)psrc++;
            }
        }

        unsafe public static void BlitShortPtrUShortArr(ushort[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (ushort *pfdst = &dst[0])
            {
                ushort *pdst = pfdst + idst;
                short *psrc = (short *)srcaddr;
                while (n-- > 0) *pdst++ = *(ushort *)psrc++;
            }
        }

        unsafe public static void BlitUShortArrCharPtr(IntPtr dstaddr, ushort[] src, int n)
        {
            fixed (ushort *pfsrc = &src[0])
            {
                char *pdst = (char *)dstaddr;
                ushort *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(char *)psrc++;
            }
        }

        unsafe public static void BlitUShortArrCharPtr(IntPtr dstaddr, ushort[] src, int isrc, int n)
        {
            fixed (ushort *pfsrc = &src[0])
            {
                char *pdst = (char *)dstaddr;
                ushort *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(char *)psrc++;
            }
        }

        unsafe public static void BlitUShortPtrCharArr(char[] dst, IntPtr srcaddr, int n)
        {
            fixed (char *pfdst = &dst[0])
            {
                char *pdst = pfdst;
                ushort *psrc = (ushort *)srcaddr;
                while (n-- > 0) *pdst++ = *(char *)psrc++;
            }
        }

        unsafe public static void BlitUShortPtrCharArr(char[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (char *pfdst = &dst[0])
            {
                char *pdst = pfdst + idst;
                ushort *psrc = (ushort *)srcaddr;
                while (n-- > 0) *pdst++ = *(char *)psrc++;
            }
        }

        unsafe public static void BlitUShortArrShortPtr(IntPtr dstaddr, ushort[] src, int n)
        {
            fixed (ushort *pfsrc = &src[0])
            {
                short *pdst = (short *)dstaddr;
                ushort *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(short *)psrc++;
            }
        }

        unsafe public static void BlitUShortArrShortPtr(IntPtr dstaddr, ushort[] src, int isrc, int n)
        {
            fixed (ushort *pfsrc = &src[0])
            {
                short *pdst = (short *)dstaddr;
                ushort *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(short *)psrc++;
            }
        }

        unsafe public static void BlitUShortPtrShortArr(short[] dst, IntPtr srcaddr, int n)
        {
            fixed (short *pfdst = &dst[0])
            {
                short *pdst = pfdst;
                ushort *psrc = (ushort *)srcaddr;
                while (n-- > 0) *pdst++ = *(short *)psrc++;
            }
        }

        unsafe public static void BlitUShortPtrShortArr(short[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (short *pfdst = &dst[0])
            {
                short *pdst = pfdst + idst;
                ushort *psrc = (ushort *)srcaddr;
                while (n-- > 0) *pdst++ = *(short *)psrc++;
            }
        }

        unsafe public static void BlitUShortArrUShortPtr(IntPtr dstaddr, ushort[] src, int n)
        {
            fixed (ushort *pfsrc = &src[0])
            {
                ushort *pdst = (ushort *)dstaddr;
                ushort *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitUShortArrUShortPtr(IntPtr dstaddr, ushort[] src, int isrc, int n)
        {
            fixed (ushort *pfsrc = &src[0])
            {
                ushort *pdst = (ushort *)dstaddr;
                ushort *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitUShortPtrUShortArr(ushort[] dst, IntPtr srcaddr, int n)
        {
            fixed (ushort *pfdst = &dst[0])
            {
                ushort *pdst = pfdst;
                ushort *psrc = (ushort *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitUShortPtrUShortArr(ushort[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (ushort *pfdst = &dst[0])
            {
                ushort *pdst = pfdst + idst;
                ushort *psrc = (ushort *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitIntArrBoolPtr(IntPtr dstaddr, int[] src, int n)
        {
            fixed (int *pfsrc = &src[0])
            {
                bool *pdst = (bool *)dstaddr;
                int *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitIntArrBoolPtr(IntPtr dstaddr, int[] src, int isrc, int n)
        {
            fixed (int *pfsrc = &src[0])
            {
                bool *pdst = (bool *)dstaddr;
                int *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitIntPtrBoolArr(bool[] dst, IntPtr srcaddr, int n)
        {
            fixed (bool *pfdst = &dst[0])
            {
                bool *pdst = pfdst;
                int *psrc = (int *)srcaddr;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitIntPtrBoolArr(bool[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (bool *pfdst = &dst[0])
            {
                bool *pdst = pfdst + idst;
                int *psrc = (int *)srcaddr;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitIntArrIntPtr(IntPtr dstaddr, int[] src, int n)
        {
            fixed (int *pfsrc = &src[0])
            {
                int *pdst = (int *)dstaddr;
                int *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitIntArrIntPtr(IntPtr dstaddr, int[] src, int isrc, int n)
        {
            fixed (int *pfsrc = &src[0])
            {
                int *pdst = (int *)dstaddr;
                int *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitIntPtrIntArr(int[] dst, IntPtr srcaddr, int n)
        {
            fixed (int *pfdst = &dst[0])
            {
                int *pdst = pfdst;
                int *psrc = (int *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitIntPtrIntArr(int[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (int *pfdst = &dst[0])
            {
                int *pdst = pfdst + idst;
                int *psrc = (int *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitIntArrUIntPtr(IntPtr dstaddr, int[] src, int n)
        {
            fixed (int *pfsrc = &src[0])
            {
                uint *pdst = (uint *)dstaddr;
                int *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitIntArrUIntPtr(IntPtr dstaddr, int[] src, int isrc, int n)
        {
            fixed (int *pfsrc = &src[0])
            {
                uint *pdst = (uint *)dstaddr;
                int *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitIntPtrUIntArr(uint[] dst, IntPtr srcaddr, int n)
        {
            fixed (uint *pfdst = &dst[0])
            {
                uint *pdst = pfdst;
                int *psrc = (int *)srcaddr;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitIntPtrUIntArr(uint[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (uint *pfdst = &dst[0])
            {
                uint *pdst = pfdst + idst;
                int *psrc = (int *)srcaddr;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitIntArrFloatPtr(IntPtr dstaddr, int[] src, int n)
        {
            fixed (int *pfsrc = &src[0])
            {
                float *pdst = (float *)dstaddr;
                int *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitIntArrFloatPtr(IntPtr dstaddr, int[] src, int isrc, int n)
        {
            fixed (int *pfsrc = &src[0])
            {
                float *pdst = (float *)dstaddr;
                int *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitIntPtrFloatArr(float[] dst, IntPtr srcaddr, int n)
        {
            fixed (float *pfdst = &dst[0])
            {
                float *pdst = pfdst;
                int *psrc = (int *)srcaddr;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitIntPtrFloatArr(float[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (float *pfdst = &dst[0])
            {
                float *pdst = pfdst + idst;
                int *psrc = (int *)srcaddr;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitUIntArrBoolPtr(IntPtr dstaddr, uint[] src, int n)
        {
            fixed (uint *pfsrc = &src[0])
            {
                bool *pdst = (bool *)dstaddr;
                uint *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitUIntArrBoolPtr(IntPtr dstaddr, uint[] src, int isrc, int n)
        {
            fixed (uint *pfsrc = &src[0])
            {
                bool *pdst = (bool *)dstaddr;
                uint *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitUIntPtrBoolArr(bool[] dst, IntPtr srcaddr, int n)
        {
            fixed (bool *pfdst = &dst[0])
            {
                bool *pdst = pfdst;
                uint *psrc = (uint *)srcaddr;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitUIntPtrBoolArr(bool[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (bool *pfdst = &dst[0])
            {
                bool *pdst = pfdst + idst;
                uint *psrc = (uint *)srcaddr;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitUIntArrIntPtr(IntPtr dstaddr, uint[] src, int n)
        {
            fixed (uint *pfsrc = &src[0])
            {
                int *pdst = (int *)dstaddr;
                uint *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitUIntArrIntPtr(IntPtr dstaddr, uint[] src, int isrc, int n)
        {
            fixed (uint *pfsrc = &src[0])
            {
                int *pdst = (int *)dstaddr;
                uint *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitUIntPtrIntArr(int[] dst, IntPtr srcaddr, int n)
        {
            fixed (int *pfdst = &dst[0])
            {
                int *pdst = pfdst;
                uint *psrc = (uint *)srcaddr;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitUIntPtrIntArr(int[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (int *pfdst = &dst[0])
            {
                int *pdst = pfdst + idst;
                uint *psrc = (uint *)srcaddr;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitUIntArrUIntPtr(IntPtr dstaddr, uint[] src, int n)
        {
            fixed (uint *pfsrc = &src[0])
            {
                uint *pdst = (uint *)dstaddr;
                uint *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitUIntArrUIntPtr(IntPtr dstaddr, uint[] src, int isrc, int n)
        {
            fixed (uint *pfsrc = &src[0])
            {
                uint *pdst = (uint *)dstaddr;
                uint *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitUIntPtrUIntArr(uint[] dst, IntPtr srcaddr, int n)
        {
            fixed (uint *pfdst = &dst[0])
            {
                uint *pdst = pfdst;
                uint *psrc = (uint *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitUIntPtrUIntArr(uint[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (uint *pfdst = &dst[0])
            {
                uint *pdst = pfdst + idst;
                uint *psrc = (uint *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitUIntArrFloatPtr(IntPtr dstaddr, uint[] src, int n)
        {
            fixed (uint *pfsrc = &src[0])
            {
                float *pdst = (float *)dstaddr;
                uint *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitUIntArrFloatPtr(IntPtr dstaddr, uint[] src, int isrc, int n)
        {
            fixed (uint *pfsrc = &src[0])
            {
                float *pdst = (float *)dstaddr;
                uint *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitUIntPtrFloatArr(float[] dst, IntPtr srcaddr, int n)
        {
            fixed (float *pfdst = &dst[0])
            {
                float *pdst = pfdst;
                uint *psrc = (uint *)srcaddr;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitUIntPtrFloatArr(float[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (float *pfdst = &dst[0])
            {
                float *pdst = pfdst + idst;
                uint *psrc = (uint *)srcaddr;
                while (n-- > 0) *pdst++ = *(float *)psrc++;
            }
        }

        unsafe public static void BlitLongArrLongPtr(IntPtr dstaddr, long[] src, int n)
        {
            fixed (long *pfsrc = &src[0])
            {
                long *pdst = (long *)dstaddr;
                long *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitLongArrLongPtr(IntPtr dstaddr, long[] src, int isrc, int n)
        {
            fixed (long *pfsrc = &src[0])
            {
                long *pdst = (long *)dstaddr;
                long *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitLongPtrLongArr(long[] dst, IntPtr srcaddr, int n)
        {
            fixed (long *pfdst = &dst[0])
            {
                long *pdst = pfdst;
                long *psrc = (long *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitLongPtrLongArr(long[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (long *pfdst = &dst[0])
            {
                long *pdst = pfdst + idst;
                long *psrc = (long *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitLongArrULongPtr(IntPtr dstaddr, long[] src, int n)
        {
            fixed (long *pfsrc = &src[0])
            {
                ulong *pdst = (ulong *)dstaddr;
                long *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(ulong *)psrc++;
            }
        }

        unsafe public static void BlitLongArrULongPtr(IntPtr dstaddr, long[] src, int isrc, int n)
        {
            fixed (long *pfsrc = &src[0])
            {
                ulong *pdst = (ulong *)dstaddr;
                long *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(ulong *)psrc++;
            }
        }

        unsafe public static void BlitLongPtrULongArr(ulong[] dst, IntPtr srcaddr, int n)
        {
            fixed (ulong *pfdst = &dst[0])
            {
                ulong *pdst = pfdst;
                long *psrc = (long *)srcaddr;
                while (n-- > 0) *pdst++ = *(ulong *)psrc++;
            }
        }

        unsafe public static void BlitLongPtrULongArr(ulong[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (ulong *pfdst = &dst[0])
            {
                ulong *pdst = pfdst + idst;
                long *psrc = (long *)srcaddr;
                while (n-- > 0) *pdst++ = *(ulong *)psrc++;
            }
        }

        unsafe public static void BlitLongArrDoublePtr(IntPtr dstaddr, long[] src, int n)
        {
            fixed (long *pfsrc = &src[0])
            {
                double *pdst = (double *)dstaddr;
                long *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(double *)psrc++;
            }
        }

        unsafe public static void BlitLongArrDoublePtr(IntPtr dstaddr, long[] src, int isrc, int n)
        {
            fixed (long *pfsrc = &src[0])
            {
                double *pdst = (double *)dstaddr;
                long *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(double *)psrc++;
            }
        }

        unsafe public static void BlitLongPtrDoubleArr(double[] dst, IntPtr srcaddr, int n)
        {
            fixed (double *pfdst = &dst[0])
            {
                double *pdst = pfdst;
                long *psrc = (long *)srcaddr;
                while (n-- > 0) *pdst++ = *(double *)psrc++;
            }
        }

        unsafe public static void BlitLongPtrDoubleArr(double[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (double *pfdst = &dst[0])
            {
                double *pdst = pfdst + idst;
                long *psrc = (long *)srcaddr;
                while (n-- > 0) *pdst++ = *(double *)psrc++;
            }
        }

        unsafe public static void BlitULongArrLongPtr(IntPtr dstaddr, ulong[] src, int n)
        {
            fixed (ulong *pfsrc = &src[0])
            {
                long *pdst = (long *)dstaddr;
                ulong *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(long *)psrc++;
            }
        }

        unsafe public static void BlitULongArrLongPtr(IntPtr dstaddr, ulong[] src, int isrc, int n)
        {
            fixed (ulong *pfsrc = &src[0])
            {
                long *pdst = (long *)dstaddr;
                ulong *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(long *)psrc++;
            }
        }

        unsafe public static void BlitULongPtrLongArr(long[] dst, IntPtr srcaddr, int n)
        {
            fixed (long *pfdst = &dst[0])
            {
                long *pdst = pfdst;
                ulong *psrc = (ulong *)srcaddr;
                while (n-- > 0) *pdst++ = *(long *)psrc++;
            }
        }

        unsafe public static void BlitULongPtrLongArr(long[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (long *pfdst = &dst[0])
            {
                long *pdst = pfdst + idst;
                ulong *psrc = (ulong *)srcaddr;
                while (n-- > 0) *pdst++ = *(long *)psrc++;
            }
        }

        unsafe public static void BlitULongArrULongPtr(IntPtr dstaddr, ulong[] src, int n)
        {
            fixed (ulong *pfsrc = &src[0])
            {
                ulong *pdst = (ulong *)dstaddr;
                ulong *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitULongArrULongPtr(IntPtr dstaddr, ulong[] src, int isrc, int n)
        {
            fixed (ulong *pfsrc = &src[0])
            {
                ulong *pdst = (ulong *)dstaddr;
                ulong *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitULongPtrULongArr(ulong[] dst, IntPtr srcaddr, int n)
        {
            fixed (ulong *pfdst = &dst[0])
            {
                ulong *pdst = pfdst;
                ulong *psrc = (ulong *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitULongPtrULongArr(ulong[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (ulong *pfdst = &dst[0])
            {
                ulong *pdst = pfdst + idst;
                ulong *psrc = (ulong *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitULongArrDoublePtr(IntPtr dstaddr, ulong[] src, int n)
        {
            fixed (ulong *pfsrc = &src[0])
            {
                double *pdst = (double *)dstaddr;
                ulong *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(double *)psrc++;
            }
        }

        unsafe public static void BlitULongArrDoublePtr(IntPtr dstaddr, ulong[] src, int isrc, int n)
        {
            fixed (ulong *pfsrc = &src[0])
            {
                double *pdst = (double *)dstaddr;
                ulong *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(double *)psrc++;
            }
        }

        unsafe public static void BlitULongPtrDoubleArr(double[] dst, IntPtr srcaddr, int n)
        {
            fixed (double *pfdst = &dst[0])
            {
                double *pdst = pfdst;
                ulong *psrc = (ulong *)srcaddr;
                while (n-- > 0) *pdst++ = *(double *)psrc++;
            }
        }

        unsafe public static void BlitULongPtrDoubleArr(double[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (double *pfdst = &dst[0])
            {
                double *pdst = pfdst + idst;
                ulong *psrc = (ulong *)srcaddr;
                while (n-- > 0) *pdst++ = *(double *)psrc++;
            }
        }

        unsafe public static void BlitFloatArrBoolPtr(IntPtr dstaddr, float[] src, int n)
        {
            fixed (float *pfsrc = &src[0])
            {
                bool *pdst = (bool *)dstaddr;
                float *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitFloatArrBoolPtr(IntPtr dstaddr, float[] src, int isrc, int n)
        {
            fixed (float *pfsrc = &src[0])
            {
                bool *pdst = (bool *)dstaddr;
                float *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitFloatPtrBoolArr(bool[] dst, IntPtr srcaddr, int n)
        {
            fixed (bool *pfdst = &dst[0])
            {
                bool *pdst = pfdst;
                float *psrc = (float *)srcaddr;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitFloatPtrBoolArr(bool[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (bool *pfdst = &dst[0])
            {
                bool *pdst = pfdst + idst;
                float *psrc = (float *)srcaddr;
                while (n-- > 0) *pdst++ = *(bool *)psrc++;
            }
        }

        unsafe public static void BlitFloatArrIntPtr(IntPtr dstaddr, float[] src, int n)
        {
            fixed (float *pfsrc = &src[0])
            {
                int *pdst = (int *)dstaddr;
                float *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitFloatArrIntPtr(IntPtr dstaddr, float[] src, int isrc, int n)
        {
            fixed (float *pfsrc = &src[0])
            {
                int *pdst = (int *)dstaddr;
                float *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitFloatPtrIntArr(int[] dst, IntPtr srcaddr, int n)
        {
            fixed (int *pfdst = &dst[0])
            {
                int *pdst = pfdst;
                float *psrc = (float *)srcaddr;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitFloatPtrIntArr(int[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (int *pfdst = &dst[0])
            {
                int *pdst = pfdst + idst;
                float *psrc = (float *)srcaddr;
                while (n-- > 0) *pdst++ = *(int *)psrc++;
            }
        }

        unsafe public static void BlitFloatArrUIntPtr(IntPtr dstaddr, float[] src, int n)
        {
            fixed (float *pfsrc = &src[0])
            {
                uint *pdst = (uint *)dstaddr;
                float *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitFloatArrUIntPtr(IntPtr dstaddr, float[] src, int isrc, int n)
        {
            fixed (float *pfsrc = &src[0])
            {
                uint *pdst = (uint *)dstaddr;
                float *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitFloatPtrUIntArr(uint[] dst, IntPtr srcaddr, int n)
        {
            fixed (uint *pfdst = &dst[0])
            {
                uint *pdst = pfdst;
                float *psrc = (float *)srcaddr;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitFloatPtrUIntArr(uint[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (uint *pfdst = &dst[0])
            {
                uint *pdst = pfdst + idst;
                float *psrc = (float *)srcaddr;
                while (n-- > 0) *pdst++ = *(uint *)psrc++;
            }
        }

        unsafe public static void BlitFloatArrFloatPtr(IntPtr dstaddr, float[] src, int n)
        {
            fixed (float *pfsrc = &src[0])
            {
                float *pdst = (float *)dstaddr;
                float *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitFloatArrFloatPtr(IntPtr dstaddr, float[] src, int isrc, int n)
        {
            fixed (float *pfsrc = &src[0])
            {
                float *pdst = (float *)dstaddr;
                float *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitFloatPtrFloatArr(float[] dst, IntPtr srcaddr, int n)
        {
            fixed (float *pfdst = &dst[0])
            {
                float *pdst = pfdst;
                float *psrc = (float *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitFloatPtrFloatArr(float[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (float *pfdst = &dst[0])
            {
                float *pdst = pfdst + idst;
                float *psrc = (float *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitDoubleArrLongPtr(IntPtr dstaddr, double[] src, int n)
        {
            fixed (double *pfsrc = &src[0])
            {
                long *pdst = (long *)dstaddr;
                double *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(long *)psrc++;
            }
        }

        unsafe public static void BlitDoubleArrLongPtr(IntPtr dstaddr, double[] src, int isrc, int n)
        {
            fixed (double *pfsrc = &src[0])
            {
                long *pdst = (long *)dstaddr;
                double *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(long *)psrc++;
            }
        }

        unsafe public static void BlitDoublePtrLongArr(long[] dst, IntPtr srcaddr, int n)
        {
            fixed (long *pfdst = &dst[0])
            {
                long *pdst = pfdst;
                double *psrc = (double *)srcaddr;
                while (n-- > 0) *pdst++ = *(long *)psrc++;
            }
        }

        unsafe public static void BlitDoublePtrLongArr(long[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (long *pfdst = &dst[0])
            {
                long *pdst = pfdst + idst;
                double *psrc = (double *)srcaddr;
                while (n-- > 0) *pdst++ = *(long *)psrc++;
            }
        }

        unsafe public static void BlitDoubleArrULongPtr(IntPtr dstaddr, double[] src, int n)
        {
            fixed (double *pfsrc = &src[0])
            {
                ulong *pdst = (ulong *)dstaddr;
                double *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *(ulong *)psrc++;
            }
        }

        unsafe public static void BlitDoubleArrULongPtr(IntPtr dstaddr, double[] src, int isrc, int n)
        {
            fixed (double *pfsrc = &src[0])
            {
                ulong *pdst = (ulong *)dstaddr;
                double *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *(ulong *)psrc++;
            }
        }

        unsafe public static void BlitDoublePtrULongArr(ulong[] dst, IntPtr srcaddr, int n)
        {
            fixed (ulong *pfdst = &dst[0])
            {
                ulong *pdst = pfdst;
                double *psrc = (double *)srcaddr;
                while (n-- > 0) *pdst++ = *(ulong *)psrc++;
            }
        }

        unsafe public static void BlitDoublePtrULongArr(ulong[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (ulong *pfdst = &dst[0])
            {
                ulong *pdst = pfdst + idst;
                double *psrc = (double *)srcaddr;
                while (n-- > 0) *pdst++ = *(ulong *)psrc++;
            }
        }

        unsafe public static void BlitDoubleArrDoublePtr(IntPtr dstaddr, double[] src, int n)
        {
            fixed (double *pfsrc = &src[0])
            {
                double *pdst = (double *)dstaddr;
                double *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitDoubleArrDoublePtr(IntPtr dstaddr, double[] src, int isrc, int n)
        {
            fixed (double *pfsrc = &src[0])
            {
                double *pdst = (double *)dstaddr;
                double *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitDoublePtrDoubleArr(double[] dst, IntPtr srcaddr, int n)
        {
            fixed (double *pfdst = &dst[0])
            {
                double *pdst = pfdst;
                double *psrc = (double *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }

        unsafe public static void BlitDoublePtrDoubleArr(double[] dst, int idst, IntPtr srcaddr, int n)
        {
            fixed (double *pfdst = &dst[0])
            {
                double *pdst = pfdst + idst;
                double *psrc = (double *)srcaddr;
                while (n-- > 0) *pdst++ = *psrc++;
            }
        }
    }
}


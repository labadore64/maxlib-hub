﻿using System;

namespace MaxKernel
{
    // Arithmetic Utilities
    public static class Arithmetic
    {
        public static ulong NextPowerOfTwo(ulong n)
        {
            n |= n >> 0x01; // smear 1
            n |= n >> 0x02; // smear 2
            n |= n >> 0x04; // smear 4
            n |= n >> 0x08; // smear 8
            n |= n >> 0x10; // smear 16
            n |= n >> 0x20; // smear 32
            return n + 1;
        }

        public static uint NextPowerOfTwo(uint n)
        {
            n |= n >> 0x01; // smear 1
            n |= n >> 0x02; // smear 2
            n |= n >> 0x04; // smear 4
            n |= n >> 0x08; // smear 8
            n |= n >> 0x10; // smear 16
            return n + 1;
        }

        public static ushort NextPowerOfTwo(ushort n)
        {
            n |= (ushort)(n >> 0x01); // smear 1
            n |= (ushort)(n >> 0x02); // smear 2
            n |= (ushort)(n >> 0x04); // smear 4
            n |= (ushort)(n >> 0x08); // smear 8
            return (ushort)(n + 1);
        }

        public static byte NextPowerOfTwo(byte n)
        {
            n |= (byte)(n >> 0x01); // smear 1
            n |= (byte)(n >> 0x02); // smear 2
            n |= (byte)(n >> 0x04); // smear 4
            return (byte)(n + 1);
        }
    }
}

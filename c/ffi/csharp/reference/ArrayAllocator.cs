﻿// 2020-04-18
// allocator now handles alignment in the same way mxmem does
//
// 2020-04-17
// added documentation
// added missing ProjectDoubles method to ArrayManager
// added commentary regarding Max Library Machine integration
//
// 2020-04-16
// initial version
//
// William 

// Obviously, you have an established system for type constants and avoiding
// reflection that is outside the scope of this. The intent is for this to be
// integrated with what you have. Using this, each property may be stored as an
// IntPtr, so an array or table of properties is very light. The idea is that
// when a property is retrieved, a transient array projection is returned that
// provides an indexing method and a way to provide an actual pointer (inside of
// an "unsafe" block, which doesn't appear to be a limitation that can be
// circumvented).

using System;
using System.Collections.Generic;
using max.aux;
using max.process.arr.type;
using max.processor.mem;

namespace max.process.arr
{
    // This is the array manager. For the sake of this example, it takes on the
    // function of alignment in a way that's convenient in C#. The aligned
    // allocation option should actually be part of the Allocator interface
    // which mirrors mxmem instances, and ArrayManager should simply call it
    // with the desired alignment. The error checking of mxmem's builtin aligned
    // allocator wrapper is desirable compared to this.
    internal class ArrayManager
    {
        // how to remember the array blocks created
        internal struct MemorySegmentSpec
        {
            internal MemorySegmentSpec(IntPtr p, ulong nbytes, ulong nalign)
            {
                Address = p;
                ByteCount = nbytes;
                Alignment = nalign;
                RefCount = 1;
            }

            internal IntPtr Address   { get; } // aligned address for use
            internal ulong  ByteCount { get; } // guaranteed bytes
            internal ulong  Alignment { get; } // guaranteed alignment
            internal int    RefCount  { get; set; } // reference counter
        }

        private static Dictionary<IntPtr, MemorySegmentSpec> segments_; // array blocks

        // getter for the allocator
        internal static Allocator Alloc
        {
            get; private set;
        }

        internal ArrayManager(Allocator alloc)
        {
            Alloc = alloc;
            segments_ = new Dictionary<IntPtr, MemorySegmentSpec>();
        }

        ~ArrayManager()
        {
            // deallocate all the blocks
            foreach (KeyValuePair<IntPtr, MemorySegmentSpec> s in segments_)
            {
                Alloc.DeallocateAligned(s.Value.Address, s.Value.ByteCount, s.Value.Alignment);
            }
        }

        // allocate memory for an array whose size in bytes is the size of the
        // type T in bytes times the number of elements desired, and with at least
        // the specified alignment.
        internal IntPtr CreateArrayBlock<T>(ulong nelements, ulong alignment)
          where T : unmanaged
        {
            // at least align on the next power of two after the size of the type
            ulong nalign = Arithmetic.NextPowerOfTwo((ulong)Arithmetic.ByteSizeOf<T>(1) - 1);

            // follow the users alignment wish if it's greater than the minimum
            if (alignment > nalign)
                nalign = Arithmetic.NextPowerOfTwo(alignment - 1);

            // calculate bytes needed from the template type.
            ulong nbytes = (ulong)Arithmetic.ByteSizeOf<T>((int)nelements);

            // allocate the memory for the array block.
            IntPtr p = Alloc.AllocateAligned(nbytes, nalign);

            // create the memory info struct and add it to the table
            MemorySegmentSpec spec = new MemorySegmentSpec(p, nbytes, nalign);
            segments_.Add(spec.Address, spec);

            // return the usable address
            return spec.Address;
        }

        // reference array memory
        internal void RefArrayBlock(IntPtr p)
        {
            MemorySegmentSpec spec = segments_[p];
            spec.RefCount++;
        }

        // unreference array memory
        internal void UnrefArrayBlock(IntPtr p)
        {
            DestroyArrayBlock(p);
        }

        // deprecated
        internal void DestroyArrayBlock(IntPtr p)
        {
            // to be clear, we want an exception to be thrown if an unknown pointer
            // is passed in
            MemorySegmentSpec spec = segments_[p];

            if (--spec.RefCount <= 0)
            {
                Alloc.DeallocateAligned(spec.Address, spec.ByteCount, spec.Alignment);
                segments_.Remove(p);
            }
        }

        // interpret an array memory segment as signed bytes
        internal CharArray ProjectChars(IntPtr p)
        {
            MemorySegmentSpec spec = segments_[p];
            return new CharArray(spec.Address, spec.ByteCount, spec.Alignment);
        }

        // interpret an array memory segment as unsigned bytes
        internal ByteArray ProjectBytes(IntPtr p)
        {
            MemorySegmentSpec spec = segments_[p];
            return new ByteArray(spec.Address, spec.ByteCount, spec.Alignment);
        }

        // interpret an array memory segment as signed bytes
        internal SByteArray ProjectSBytes(IntPtr p)
        {
            MemorySegmentSpec spec = segments_[p];
            return new SByteArray(spec.Address, spec.ByteCount, spec.Alignment);
        }

        // interpret an array memory segment as bools
        internal BoolArray ProjectBools(IntPtr p)
        {
            MemorySegmentSpec spec = segments_[p];
            return new BoolArray(spec.Address, spec.ByteCount, spec.Alignment);
        }

        // interpret an array memory segment as signed shorts
        internal ShortArray ProjectShorts(IntPtr p)
        {
            MemorySegmentSpec spec = segments_[p];
            return new ShortArray(spec.Address, spec.ByteCount >> 1, spec.Alignment);
        }

        // interpret an array memory segment as unsigned shorts
        internal UShortArray ProjectUShorts(IntPtr p)
        {
            MemorySegmentSpec spec = segments_[p];
            return new UShortArray(spec.Address, spec.ByteCount >> 1, spec.Alignment);
        }

        // interpret an array memory segment as signed ints
        internal IntArray ProjectInts(IntPtr p)
        {
            MemorySegmentSpec spec = segments_[p];
            return new IntArray(spec.Address, spec.ByteCount >> 2, spec.Alignment);
        }

        // interpret an array memory segment as unsigned ints
        internal UIntArray ProjectUInts(IntPtr p)
        {
            MemorySegmentSpec spec = segments_[p];
            return new UIntArray(spec.Address, spec.ByteCount >> 2, spec.Alignment);
        }

        // interpret an array memory segment as signed longs
        internal LongArray ProjectLongs(IntPtr p)
        {
            MemorySegmentSpec spec = segments_[p];
            return new LongArray(spec.Address, spec.ByteCount >> 3, spec.Alignment);
        }

        // interpret an array memory segment as unsigned longs
        internal ULongArray ProjectULongs(IntPtr p)
        {
            MemorySegmentSpec spec = segments_[p];
            return new ULongArray(spec.Address, spec.ByteCount >> 3, spec.Alignment);
        }

        // interpret an array memory segment as doubles
        internal DoubleArray ProjectDoubles(IntPtr p)
        {
            MemorySegmentSpec spec = segments_[p];
            return new DoubleArray(spec.Address, spec.ByteCount >> 3, spec.Alignment);
        }
    }
}

﻿using System;
using max.aux;

namespace max.process.arr.type
{
    // double array
    public class DoubleArray : Array<double>
    {
        // indexer
        public double this[int i]
        {
            get { return getDouble(i); }
            set { setDouble(i,value); }
        }

        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(double);
            }
        }

        public override ulong ByteLength
        {
            get { return nelements_ * sizeof(double); }
        }

        public unsafe double* Pointer
        {
            get { return (double*)addr_; }
        }

        protected override void copyFromLen(double[] elements, int len)
        {
            Arithmetic.FastCopyDoubles(addr_, elements, len);
        }

        public DoubleArray(IntPtr p, ulong nelements, ulong nalign)
            : base(p, nelements, nalign)
        { }

        public DoubleArray(ulong nelements)
            : base(nelements)
        { }

        public DoubleArray(double[] elements)
            : base(elements)
        { }

        public DoubleArray()
        {

        }

        public override DataType CreateInstance(ulong length)
        {
            return new DoubleArray(length);
        }

        public static implicit operator DoubleArray(double[] value) => new DoubleArray(value);
    }
}

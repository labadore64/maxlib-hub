﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using max.aux;

namespace max.process.arr.type
{
    // char array (beware elders; these are 16 bit wide chars)
    public class CharArray : Array<char>
    {
        // indexer
        public char this[int i]
        {
            get { return (char)getShort(i); }
            set { setShort(i,(short)value); }
        }

        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(char);
            }
        }

        public override ulong ByteLength
        {
            get { return nelements_ * sizeof(char); }
        }

        public unsafe char* Pointer
        {
            get { return (char*)addr_; }
        }

        public override string ToString()
        {
            int nbytes = Length << 1;

            var result = new StringBuilder(Length);
            for (int ibyte = 0; ibyte < nbytes; ibyte += 2)
                result.Append((char)Marshal.ReadInt16(addr_, ibyte));

            return result.ToString();
        }

        protected override void copyFromLen(char[] elements, int len)
        {
            Arithmetic.FastCopyChars(addr_, elements, len);
        }

        public CharArray(IntPtr p, ulong nelements, ulong nalign)
            : base(p, nelements, nalign)
        { }

        public CharArray(ulong nelements)
            : base(nelements)
        { }

        public CharArray(char[] elements)
            : base(elements)
        { }
        
        public CharArray(string s)
            : base((ulong)s.Length)
        {
            unsafe
            {
                int n = s.Length;
                char *p = Pointer;
                for (int i = 0; i < n; ++i)
                    *p++ = s[i];
            }
        }

        public CharArray()
        {

        }

        public override DataType CreateInstance(ulong length)
        {
            return new CharArray(length);
        }

        public static implicit operator CharArray(char[] value) => new CharArray(value);
    }
}

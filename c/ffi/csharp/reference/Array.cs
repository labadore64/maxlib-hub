﻿using System;
using System.Runtime.InteropServices;
using max.aux;
using max.duplex;

namespace max.process.arr.type
{
    // the base class of array projection.
    public abstract class Array<T> : DataType where T : unmanaged
    {
        public void UpdatePointer(DataType Data)
        {
            RePoint(Data.ArrayPointer, Data.ArrayLength, Data.Alignment);
        }

        public abstract DataType CreateInstance(ulong length);

        protected const ulong ALIGN_DEFAULT = 64;

        protected IntPtr addr_;
        protected ulong nelements_;
        protected ulong nalign_;

        // bytes per element
        public abstract ulong ElementByteSize { get; }

        // if this is abstract, it defeats the whole purpose since it can't
        // be quick then. we'll just hardcode the shifts since they're
        // consistent across C# due to the spec
        //protected abstract int byteIndexOf(int i);

        // length in bytes
        public abstract ulong ByteLength { get; }

        // length in elements
        public int Length
        {
            get { return (int)nelements_; }
        }

        // length in elements
        public ulong ArrayLength
        {
            get { return nelements_; }
        }

        // guaranteed alignment
        public ulong Alignment
        {
            get { return nelements_; }
        }

        /// <summary>
        /// Gets the IntPtr of the array.
        /// </summary>
        /// <value>IntPtr</value>
        public IntPtr ArrayPointer
        {
            get { return addr_; }
        }

        protected abstract void copyFromLen(T[] elements, int len);

        // copy data from an array (post construction)
        public void CopyFrom(T[] elements)
        {
            copyFromLen(elements,
                Length < elements.Length ? Length : elements.Length);
        }

        protected void Construct(ulong nelements)
        {
            // create an array block, comes with a reference (ours)
            IntPtr p = Game.CurrentGame.ArrayManager.CreateArrayBlock<T>(
                        nelements,
                        ALIGN_DEFAULT);

            addr_ = p;
            nelements_ = nelements;
            nalign_ = ALIGN_DEFAULT;
        }

        protected Array(IntPtr p, ulong nelements, ulong nalign)
        {
            RePoint(p, nelements, nalign);
        }

        protected void RePoint(IntPtr p, ulong nelements, ulong nalign)
        {
            addr_ = p;
            nelements_ = nelements;
            nalign_ = nalign;

            // reference the array block to make sure it's not destroyed if
            // someone else unreferences it
            Game.CurrentGame.ArrayManager.RefArrayBlock(p);
        }

        protected Array(ulong nelements)
        {
            Construct(nelements);
        }

        protected Array()
        {
            Construct(0);
        }

        protected Array(T[] elements)
        {
            int len = elements.Length;
            Construct((uint)len);
            copyFromLen(elements, len);
        }

        ~Array()
        {
            // unreference the array block. if this is the last array to know
            // about it, it gets destroyed
            Game.CurrentGame.ArrayManager.UnrefArrayBlock(addr_);
        }

#if DEBUG
        private void _ckIndex(int i)
        {
            if (i < 0 || i >= Length)
                throw new IndexOutOfRangeException("Index " + i.ToString()
                        + " out of range in array projection of type "
                        + typeof(T) + " having "
                        + nelements_.ToString() + " elements.");
        }
#endif

        // here are the protected indexer implementations
        protected byte getByte(int i)
        {
#if DEBUG
            _ckIndex(i);
#endif
            return Marshal.ReadByte(addr_, i);
        }

        protected void setByte(int i, byte v)
        {
#if DEBUG
            _ckIndex(i);
#endif
            Marshal.WriteByte(addr_, i, v);
        }

        protected short getShort(int i)
        {
#if DEBUG
            _ckIndex(i);
#endif
            return Marshal.ReadInt16(addr_, i << 1);
        }

        protected void setShort(int i, short v)
        {
#if DEBUG
            _ckIndex(i);
#endif
            Marshal.WriteInt16(addr_, i << 1, v);
        }

        protected int getInt(int i)
        {
#if DEBUG
            _ckIndex(i);
#endif
            return Marshal.ReadInt32(addr_, i << 2);
        }

        protected void setInt(int i, int v)
        {
#if DEBUG
            _ckIndex(i);
#endif
            Marshal.WriteInt32(addr_, i << 2, v);
        }

        protected long getLong(int i)
        {
#if DEBUG
            _ckIndex(i);
#endif
            return Marshal.ReadInt64(addr_, i << 3);
        }

        protected void setLong(int i, long v)
        {
#if DEBUG
            _ckIndex(i);
#endif
            Marshal.WriteInt64(addr_, i << 3, v);
        }

        protected float getFloat(int i)
        {
#if DEBUG
            _ckIndex(i);
#endif
            return Arithmetic.IntToFloat(Marshal.ReadInt32(addr_, i << 2));
        }

        protected void setFloat(int i, float v)
        {
#if DEBUG
            _ckIndex(i);
#endif
            Marshal.WriteInt32(addr_, i << 2, Arithmetic.FloatToInt(v));
        }

        protected double getDouble(int i)
        {
#if DEBUG
            _ckIndex(i);
#endif
            return Arithmetic.LongToDouble(Marshal.ReadInt64(addr_, i << 3));
        }

        protected void setDouble(int i, double v)
        {
#if DEBUG
            _ckIndex(i);
#endif
            Marshal.WriteInt64(addr_, i << 3, Arithmetic.DoubleToLong(v));
        }
        
        protected bool getBool(int i)
        {
#if DEBUG
            _ckIndex(i);
#endif
            return Arithmetic.IntToBool(Marshal.ReadInt32(addr_, i << 2));
        }

        protected void setBool(int i, bool v)
        {
#if DEBUG
            _ckIndex(i);
#endif
            Marshal.WriteInt32(addr_, i << 2, Arithmetic.BoolToInt(v));
        }
    }
}

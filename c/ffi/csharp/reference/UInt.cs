﻿using System;
using max.aux;

namespace max.process.arr.type
{
    // unsigned int32 array
    public class UIntArray : Array<uint>
    {
        // indexer
        public uint this[int i]
        {
            get { return (uint)getInt(i); }
            set { setInt(i,(int)value); }
        }

        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(uint);
            }
        }

        public override ulong ByteLength
        {
            get { return nelements_ * sizeof(uint); }
        }

        public unsafe uint* Pointer
        {
            get { return (uint*)addr_; }
        }

        protected override void copyFromLen(uint[] elements, int len)
        {
            Arithmetic.FastCopyUInts(addr_, elements, len);
        }

        public UIntArray(IntPtr p, ulong nelements, ulong nalign)
            : base(p, nelements, nalign)
        { }

        public UIntArray(ulong nelements)
            : base(nelements)
        { }

        public UIntArray(uint[] elements)
            : base(elements)
        { }

        public UIntArray()
        {

        }

        public override DataType CreateInstance(ulong length)
        {
            return new UIntArray(length);
        }

        public static implicit operator UIntArray(uint[] value) => new UIntArray(value);
    }
}

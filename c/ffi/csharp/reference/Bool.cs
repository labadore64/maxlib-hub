﻿using System;
using max.aux;

namespace max.process.arr.type
{
    // unsigned byte array
    public class BoolArray : Array<bool>
    {
        // indexer
        public bool this[int i]
        {
            get { return getBool(i); }
            set { setBool(i, value); }
        }

        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(bool);
            }
        }

        public override ulong ByteLength 
        {
            get { return nelements_ * sizeof(bool); }
        }

        // get the real pointer. fastest, but only allowed in IL
        public unsafe bool* Pointer
        {
            get { return (bool*)addr_; }
        }

        protected override void copyFromLen(bool[] elements, int len)
        {
            Arithmetic.FastCopyBools(addr_, elements, len);
        }

        public BoolArray(IntPtr p, ulong nelements, ulong nalign)
            : base(p, nelements, nalign)
        { }

        public BoolArray(ulong nelements)
            : base(nelements)
        { }

        public BoolArray(bool[] elements)
            : base(elements)
        { }

        public BoolArray()
        {

        }

        public override DataType CreateInstance(ulong length)
        {
            return new BoolArray(length);
        }

        public static implicit operator BoolArray(bool[] value) => new BoolArray(value);
    }
}

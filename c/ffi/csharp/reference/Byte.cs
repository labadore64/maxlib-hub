﻿using System;
using max.aux;

namespace max.process.arr.type
{
    // unsigned byte array
    public class ByteArray : Array<byte>
    {
        // indexer
        public byte this[int i]
        {
            get { return getByte(i); }
            set { setByte(i,value); }
        }

        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(byte);
            }
        }

        public override ulong ByteLength
        {
            get { return nelements_ * sizeof(byte); }
        }

        public unsafe byte* Pointer
        {
            get { return (byte*)addr_; }
        }

        protected override void copyFromLen(byte[] elements, int len)
        {
            Arithmetic.FastCopyBytes(addr_, elements, len);
        }

        public ByteArray(IntPtr p, ulong nelements, ulong nalign)
            : base(p, nelements, nalign)
        { }

        public ByteArray(ulong nelements)
            : base(nelements)
        { }

        public ByteArray(byte[] elements)
            : base(elements)
        { }

        public ByteArray()
        {

        }

        public override DataType CreateInstance(ulong length)
        {
            return new ByteArray(length);
        }

        public static implicit operator ByteArray(byte[] value) => new ByteArray(value);
    }
}

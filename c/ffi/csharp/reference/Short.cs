﻿using System;
using max.aux;

namespace max.process.arr.type
{
    // signed int16 array
    public class ShortArray : Array<short>
    {
        // indexer
        public short this[int i]
        {
            get { return getShort(i); }
            set { setShort(i,value); }
        }

        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(short);
            }
        }

        public override ulong ByteLength
        {
            get { return nelements_ * sizeof(short); }
        }

        public unsafe short* Pointer
        {
            get { return (short*)addr_; }
        }

        protected override void copyFromLen(short[] elements, int len)
        {
            Arithmetic.FastCopyShorts(addr_, elements, len);
        }

        public ShortArray(IntPtr p, ulong nelements, ulong nalign)
            : base(p, nelements, nalign)
        { }

        public ShortArray(ulong nelements)
            : base(nelements)
        { }

        public ShortArray(short[] elements)
            : base(elements)
        { }

        public ShortArray()
        {

        }

        public override DataType CreateInstance(ulong length)
        {
            return new ShortArray(length);
        }

        public static implicit operator ShortArray(short[] value) => new ShortArray(value);
    }
}

﻿using System;
using max.aux;

namespace max.process.arr.type
{
    // unsigned byte array
    public class SByteArray : Array<sbyte>
    { 
        // indexer
        public sbyte this[int i]
        {
            get { return (sbyte)getByte(i); }
            set { setByte(i,(byte)value); }
        }

        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(sbyte);
            }
        }

        public unsafe sbyte* Pointer
        {
            get { return (sbyte*)addr_; }
        }

        public override ulong ByteLength
        {
            get { return nelements_ * sizeof(sbyte); }
        }

        protected override void copyFromLen(sbyte[] elements, int len)
        {
            Arithmetic.FastCopySBytes(addr_, elements, len);
        }

        public SByteArray(IntPtr p, ulong nelements, ulong nalign)
            : base(p, nelements, nalign)
        { }

        public SByteArray(ulong nelements)
            : base(nelements)
        { }

        public SByteArray(sbyte[] elements)
            : base(elements)
        { }

        public SByteArray()
        {

        }

        public override DataType CreateInstance(ulong length)
        {
            return new SByteArray(length);
        }

        public static implicit operator SByteArray(sbyte[] value) => new SByteArray(value);
    }
}

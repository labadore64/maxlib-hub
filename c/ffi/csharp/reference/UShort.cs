﻿using System;
using max.aux;

namespace max.process.arr.type
{
    // unsigned int16 array
    public class UShortArray : Array<ushort>
    {
        // indexer
        public ushort this[int i]
        {
            get { return (ushort)getShort(i); }
            set { setShort(i,(short)value); }
        }

        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(ushort);
            }
        }

        public override ulong ByteLength
        {
            get { return nelements_ * sizeof(ushort); }
        }

        public unsafe ushort* Pointer
        {
            get { return (ushort*)addr_; }
        }

        protected override void copyFromLen(ushort[] elements, int len)
        {
            Arithmetic.FastCopyUShorts(addr_, elements, len);
        }

        public UShortArray(IntPtr p, ulong nelements, ulong nalign)
            : base(p, nelements, nalign)
        { }

        public UShortArray(ulong nelements)
            : base(nelements)
        { }

        public UShortArray(ushort[] elements)
            : base(elements)
        { }

        public UShortArray()
        {

        }

        public override DataType CreateInstance(ulong length)
        {
            return new UShortArray(length);
        }

        public static implicit operator UShortArray(ushort[] value) => new UShortArray(value);
    }
}

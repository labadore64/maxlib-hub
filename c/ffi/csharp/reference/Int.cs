﻿using System;
using max.aux;

namespace max.process.arr.type
{
    // signed int32 array
    public class IntArray : Array<int>
    {
        // indexer
        public int this[int i]
        {
            get { return getInt(i); }
            set { setInt(i,value); }
        }

        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(int);
            }
        }

        public override ulong ByteLength
        {
            get { return nelements_ * sizeof(int); }
        }

        public unsafe int* Pointer
        {
            get { return (int*)addr_; }
        }

        protected override void copyFromLen(int[] elements, int len)
        {
            Arithmetic.FastCopyInts(addr_, elements, len);
        }

        public IntArray(IntPtr p, ulong nelements, ulong nalign)
            : base(p, nelements, nalign)
        { }

        public IntArray(ulong nelements)
            : base(nelements)
        { }

        public IntArray(int[] elements)
            : base(elements)
        { }

        public IntArray()
        {

        }

        public override DataType CreateInstance(ulong length)
        {
            return new IntArray(length);
        }

        public static implicit operator IntArray(int[] value) => new IntArray(value);
    }
}

﻿// 2020-04-18
// allocator now handles alignment in the same way mxmem does
//
// 2020-04-17
// added documentation
// added missing ProjectDoubles method to ArrayManager
// added commentary regarding Max Library Machine integration
//
// 2020-04-16
// initial version
//
// William 

// Obviously, you have an established system for type constants and avoiding
// reflection that is outside the scope of this. The intent is for this to be
// integrated with what you have. Using this, each property may be stored as an
// IntPtr, so an array or table of properties is very light. The idea is that
// when a property is retrieved, a transient array projection is returned that
// provides an indexing method and a way to provide an actual pointer (inside of
// an "unsafe" block, which doesn't appear to be a limitation that can be
// circumvented).

using System;

namespace max.aux
{
    public static class Arithmetic
    {
        // NOTE: ByteSizeOf is deprecated due to the switch to the unmanaged
        //       type restriction, which has made it redundant.

        // Utility function to find out what size to request for a scalar
        // of a certain unmanaged type
        public static int ByteSizeOf<T>()
          where T : unmanaged
        {
            unsafe
            {
                return sizeof(T);
            }
        }

        // Utility function to find out what size to request for an array
        // of a certain length and certain type.
        public static int ByteSizeOf<T>(int n)
          where T : unmanaged
        {
            unsafe
            {
                return sizeof(T) * n;
            }
        }

        public static ulong NextPowerOfTwo(ulong n)
        {
            n |= n >> 0x01; // smear 1
            n |= n >> 0x02; // smear 2
            n |= n >> 0x04; // smear 4
            n |= n >> 0x08; // smear 8
            n |= n >> 0x10; // smear 16
            n |= n >> 0x20; // smear 32
            return n + 1;
        }

        public static uint NextPowerOfTwo(uint n)
        {
            n |= n >> 0x01; // smear 1
            n |= n >> 0x02; // smear 2
            n |= n >> 0x04; // smear 4
            n |= n >> 0x08; // smear 8
            n |= n >> 0x10; // smear 16
            return n + 1;
        }

        public static ushort NextPowerOfTwo(ushort n)
        {
            n |= (ushort)(n >> 0x01); // smear 1
            n |= (ushort)(n >> 0x02); // smear 2
            n |= (ushort)(n >> 0x04); // smear 4
            n |= (ushort)(n >> 0x08); // smear 8
            return (ushort)(n + 1);
        }

        public static byte NextPowerOfTwo(byte n)
        {
            n |= (byte)(n >> 0x01); // smear 1
            n |= (byte)(n >> 0x02); // smear 2
            n |= (byte)(n >> 0x04); // smear 4
            return (byte)(n + 1);
        }

        public static double LongToDouble(long li)
        {
            unsafe
            {
                long *pli = &li;
                return *((double *)pli);
            }
        }

        public static long DoubleToLong(double d)
        {
            unsafe
            {
                double *pd = &d;
                return *((long *)pd);
            }
        }

        public static float IntToFloat(int i)
        {
            unsafe
            {
                int *pi = &i;
                return *((float *)pi);
            }
        }

        public static int FloatToInt(float f)
        {
            unsafe
            {
                float *pf = &f;
                return *((int *)pf);
            }
        }

        public static int BoolToInt(bool b)
        {
            unsafe
            {
                bool *pb = &b;
                return *((int *)pb);
            }
        }

        public static bool IntToBool(int i)
        {
            unsafe
            {
                int *pi = &i;
                return *((bool *)pi);
            }
        }

        unsafe public static void FastCopyBools(IntPtr ptr, bool[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            bool *p = (bool *)ptr;
            fixed (bool *qfix = &elems[0])
            {
                bool *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe public static void FastCopyBytes(IntPtr ptr, byte[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            byte *p = (byte *)ptr;
            fixed (byte *qfix = &elems[0])
            {
                byte *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe public static void FastCopySBytes(IntPtr ptr, sbyte[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            sbyte *p = (sbyte *)ptr;
            fixed (sbyte *qfix = &elems[0])
            {
                sbyte *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe public static void FastCopyChars(IntPtr ptr, char[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            char *p = (char *)ptr;
            fixed (char *qfix = &elems[0])
            {
                char *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe public static void FastCopyShorts(IntPtr ptr, short[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            short *p = (short *)ptr;
            fixed (short *qfix = &elems[0])
            {
                short *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe public static void FastCopyUShorts(IntPtr ptr, ushort[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            ushort *p = (ushort *)ptr;
            fixed (ushort *qfix = &elems[0])
            {
                ushort *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe public static void FastCopyInts(IntPtr ptr, int[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            int *p = (int *)ptr;
            fixed (int *qfix = &elems[0])
            {
                int *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe public static void FastCopyUInts(IntPtr ptr, uint[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            uint *p = (uint *)ptr;
            fixed (uint *qfix = &elems[0])
            {
                uint *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe public static void FastCopyLongs(IntPtr ptr, long[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            long *p = (long *)ptr;
            fixed (long *qfix = &elems[0])
            {
                long *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe public static void FastCopyULongs(IntPtr ptr, ulong[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            ulong *p = (ulong *)ptr;
            fixed (ulong *qfix = &elems[0])
            {
                ulong *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe public static void FastCopyFloats(IntPtr ptr, float[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            float *p = (float *)ptr;
            fixed (float *qfix = &elems[0])
            {
                float *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe public static void FastCopyDoubles(IntPtr ptr, double[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            double *p = (double *)ptr;
            fixed (double *qfix = &elems[0])
            {
                double *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }
    }
}

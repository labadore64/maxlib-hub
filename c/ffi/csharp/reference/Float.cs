﻿using System;
using max.aux;

namespace max.process.arr.type
{
    // float array
    public class FloatArray : Array<float>
    {
        // indexer
        public float this[int i]
        {
            get { return getFloat(i); }
            set { setFloat(i,value); }
        }

        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(float);
            }
        }

        public override ulong ByteLength
        {
            get { return nelements_ * sizeof(float); }
        }

        public unsafe float* Pointer
        {
            get { return (float*)addr_; }
        }

        protected override void copyFromLen(float[] elements, int len)
        {
            Arithmetic.FastCopyFloats(addr_, elements, len);
        }

        public FloatArray(IntPtr p, ulong nelements, ulong nalign)
            : base(p, nelements, nalign)
        { }

        public FloatArray(ulong nelements)
            : base(nelements)
        { }

        public FloatArray(float[] elements)
            : base(elements)
        { }

        public FloatArray()
        {

        }

        public override DataType CreateInstance(ulong length)
        {
            return new FloatArray(length);
        }

        public static implicit operator FloatArray(float[] value) => new FloatArray(value);
    }
}


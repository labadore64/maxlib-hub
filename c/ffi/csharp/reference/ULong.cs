﻿using System;
using max.aux;

namespace max.process.arr.type
{
    // unsigned int64 array
    public class ULongArray : Array<ulong>
    {
        // indexer
        public ulong this[int i]
        {
            get { return (ulong)getLong(i); }
            set { setLong(i,(long)value); }
        }

        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(ulong);
            }
        }

        public override ulong ByteLength
        {
            get { return nelements_ * sizeof(ulong); }
        }

        public unsafe ulong* Pointer
        {
            get { return (ulong*)addr_; }
        }

        protected override void copyFromLen(ulong[] elements, int len)
        {
            Arithmetic.FastCopyULongs(addr_, elements, len);
        }

        public ULongArray(IntPtr p, ulong nelements, ulong nalign)
            : base(p, nelements, nalign)
        { }

        public ULongArray(ulong nelements)
            : base(nelements)
        { }

        public ULongArray(ulong[] elements)
            : base(elements)
        { }

        public ULongArray()
        {

        }

        public override DataType CreateInstance(ulong length)
        {
            return new ULongArray(length);
        }

        public static implicit operator ULongArray(ulong[] value) => new ULongArray(value);
    }
}

﻿using System;
using max.aux;

namespace max.process.arr.type
{
    // signed int64 array
    public class LongArray : Array<long>
    {
        // indexer
        public long this[int i]
        {
            get { return getLong(i); }
            set { setLong(i,value); }
        }

        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(long);
            }
        }

        public override ulong ByteLength
        {
            get { return nelements_ * sizeof(long); }
        }

        public unsafe long* Pointer
        {
            get { return (long*)addr_; }
        }

        protected override void copyFromLen(long[] elements, int len)
        {
            Arithmetic.FastCopyLongs(addr_, elements, len);
        }

        public LongArray(IntPtr p, ulong nelements, ulong nalign)
            : base(p, nelements, nalign)
        { }

        public LongArray(ulong nelements)
            : base(nelements)
        { }

        public LongArray(long[] elements)
            : base(elements)
        { }

        public LongArray()
        {

        }

        public override DataType CreateInstance(ulong length)
        {
            return new LongArray(length);
        }

        public static implicit operator LongArray(long[] value) => new LongArray(value);
    }
}

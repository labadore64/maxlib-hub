# Overview

![Max logo - a white dog wearing a black handkerchief](https://gitlab.com/labadore64/maxlib-hub/-/raw/roger/img/max-logo.png "Max Lib Logo")

Max Lib is a series of open source libraries written in a pipeline of several languages that communicate together with interchangable parts that lack internal organization to build a game engine.

[to do: rewrite readme docs]
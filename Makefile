# Copyright 2020 Roger Merryfield
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# file: Makefile
# description:
#   The Makefile for building Max Library Machine.
# author(s):
#   William
# .

# TODO: dummy targets in the subordinate makefiles to dump files to be
#       copied up to the project root bin and lib directories, calling those
#       targets to consolidate output on successful builds.

# If you want a particular kernel version, override this with an environment
# variable.
MXKER_VERSION := 0

# If you want a particular kernel family, override this with an environment
# variable.
MXKER_FAMILY := roger

# Special files.
MXKER_MAKEFILE = c/Makefile
MXKER_CONF     = c/config/mxker.conf

# make unit identifier
ECHO = @echo 'MLM '

# Build everything rule
.PHONY: all
all: mxker-runtime mxker-test mxker-cs mlm-cs mlm-cs-test

# This file proves the bootstrap is done for the kernel; so it's a hard file
# dependency of all recursive make invocations.
$(MXKER_MAKEFILE):
	$(ECHO) "Configuring mxker $(MXKER_FAMILY) with version blob '$(MXKER_VERSION)'..."
	@cd c && ./bootstrap making 'MLM(mxker:bootstrap) ' "$(MXKER_FAMILY)" "$(MXKER_VERSION)"
	$(ECHO) "mxker bootstrap complete."

# mxker configurator rule
.PHONY: mxker-conf
mxker-conf: $(MXKER_MAKEFILE)
	$(ECHO) "Updating mxker configuration..."
	@$(MAKE) --no-print-directory --quiet -C c conf
	$(ECHO) "mxker configured."

# mxker runtime build rule
.PHONY: mxker-runtime
mxker-runtime: $(MXKER_MAKEFILE)
	$(ECHO) "Building mxker..."
	@$(MAKE) --no-print-directory --quiet -C c kernel
	$(ECHO) "mxker built."

# mxker C# bindings build rule
.PHONY: mxker-cs
mxker-cs: $(MXKER_MAKEFILE)
	$(ECHO) "Building mxker C# bindings..."
	@$(MAKE) --no-print-directory --quiet -C c kernel-cs
	$(ECHO) "mxker C# bindings built."

# mxker tests build rule
.PHONY: mxker-test
mxker-test: $(MXKER_MAKEFILE)
	$(ECHO) "Building mxker tests..."
	@$(MAKE) --no-print-directory --quiet -C c kerneltest
	$(ECHO) "mxker tests built."

# Max Library Machine C# build rule
.PHONY: mlm-cs
mlm-cs: mxker-runtime
	$(ECHO) "Building MLM C# assembly..."
	@$(MAKE) --no-print-directory --quiet -C csharp lib
	$(ECHO) "MLM C# assembly built."

# Max Library Machine C# tests build rule
.PHONY: mlm-cs-test
mlm-cs-test: mlm-cs
	$(ECHO) "Building MLM C# tests..."
	@$(MAKE) --no-print-directory --quiet -C csharp test
	$(ECHO) "MLM C# tests built."

# mxker clean rule
.PHONY: mxker-clean
mxker-clean:
	$(ECHO) "Cleaning mxker..."
ifneq ("$(wildcard c/Makefile)","")
	@$(MAKE) --no-print-directory --quiet -C c clean
	$(ECHO) "Finished cleaning mxker."
else
	$(ECHO) "Nothing to clean for mxker."
endif
	
# mxker deepclean rule (reverse bootstrap)
.PHONY: mxker-deepclean
mxker-deepclean:
	$(ECHO) "Cleaning mxker bootstrap..."
ifneq ("$(wildcard c/Makefile)","")
	@$(MAKE) --no-print-directory --quiet -C c deepclean
	$(ECHO) "Finished cleaning mxker bootstrap."
else
	$(ECHO) "Nothing to reverse for mxker."
endif

# Max Library Machine C# clean rule
.PHONY: mlm-cs-clean
mlm-cs-clean:
	$(ECHO) "Cleaning MLM C#..."
	@$(MAKE) --no-print-directory --quiet -C csharp clean
	$(ECHO) "Finished cleaning MLM C#."

# clean all rule
.PHONY: clean
clean: mlm-cs-clean mxker-clean

# deepclean all rule (only effects mxker differently)
.PHONY: deepclean
deepclean: mlm-cs-clean mxker-deepclean


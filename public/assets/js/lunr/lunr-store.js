var store = [{
        "title": "Max C# API Documentation",
        "excerpt":"Max C# API documentation This API documentation explains all public and protected methods of the components in Max C#. Table of Contents max max.game Game GameState max.process max.process.env MaxEnvironment max.process.arr max.process.arr.type IDataType MaxProperty MaxObject Array &lt; T &gt; BoolArray ByteArray CharArray DoubleArray FloatArray IntArray LongArray SByteArray ShortArray UIntArray ULongArray UShortArray...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/index/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "class Game",
        "excerpt":"class Game Summary A class that represents the entire game. syntax: public abstract class Game namespace: max.game Properties CurrentState Current gamestate. syntax: protected GameState CurrentState { get; set; } CurrentGame The current running game. syntax: public static Game CurrentGame { get; private set; } Environment The Max environment. syntax: public...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.game.Game/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "enum GameState",
        "excerpt":"enum GameState   Represents different states of the game running.   syntax: public enum GameState  namespace: max.game ","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.game.GameState/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "namespace max.game",
        "excerpt":"namespace max.game   Members      Game - A class that represents the entire game.   GameState - Represents different states of the game running.  ","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.game/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "namespace max",
        "excerpt":"namespace max   Members      max.game   max.process  ","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "namespace max.process.arr",
        "excerpt":"namespace max.process.arr   Members      max.process.arr.type  ","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "class Array<T>",
        "excerpt":"class Array Summary This abstract class represents an array managed by the Max Environment. syntax: public abstract class Array&lt;T&gt; : IDataType where T : unmanaged namespace: max.process.arr.type Constructors Array(IntPtr Pointer, ulong Count, ulong Alignment) Initializes the array with a pointer, array length and alignment. syntax: protected Array(IntPtr Pointer, ulong Count,...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr.type.ArrayT/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "class BoolArray",
        "excerpt":"class BoolArray Summary This class represents a boolean array managed by the Max Environment. syntax: public class BoolArray : Array&lt;bool&gt; namespace: max.process.arr.type Constructors BoolArray(IntPtr Pointer, ulong Count, ulong Alignment) Instantiates an array of bools with a given pointer, element count and alignment. syntax: public BoolArray(IntPtr Pointer, ulong Count, ulong Alignment)...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr.type.BoolArray/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "class ByteArray",
        "excerpt":"class ByteArray Summary This class represents a byte array managed by the Max Environment. syntax: public class ByteArray : Array&lt;byte&gt; namespace: max.process.arr.type Constructors ByteArray(IntPtr Pointer, ulong Count, ulong Alignment) Instantiates an array of bytes with a given pointer, element count and alignment. syntax: public ByteArray(IntPtr Pointer, ulong Count, ulong Alignment)...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr.type.ByteArray/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "class CharArray",
        "excerpt":"class CharArray Summary This class represents a char array managed by the Max Environment. Note that chars are 16-bit wide. syntax: public class CharArray : Array&lt;char&gt; namespace: max.process.arr.type Constructors CharArray(IntPtr Pointer, ulong Count, ulong Alignment) Instantiates an array of chars with a given pointer, element count and alignment. syntax: public...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr.type.CharArray/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "class DoubleArray",
        "excerpt":"class DoubleArray Summary This class represents a double array managed by the Max Environment. syntax: public class DoubleArray : Array&lt;double&gt; namespace: max.process.arr.type Constructors DoubleArray(IntPtr Pointer, ulong Count, ulong Alignment) Instantiates an array of doubles with a given pointer, element count and alignment. syntax: public DoubleArray(IntPtr Pointer, ulong Count, ulong Alignment)...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr.type.DoubleArray/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "class FloatArray",
        "excerpt":"class FloatArray Summary This class represents a float array managed by the Max Environment. syntax: public class FloatArray : Array&lt;float&gt; namespace: max.process.arr.type Constructors FloatArray(IntPtr Pointer, ulong Count, ulong Alignment) Instantiates an array of floats with a given pointer, element count and alignment. syntax: public FloatArray(IntPtr Pointer, ulong Count, ulong Alignment)...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr.type.FloatArray/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "interface IDataType",
        "excerpt":"interface IDataType An interface representing a data type in the Max environment. syntax: public interface IDataType namespace: max.process.arr.type Properties DataPointer Pointer to the data. syntax: IntPtr DataPointer { get; } DataLength Length of the data in array elements, as a ulong. syntax: ulong DataLength { get; } Length Length of...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr.type.IDataType/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "class IntArray",
        "excerpt":"class IntArray Summary This class represents a signed 32-bit array managed by the Max Environment. syntax: public class IntArray : Array&lt;int&gt; namespace: max.process.arr.type Constructors IntArray(IntPtr Pointer, ulong Count, ulong Alignment) Instantiates an array of ints with a given pointer, element count and alignment. syntax: public IntArray(IntPtr Pointer, ulong Count, ulong...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr.type.IntArray/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "class LongArray",
        "excerpt":"class LongArray Summary This class represents a signed 64-bit array managed by the Max Environment. syntax: public class LongArray : Array&lt;long&gt; namespace: max.process.arr.type Constructors LongArray(IntPtr Pointer, ulong Count, ulong Alignment) Instantiates an array of longs with a given pointer, element count and alignment. syntax: public LongArray(IntPtr Pointer, ulong Count, ulong...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr.type.LongArray/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "class MaxObject",
        "excerpt":"class MaxObject Summary An abstract class that represents an object in the max environment. syntax: public abstract class MaxObject namespace: max.process.arr.type Properties Disposed Whether or not this object has been disposed. If not disposed, it has not been removed from the Max Library Machine kernel’s space yet. syntax: public bool...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr.type.MaxObject/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "class MaxProperty",
        "excerpt":"class MaxProperty Summary An attribute used on MaxObject properties to add them to the Max Environment. syntax: public class MaxProperty : Attribute namespace: max.process.arr.type Constructors MaxProperty() Initializes the property with the name of the calling member. syntax: public MaxProperty([CallerMemberName] string propertyName = null) Parameters Type Name Description [CallerMemberName] propertyName Calling...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr.type.MaxProperty/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "class SByteArray",
        "excerpt":"class SByteArray Summary This class represents a signed byte array managed by the Max Environment. syntax: public class SByteArray : Array&lt;sbyte&gt; namespace: max.process.arr.type Constructors SByteArray(IntPtr Pointer, ulong Count, ulong Alignment) Instantiates an array of sbytes with a given pointer, element count and alignment. syntax: public SByteArray(IntPtr Pointer, ulong Count, ulong...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr.type.SByteArray/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "class ShortArray",
        "excerpt":"class ShortArray Summary This class represents a signed 16-bit array managed by the Max Environment. syntax: public class ShortArray : Array&lt;short&gt; namespace: max.process.arr.type Constructors ShortArray(IntPtr Pointer, ulong Elements, ulong Alignment) Instantiates an array of shorts with a given pointer, element count and alignment. syntax: public ShortArray(IntPtr Pointer, ulong Elements, ulong...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr.type.ShortArray/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "class UIntArray",
        "excerpt":"class UIntArray Summary This class represents an unsigned 32-bit array managed by the Max Environment. syntax: public class UIntArray : Array&lt;uint&gt; namespace: max.process.arr.type Constructors UIntArray(IntPtr Pointer, ulong Count, ulong Alignment) Instantiates an array of uints with a given pointer, element count and alignment. syntax: public UIntArray(IntPtr Pointer, ulong Count, ulong...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr.type.UIntArray/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "class ULongArray",
        "excerpt":"class ULongArray Summary This class represents an unsigned 64-bit array managed by the Max Environment. syntax: public class ULongArray : Array&lt;ulong&gt; namespace: max.process.arr.type Constructors ULongArray(IntPtr Pointer, ulong Count, ulong Alignment) Instantiates an array of ulongs with a given pointer, element count and alignment. syntax: public ULongArray(IntPtr Pointer, ulong Count, ulong...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr.type.ULongArray/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "class UShortArray",
        "excerpt":"class UShortArray Summary This class represents an unsigned 16-bit array managed by the Max Environment. syntax: public class UShortArray : Array&lt;ushort&gt; namespace: max.process.arr.type Constructors UShortArray(IntPtr Pointer, ulong Count, ulong Alignment) Instantiates an array of ushorts with a given pointer, element count and alignment. syntax: public UShortArray(IntPtr Pointer, ulong Count, ulong...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr.type.UShortArray/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "namespace max.process.arr.type",
        "excerpt":"namespace max.process.arr.type Members IDataType - An interface representing a data type in the Max environment. MaxProperty - An attribute used on MaxObject properties to add them to the Max Environment. MaxObject - An abstract class that represents an object in the max environment. Array &lt; T &gt; - This abstract...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.arr.type/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "class MaxEnvironment",
        "excerpt":"class MaxEnvironment  Summary  This class represents the Environment Controller for the MaxEnvironment. This controller manages the various components of the MaxEnvironment and acts as a public interface outside of the library for the MaxEnvironment.   syntax: public class MaxEnvironment  namespace: max.process.env  ","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.env.MaxEnvironment/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "namespace max.process.env",
        "excerpt":"namespace max.process.env   Members      MaxEnvironment - This class represents the Environment Controller for the MaxEnvironment. This controller manages the various components of the MaxEnvironment and acts as a public interface outside of the library for the MaxEnvironment.  ","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process.env/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "namespace max.process",
        "excerpt":"namespace max.process   Members      max.process.env   max.process.arr  ","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/apidocs/csharp/max.process/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "About",
        "excerpt":"About  ","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/max/meta/about/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "Contributing",
        "excerpt":"Notice: these documentation pages are a work in progress and will change periodically as they are updated. Contributing Max Library Machine is always open to new contributions… Directory Structure The current directory structure of the project, with the common repository files removed, is like this: - bin - c -...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/max/meta/contributing/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "License",
        "excerpt":"MIT License Copyright 2020 Roger Merryfield Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/max/meta/license/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "Installation",
        "excerpt":"Notice: these documentation pages are a work in progress and will change periodically as they are updated.   Installation  ","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/max/users/installation/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "Introduction",
        "excerpt":"Notice: these documentation pages are a work in progress and will change periodically as they are updated. Introduction Max Library Machine is a collection of small programs that work together to build a video game environment. When compiled and configured, it forms a game engine that allows the internal topology...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/max/users/introduction/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "max.game",
        "excerpt":"max.game max.game represents the entire game. Game - An abstract class that contains all the game functions a developer uses to interact with Max Library Machine. Game Methods - Methods in the Game class used to manage the state of the game. Control - Methods for controlling the flow of...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/game/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "Game",
        "excerpt":"Game Game is an abstract class that represents the game’s main interface for the library. To use it, create your own class that inherits Game and implement OnInitialize(), OnUpdate() and OnExit(). using max.game; namespace mygame { public class MyGame : Game { // put the initialize code here protected override...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/game/Game/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "max.game",
        "excerpt":"max.game max.game represents the entire game. Game - An abstract class that contains all the game functions a developer uses to interact with Max Library Machine. Game Methods - Methods in the Game class used to manage the state of the game. Control - Methods for controlling the flow of...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/game/max.game/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "Game Methods",
        "excerpt":"Game Methods   Game contains many static methods for managing the current game. This section helps explain these methods.      Control - Methods for controlling the flow of the game.   Instance Management - Methods for managing instances.   Datatypes - Methods for managing data types.  ","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/game/method/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "Control Methods",
        "excerpt":"Control Methods Control methods help maintain the flow of the game and manage functions such as game initialization, updating and exiting. Methods Run() Runs the game. It will keep running until an error occurs or if Game.Exit() is called. // Runs the game Game.Run(); Initialize() An internal method that runs...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/game/method/control/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "Datatype Methods",
        "excerpt":"Datatype Methods   Datatype methods allow you to create Datatypes easily.   Methods   DatatypeCreate()   Create an instance of a given datatype.   // creates a bool array of length 10. BoolArray array = Game.DatatypeCreate&lt;BoolArray&gt;(10);  ","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/game/method/datatype/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "Instance Management",
        "excerpt":"Instance Management Instance management methods allow you to create, destroy and check if instances exist in the Max Environment. Methods InstanceCreate() To create an instance of a MaxObject class, use InstanceCreate(). // creates a new instance of TestObject TestObject obj = Game.InstanceCreate&lt;TestObject&gt;(); This will create a new instance of your...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/game/method/instance/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "Guidelines",
        "excerpt":"Project Guidelines This is a list of basic coding guidelines to help you both understand how the main Max Library Machine C# project is structured as well as helping guide developers on how to make clean contributions. General Guidelines Minimum Design Requirements While Max Library Machine C# is designed to...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/guidelines/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "Max Library Machine C#",
        "excerpt":"Max Library Machine C# Max Library Machine C# (also called Max C#) is a library (maxlib.dll) that creates an object environment for game development with mxker in C#. In-Depth Documentation This documentation goes into depth about the internal structure of our implementation of Max Library Machine C# - forks should...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/index/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "Reflection Utilities",
        "excerpt":"Reflection Utilities These utilities allow you to use C# reflection to gather information on methods and types. Please note that reflection is computationally expensive and should be minimized as much as possible. String Conversion Types can be converted into a string with the method TypeToString(), which will convert it to...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/maux/ReflectionUtil/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "max.aux",
        "excerpt":"max.aux   max.aux represents auxillary classes and structs that are used throughout the Max Library Machine.      ReflectionUtil - Reflect on types.  ","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/maux",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "max.process",
        "excerpt":"max.process   max.process represents classes that help connect the UI, duplex and kernel components into a single abstract environment.      arr   env  ","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/process/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "max.process.arr",
        "excerpt":"max.process.arr   max.process.arr manages arrays, and contains data types and objects.      ArrayManager - The class that manages arrays in memory.   MaxProperty - Attribute for Max Properties.   Types - Types managed by the Max Library Machine.            Arrays - Array types.       MaxObject - The base Max Library Object type.          ","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/process/arr/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "ArrayManager",
        "excerpt":"ArrayManager The ArrayManager class is an internal class that manages arrays allocated in memory. Allocation and Referencing When you create a new array, it needs to have space in memory allocated for it. CreateArrayBlock&lt;T&gt;() is called in the array’s Construct method, called when an array is instantiated. Memory segments generated...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/process/arr/ArrayManager/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "Types",
        "excerpt":"Types Types are managed data in the Max Library Machine. You can add types to your MaxObjects so they can be managed by Max. All types contained natively in Max are public. Provided Types The following types are provided natively by Max Library Machine: bool byte sbyte short ushort int...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/process/arr/Type/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "Arrays",
        "excerpt":"Arrays Arrays are classes that extend Array&lt;T&gt; and work like regular arrays of given types, but directly interact with Max Library Machine’s memory space. Arrays manage their memory with the ArrayManager class, and Array&lt;T&gt; acts as an interface to interact with this memory. Using Arrays Arrays can be instantiated and...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/process/arr/Type/Arrays/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "MaxObject",
        "excerpt":"MaxObject MaxObject is an abstract class that represents an object in the Max Environment. UID UID is the ulong unique ID assigned to the object by the kernel. Control Control methods are called during certain points of the life cycle of the MaxObject. Some methods are internal and are called...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/process/arr/Type/MaxObject/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "MaxProperty",
        "excerpt":"MaxProperty MaxProperty is an attribute that can be assigned to properties in a class that inherits MaxObject to signify properties that are part of the Max Library Machine. Declaration You can declare any object that implements IDataType as a MaxProperty, for example arrays. [MaxProperty] public IntArray Values { get =&gt;...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/process/arr/Type/MaxProperty/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "max.process.env",
        "excerpt":"max.process.env   max.process.env represents the virtual environment for MaxObjects.      MaxEnvironment - Represents the Max Environment.  ","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/process/env/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "MaxEnvironment",
        "excerpt":"MaxEnvironment MaxEnvironment is a class that represents objects and instances in the Max Environment. Type Templates When Game initializes, MaxEnvironment will use reflection to collect information about certain types used in the Max Environment. MaxObjectTypes - Dictionary&lt;Type, MaxObject&gt; that contains a set of template instances of every class that inherits...","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/process/env/MaxEnvironment/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "Max Library Machine C#",
        "excerpt":"Structure   write about the structure here some time  ","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxcsharp/structure/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "Developer Docs",
        "excerpt":"Max Library Machine Developer Docs      Max Library Machine is documented to allow developers and end users to understand, modify and reimplement Max as they see fit.   Select one of the topics to the left to get started.  ","categories": [],
        "tags": [],
        "url": "max-lib.com/docs/maxlib/",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      },{
        "title": "Welcome to Max!",
        "excerpt":"Hello, and welcome to the new Max Library Machine website and blog! I spent the last week setting up the skeleton of the website, please pardon the mess as I continue to build out the website and its documentation. Introduction Max Library Machine is a game engine built with accessibility...","categories": ["update","intro"],
        "tags": [],
        "url": "max-lib.com/update/intro/2020/05/21/test.html",
        "teaser": "max-lib.com/assets/images/maxlogo_big.png"
      }]

﻿using System;

namespace consoletest
{
    public class Game : max.game.Game
    {
        TestObject Test;

        protected override void OnExit()
        {

        }

        protected override void OnInitialize()
        {
            Test = InstanceCreate<TestObject>();
            test();
        }

        protected override void OnUpdate()
        {

        }


        // alloc test

        private void test()
        {
            Console.WriteLine("ok");
            /*
            HeapAllocator alloc = new HeapAllocator();

            // can I do raw allocation?
            IntPtr p = alloc.Allocate(Arithmetic.ByteSizeOf<ulong>(64));
            Console.WriteLine(p.ToString("X16"));
            alloc.Deallocate(p, Arithmetic.ByteSizeOf<ulong>(64));

            // can I do array projection?
            ArrayManager arrays = new ArrayManager(alloc);

            IntPtr q = arrays.CreateArrayBlock<long>(4, 16);

            // we have a bunch of different views! >:-)
            ULongArray ulongs = arrays.ProjectULongs(q);
            UIntArray uints = arrays.ProjectUInts(q);
            UShortArray ushorts = arrays.ProjectUShorts(q);
            ByteArray bytes = arrays.ProjectBytes(q);

            Console.WriteLine("Allocated 4 ulongs, align at least to 16 bytes.");

            Console.WriteLine("ulongs.ElementByteSize " + ulongs.ElementByteSize);
            Console.WriteLine("uints.ElementByteSize " + uints.ElementByteSize);
            Console.WriteLine("ushorts.ElementByteSize " + ushorts.ElementByteSize);
            Console.WriteLine("ubytes.ElementByteSize " + bytes.ElementByteSize);

            Console.WriteLine("ulongs.ByteLength " + ulongs.ByteLength);
            Console.WriteLine("uints.ByteLength " + uints.ByteLength);
            Console.WriteLine("ushorts.ByteLength " + ushorts.ByteLength);
            Console.WriteLine("ubytes.ByteLength " + bytes.ByteLength);

            Console.WriteLine("ulongs.Length " + ulongs.Length);
            Console.WriteLine("uints.Length " + uints.Length);
            Console.WriteLine("ushorts.Length " + ushorts.Length);
            Console.WriteLine("ubytes.Length " + bytes.Length);

            Console.WriteLine("ulongs.Alignment " + ulongs.Alignment);
            Console.WriteLine("uints.Alignment " + uints.Alignment);
            Console.WriteLine("ushorts.Alignment " + ushorts.Alignment);
            Console.WriteLine("ubytes.Alignment " + bytes.Alignment);

            Console.WriteLine("Filling memtest number 0x5A3C0FF0 in uints");
            for (int i = 0; i < uints.Length; ++i)
                uints[i] = 0x5A3C0FF0;

            Console.WriteLine("Reading from each view safely");

            Console.Write("bytes");
            for (int i = 0; i < bytes.Length; ++i)
            {
                Console.Write(" ");
                Console.Write(bytes[i].ToString("X2"));
            }
            Console.WriteLine("");

            Console.Write("ushorts");
            for (int i = 0; i < ushorts.Length; ++i)
            {
                Console.Write(" ");
                Console.Write(ushorts[i].ToString("X4"));
            }
            Console.WriteLine("");

            Console.Write("uints");
            for (int i = 0; i < uints.Length; ++i)
            {
                Console.Write(" ");
                Console.Write(uints[i].ToString("X8"));
            }
            Console.WriteLine("");

            Console.Write("ulongs");
            for (int i = 0; i < ulongs.Length; ++i)
            {
                Console.Write(" ");
                Console.Write(ulongs[i].ToString("X16"));
            }
            Console.WriteLine("");

            Console.WriteLine("Reading from each view quickly");

            int nbytes = bytes.Length, nshorts = ushorts.Length,
                nints = uints.Length, nlongs = ulongs.Length;

            unsafe
            {
                byte* pbytes = bytes.Pointer;
                ushort* pshorts = ushorts.Pointer;
                uint* pints = uints.Pointer;
                ulong* plongs = ulongs.Pointer;

                Console.Write("bytes");
                for (int i = 0; i < nbytes; ++i)
                {
                    Console.Write(" ");
                    Console.Write(pbytes[i].ToString("X2"));
                }
                Console.WriteLine("");

                Console.Write("ushorts");
                for (int i = 0; i < nshorts; ++i)
                {
                    Console.Write(" ");
                    Console.Write(pshorts[i].ToString("X4"));
                }
                Console.WriteLine("");

                Console.Write("uints");
                for (int i = 0; i < nints; ++i)
                {
                    Console.Write(" ");
                    Console.Write(pints[i].ToString("X8"));
                }
                Console.WriteLine("");

                Console.Write("ulongs");
                for (int i = 0; i < nlongs; ++i)
                {
                    Console.Write(" ");
                    Console.Write(plongs[i].ToString("X16"));
                }
                Console.WriteLine("");
            }

            // clean up array block
            arrays.DestroyArrayBlock(q);
            */
        }
    }
}

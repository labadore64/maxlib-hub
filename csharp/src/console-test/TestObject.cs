﻿using max.process.arr.type;

namespace consoletest
{
    public class TestObject : MaxObject
    {

        protected override MaxObject InstanceCreate()
        {
            return new TestObject();
        }

        [MaxProperty]
        public CharArray Chars
        {
            get => (CharArray)get();
            set => set(value);
        }

        public override void Create()
        {
            Chars = new char[] { 'a', 'b', 'c' };
        }

    }
}

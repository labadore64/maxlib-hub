﻿
using System;

namespace consoletest
{
    class MainClass
    {
        public static void Main(string[] args)
        {

            Game Game = new Game();
            Game.Run();

            Console.WriteLine("Game Complete");
        }

    }
}

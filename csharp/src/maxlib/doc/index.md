﻿# Max Library Machine

The Max Library Machine is a C# library that creates a game engine through mxker.

To make a game, you must create a class that inherits ``Game`` from ``max.game``. Then, call the game's ``Run()`` method.

```
// create a new instance of MyGame,
// which inherits Game
MyGame game = new MyGame();

// run the game
game.Run();
```

## Access

Only components found in the [API Documentation](../api/index.md) are visible outside of the library. All other components are ``internal``.

## Components

* [game](../max/game/doc/index.md)
* [process](../max/process/doc/index.md)
* kernel
* UI
* [aux](../max/maux/doc/index.md)
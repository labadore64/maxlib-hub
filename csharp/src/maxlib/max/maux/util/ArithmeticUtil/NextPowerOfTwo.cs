﻿// 2020-04-18
// allocator now handles alignment in the same way mxmem does
//
// 2020-04-17
// added documentation
// added missing ProjectDoubles method to ArrayManager
// added commentary regarding Max Library Machine integration
//
// 2020-04-16
// initial version
//
// William 

// Obviously, you have an established system for type constants and avoiding
// reflection that is outside the scope of this. The intent is for this to be
// integrated with what you have. Using this, each property may be stored as an
// IntPtr, so an array or table of properties is very light. The idea is that
// when a property is retrieved, a transient array projection is returned that
// provides an indexing method and a way to provide an actual pointer (inside of
// an "unsafe" block, which doesn't appear to be a limitation that can be
// circumvented).

using System;

namespace max.aux
{
    internal static partial class Arithmetic
    {
        // the ole right smear
        internal static ulong NextPowerOfTwo(ulong n)
        {
            n |= n >> 0x01; // smear 1
            n |= n >> 0x02; // smear 2
            n |= n >> 0x04; // smear 4
            n |= n >> 0x08; // smear 8
            n |= n >> 0x10; // smear 16
            n |= n >> 0x20; // smear 32
            return n + 1;
        }

        internal static uint NextPowerOfTwo(uint n)
        {
            n |= n >> 0x01; // smear 1
            n |= n >> 0x02; // smear 2
            n |= n >> 0x04; // smear 4
            n |= n >> 0x08; // smear 8
            n |= n >> 0x10; // smear 16
            return n + 1;
        }

        // the ole right smear
        internal static ushort NextPowerOfTwo(ushort n)
        {
            n |= (ushort)(n >> 0x01); // smear 1
            n |= (ushort)(n >> 0x02); // smear 2
            n |= (ushort)(n >> 0x04); // smear 4
            n |= (ushort)(n >> 0x08); // smear 8
            return (ushort)(n + 1);
        }

        // the ole right smear
        internal static byte NextPowerOfTwo(byte n)
        {
            n |= (byte)(n >> 0x01); // smear 1
            n |= (byte)(n >> 0x02); // smear 2
            n |= (byte)(n >> 0x04); // smear 4
            return (byte)(n + 1);
        }
    }
}

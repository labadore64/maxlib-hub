﻿// 2020-04-18
// allocator now handles alignment in the same way mxmem does
//
// 2020-04-17
// added documentation
// added missing ProjectDoubles method to ArrayManager
// added commentary regarding Max Library Machine integration
//
// 2020-04-16
// initial version
//
// William 

// Obviously, you have an established system for type constants and avoiding
// reflection that is outside the scope of this. The intent is for this to be
// integrated with what you have. Using this, each property may be stored as an
// IntPtr, so an array or table of properties is very light. The idea is that
// when a property is retrieved, a transient array projection is returned that
// provides an indexing method and a way to provide an actual pointer (inside of
// an "unsafe" block, which doesn't appear to be a limitation that can be
// circumvented).

using System;

namespace max.aux
{
    internal static partial class Arithmetic
    {
        unsafe internal static void FastCopyBools(IntPtr ptr, bool[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            bool *p = (bool *)ptr;
            fixed (bool *qfix = &elems[0])
            {
                bool *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe internal static void FastCopyBytes(IntPtr ptr, byte[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            byte *p = (byte *)ptr;
            fixed (byte *qfix = &elems[0])
            {
                byte *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe internal static void FastCopySBytes(IntPtr ptr, sbyte[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            sbyte *p = (sbyte *)ptr;
            fixed (sbyte *qfix = &elems[0])
            {
                sbyte *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe internal static void FastCopyChars(IntPtr ptr, char[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            char *p = (char *)ptr;
            fixed (char *qfix = &elems[0])
            {
                char *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe internal static void FastCopyShorts(IntPtr ptr, short[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            short *p = (short *)ptr;
            fixed (short *qfix = &elems[0])
            {
                short *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe internal static void FastCopyUShorts(IntPtr ptr, ushort[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            ushort *p = (ushort *)ptr;
            fixed (ushort *qfix = &elems[0])
            {
                ushort *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe internal static void FastCopyInts(IntPtr ptr, int[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            int *p = (int *)ptr;
            fixed (int *qfix = &elems[0])
            {
                int *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe internal static void FastCopyUInts(IntPtr ptr, uint[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            uint *p = (uint *)ptr;
            fixed (uint *qfix = &elems[0])
            {
                uint *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe internal static void FastCopyLongs(IntPtr ptr, long[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            long *p = (long *)ptr;
            fixed (long *qfix = &elems[0])
            {
                long *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe internal static void FastCopyULongs(IntPtr ptr, ulong[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            ulong *p = (ulong *)ptr;
            fixed (ulong *qfix = &elems[0])
            {
                ulong *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe internal static void FastCopyFloats(IntPtr ptr, float[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            float *p = (float *)ptr;
            fixed (float *qfix = &elems[0])
            {
                float *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }

        unsafe internal static void FastCopyDoubles(IntPtr ptr, double[] elems, int n)
        {
            // the copy should be ascending and incrementing to maximize
            // throughput
            double *p = (double *)ptr;
            fixed (double *qfix = &elems[0])
            {
                double *q = qfix;
                while (n-- > 0)
                    *p++ = *q++;
            }
        }
    }
}

﻿// 2020-04-18
// allocator now handles alignment in the same way mxmem does
//
// 2020-04-17
// added documentation
// added missing ProjectDoubles method to ArrayManager
// added commentary regarding Max Library Machine integration
//
// 2020-04-16
// initial version
//
// William 

// Obviously, you have an established system for type constants and avoiding
// reflection that is outside the scope of this. The intent is for this to be
// integrated with what you have. Using this, each property may be stored as an
// IntPtr, so an array or table of properties is very light. The idea is that
// when a property is retrieved, a transient array projection is returned that
// provides an indexing method and a way to provide an actual pointer (inside of
// an "unsafe" block, which doesn't appear to be a limitation that can be
// circumvented).

namespace max.aux
{
    internal static partial class Arithmetic
    {

        internal static double LongToDouble(long li)
        {
            unsafe
            {
                long *pli = &li;
                return *((double *)pli);
            }
        }

        internal static long DoubleToLong(double d)
        {
            unsafe
            {
                double *pd = &d;
                return *((long *)pd);
            }
        }

        internal static float IntToFloat(int i)
        {
            unsafe
            {
                int *pi = &i;
                return *((float *)pi);
            }
        }

        internal static int FloatToInt(float f)
        {
            unsafe
            {
                float *pf = &f;
                return *((int *)pf);
            }
        }

        internal static int BoolToInt(bool b)
        {
            unsafe
            {
                bool *pb = &b;
                return *((int *)pb);
            }
        }

        internal static bool IntToBool(int i)
        {
            unsafe
            {
                int *pi = &i;
                return *((bool *)pi);
            }
        }
    }
}

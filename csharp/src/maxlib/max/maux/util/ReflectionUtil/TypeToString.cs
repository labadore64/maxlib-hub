﻿using System;

namespace max.aux.util
{
    internal static partial class ReflectionUtil
    {
        /// <summary>
        /// Returns the full name of the type and its assembly, delimited by ", ".
        /// </summary>
        /// <param name="Type">Type</param>
        /// <returns>Full name and assembly name in string</returns>
        public static string FullTypeToString(Type Type)
        {
            return Type.FullName + ", " + Type.Assembly.GetName().Name;
        }


        /// <summary>
        /// Returns the name of the type and its assembly, delimited by ", ".
        /// </summary>
        /// <param name="Type">Type</param>
        /// <returns>Full name and assembly name in string</returns>
        public static string TypeToString(Type Type)
        {
            return Type.Name + ", " + Type.Assembly.GetName().Name;
        }

    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace max.aux.util
{
    internal static partial class ReflectionUtil
    {
        /// <summary>
        /// Returns all types in the current loaded assemblies that inherit the passed type.
        /// </summary>
        /// <param name="Type">Type to check</param>
        /// <returns>List of types found</returns>
        public static List<Type> GetTypesContainingType(Type Type)
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(s => s.GetTypes())
                    .Where(Type.IsAssignableFrom).ToList();
        }
    }
}


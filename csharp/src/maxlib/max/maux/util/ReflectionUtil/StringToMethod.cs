﻿using System;
using System.Reflection;

namespace max.aux.util
{
    internal static partial class ReflectionUtil
    {

        /// <summary>
        /// Gets the method from a string.
        /// </summary>
        /// <param name="MethodID">Method's ID string</param>
        /// <returns>Method</returns>
        public static MethodInfo FullStringToMethod(string MethodID)
        {
            // gets the index to split on
            int index = MethodID.LastIndexOf(":");

            string type;

            if (index > 0)
            {
                // split the string in two - the left side is the type, the right side is the Method
                type = MethodID.Substring(0, index);
                MethodID = MethodID.Substring(index + 1, MethodID.Length - index - 1);
            }
            else
            {
                // if the split fails throw an exception
                throw new ArgumentException("MethodID must be be delimited by \":\". The left should contain the" +
                    " method's type and assembly, the right should be the name of the method.");
            }

            try
            {
                // Get the type of a specified class.
                return Type.GetType(type).GetMethod(MethodID);
            }
            catch (TypeLoadException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return null;
        }

        /// <summary>
        /// Gets the method from a string.
        /// </summary>
        /// <param name="MethodID">Method's ID string</param>
        /// <returns>Method</returns>
        public static MethodInfo StringToMethod(string MethodID)
        {
            // gets the index to split on
            int index = MethodID.LastIndexOf(":");

            string type;

            if (index > 0)
            {
                // split the string in two - the left side is the type, the right side is the Method
                type = MethodID.Substring(0, index);
                MethodID = MethodID.Substring(index + 1, MethodID.Length - index - 1);
            }
            else
            {
                // if the split fails throw an exception
                throw new ArgumentException("MethodID must be be delimited by \":\". The left should contain the" +
                    " method's type and assembly, the right should be the name of the method.");
            }

            try
            {
                // Get the type of a specified class.
                return StringToType(type).GetMethod(MethodID);
            }
            catch (TypeLoadException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return null;
        }

    }
}


﻿using System;
using System.Reflection;

namespace max.aux.util
{
    internal static partial class ReflectionUtil
    {

        /// <summary>
        /// Returns the full name of the type and the method defined by the passed Delegate, delimited by ":".
        /// </summary>
        /// <param name="Delegate">Method</param>
        /// <returns>Full name and type in string</returns>
        public static string DelegateToString(Delegate Delegate)
        {
            return TypeToString(Delegate.Method.ReflectedType) + ":" + Delegate.Method.Name;
        }

        /// <summary>
        /// Returns the full name of the type and the method defined by the passed Delegate, delimited by ":".
        /// </summary>
        /// <param name="Delegate">Method</param>
        /// <returns>Full name and type in string</returns>
        public static string FullDelegateToString(Delegate Delegate)
        {
            return FullTypeToString(Delegate.Method.ReflectedType) + ":" + Delegate.Method.Name;
        }

        /// <summary>
        /// Returns the full name of the type and the method defined by the passed MethodInfo, delimited by ":".
        /// </summary>
        /// <param name="Method">Method</param>
        /// <returns>Full name and type in string</returns>
        public static string MethodToString(MethodInfo Method)
        {
            return FullTypeToString(Method.ReflectedType) + ":" + Method.Name;
        }

        /// <summary>
        /// Returns the method and its type and assembly, delimited by ".".
        /// </summary>
        /// <param name="Method">Method</param>
        /// <returns>Full name and assembly name in string</returns>
        public static string FullMethodToString(MethodInfo Method)
        {
            return FullTypeToString(Method.ReflectedType) + "." + Method.Name;
        }
    }
}


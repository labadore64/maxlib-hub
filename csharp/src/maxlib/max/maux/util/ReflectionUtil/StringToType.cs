﻿using System;
using System.Linq;

namespace max.aux.util
{
    internal static partial class ReflectionUtil
    {
        /// <summary>
        /// Gets the type from a full type string.
        /// </summary>
        /// <param name="TypeID">Type string</param>
        /// <returns>Type</returns>
        public static Type FullStringToType(string TypeID)
        {
            return Type.GetType(TypeID);
        }

        /// <summary>
        /// Gets the type from a type string.
        /// </summary>
        /// <param name="TypeID">Type string</param>
        /// <returns>Type</returns>
        public static Type StringToType(string TypeID)
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .First(p => p.AssemblyQualifiedName.Contains(TypeID));
        }
    }
}


﻿using System;

namespace max.process.arr.type
{
    /// <summary>
    /// Represents a prototype of a property that is turned into an
    /// actual property when an object is instantiated.
    /// </summary>
    internal struct PrototypeProperty
    {
        /// <summary>
        /// Name of the property.
        /// </summary>
        /// <value>Name</value>
        internal string Name { get; private set; }

        /// <summary>
        /// Type of the property.
        /// </summary>
        /// <value>Type</value>
        internal Type DataType { get; private set; }

        /// <summary>
        /// Initializes a new instance of a prototype property.
        /// </summary>
        /// <param name="Name">Name</param>
        /// <param name="DataType">Type</param>
        internal PrototypeProperty(string Name, Type DataType)
        {
            this.Name = Name;
            this.DataType = DataType;
        }
    }
}
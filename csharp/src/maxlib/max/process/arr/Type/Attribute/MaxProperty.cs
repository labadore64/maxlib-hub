﻿using System;
using System.Runtime.CompilerServices;

namespace max.process.arr.type
{
    /// <summary>
    /// An attribute used on MaxObject properties to add them to the Max Environment.
    /// </summary>
    [AttributeUsage
        (AttributeTargets.Property)
        ]
    public class MaxProperty : Attribute
    {
        /// <summary>
        /// Name of the property
        /// </summary>
        /// <value>Name</value>
        public string Name { get; private set; }

        /// <summary>
        /// Initializes the property with the name of the calling member.
        /// </summary>
        /// <param name="propertyName">Calling member's name.</param>
        public MaxProperty([CallerMemberName] string propertyName = null)
        {
            Name = propertyName;
        }
    }
}
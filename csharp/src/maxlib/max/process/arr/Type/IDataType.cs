﻿using System;

namespace max.process.arr.type
{
    /// <summary>
    /// An interface representing a data type in the Max environment.
    /// </summary>
    public interface IDataType
    {
        /// <summary>
        /// Pointer to the data.
        /// </summary>
        /// <value>Pointer</value>
        IntPtr DataPointer { get; }

        /// <summary>
        /// Length of the data in array elements, as a ulong.
        /// </summary>
        /// <value>Length</value>
        ulong DataLength { get; }

        /// <summary>
        /// Length of the data in array elements.
        /// </summary>
        /// <value>Length</value>
        int Length { get; }

        /// <summary>
        /// Alignment of the data.
        /// </summary>
        /// <value>Alignment</value>
        ulong Alignment { get; }

        /// <summary>
        /// Creates an instance of this datatype, of a certain length.
        /// </summary>
        /// <returns>Instance</returns>
        /// <param name="length">Length</param>
        IDataType InstanceCreate(ulong length);

        /// <summary>
        /// Sets the data in this datatype to be the same as a different datatype.
        /// </summary>
        /// <param name="Data">Data</param>
        void SetData(IDataType Data);
    }
} 

﻿namespace max.process.arr.type
{
    public abstract partial class MaxObject
    {
        /// <summary>
        /// Creates a new empty instance of this object.
        /// </summary>
        /// <returns>New instance</returns>
        protected abstract MaxObject InstanceCreate();

        internal MaxObject InstanceCreateInternal()
        {
            MaxObject instance = InstanceCreate();
            instance.AddPropertiesFromPrototypes(Prototypes);
            return instance;
        }
    }
}

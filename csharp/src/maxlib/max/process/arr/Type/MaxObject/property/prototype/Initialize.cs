﻿using System;
using System.Linq;
using System.Reflection;

namespace max.process.arr.type
{

    public partial class MaxObject
    {
        /// <summary>
        /// Initializes the prototypes that represent properties for this type.
        /// </summary>
        internal void InitializePropertyPrototypes()
        { 
            // Get all methods in this class, and put them
            // in an array of System.Reflection.MemberInfo objects.
            PropertyInfo[] MyMemberInfo = GetType().
                            GetProperties().Where(
                                p => (MaxProperty)
                                Attribute.GetCustomAttribute(
                                    p, typeof(MaxProperty)) != null)
                           .ToArray();

            // set the prototype properties array
            // to the length of the members
            Prototypes = new PrototypeProperty[MyMemberInfo.Length];

            // Loop through all methods in this class that are in the
            // MyMemberInfo array.
            for (int i = 0; i < MyMemberInfo.Length; i++)
            {
                Prototypes[i] = new PrototypeProperty(MyMemberInfo[i].Name, MyMemberInfo[i].PropertyType);
            }
        }
    }
}

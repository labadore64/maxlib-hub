﻿namespace max.process.arr.type
{

    public abstract partial class MaxObject
    {
        /// <summary>
        /// The object's creation method. The developer can override this method
        /// so that custom routines can be defined. If not overriden, this method does nothing.
        /// </summary>
        public virtual void Create()
        {

        }
    }
}

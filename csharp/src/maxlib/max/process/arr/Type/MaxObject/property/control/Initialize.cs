﻿namespace max.process.arr.type
{

    public abstract partial class MaxObject
    {
        /// <summary>
        /// Whether or not this object has been initialized. If not initialized,
        /// it does not exist in the Max Library Machine kernel's space yet.
        /// </summary>
        /// <value>True/False</value>
        public bool Initialized { get; private set; }
    }
}

﻿namespace max.process.arr.type
{

    public abstract partial class MaxObject
    {
        /// <summary>
        /// The max UID value, used to identify this object in the kernel.
        /// </summary>
        /// <value>Unique</value>
        public ulong UID { get; private set; }
    }
}

﻿namespace max.process.arr.type
{
    public partial class MaxObject
    {
        /// <summary>
        /// Sets a property's value.
        /// </summary>
        /// <param name="Value">Value</param>
        protected void set(IDataType Value, [System.Runtime.CompilerServices.CallerMemberName] string Name = "")
        {
            try
            {
                Properties[Name].SetData(Value);
            }
            catch
            {
                throw new System.ArgumentException("Property " + Name + " appears to not have been initialized. Did you add [MaxProperty] Attribute?");
            }
        }
    }
}

﻿namespace max.process.arr.type
{

    public abstract partial class MaxObject
    {
        /// <summary>
        /// Initializes this MaxObject and prepares it to be created in the kernel.
        /// </summary>
        /// <param name="MaxUnique">Unique</param>
        internal void Initialize(ulong MaxUnique)
        {
            // sets the unique.
            UID = MaxUnique;

            // sets the initialized state to true.
            Initialized = true;
        }
    }
}

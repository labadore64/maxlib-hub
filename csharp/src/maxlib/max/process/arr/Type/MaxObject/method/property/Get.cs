﻿namespace max.process.arr.type
{
    public partial class MaxObject
    {
        /// <summary>
        /// Gets a property's value.
        /// </summary>
        /// <returns>Value</returns>
        protected IDataType get([System.Runtime.CompilerServices.CallerMemberName] string Name = "")
        {
            try
            {
                return Properties[Name];
            }
            catch
            {
                throw new System.ArgumentException("Property " + Name + " appears to not have been initialized. Did you add [MaxProperty] Attribute?");
            }
        }
    }
}

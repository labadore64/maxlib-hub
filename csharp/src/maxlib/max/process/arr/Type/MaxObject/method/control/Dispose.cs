﻿using max.game;

namespace max.process.arr.type
{

    public abstract partial class MaxObject
    {
        /// <summary>
        /// Disposes this MaxObject and prepares it to be destroyed in the kernel.
        /// </summary>
        public void Dispose()
        {
            // runs the Destroy routine
            Destroy();

            // Stage the object to be destroyed
            Game.CurrentGame.Environment.StageDestroyedInstance(this);

            // sets the disposed state to true.
            Disposed = true;
        }
    }
}

﻿namespace max.process.arr.type
{

    public abstract partial class MaxObject
    {
        /// <summary>
        /// The object's destruction method. The developer can override this method
        /// so that custom routines can be defined. 
        /// The developer should release resources and destroy child objects
        /// with this method as well as general destruction methods.
        /// If not overriden, this method does nothing.
        /// </summary>
        public virtual void Destroy()
        {

        }
    }
}

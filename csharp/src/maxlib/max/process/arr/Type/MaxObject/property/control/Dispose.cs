﻿namespace max.process.arr.type
{

    public abstract partial class MaxObject
    {
        /// <summary>
        /// Whether or not this object has been disposed. If not disposed,
        /// it has not been removed from the Max Library Machine kernel's space yet.
        /// </summary>
        /// <value>True/False</value>
        public bool Disposed { get; private set; }
    }
}

﻿using System.Collections.Generic;

namespace max.process.arr.type
{

    public partial class MaxObject
    {
        /// <summary>
        /// The dictionary of this object's mxker properties.
        /// </summary>
        /// <value>Properties</value>
        Dictionary<string, IDataType> Properties { get; set; } = new Dictionary<string, IDataType>();
    }
}

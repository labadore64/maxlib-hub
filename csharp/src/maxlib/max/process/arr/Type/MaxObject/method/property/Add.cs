﻿using max.game;

namespace max.process.arr.type
{
    public partial class MaxObject
    {
        /// <summary>
        /// Adds a new property to the object's property list.
        /// </summary>
        /// <returns>Property ID</returns>
        /// <param name="Name">Name</param>
        /// <param name="Type">Type</param>
        protected void AddProperty(string Name, IDataType Type)
        {
            Properties.Add(Name, Type);
        }

        internal void AddPropertiesFromPrototypes(PrototypeProperty[] _prototypes)
        {
            for (int i = 0; i < _prototypes.Length; i++)
            {
                AddProperty(_prototypes[i].Name, Game.DatatypeCreate(_prototypes[i].DataType));
            }
        }
    }
}

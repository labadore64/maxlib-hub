﻿using System.Runtime.InteropServices;
using max.aux;

namespace max.process.arr.type
{
    public abstract partial class Array<T> : IDataType where T : unmanaged
    {
        /// <summary>
        /// Gets the value at a certain index as a byte.
        /// </summary>
        /// <returns>Byte value</returns>
        /// <param name="Index">Index</param>
        protected byte getByte(int Index)
        {
#if DEBUG
            _ckIndex(Index);
#endif
            return Marshal.ReadByte(Address, Index);
        }

        /// <summary>
        /// Gets the value at a certain index as a short.
        /// </summary>
        /// <returns>Short value</returns>
        /// <param name="Index">Index</param>
        protected short getShort(int Index)
        {
#if DEBUG
            _ckIndex(Index);
#endif
            return Marshal.ReadInt16(Address, Index << 1);
        }

        /// <summary>
        /// Gets the value at a certain index as an int.
        /// </summary>
        /// <returns>Int value</returns>
        /// <param name="Index">Index</param>
        protected int getInt(int Index)
        {
#if DEBUG
            _ckIndex(Index);
#endif
            return Marshal.ReadInt32(Address, Index << 2);
        }

        /// <summary>
        /// Gets the value at a certain index as a long.
        /// </summary>
        /// <returns>Long value</returns>
        /// <param name="Index">Index</param>
        protected long getLong(int Index)
        {
#if DEBUG
            _ckIndex(Index);
#endif
            return Marshal.ReadInt64(Address, Index << 3);
        }

        /// <summary>
        /// Gets the value at a certain index as a float.
        /// </summary>
        /// <returns>Float value</returns>
        /// <param name="Index">Index</param>
        protected float getFloat(int Index)
        {
#if DEBUG
            _ckIndex(Index);
#endif
            return Arithmetic.IntToFloat(Marshal.ReadInt32(Address, Index << 2));
        }

        /// <summary>
        /// Gets the value at a certain index as a double.
        /// </summary>
        /// <returns>Double value</returns>
        /// <param name="Index">Index</param>
        protected double getDouble(int Index)
        {
#if DEBUG
            _ckIndex(Index);
#endif
            return Arithmetic.LongToDouble(Marshal.ReadInt64(Address, Index << 3));
        }

        /// <summary>
        /// Gets the value at a certain index as a boolean.
        /// </summary>
        /// <returns>Bool value</returns>
        /// <param name="Index">Index</param>
        protected bool getBool(int Index)
        {
#if DEBUG
            _ckIndex(Index);
#endif
            return Arithmetic.IntToBool(Marshal.ReadInt32(Address, Index << 2));
        }

    }
}

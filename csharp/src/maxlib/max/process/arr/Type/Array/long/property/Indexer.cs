﻿namespace max.process.arr.type
{
    // signed int64 array
    public partial class LongArray : Array<long>
    {
        /// <summary>
        /// Gets/Sets the long stored at the given index.
        /// </summary>
        /// <param name="i">Index</param>
        /// <value>Long</value>
        public long this[int i]
        {
            get { return getLong(i); }
            set { setLong(i,value); }
        }
    }
}

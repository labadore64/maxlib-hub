﻿using System;

namespace max.process.arr.type
{
    // unsigned int32 array
    public partial class UIntArray : Array<uint>
    {
        /// <summary>
        /// Instantiates an array of uints with a given pointer, element count and alignment.
        /// </summary>
        /// <param name="Pointer">Array pointer</param>
        /// <param name="Count">Number of elements</param>
        /// <param name="Alignment">Alignment</param>
        public UIntArray(IntPtr Pointer, ulong Count, ulong Alignment)
            : base(Pointer, Count, Alignment)
        { }

        /// <summary>
        /// Instantiates an array of uints with a certain number of elements.
        /// </summary>
        /// <param name="Count">Number of elements</param>
        public UIntArray(ulong Count)
            : base(Count)
        { }

        /// <summary>
        /// Instantiates an array of uints with an array.
        /// </summary>
        /// <param name="Elements">Elements</param>
        public UIntArray(uint[] Elements)
            : base(Elements)
        { }
        public UIntArray()
        {

        }
    }
}

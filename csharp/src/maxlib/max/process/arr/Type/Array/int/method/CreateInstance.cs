﻿namespace max.process.arr.type
{
    // signed int32 array
    public partial class IntArray : Array<int>
    {
        /// <summary>
        /// Creates an instance of IntArray of a specified length.
        /// </summary>
        /// <returns>Instance</returns>
        /// <param name="Length">Length</param>
        public override IDataType InstanceCreate(ulong Length)
        {
            return new IntArray(Length);
        }
    }
}

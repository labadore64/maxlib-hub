﻿namespace max.process.arr.type
{
    // unsigned byte array
    public partial class SByteArray : Array<sbyte>
    {
        /// <summary>
        /// The size of each element of the array in bytes.
        /// </summary>
        /// <value>Size</value>
        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(sbyte);
            }
        }

        /// <summary>
        /// Length of the array in bytes.
        /// </summary>
        /// <value>Byte length</value>
        public unsafe sbyte* Pointer
        {
            get { return (sbyte*)Address; }
        }

        /// <summary>
        /// Gets the real pointer to the array. Unsafe.
        /// </summary>
        /// <value>Pointer</value>
        public override ulong ByteLength
        {
            get { return nElements * sizeof(sbyte); }
        }
    }
}

﻿namespace max.process.arr.type
{
    /// <summary>
    /// This class represents a double array managed by the
    /// Max Environment.
    /// </summary>
    public partial class DoubleArray : Array<double>
    {
    }
}

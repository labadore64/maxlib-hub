﻿using max.aux;

namespace max.process.arr.type
{
    // unsigned int16 array
    public partial class UShortArray : Array<ushort>
    {
        /// <summary>
        /// Copies a source array to this one, of a certain length of elements.
        /// </summary>
        /// <param name="Array">Source</param>
        /// <param name="Length">Length</param>
        protected override void CopyFromLength(ushort[] Array, int Length)
        {
            Arithmetic.FastCopyUShorts(Address, Array, Length);
        }
    }
}

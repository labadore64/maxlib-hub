﻿namespace max.process.arr.type
{
    // unsigned byte array
    public partial class ByteArray : Array<byte>
    {
        /// <summary>
        /// Implicitly converts a byte array to an instance of ByteArray.
        /// </summary>
        /// <returns>ByteArray</returns>
        /// <param name="Value">Source</param>
        public static implicit operator ByteArray(byte[] Value) => new ByteArray(Value);
    }
}

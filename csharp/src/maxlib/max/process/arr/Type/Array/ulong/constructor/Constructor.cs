﻿using System;

namespace max.process.arr.type
{
    // unsigned int64 array
    public partial class ULongArray : Array<ulong>
    {
        /// <summary>
        /// Instantiates an array of ulongs with a given pointer, element count and alignment.
        /// </summary>
        /// <param name="Pointer">Array pointer</param>
        /// <param name="Count">Number of elements</param>
        /// <param name="Alignment">Alignment</param>
        public ULongArray(IntPtr Pointer, ulong Count, ulong Alignment)
            : base(Pointer, Count, Alignment)
        { }

        /// <summary>
        /// Instantiates an array of ulongs with a certain number of elements.
        /// </summary>
        /// <param name="Count">Number of elements</param>
        public ULongArray(ulong Count)
            : base(Count)
        { }

        /// <summary>
        /// Instantiates an array of ulongs with an array.
        /// </summary>
        /// <param name="Elements">Elements</param>
        public ULongArray(ulong[] Elements)
            : base(Elements)
        { }
        public ULongArray()
        {

        }
    }
}

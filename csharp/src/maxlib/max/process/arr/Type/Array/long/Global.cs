﻿namespace max.process.arr.type
{
    /// <summary>
    /// This class represents a signed 64-bit array managed by the
    /// Max Environment.
    /// </summary>
    public partial class LongArray : Array<long>
    {
    }
}

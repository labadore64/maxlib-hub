﻿namespace max.process.arr.type
{
    /// <summary>
    /// This class represents a signed 16-bit array managed by the
    /// Max Environment.
    /// </summary>
    public partial class ShortArray : Array<short>
    {
    }
}

﻿namespace max.process.arr.type
{
    // signed int64 array
    public partial class LongArray : Array<long>
    {
        /// <summary>
        /// Implicitly converts a long array to an instance of LongArray.
        /// </summary>
        /// <returns>LongArray</returns>
        /// <param name="Value">Source</param>
        public static implicit operator LongArray(long[] Value) => new LongArray(Value);
    }
}

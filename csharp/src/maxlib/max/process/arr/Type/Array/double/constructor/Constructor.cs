﻿using System;

namespace max.process.arr.type
{
    // double array
    public partial class DoubleArray : Array<double>
    {
        /// <summary>
        /// Instantiates an array of doubles with a given pointer, element count and alignment.
        /// </summary>
        /// <param name="Pointer">Array pointer</param>
        /// <param name="Count">Number of elements</param>
        /// <param name="Alignment">Alignment</param>
        public DoubleArray(IntPtr Pointer, ulong Count, ulong Alignment)
            : base(Pointer, Count, Alignment)
        { }

        /// <summary>
        /// Instantiates an array of doubles with a certain number of elements.
        /// </summary>
        /// <param name="Count">Number of elements</param>
        public DoubleArray(ulong Count)
            : base(Count)
        { }

        /// <summary>
        /// Instantiates an array of doubles with an array.
        /// </summary>
        /// <param name="Elements">Elements</param>
        public DoubleArray(double[] Elements)
            : base(Elements)
        { }

        public DoubleArray()
        {

        }
    }
}

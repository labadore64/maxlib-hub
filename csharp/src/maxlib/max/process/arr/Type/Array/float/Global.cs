﻿namespace max.process.arr.type
{
    /// <summary>
    /// This class represents a float array managed by the
    /// Max Environment.
    /// </summary>
    public partial class FloatArray : Array<float>
    {
    }
}


﻿namespace max.process.arr.type
{
    public partial class DoubleArray : Array<double>
    {
        /// <summary>
        /// Implicitly converts a double array to an instance of DoubleArray.
        /// </summary>
        /// <returns>DoubleArray</returns>
        /// <param name="Value">Source</param>
        public static implicit operator DoubleArray(double[] Value) => new DoubleArray(Value);
    }
}

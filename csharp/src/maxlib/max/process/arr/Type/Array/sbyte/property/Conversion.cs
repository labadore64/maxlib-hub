﻿namespace max.process.arr.type
{
    // unsigned byte array
    public partial class SByteArray : Array<sbyte>
    {
        /// <summary>
        /// Implicitly converts a sbyte array to an instance of SByteArray.
        /// </summary>
        /// <returns>SByteArray</returns>
        /// <param name="Value">Source</param>
        public static implicit operator SByteArray(sbyte[] Value) => new SByteArray(Value);
    }
}

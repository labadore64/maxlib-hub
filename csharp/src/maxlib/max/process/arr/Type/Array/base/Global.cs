﻿namespace max.process.arr.type
{
    /// <summary>
    /// This abstract class represents an array managed by the
    /// Max Environment.
    /// </summary>
    public abstract partial class Array<T> : IDataType where T : unmanaged
    {

    }
}

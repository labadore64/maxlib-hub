﻿using System;

namespace max.process.arr.type
{
    // unsigned bool array
    public partial class BoolArray : Array<bool>
    {
        /// <summary>
        /// Instantiates an array of bools with a given pointer, element count and alignment.
        /// </summary>
        /// <param name="Pointer">Array pointer</param>
        /// <param name="Count">Number of elements</param>
        /// <param name="Alignment">Alignment</param>
        public BoolArray(IntPtr Pointer, ulong Count, ulong Alignment)
            : base(Pointer, Count, Alignment)
        { }

        /// <summary>
        /// Instantiates an array of bools with a certain number of elements.
        /// </summary>
        /// <param name="Count">Number of elements</param>
        public BoolArray(ulong Count)
            : base(Count)
        { }

        /// <summary>
        /// Instantiates an array of bools with an array.
        /// </summary>
        /// <param name="Elements">Elements</param>
        public BoolArray(bool[] Elements)
            : base(Elements)
        { }

        public BoolArray()
        {

        }
    }
}

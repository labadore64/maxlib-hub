﻿namespace max.process.arr.type
{
    // char array (beware elders; these are 16 bit wide chars)
    public partial class CharArray : Array<char>
    {
        /// <summary>
        /// Gets/Sets the char stored at the given index.
        /// </summary>
        /// <param name="i">Index</param>
        /// <value>Char</value>
        public char this[int i]
        {
            get { return (char)getShort(i); }
            set { setShort(i,(short)value); }
        }
    }
}

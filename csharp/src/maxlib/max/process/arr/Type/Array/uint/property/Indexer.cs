﻿namespace max.process.arr.type
{
    // unsigned int32 array
    public partial class UIntArray : Array<uint>
    {
        /// <summary>
        /// Gets/Sets the uint stored at the given index.
        /// </summary>
        /// <param name="i">Index</param>
        /// <value>UInt</value>
        public uint this[int i]
        {
            get { return (uint)getInt(i); }
            set { setInt(i,(int)value); }
        }
    }
}

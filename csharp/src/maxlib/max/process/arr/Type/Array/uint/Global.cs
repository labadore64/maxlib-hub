﻿namespace max.process.arr.type
{
    /// <summary>
    /// This class represents an unsigned 32-bit array managed by the
    /// Max Environment.
    /// </summary>
    public partial class UIntArray : Array<uint>
    {
    }
}

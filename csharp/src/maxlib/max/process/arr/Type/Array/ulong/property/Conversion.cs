﻿namespace max.process.arr.type
{
    // unsigned int64 array
    public partial class ULongArray : Array<ulong>
    {
        /// <summary>
        /// Implicitly converts a ulong array to an instance of ULongArray.
        /// </summary>
        /// <returns>ULongArray</returns>
        /// <param name="Value">Source</param>
        public static implicit operator ULongArray(ulong[] Value) => new ULongArray(Value);
    }
}

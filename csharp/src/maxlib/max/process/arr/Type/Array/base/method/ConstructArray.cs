﻿using System;
using max.game;

namespace max.process.arr.type
{
    public abstract partial class Array<T> : IDataType where T : unmanaged
    {
        /// <summary>
        /// Creates a new array with a certain number of elements.
        /// </summary>
        /// <param name="ElementCount">Number of elements</param>
        protected void Construct(ulong ElementCount)
        {
            // create an array block, comes with a reference (ours)
            IntPtr p = Game.CurrentGame.ArrayManager.CreateArrayBlock<T>(
                        ElementCount,
                        ALIGN_DEFAULT);

            Address = p;
            nElements = ElementCount;
            nAlign = ALIGN_DEFAULT;
        }
    }
}

﻿namespace max.process.arr.type
{
    // unsigned byte array
    public partial class BoolArray : Array<bool>
    {
        /// <summary>
        /// Implicitly converts a bool array to an instance of BoolArray.
        /// </summary>
        /// <returns>BoolArray</returns>
        /// <param name="Value">Source</param>
        public static implicit operator BoolArray(bool[] Value) => new BoolArray(Value);
    }
}

﻿namespace max.process.arr.type
{
    // unsigned byte array
    public partial class BoolArray : Array<bool>
    {
        /// <summary>
        /// Gets/Sets the bool stored at the given index.
        /// </summary>
        /// <param name="i">Index</param>
        /// <value>Bool</value>
        public bool this[int i]
        {
            get { return getBool(i); }
            set { setBool(i, value); }
        }

    }
}

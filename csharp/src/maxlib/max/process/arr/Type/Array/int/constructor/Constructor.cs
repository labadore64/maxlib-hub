﻿using System;

namespace max.process.arr.type
{
    // signed int32 array
    public partial class IntArray : Array<int>
    {
        /// <summary>
        /// Instantiates an array of ints with a given pointer, element count and alignment.
        /// </summary>
        /// <param name="Pointer">Array pointer</param>
        /// <param name="Count">Number of elements</param>
        /// <param name="Alignment">Alignment</param>
        public IntArray(IntPtr Pointer, ulong Count, ulong Alignment)
            : base(Pointer, Count, Alignment)
        { }

        /// <summary>
        /// Instantiates an array of ints with a certain number of elements.
        /// </summary>
        /// <param name="Count">Number of elements</param>
        public IntArray(ulong Count)
            : base(Count)
        { }

        /// <summary>
        /// Instantiates an array of ints with an array.
        /// </summary>
        /// <param name="Elements">Elements</param>
        public IntArray(int[] Elements)
            : base(Elements)
        { }
        public IntArray()
        {

        }
    }
}

﻿namespace max.process.arr.type
{
    // unsigned int64 array
    public partial class ULongArray : Array<ulong>
    {
        /// <summary>
        /// Gets/Sets the ulong stored at the given index.
        /// </summary>
        /// <param name="i">Index</param>
        /// <value>ULong</value>
        public ulong this[int i]
        {
            get { return (ulong)getLong(i); }
            set { setLong(i,(long)value); }
        }
    }
}

﻿using max.aux;

namespace max.process.arr.type
{
    // unsigned byte array
    public partial class SByteArray : Array<sbyte>
    {
        /// <summary>
        /// Creates an instance of SByteArray of a specified length.
        /// </summary>
        /// <returns>Instance</returns>
        /// <param name="Length">Length</param>
        public override IDataType InstanceCreate(ulong Length)
        {
            return new SByteArray(Length);
        }
    }
}

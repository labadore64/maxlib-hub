﻿namespace max.process.arr.type
{
    // float array
    public partial class FloatArray : Array<float>
    {
        /// <summary>
        /// The size of each element of the array in bytes.
        /// </summary>
        /// <value>Size</value>
        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(float);
            }
        }

        /// <summary>
        /// Length of the array in bytes.
        /// </summary>
        /// <value>Byte length</value>
        public override ulong ByteLength
        {
            get { return nElements * sizeof(float); }
        }

        /// <summary>
        /// Gets the real pointer to the array. Unsafe.
        /// </summary>
        /// <value>Pointer</value>
        public unsafe float* Pointer
        {
            get { return (float*)Address; }
        }
    }
}


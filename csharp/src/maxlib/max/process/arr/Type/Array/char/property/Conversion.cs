﻿namespace max.process.arr.type
{
    // char array (beware elders; these are 16 bit wide chars)
    public partial class CharArray : Array<char>
    {
        /// <summary>
        /// Implicitly converts a char array to an instance of CharArray.
        /// </summary>
        /// <returns>CharArray</returns>
        /// <param name="Value">Source</param>
        public static implicit operator CharArray(char[] Value) => new CharArray(Value);
    }
}

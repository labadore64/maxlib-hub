﻿namespace max.process.arr.type
{
    // signed int32 array
    public partial class IntArray : Array<int>
    {
        /// <summary>
        /// Gets/Sets the int stored at the given index.
        /// </summary>
        /// <param name="i">Index</param>
        /// <value>Int</value>
        public int this[int i]
        {
            get { return getInt(i); }
            set { setInt(i,value); }
        }
    }
}

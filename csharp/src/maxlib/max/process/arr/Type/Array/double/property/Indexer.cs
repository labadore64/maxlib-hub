﻿namespace max.process.arr.type
{
    public partial class DoubleArray : Array<double>
    {
        /// <summary>
        /// Gets/Sets the double stored at the given index.
        /// </summary>
        /// <param name="i">Index</param>
        /// <value>Double</value>
        public double this[int i]
        {
            get { return getDouble(i); }
            set { setDouble(i,value); }
        }
    }
}

﻿using max.aux;

namespace max.process.arr.type
{
    // unsigned int32 array
    public partial class UIntArray : Array<uint>
    {
        /// <summary>
        /// Copies a source array to this one, of a certain length of elements.
        /// </summary>
        /// <param name="Array">Source</param>
        /// <param name="Length">Length</param>
        protected override void CopyFromLength(uint[] Array, int Length)
        {
            Arithmetic.FastCopyUInts(Address, Array, Length);
        }
    }
}

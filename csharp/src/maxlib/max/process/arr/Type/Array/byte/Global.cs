﻿namespace max.process.arr.type
{
    /// <summary>
    /// This class represents a byte array managed by the
    /// Max Environment.
    /// </summary>
    public partial class ByteArray : Array<byte>
    {
    }
}

﻿using max.aux;

namespace max.process.arr.type
{
    // unsigned int32 array
    public partial class UIntArray : Array<uint>
    {
        /// <summary>
        /// Creates an instance of UIntArray of a specified length.
        /// </summary>
        /// <returns>Instance</returns>
        /// <param name="Length">Length</param>
        public override IDataType InstanceCreate(ulong Length)
        {
            return new UIntArray(Length);
        }
    }
}

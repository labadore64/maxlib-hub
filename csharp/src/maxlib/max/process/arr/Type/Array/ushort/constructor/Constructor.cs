﻿using System;

namespace max.process.arr.type
{
    // unsigned int16 array
    public partial class UShortArray : Array<ushort>
    {
        /// <summary>
        /// Instantiates an array of ushorts with a given pointer, element count and alignment.
        /// </summary>
        /// <param name="Pointer">Array pointer</param>
        /// <param name="Count">Number of elements</param>
        /// <param name="Alignment">Alignment</param>
        public UShortArray(IntPtr Pointer, ulong Count, ulong Alignment)
            : base(Pointer, Count, Alignment)
        { }

        /// <summary>
        /// Instantiates an array of ushorts with a certain number of elements.
        /// </summary>
        /// <param name="Count">Number of elements</param>
        public UShortArray(ulong Count)
            : base(Count)
        { }

        /// <summary>
        /// Instantiates an array of ushorts with an array.
        /// </summary>
        /// <param name="Elements">Elements</param>
        public UShortArray(ushort[] Elements)
            : base(Elements)
        { }
        public UShortArray()
        {

        }
    }
}

﻿using max.game;

namespace max.process.arr.type
{
    public abstract partial class Array<T> : IDataType where T : unmanaged
    {
        /// <summary>
        /// Unreference the array block. If this is the last array to know
        /// about it, it gets destroyed.
        /// </summary>
        ~Array()
        {
            Game.CurrentGame.ArrayManager.UnrefArrayBlock(Address);
        }

    }
}

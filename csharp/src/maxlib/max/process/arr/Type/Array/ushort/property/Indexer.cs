﻿namespace max.process.arr.type
{
    // unsigned int16 array
    public partial class UShortArray : Array<ushort>
    {
        /// <summary>
        /// Gets/Sets the ushort stored at the given index.
        /// </summary>
        /// <param name="i">Index</param>
        /// <value>UShort</value>
        public ushort this[int i]
        {
            get { return (ushort)getShort(i); }
            set { setShort(i,(short)value); }
        }
    }
}

﻿using max.aux;

namespace max.process.arr.type
{
    // unsigned byte array
    public partial class BoolArray : Array<bool>
    {
        /// <summary>
        /// Copies a source array to this one, of a certain length of elements.
        /// </summary>
        /// <param name="Array">Source</param>
        /// <param name="Length">Length</param>
        protected override void CopyFromLength(bool[] Array, int Length)
        {
            Arithmetic.FastCopyBools(Address, Array, Length);
        }
    }
}

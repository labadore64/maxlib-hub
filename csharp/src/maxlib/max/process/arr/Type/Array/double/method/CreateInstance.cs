﻿namespace max.process.arr.type
{
    public partial class DoubleArray : Array<double>
    {
        /// <summary>
        /// Creates an instance of DoubleArray of a specified length.
        /// </summary>
        /// <returns>Instance</returns>
        /// <param name="Length">Length</param>
        public override IDataType InstanceCreate(ulong Length)
        {
            return new DoubleArray(Length);
        }
    }
}

﻿using System;

namespace max.process.arr.type
{
    // signed int64 array
    public partial class LongArray : Array<long>
    {
        /// <summary>
        /// Instantiates an array of longs with a given pointer, element count and alignment.
        /// </summary>
        /// <param name="Pointer">Array pointer</param>
        /// <param name="Count">Number of elements</param>
        /// <param name="Alignment">Alignment</param>
        public LongArray(IntPtr Pointer, ulong Count, ulong Alignment)
            : base(Pointer, Count, Alignment)
        { }

        /// <summary>
        /// Instantiates an array of longs with a certain number of elements.
        /// </summary>
        /// <param name="Count">Number of elements</param>
        public LongArray(ulong Count)
            : base(Count)
        { }

        /// <summary>
        /// Instantiates an array of longs with an array.
        /// </summary>
        /// <param name="Elements">Elements</param>
        public LongArray(long[] Elements)
            : base(Elements)
        { }
        public LongArray()
        {

        }
    }
}

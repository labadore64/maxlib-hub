﻿namespace max.process.arr.type
{
    // signed int16 array
    public partial class ShortArray : Array<short>
    {
        /// <summary>
        /// Gets/Sets the short stored at the given index.
        /// </summary>
        /// <param name="i">Index</param>
        /// <value>Short</value>
        public short this[int i]
        {
            get { return getShort(i); }
            set { setShort(i,value); }
        }
    }
}

﻿*Parent: [max.process.arr](../../../doc/index.md)*

# Arrays

Arrays are classes that extend ``Array<T>`` and work like regular arrays of given types, but directly interact with Max Library Machine's memory space. Arrays manage their memory with the ``ArrayManager`` class, and ``Array<T>`` acts as an interface to interact with this memory.

## Using Arrays

Arrays can be instantiated and used as an object:
```
// Instantiates an array with 1 index containing false
BoolArray array = new BoolArray(new bool[]{ false });
```

You can then access values directly with the indexer:
```
// get the value at index 0
bool value = array[0];

// set the value at index 0
array[0] = true;
```

Keep in mind that when converting from a base array type, such as ``bool[]``, to its Max Array type, such as ``BoolArray``, it will do an implicit conversion that creates a new instance of the array type. Like basic arrays, they cannot be resized after being instantiated, so you will need to create a new array if you want to change it's size.
```
// this will create a new instance of BoolArray!
BoolArray array = new bool[] { false } ;
```

When an array is created, it allocates memory for its elements. The array can be referenced in other Array objects, but when the last reference to the array is released, the array's memory is released.

## Array Functions

Array's methods and functions are inherited from ``IDataType`` but there are also a few additional properties and methods:

* ``CopyFrom()`` - Copies elements from a basic array to the Array. 
* ``ByteLength`` - the length of the array in bytes.
* ``ElementByteSize`` - how many bytes each element takes up.

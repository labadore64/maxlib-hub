﻿using System;

namespace max.process.arr.type
{
    // char array (beware elders; these are 16 bit wide chars)
    public partial class CharArray : Array<char>
    {

        /// <summary>
        /// Instantiates an array of chars with a given pointer, element count and alignment.
        /// </summary>
        /// <param name="Pointer">Array pointer</param>
        /// <param name="Count">Number of elements</param>
        /// <param name="Alignment">Alignment</param>
        public CharArray(IntPtr Pointer, ulong Count, ulong Alignment)
            : base(Pointer, Count, Alignment)
        { }

        /// <summary>
        /// Instantiates an array of chars with a certain number of elements.
        /// </summary>
        /// <param name="Count">Number of elements</param>
        public CharArray(ulong Count)
            : base(Count)
        { }

        /// <summary>
        /// Instantiates an array of chars with an array.
        /// </summary>
        /// <param name="Elements">Elements</param>
        public CharArray(char[] Elements)
            : base(Elements)
        { }

        /// <summary>
        /// Instantiates an array of chars with a string.
        /// </summary>
        /// <param name="String">String</param>
        public CharArray(string String)
            : base((ulong)String.Length)
        {
            unsafe
            {
                int n = String.Length;
                char *p = Pointer;
                for (int i = 0; i < n; ++i)
                    *p++ = String[i];
            }
        }

        public CharArray()
        {

        }

    }
}

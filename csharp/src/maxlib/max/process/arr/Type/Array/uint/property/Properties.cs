﻿namespace max.process.arr.type
{
    // unsigned int32 array
    public partial class UIntArray : Array<uint>
    {
        /// <summary>
        /// The size of each element of the array in bytes.
        /// </summary>
        /// <value>Size</value>
        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(uint);
            }
        }

        /// <summary>
        /// Length of the array in bytes.
        /// </summary>
        /// <value>Byte length</value>
        public override ulong ByteLength
        {
            get { return nElements * sizeof(uint); }
        }

        /// <summary>
        /// Gets the real pointer to the array. Unsafe.
        /// </summary>
        /// <value>Pointer</value>
        public unsafe uint* Pointer
        {
            get { return (uint*)Address; }
        }
    }
}

﻿namespace max.process.arr.type
{
    /// <summary>
    /// This class represents a char array managed by the
    /// Max Environment. Note that chars are 16-bit wide.
    /// </summary>
    public partial class CharArray : Array<char>
    {

    }
}

﻿using max.aux;

namespace max.process.arr.type
{
    // float array
    public partial class FloatArray : Array<float>
    {
        /// <summary>
        /// Copies a source array to this one, of a certain length of elements.
        /// </summary>
        /// <param name="Array">Source</param>
        /// <param name="Length">Length</param>
        protected override void CopyFromLength(float[] Array, int Length)
        {
            Arithmetic.FastCopyFloats(Address, Array, Length);
        }
    }
}


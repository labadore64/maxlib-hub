﻿namespace max.process.arr.type
{
    /// <summary>
    /// This class represents a signed byte array managed by the
    /// Max Environment.
    /// </summary>
    public partial class SByteArray : Array<sbyte>
    { 
    }
}

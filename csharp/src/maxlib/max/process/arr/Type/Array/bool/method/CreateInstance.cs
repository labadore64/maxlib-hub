﻿namespace max.process.arr.type
{
    // unsigned byte array
    public partial class BoolArray : Array<bool>
    {
        /// <summary>
        /// Creates an instance of BoolArray of a specified length.
        /// </summary>
        /// <returns>Instance</returns>
        /// <param name="Length">Length</param>
        public override IDataType InstanceCreate(ulong Length)
        {
            return new BoolArray(Length);
        }
    }
}

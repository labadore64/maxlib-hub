﻿namespace max.process.arr.type
{
    public abstract partial class Array<T> : IDataType where T : unmanaged
    {
        /// <summary>
        /// Copies a set number of values from one array to another.
        /// </summary>
        /// <param name="Array">Array to copy</param>
        /// <param name="Length">Number of elements</param>
        protected abstract void CopyFromLength(T[] Array, int Length);

        /// <summary>
        /// Copies values from one array to this one.
        /// </summary>
        /// <param name="Array">Array</param>
        public void CopyFrom(T[] Array)
        {
            CopyFromLength(Array,
                Length < Array.Length ? Length : Array.Length);
        }

    }
}

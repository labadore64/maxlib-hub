﻿using max.aux;

namespace max.process.arr.type
{
    // unsigned int64 array
    public partial class ULongArray : Array<ulong>
    {
        /// <summary>
        /// Copies a source array to this one, of a certain length of elements.
        /// </summary>
        /// <param name="Array">Source</param>
        /// <param name="Length">Length</param>
        protected override void CopyFromLength(ulong[] Array, int Length)
        {
            Arithmetic.FastCopyULongs(Address, Array, Length);
        }
    }
}

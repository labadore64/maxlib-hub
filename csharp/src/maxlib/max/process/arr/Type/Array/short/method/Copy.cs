﻿using max.aux;

namespace max.process.arr.type
{
    // signed int16 array
    public partial class ShortArray : Array<short>
    {
        /// <summary>
        /// Copies a source array to this one, of a certain length of elements.
        /// </summary>
        /// <param name="Array">Source</param>
        /// <param name="Length">Length</param>
        protected override void CopyFromLength(short[] Array, int Length)
        {
            Arithmetic.FastCopyShorts(Address, Array, Length);
        }
    }
}

﻿namespace max.process.arr.type
{
    // unsigned int16 array
    public partial class UShortArray : Array<ushort>
    {
        /// <summary>
        /// Creates an instance of UShortArray of a specified length.
        /// </summary>
        /// <returns>Instance</returns>
        /// <param name="Length">Length</param>
        public override IDataType InstanceCreate(ulong Length)
        {
            return new UShortArray(Length);
        }
    }
}

﻿namespace max.process.arr.type
{
    // float array
    public partial class FloatArray : Array<float>
    {
        /// <summary>
        /// Implicitly converts a float array to an instance of FloatArray.
        /// </summary>
        /// <returns>FloatArray</returns>
        /// <param name="Value">Source</param>
        public static implicit operator FloatArray(float[] Value) => new FloatArray(Value);
    }
}


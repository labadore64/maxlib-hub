﻿using System.Runtime.InteropServices;
using max.aux;

namespace max.process.arr.type
{
    public abstract partial class Array<T> : IDataType where T : unmanaged
    {
        /// <summary>
        /// Sets the value at the given index as a byte.
        /// </summary>
        /// <param name="Index">Index</param>
        /// <param name="Value">Value</param>
        protected void setByte(int Index, byte Value)
        {
#if DEBUG
            _ckIndex(Index);
#endif
            Marshal.WriteByte(Address, Index, Value);
        }

        /// <summary>
        /// Sets the value at the given index as a short.
        /// </summary>
        /// <param name="Index">Index</param>
        /// <param name="Value">Value</param>
        protected void setShort(int Index, short Value)
        {
#if DEBUG
            _ckIndex(Index);
#endif
            Marshal.WriteInt16(Address, Index << 1, Value);
        }

        /// <summary>
        /// Sets the value at the given index as an int.
        /// </summary>
        /// <param name="Index">Index</param>
        /// <param name="Value">Value</param>
        protected void setInt(int Index, int Value)
        {
#if DEBUG
            _ckIndex(Index);
#endif
            Marshal.WriteInt32(Address, Index << 2, Value);
        }

        /// <summary>
        /// Sets the value at the given index as a long.
        /// </summary>
        /// <param name="Index">Index</param>
        /// <param name="Value">Value</param>
        protected void setLong(int Index, long Value)
        {
#if DEBUG
            _ckIndex(Index);
#endif
            Marshal.WriteInt64(Address, Index << 3, Value);
        }

        /// <summary>
        /// Sets the value at the given index as a float.
        /// </summary>
        /// <param name="Index">Index</param>
        /// <param name="Value">Value</param>
        protected void setFloat(int Index, float Value)
        {
#if DEBUG
            _ckIndex(Index);
#endif
            Marshal.WriteInt32(Address, Index << 2, Arithmetic.FloatToInt(Value));
        }

        /// <summary>
        /// Sets the value at the given index as a double.
        /// </summary>
        /// <param name="Index">Index</param>
        /// <param name="Value">Value</param>
        protected void setDouble(int Index, double Value)
        {
#if DEBUG
            _ckIndex(Index);
#endif
            Marshal.WriteInt64(Address, Index << 3, Arithmetic.DoubleToLong(Value));
        }

        /// <summary>
        /// Sets the value at the given index as a bool.
        /// </summary>
        /// <param name="Index">Index</param>
        /// <param name="Value">Value</param>
        protected void setBool(int Index, bool Value)
        {
#if DEBUG
            _ckIndex(Index);
#endif
            Marshal.WriteInt32(Address, Index << 2, Arithmetic.BoolToInt(Value));
        }
    }
}

﻿namespace max.process.arr.type
{
    // unsigned byte array
    public partial class ByteArray : Array<byte>
    {
        /// <summary>
        /// The size of each element of the array in bytes.
        /// </summary>
        /// <value>Size</value>
        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(byte);
            }
        }

        /// <summary>
        /// Length of the array in bytes.
        /// </summary>
        /// <value>Byte length</value>
        public override ulong ByteLength
        {
            get { return nElements * sizeof(byte); }
        }

        /// <summary>
        /// Gets the real pointer to the array. Unsafe.
        /// </summary>
        /// <value>Pointer</value>
        public unsafe byte* Pointer
        {
            get { return (byte*)Address; }
        }
    }
}

﻿namespace max.process.arr.type
{
    // unsigned byte array
    public partial class BoolArray : Array<bool>
    {
        /// <summary>
        /// The size of each element of the array in bytes.
        /// </summary>
        /// <value>Size</value>
        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(bool);
            }
        }

        /// <summary>
        /// Length of the array in bytes.
        /// </summary>
        /// <value>Byte length</value>
        public override ulong ByteLength 
        {
            get { return nElements * sizeof(bool); }
        }

        /// <summary>
        /// Gets the real pointer to the array. Unsafe.
        /// </summary>
        /// <value>Pointer</value>
        public unsafe bool* Pointer
        {
            get { return (bool*)Address; }
        }

    }
}

﻿namespace max.process.arr.type
{
    // unsigned int16 array
    public partial class UShortArray : Array<ushort>
    {
        /// <summary>
        /// Implicitly converts a ushort array to an instance of UShortArray.
        /// </summary>
        /// <returns>UShortArray</returns>
        /// <param name="Value">Source</param>
        public static implicit operator UShortArray(ushort[] Value) => new UShortArray(Value);
    }
}

﻿namespace max.process.arr.type
{
    // signed int16 array
    public partial class ShortArray : Array<short>
    {
        /// <summary>
        /// Creates an instance of ShortArray of a specified length.
        /// </summary>
        /// <returns>Instance</returns>
        /// <param name="Length">Length</param>
        public override IDataType InstanceCreate(ulong Length)
        {
            return new ShortArray(Length);
        }

    }
}

﻿using System;

namespace max.process.arr.type
{
    public abstract partial class Array<T> : IDataType where T : unmanaged
    {
        /// <summary>
        /// Pointer address to the array.
        /// </summary>
        /// <value>Pointer address</value>
        protected IntPtr Address;
        /// <summary>
        /// Number of elements in the array.
        /// </summary>
        /// <value>Number of elements</value>
        protected ulong nElements;
        /// <summary>
        /// Alignment of the array.
        /// </summary>
        /// <value>Alignment</value>
        protected ulong nAlign;

        /// <summary>
        /// The size of each element of the array in bytes.
        /// </summary>
        /// <value>Size</value>
        public abstract ulong ElementByteSize { get; }

        /// <summary>
        /// Length of the array in bytes.
        /// </summary>
        /// <value>Byte length</value>
        public abstract ulong ByteLength { get; }

        /// <summary>
        /// Length of the array in elements.
        /// </summary>
        /// <value>The length of the array.</value>
        public int Length
        {
            get { return (int)nElements; }
        }

        /// <summary>
        /// Length of the array in elements as a ulong.
        /// </summary>
        /// <value>The length of the array.</value>
        public ulong DataLength
        {
            get { return nElements; }
        }

        /// <summary>
        /// Alignment of the array.
        /// </summary>
        /// <value>Alignment</value>
        public ulong Alignment
        {
            get { return nElements; }
        }

        /// <summary>
        /// Pointer to the array.
        /// </summary>
        /// <value>Pointer</value>
        public IntPtr DataPointer
        {
            get { return Address; }
        }

    }
}

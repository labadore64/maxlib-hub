﻿using max.aux;

namespace max.process.arr.type
{
    // signed int64 array
    public partial class LongArray : Array<long>
    {
        /// <summary>
        /// Copies a source array to this one, of a certain length of elements.
        /// </summary>
        /// <param name="Array">Source</param>
        /// <param name="Length">Length</param>
        protected override void CopyFromLength(long[] Array, int Length)
        {
            Arithmetic.FastCopyLongs(Address, Array, Length);
        }
    }
}

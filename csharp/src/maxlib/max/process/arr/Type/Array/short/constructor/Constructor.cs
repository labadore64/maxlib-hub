﻿using System;

namespace max.process.arr.type
{
    // signed int16 array
    public partial class ShortArray : Array<short>
    {
        /// <summary>
        /// Instantiates an array of shorts with a given pointer, element count and alignment.
        /// </summary>
        /// <param name="Pointer">Array pointer</param>
        /// <param name="Count">Number of elements</param>
        /// <param name="Alignment">Alignment</param>
        public ShortArray(IntPtr Pointer, ulong Elements, ulong Alignment)
            : base(Pointer, Elements, Alignment)
        { }

        /// <summary>
        /// Instantiates an array of shorts with a certain number of elements.
        /// </summary>
        /// <param name="Count">Number of elements</param>
        public ShortArray(ulong Count)
            : base(Count)
        { }

        /// <summary>
        /// Instantiates an array of shorts with an array.
        /// </summary>
        /// <param name="Elements">Elements</param>
        public ShortArray(short[] Elements)
            : base(Elements)
        { }
        public ShortArray()
        {

        }
    }
}

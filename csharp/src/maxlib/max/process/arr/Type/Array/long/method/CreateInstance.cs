﻿namespace max.process.arr.type
{
    // signed int64 array
    public partial class LongArray : Array<long>
    {
        /// <summary>
        /// Creates an instance of LongArray of a specified length.
        /// </summary>
        /// <returns>Instance</returns>
        /// <param name="Length">Length</param>
        public override IDataType InstanceCreate(ulong Length)
        {
            return new LongArray(Length);
        }
    }
}

﻿using System;

namespace max.process.arr.type
{
    // float array
    public partial class FloatArray : Array<float>
    {
        /// <summary>
        /// Instantiates an array of floats with a given pointer, element count and alignment.
        /// </summary>
        /// <param name="Pointer">Array pointer</param>
        /// <param name="Count">Number of elements</param>
        /// <param name="Alignment">Alignment</param>
        public FloatArray(IntPtr Pointer, ulong Count, ulong Alignment)
            : base(Pointer, Count, Alignment)
        { }

        /// <summary>
        /// Instantiates an array of floats with a certain number of elements.
        /// </summary>
        /// <param name="Count">Number of elements</param>
        public FloatArray(ulong Count)
            : base(Count)
        { }

        /// <summary>
        /// Instantiates an array of floats with an array.
        /// </summary>
        /// <param name="Elements">Elements</param>
        public FloatArray(float[] Elements)
            : base(Elements)
        { }

        public FloatArray()
        {

        }
    }
}


﻿using max.aux;

namespace max.process.arr.type
{
    // signed int32 array
    public partial class IntArray : Array<int>
    {
        /// <summary>
        /// Copies a source array to this one, of a certain length of elements.
        /// </summary>
        /// <param name="Array">Source</param>
        /// <param name="Length">Length</param>
        protected override void CopyFromLength(int[] Array, int Length)
        {
            Arithmetic.FastCopyInts(Address, Array, Length);
        }
    }
}

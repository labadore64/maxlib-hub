﻿namespace max.process.arr.type
{
    // signed int64 array
    public partial class LongArray : Array<long>
    {
        /// <summary>
        /// The size of each element of the array in bytes.
        /// </summary>
        /// <value>Size</value>
        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(long);
            }
        }

        /// <summary>
        /// Length of the array in bytes.
        /// </summary>
        /// <value>Byte length</value>
        public override ulong ByteLength
        {
            get { return nElements * sizeof(long); }
        }

        /// <summary>
        /// Gets the real pointer to the array. Unsafe.
        /// </summary>
        /// <value>Pointer</value>
        public unsafe long* Pointer
        {
            get { return (long*)Address; }
        }
    }
}

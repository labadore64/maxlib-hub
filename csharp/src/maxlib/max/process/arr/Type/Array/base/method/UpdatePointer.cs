﻿using System;
using max.game;

namespace max.process.arr.type
{
    public abstract partial class Array<T> : IDataType where T : unmanaged
    {
        /// <summary>
        /// Copies the data from the passed DataType to this array.
        /// </summary>
        /// <param name="Data">Data</param>
        public void SetData(IDataType Data)
        {
            RePoint(Data.DataPointer, Data.DataLength, Data.Alignment);
        }

        /// <summary>
        /// Repoints the array somewhere else, with new number of elements and alignment.
        /// </summary>
        /// <param name="Pointer">Pointer</param>
        /// <param name="Count">Length</param>
        /// <param name="Alignment">Alignment</param>
        protected void RePoint(IntPtr Pointer, ulong Count, ulong Alignment)
        {
            Address = Pointer;
            nElements = Count;
            nAlign = Alignment;

            // reference the array block to make sure it's not destroyed if
            // someone else unreferences it
            Game.CurrentGame.ArrayManager.RefArrayBlock(Pointer);
        }

    }
}

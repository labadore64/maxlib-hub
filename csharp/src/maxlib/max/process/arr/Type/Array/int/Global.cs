﻿namespace max.process.arr.type
{
    /// <summary>
    /// This class represents a signed 32-bit array managed by the
    /// Max Environment.
    /// </summary>
    public partial class IntArray : Array<int>
    {
    }
}

﻿using System;

namespace max.process.arr.type
{
    public abstract partial class Array<T> : IDataType where T : unmanaged
    {
        /// <summary>
        /// Initializes the array with a pointer, array length and alignment.
        /// </summary>
        /// <param name="Pointer">Pointer</param>
        /// <param name="Count">Length</param>
        /// <param name="Alignment">Alignment</param>
        protected Array(IntPtr Pointer, ulong Count, ulong Alignment)
        {
            RePoint(Pointer, Count, Alignment);
        }

        /// <summary>
        /// Initializes an array of a specified length.
        /// </summary>
        /// <param name="Count">Count.</param>
        protected Array(ulong Count)
        {
            Construct(Count);
        }

        /// <summary>
        /// Initializes an array with 0 length.
        /// </summary>
        protected Array()
        {
            Construct(0);
        }

        /// <summary>
        /// Initializes an array with an array of elements.
        /// </summary>
        /// <param name="Elements">Elements</param>
        protected Array(T[] Elements)
        {
            int len = Elements.Length;
            Construct((uint)len);
            CopyFromLength(Elements, len);
        }

    }
}

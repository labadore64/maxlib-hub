﻿namespace max.process.arr.type
{
    // unsigned int16 array
    public partial class UShortArray : Array<ushort>
    {
        /// <summary>
        /// The size of each element of the array in bytes.
        /// </summary>
        /// <value>Size</value>
        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(ushort);
            }
        }

        /// <summary>
        /// Length of the array in bytes.
        /// </summary>
        /// <value>Byte length</value>
        public override ulong ByteLength
        {
            get { return nElements * sizeof(ushort); }
        }

        /// <summary>
        /// Gets the real pointer to the array. Unsafe.
        /// </summary>
        /// <value>Pointer</value>
        public unsafe ushort* Pointer
        {
            get { return (ushort*)Address; }
        }
    }
}

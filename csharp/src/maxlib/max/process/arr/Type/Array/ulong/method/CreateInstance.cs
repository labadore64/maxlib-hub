﻿namespace max.process.arr.type
{
    // unsigned int64 array
    public partial class ULongArray : Array<ulong>
    {
        /// <summary>
        /// Creates an instance of ULongArray of a specified length.
        /// </summary>
        /// <returns>Instance</returns>
        /// <param name="Length">Length</param>
        public override IDataType InstanceCreate(ulong Length)
        {
            return new ULongArray(Length);
        }
    }
}

﻿namespace max.process.arr.type
{
    // float array
    public partial class FloatArray : Array<float>
    {
        /// <summary>
        /// Gets/Sets the float stored at the given index.
        /// </summary>
        /// <param name="i">Index</param>
        /// <value>Float</value>
        public float this[int i]
        {
            get { return getFloat(i); }
            set { setFloat(i,value); }
        }
    }
}


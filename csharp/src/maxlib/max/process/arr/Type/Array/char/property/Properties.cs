﻿using System.Runtime.InteropServices;
using System.Text;

namespace max.process.arr.type
{
    // char array (beware elders; these are 16 bit wide chars)
    public partial class CharArray : Array<char>
    {
        /// <summary>
        /// The size of each element of the array in bytes.
        /// </summary>
        /// <value>Size</value>
        public override ulong ElementByteSize
        {
            get
            {
                return sizeof(char);
            }
        }

        /// <summary>
        /// Length of the array in bytes.
        /// </summary>
        /// <value>Byte length</value>
        public override ulong ByteLength
        {
            get { return nElements * sizeof(char); }
        }

        /// <summary>
        /// Gets the real pointer to the array. Unsafe.
        /// </summary>
        /// <value>Pointer</value>
        public unsafe char* Pointer
        {
            get { return (char*)Address; }
        }

        /// <summary>
        /// Returns the string represented in the char array.
        /// </summary>
        /// <returns>String</returns>
        public override string ToString()
        {
            int nbytes = Length << 1;

            var result = new StringBuilder(Length);
            for (int ibyte = 0; ibyte < nbytes; ibyte += 2)
                result.Append((char)Marshal.ReadInt16(Address, ibyte));

            return result.ToString();
        }
    }
}

﻿using max.aux;

namespace max.process.arr.type
{
    // unsigned byte array
    public partial class ByteArray : Array<byte>
    {
        /// <summary>
        /// Creates an instance of ByteArray of a specified length.
        /// </summary>
        /// <returns>Instance</returns>
        /// <param name="Length">Length</param>
        public override IDataType InstanceCreate(ulong Length)
        {
            return new ByteArray(Length);
        }

    }
}

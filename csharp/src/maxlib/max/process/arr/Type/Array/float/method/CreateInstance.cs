﻿namespace max.process.arr.type
{
    // float array
    public partial class FloatArray : Array<float>
    {
        /// <summary>
        /// Creates an instance of FloatArray of a specified length.
        /// </summary>
        /// <returns>Instance</returns>
        /// <param name="Length">Length</param>
        public override IDataType InstanceCreate(ulong Length)
        {
            return new FloatArray(Length);
        }
    }
}


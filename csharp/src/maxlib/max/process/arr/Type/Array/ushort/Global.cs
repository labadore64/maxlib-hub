﻿namespace max.process.arr.type
{
    /// <summary>
    /// This class represents an unsigned 16-bit array managed by the
    /// Max Environment.
    /// </summary>
    public partial class UShortArray : Array<ushort>
    {
    }
}

﻿namespace max.process.arr.type
{
    public abstract partial class Array<T> : IDataType where T : unmanaged
    {
        /// <summary>
        /// Creates an instance of this datatype.
        /// </summary>
        /// <returns>Instance</returns>
        /// <param name="Length">Length</param>
        public abstract IDataType InstanceCreate(ulong Length);
    }
}

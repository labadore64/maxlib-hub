﻿namespace max.process.arr.type
{
    // unsigned int32 array
    public partial class UIntArray : Array<uint>
    {
        /// <summary>
        /// Implicitly converts a uint array to an instance of UIntArray.
        /// </summary>
        /// <returns>UIntArray</returns>
        /// <param name="Value">Source</param>
        public static implicit operator UIntArray(uint[] Value) => new UIntArray(Value);
    }
}

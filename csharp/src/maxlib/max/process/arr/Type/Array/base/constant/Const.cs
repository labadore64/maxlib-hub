﻿namespace max.process.arr.type
{
    public abstract partial class Array<T> : IDataType where T : unmanaged
    {
        /// <summary>
        /// The default alignment.
        /// </summary>
        protected const ulong ALIGN_DEFAULT = 64;
    }
}

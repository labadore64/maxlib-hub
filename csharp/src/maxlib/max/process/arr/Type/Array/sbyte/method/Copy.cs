﻿using max.aux;

namespace max.process.arr.type
{
    // unsigned byte array
    public partial class SByteArray : Array<sbyte>
    {
        /// <summary>
        /// Copies a source array to this one, of a certain length of elements.
        /// </summary>
        /// <param name="Array">Source</param>
        /// <param name="Length">Length</param>
        protected override void CopyFromLength(sbyte[] Array, int Length)
        {
            Arithmetic.FastCopySBytes(Address, Array, Length);
        }
    }
}

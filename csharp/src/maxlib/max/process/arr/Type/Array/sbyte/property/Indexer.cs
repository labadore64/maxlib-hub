﻿namespace max.process.arr.type
{
    // unsigned byte array
    public partial class SByteArray : Array<sbyte>
    {
        /// <summary>
        /// Gets/Sets the sbyte stored at the given index.
        /// </summary>
        /// <param name="i">Index</param>
        /// <value>Sbyte</value>
        public sbyte this[int i]
        {
            get { return (sbyte)getByte(i); }
            set { setByte(i,(byte)value); }
        }
    }
}

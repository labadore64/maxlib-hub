﻿namespace max.process.arr.type
{
    /// <summary>
    /// This class represents an unsigned 64-bit array managed by the
    /// Max Environment.
    /// </summary>
    public partial class ULongArray : Array<ulong>
    {
    }
}

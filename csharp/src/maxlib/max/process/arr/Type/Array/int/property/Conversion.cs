﻿namespace max.process.arr.type
{
    // signed int32 array
    public partial class IntArray : Array<int>
    {
        /// <summary>
        /// Implicitly converts a int array to an instance of IntArray.
        /// </summary>
        /// <returns>IntArray</returns>
        /// <param name="Value">Source</param>
        public static implicit operator IntArray(int[] Value) => new IntArray(Value);
    }
}

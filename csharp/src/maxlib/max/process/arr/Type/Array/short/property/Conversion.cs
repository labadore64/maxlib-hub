﻿namespace max.process.arr.type
{
    // signed int16 array
    public partial class ShortArray : Array<short>
    {
        /// <summary>
        /// Implicitly converts a short array to an instance of ShortArray.
        /// </summary>
        /// <returns>ShortArray</returns>
        /// <param name="Value">Source</param>
        public static implicit operator ShortArray(short[] Value) => new ShortArray(Value);
    }
}

﻿using max.aux;

namespace max.process.arr.type
{
    public partial class DoubleArray : Array<double>
    {
        /// <summary>
        /// Copies a source array to this one, of a certain length of elements.
        /// </summary>
        /// <param name="Array">Source</param>
        /// <param name="Length">Length</param>
        protected override void CopyFromLength(double[] Array, int Length)
        {
            Arithmetic.FastCopyDoubles(Address, Array, Length);
        }
    }
}

﻿namespace max.process.arr.type
{
    /// <summary>
    /// This class represents a boolean array managed by the
    /// Max Environment.
    /// </summary>
    public partial class BoolArray : Array<bool>
    {
    }
}

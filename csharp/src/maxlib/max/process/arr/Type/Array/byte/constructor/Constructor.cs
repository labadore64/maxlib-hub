﻿using System;

namespace max.process.arr.type
{
    // unsigned byte array
    public partial class ByteArray : Array<byte>
    {
        /// <summary>
        /// Instantiates an array of bytes with a given pointer, element count and alignment.
        /// </summary>
        /// <param name="Pointer">Array pointer</param>
        /// <param name="Count">Number of elements</param>
        /// <param name="Alignment">Alignment</param>
        public ByteArray(IntPtr Pointer, ulong Count, ulong Alignment)
            : base(Pointer, Count, Alignment)
        { }

        /// <summary>
        /// Instantiates an array of bytes with a certain number of elements.
        /// </summary>
        /// <param name="Count">Number of elements</param>
        public ByteArray(ulong Count)
            : base(Count)
        { }

        /// <summary>
        /// Instantiates an array of bytes with an array.
        /// </summary>
        /// <param name="Elements">Elements</param>
        public ByteArray(byte[] Elements)
            : base(Elements)
        { }

        public ByteArray()
        {

        }

    }
}

﻿using System;

namespace max.process.arr.type
{
    public abstract partial class Array<T> : IDataType where T : unmanaged
    {

#if DEBUG
        private void _ckIndex(int i)
        {
            if (i < 0 || i >= Length)
                throw new IndexOutOfRangeException("Index " + i.ToString()
                        + " out of range in array projection of type "
                        + typeof(T) + " having "
                        + nElements.ToString() + " elements.");
        }
#endif

    }
}

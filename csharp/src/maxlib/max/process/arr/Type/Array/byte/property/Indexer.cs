﻿namespace max.process.arr.type
{
    // unsigned byte array
    public partial class ByteArray : Array<byte>
    {
        /// <summary>
        /// Gets/Sets the byte stored at the given index.
        /// </summary>
        /// <param name="i">Index</param>
        /// <value>Byte</value>
        public byte this[int i]
        {
            get { return getByte(i); }
            set { setByte(i,value); }
        }
    }
}

﻿using max.aux;

namespace max.process.arr.type
{
    // char array (beware elders; these are 16 bit wide chars)
    public partial class CharArray : Array<char>
    {
        /// <summary>
        /// Copies a source array to this one, of a certain length of elements.
        /// </summary>
        /// <param name="Array">Source</param>
        /// <param name="Length">Length</param>
        protected override void CopyFromLength(char[] Array, int Length)
        {
            Arithmetic.FastCopyChars(Address, Array, Length);
        }
    }
}

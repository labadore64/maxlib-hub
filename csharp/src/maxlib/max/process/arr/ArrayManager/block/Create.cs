﻿using System;
using max.aux;

namespace max.process.arr
{
    internal partial class ArrayManager
    {
        // allocate memory for an array whose size in bytes is the size of the
        // type T in bytes times the number of elements desired, and with at least
        // the specified alignment.
        internal IntPtr CreateArrayBlock<T>(ulong nelements, ulong alignment)
          where T : unmanaged
        {
            // at least align on the next power of two after the size of the type
            ulong nalign = Arithmetic.NextPowerOfTwo((ulong)Arithmetic.ByteSizeOf<T>(1) - 1);

            // follow the users alignment wish if it's greater than the minimum
            if (alignment > nalign)
                nalign = Arithmetic.NextPowerOfTwo(alignment - 1);

            // calculate bytes needed from the template type.
            ulong nbytes = (ulong)Arithmetic.ByteSizeOf<T>((int)nelements);

            // allocate the memory for the array block.
            IntPtr p = Alloc.AllocateAligned(nbytes, nalign);

            // create the memory info struct and add it to the table
            MemorySegmentSpec spec = new MemorySegmentSpec(p, nbytes, nalign);
            Segments.Add(spec.Address, spec);

            // return the usable address
            return spec.Address;
        }
    }
}

﻿using System;

namespace max.process.arr
{
    internal partial class ArrayManager
    {
        // reference array memory
        internal void RefArrayBlock(IntPtr p)
        {
            MemorySegmentSpec spec = Segments[p];
            spec.RefCount++;
        }
    }
}

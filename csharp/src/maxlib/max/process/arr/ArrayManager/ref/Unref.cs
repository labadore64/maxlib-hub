﻿using System;

namespace max.process.arr
{
    internal partial class ArrayManager
    {
        // unreference array memory
        internal void UnrefArrayBlock(IntPtr p)
        {
            // to be clear, we want an exception to be thrown if an unknown pointer
            // is passed in
            MemorySegmentSpec spec = Segments[p];

            if (--spec.RefCount <= 0)
            {
                Alloc.DeallocateAligned(spec.Address, spec.ByteCount, spec.Alignment);
                Segments.Remove(p);
            }
        }
    }
}

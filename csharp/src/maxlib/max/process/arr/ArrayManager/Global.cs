﻿// 2020-04-18
// allocator now handles alignment in the same way mxmem does
//
// 2020-04-17
// added documentation
// added missing ProjectDoubles method to ArrayManager
// added commentary regarding Max Library Machine integration
//
// 2020-04-16
// initial version
//
// William 

// Obviously, you have an established system for type constants and avoiding
// reflection that is outside the scope of this. The intent is for this to be
// integrated with what you have. Using this, each property may be stored as an
// IntPtr, so an array or table of properties is very light. The idea is that
// when a property is retrieved, a transient array projection is returned that
// provides an indexing method and a way to provide an actual pointer (inside of
// an "unsafe" block, which doesn't appear to be a limitation that can be
// circumvented).

namespace max.process.arr
{
    // This is the array manager. For the sake of this example, it takes on the
    // function of alignment in a way that's convenient in C#. The aligned
    // allocation option should actually be part of the Allocator interface
    // which mirrors mxmem instances, and ArrayManager should simply call it
    // with the desired alignment. The error checking of mxmem's builtin aligned
    // allocator wrapper is desirable compared to this.
    internal partial class ArrayManager
    {

    }
}

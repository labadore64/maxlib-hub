﻿using System;

namespace max.process.arr
{
    internal partial class ArrayManager
    {
        // how to remember the array blocks created
        internal struct MemorySegmentSpec
        {
            internal MemorySegmentSpec(IntPtr p, ulong nbytes, ulong nalign)
            {
                Address = p;
                ByteCount = nbytes;
                Alignment = nalign;
                RefCount = 1;
            }

            internal IntPtr Address   { get; } // aligned address for use
            internal ulong  ByteCount { get; } // guaranteed bytes
            internal ulong  Alignment { get; } // guaranteed alignment
            internal int    RefCount  { get; set; } // reference counter
        }
    }
}

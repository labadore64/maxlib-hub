﻿using System;
using System.Collections.Generic;

namespace max.process.arr
{
    internal partial class ArrayManager
    {
        ~ArrayManager()
        {
            // deallocate all the blocks
            foreach (KeyValuePair<IntPtr, MemorySegmentSpec> s in Segments)
            {
                Alloc.DeallocateAligned(s.Value.Address, s.Value.ByteCount, s.Value.Alignment);
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using max.processor.mem;

namespace max.process.arr
{
    internal partial class ArrayManager
    {
        internal ArrayManager(Allocator alloc)
        {
            Alloc = alloc;
            Segments = new Dictionary<IntPtr, MemorySegmentSpec>();
        }
    }
}

﻿namespace max.process.env
{
    /// <summary>
    /// This class represents the Environment Controller for the
    /// MaxEnvironment. This controller manages the various components
    /// of the MaxEnvironment and acts as a public interface outside
    /// of the library for the MaxEnvironment.
    /// </summary>
    public partial class MaxEnvironment
    {

    }
}

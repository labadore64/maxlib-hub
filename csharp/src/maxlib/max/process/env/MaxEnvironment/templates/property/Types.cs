﻿using System;
using System.Collections.Generic;
using max.process.arr.type;

namespace max.process.env
{

    public partial class MaxEnvironment
    {
        /// <summary>
        /// List of max object types available.
        /// </summary>
        /// <value>Types</value>
        internal Dictionary<Type,MaxObject> MaxObjectTypes { get; set; } = new Dictionary<Type, MaxObject>();

        /// <summary>
        /// List of max data types available.
        /// </summary>
        /// <value>Types</value>
        internal Dictionary<Type, IDataType> MaxDataTypes { get; set; } = new Dictionary<Type, IDataType>();
    }
}

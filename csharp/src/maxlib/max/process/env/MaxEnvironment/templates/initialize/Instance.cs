﻿using System;
using System.Collections.Generic;
using max.aux.util;
using max.process.arr.type;

namespace max.process.env
{
    public partial class MaxEnvironment
    {
        /// <summary>
        /// Initializes all the available instances.
        /// </summary>
        private void InitializeInstance()
        {
            List<Type> types = ReflectionUtil.GetTypesContainingType(typeof(MaxObject)).FindAll(x => x.IsClass && !x.IsAbstract);
            MaxObject stage;

            for (int i = 0; i < types.Count; i++)
            {
                stage = (MaxObject)Activator.CreateInstance(types[i]);
                stage.InitializePropertyPrototypes();
                MaxObjectTypes.Add(types[i],stage);
            }

        }
    }
}

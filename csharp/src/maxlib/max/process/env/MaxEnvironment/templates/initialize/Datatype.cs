﻿using System;
using System.Collections.Generic;
using max.aux.util;
using max.process.arr.type;

namespace max.process.env
{
    public partial class MaxEnvironment
    {
        /// <summary>
        /// Initializes all the available datatypes.
        /// </summary>
        private void InitializeDatatypes()
        {
            List<Type> types = ReflectionUtil.GetTypesContainingType(typeof(IDataType)).FindAll(x => x.IsClass && !x.IsAbstract);
            IDataType stage;

            for (int i = 0; i < types.Count; i++)
            {
                stage = (IDataType)Activator.CreateInstance(types[i]);
                MaxDataTypes.Add(types[i],stage);
            }
        }
    }
}

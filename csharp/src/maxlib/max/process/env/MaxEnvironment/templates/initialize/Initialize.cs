﻿namespace max.process.env
{
    public partial class MaxEnvironment
    {
        /// <summary>
        /// Initializes the controller.
        /// </summary>
        internal void Initialize()
        {
            InitializeInstance();
            InitializeDatatypes();
        }
    }
}

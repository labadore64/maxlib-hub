﻿using max.process.arr.type;
using System;

namespace max.process.env
{
    public partial class MaxEnvironment
    {
        /// <summary>
        /// Pushes the staged objects to the kernel to be created.
        /// </summary>
        internal void PushCreatedInstances()
        {
            // gets the staged objects as an array.
            MaxObject[] Staged = StagedInstancesCreated.ToArray();

            // this will be removed later. replace this with the
            // routine that gets the list of new uniques
            ulong[] Uniques = GetUniques(Staged.Length);

            // execute the object's initialize routines.
            // these routines stage the properties to be passed to the kernel

            for (int i = 0; i < Staged.Length; i++)
            {
                // initialize the object
                Staged[i].Initialize(Uniques[i]);

                // adds the object to the objects table now that its been
                // initialized
                Instances.Add(Staged[i].UID, Staged[i]);
            }

            // change this to initialize the objects with the staged data.
            // KernelInitialize();

            // clears the list when done.
            StagedInstancesCreated.Clear();
        }

        /// <summary>
        /// Gets an array of Uniques.
        /// </summary>
        /// <returns>Uniques Array</returns>
        /// <param name="Length">Length</param>
        private ulong[] GetUniques(int Length)
        {
            ulong[] Uniques = new ulong[Length];

            // remove this section when you get the kernel stuff

            Random r = new Random();

            ulong val = 125;

            for (int i = 0; i < Length; i++)
            {
                val = (ulong)r.Next(int.MaxValue);
                Uniques[i] = (val << 32);
            }

            // end remove section

            return Uniques;
        }
    }
}

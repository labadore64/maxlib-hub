﻿namespace max.process.env
{
    public partial class MaxEnvironment
    {
        /// <summary>
        /// Updates the controller.
        /// </summary>
        internal void Update()
        {
            // if there are objects staged for creation, proceed
            if (StagedInstancesCreated.Count > 0)
            {
                PushCreatedInstances();
            }

            // if there are objects staged for destruction, proceed
            if (StagedInstancesDestroyed.Count > 0)
            {
                PushDestroyedInstances();
            }
        }
    }
}

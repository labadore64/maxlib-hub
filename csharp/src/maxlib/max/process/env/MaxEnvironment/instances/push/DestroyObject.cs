﻿using max.process.arr.type;

namespace max.process.env
{
    public partial class MaxEnvironment
    {
        /// <summary>
        /// Pushes the staged objects to the kernel to be destroyed.
        /// </summary>
        internal void PushDestroyedInstances()
        {
            // gets the staged objects as an array.
            MaxObject[] Staged = StagedInstancesDestroyed.ToArray();

            // execute the object's Dispose routines.
            // these routines remove the references to the properties
            // in the max environment

            for (int i = 0; i < Staged.Length; i++)
            {
                // dispose of the object
                Staged[i].Dispose();

                // remove the reference to the disposed object
                // in the objects Dictionary.

                Instances.Remove(Staged[i].UID);
            }

            // retrieves the list of Uniques from the Destroy list.
            ulong[] Uniques = GetUniquesFromDestroyList();

            // This will be changed later - this should be a call to release
            // the uniques in the kernel

            // ReleaseUniques(Uniques);

            // clears the list when done.
            StagedInstancesDestroyed.Clear();
        }

        /// <summary>
        /// Gets the Uniques from the list of staged destroyed objects.
        /// </summary>
        /// <returns>Array of Uniques</returns>
        private ulong[] GetUniquesFromDestroyList()
        {
            ulong[] Uniques = new ulong[StagedInstancesDestroyed.Count];

            for(int i = 0; i < StagedInstancesDestroyed.Count; i++)
            {
                Uniques[i] = StagedInstancesDestroyed[i].UID;
            }

            return Uniques;
        }
    }
}

﻿using System.Collections.Generic;
using max.process.arr.type;

namespace max.process.env
{
    public partial class MaxEnvironment
    {
        /// <summary>
        /// Represents MaxObjects staged to be created.
        /// </summary>
        /// <value>MaxObjects staged for creation</value>
        internal List<MaxObject> StagedInstancesCreated { get; set; } = new List<MaxObject>();

        /// <summary>
        /// Represents MaxObjects staged to be destroyed.
        /// </summary>
        /// <value>MaxObjects staged for destruction</value>
        internal List<MaxObject> StagedInstancesDestroyed { get; set; } = new List<MaxObject>();
    }
}

﻿using max.process.arr.type;

namespace max.process.env
{
    public partial class MaxEnvironment
    {
        /// <summary>
        /// Stages an array of objects to be created.
        /// </summary>
        /// <param name="MaxObjects">Max Objects</param>
        internal void StageCreatedInstances(MaxObject[] MaxObjects)
        {
            StagedInstancesCreated.AddRange(MaxObjects);
        }

        /// <summary>
        /// Stages an object to be created.
        /// </summary>
        /// <param name="MaxObject">Max Object</param>
        internal void StageCreatedInstance(MaxObject MaxObject)
        {
            StagedInstancesCreated.Add(MaxObject);
        }
    }
}

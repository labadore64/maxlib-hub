﻿using max.process.arr.type;

namespace max.process.env
{
    public partial class MaxEnvironment
    {
        /// <summary>
        /// Stages an array of objects to be destroyed.
        /// </summary>
        /// <param name="MaxObjects">Max Objects</param>
        internal void StageDestroyedInstances(MaxObject[] MaxObjects)
        {
            StagedInstancesDestroyed.AddRange(MaxObjects);
        }

        /// <summary>
        /// Stages an object to be destroyed.
        /// </summary>
        /// <param name="MaxObject">Max Objects</param>
        internal void StageDestroyedInstance(MaxObject MaxObject)
        {
            StagedInstancesDestroyed.Add(MaxObject);
        }
    }
}

﻿using max.process.arr.type;

namespace max.process.env
{
    public partial class MaxEnvironment
    {
        /// <summary>
        /// Gets the MaxObject based on its UID.
        /// </summary>
        /// <param name="i">UID</param>
        /// <value>Instance</value>
        public MaxObject this[ulong i]
        {
            get
            {
                return Instances[i];
            }
        }
    }

}

﻿using System.Collections.Generic;
using max.process.arr.type;

namespace max.process.env
{
    public partial class MaxEnvironment
    {
        /// <summary>
        /// Represents MaxObject instances currently in memory.
        /// </summary>
        /// <value>Objects</value>
        internal Dictionary<ulong,MaxObject> Instances { get; private set; } = new Dictionary<ulong, MaxObject>();

    }
}

﻿
using System;
using System.Runtime.InteropServices;

namespace max.processor.mem
{
    internal class HeapAllocator : Allocator
    {
        // allocate n bytes
        protected override IntPtr DoAllocation(ulong n)
        {
            return Marshal.AllocHGlobal((int)n);
        }

        // deallocate n bytes
        protected override void DoDeallocation(IntPtr p, ulong n)
        {
            // ignore n in the HeapAllocator since it doesn't care
            Marshal.FreeHGlobal(p);
        }
    }
}
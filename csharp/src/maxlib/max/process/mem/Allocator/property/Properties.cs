﻿using System;

namespace max.processor.mem
{
    // This can be swapped out for other allocators, much like mxmem interfaces.
    internal abstract partial class Allocator
    {
        protected ulong nalloced_;
        protected ulong nbytes_;
        protected ulong nusable_;

        // How many blocks are allocated right now?
        internal ulong AllocatedSegments { get { return nalloced_; } }
        internal ulong AllocatedBytes { get { return nbytes_; } }
        internal ulong UsableBytes { get { return nusable_; } }

        // override these
        protected abstract IntPtr DoAllocation(ulong n);
        protected abstract void DoDeallocation(IntPtr p, ulong n);
    }
}
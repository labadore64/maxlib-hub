﻿using System;
using System.Diagnostics;

namespace max.processor.mem
{
    // This can be swapped out for other allocators, much like mxmem interfaces.
    internal abstract partial class Allocator
    {
        protected void DeallocateImpl(IntPtr p, ulong n)
        {
            Debug.Assert(nalloced_ != 0,
              "you just tried to deallocate more than you allocated, chief");
            Debug.Assert(nbytes_ >= n,
              "you just tried to deallocate more than you allocated, chief");
            DoDeallocation(p, n);
            --nalloced_;
            nbytes_ -= n;
        }
    }
}
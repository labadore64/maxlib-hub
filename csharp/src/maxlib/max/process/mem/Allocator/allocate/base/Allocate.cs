﻿using System;

namespace max.processor.mem
{
    // This can be swapped out for other allocators, much like mxmem interfaces.
    internal abstract partial class Allocator
    {
        // Allocate
        internal IntPtr Allocate(ulong n)
        {
            IntPtr p = AllocateImpl(n); // Actually allocate
            nusable_ += n;              // All of it's usable
            return p;
        }
    }
}
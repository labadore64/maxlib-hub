﻿using System;

namespace max.processor.mem
{
    // This can be swapped out for other allocators, much like mxmem interfaces.
    internal abstract partial class Allocator
    {
        protected IntPtr AllocateImpl(ulong n)
        {
            IntPtr p = DoAllocation(n);
            ++nalloced_;
            nbytes_ += n;
            return p;
        }
    }
}
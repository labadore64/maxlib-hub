﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using max.aux;

namespace max.processor.mem
{
    // This can be swapped out for other allocators, much like mxmem interfaces.
    internal abstract partial class Allocator
    {
        // Deallocate Aligned
        internal void DeallocateAligned(IntPtr p, ulong n, ulong align)
        {
            Debug.Assert(nusable_ >= n,
              "you just tried to deallocate more than you allocated, chief");

            ulong sizeulong = (ulong)Arithmetic.ByteSizeOf<ulong>(1);
            ulong npre = sizeulong << 1;

            IntPtr r = (IntPtr)((ulong)p - npre); // get the stash address
            IntPtr m = (IntPtr)Marshal.ReadInt64(r, 0); // get the base address
                                                        // get the original alignment
            ulong salign = (ulong)Marshal.ReadInt64(r, (int)sizeulong);

            Debug.Assert(align == salign,
              "forgetting the alignment we asked for, are we?");

            // Actual deallocation with padding
            DeallocateImpl(m, n + align + npre);
            nusable_ -= n; // only the amount guaranteed to be usable should be
                           // deducted from nusable.
        }
    }
}
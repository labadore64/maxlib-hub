﻿using System;
using System.Diagnostics;

namespace max.processor.mem
{
    // This can be swapped out for other allocators, much like mxmem interfaces.
    internal abstract partial class Allocator
    {
        // Deallocate
        internal void Deallocate(IntPtr p, ulong n)
        {
            Debug.Assert(nusable_ >= n,
              "you just tried to deallocate more than you allocated, chief");
            DeallocateImpl(p, n); // Actually deallocate
            nusable_ -= n;        // All of it's usable
        }
    }
}
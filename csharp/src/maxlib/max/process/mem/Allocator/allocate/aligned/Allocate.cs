﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using max.aux;

namespace max.processor.mem
{
    // This can be swapped out for other allocators, much like mxmem interfaces.
    internal abstract partial class Allocator
    {
        // Allocate Aligned
        internal IntPtr AllocateAligned(ulong n, ulong align)
        {
            Debug.Assert(Arithmetic.NextPowerOfTwo(align) >> 1 == align,
              "alignment must be a power of two");

            // how much extra do we need to remember alignment and base address,
            // plus enough space to shift the address so it's aligned?
            ulong sizeulong = (ulong)Arithmetic.ByteSizeOf<ulong>(1);
            ulong npre = sizeulong << 1;
            ulong pad = align + npre;

            // Actual allocation with padding
            IntPtr m = AllocateImpl(n + pad);
            nusable_ += n; // only the amount requested is guaranteed to be usable
                           // because we're going to shift the address

            // the pointer to the aligned segment we're giving them
            IntPtr p = (IntPtr)(((ulong)m + pad) & ~(align - 1));
            // the pointer to the alignment and base address info (we need this
            // to recover the address and actual size we want to deallocate)
            IntPtr r = (IntPtr)((ulong)p - npre);

            // stash the base address and alignment in the two 128 span right before
            // the aligned pointer we're giving back.
            Marshal.WriteInt64(r, 0, (long)m);
            Marshal.WriteInt64(r, (int)sizeulong, (long)align);

            return p;
        }
    }
}
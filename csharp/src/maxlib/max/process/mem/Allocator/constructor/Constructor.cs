﻿namespace max.processor.mem
{
    // This can be swapped out for other allocators, much like mxmem interfaces.
    internal abstract partial class Allocator
    {

        protected Allocator()
        {
            // counters for segments and space allocated
            nalloced_ = 0;
            nbytes_ = 0;
            nusable_ = 0;
        }
    }
}
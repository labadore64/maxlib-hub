﻿using System.Diagnostics;

namespace max.processor.mem
{
    // This can be swapped out for other allocators, much like mxmem interfaces.
    internal abstract partial class Allocator
    {
        ~Allocator()
        {
            // check the counters in the end
            Debug.Assert(nalloced_ == 0,
              "gotta deallocate the same number of segments you allocated, chief");
            Debug.Assert(nbytes_ == 0,
              "gotta deallocate the same number of bytes you allocated, chief");
        }

    }
}
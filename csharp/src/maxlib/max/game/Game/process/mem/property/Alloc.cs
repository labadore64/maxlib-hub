﻿using max.process.arr;
using max.processor.mem;

namespace max.game
{
    public abstract partial class Game
    {
        /// <summary>
        /// The heap allocator.
        /// </summary>
        internal HeapAllocator Heap;

        /// <summary>
        /// The array manager.
        /// </summary>
        internal ArrayManager ArrayManager;
    }
}

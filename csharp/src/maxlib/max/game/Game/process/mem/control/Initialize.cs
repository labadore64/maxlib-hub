﻿using max.process.arr;
using max.processor.mem;

namespace max.game
{
    public abstract partial class Game
    {
        /// <summary>
        /// Initializes the mem variables.
        /// </summary>
        private void InitializeMem()
        {
            Heap = new HeapAllocator();
            ArrayManager = new ArrayManager(Heap);
        }
    }
}

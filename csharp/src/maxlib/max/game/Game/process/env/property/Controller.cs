﻿using max.process.env;

namespace max.game
{
    public abstract partial class Game
    {
        /// <summary>
        /// The Max environment.
        /// </summary>
        /// <value>Max Environment</value>
        public MaxEnvironment Environment { get; private set; } = new MaxEnvironment();
    }
}

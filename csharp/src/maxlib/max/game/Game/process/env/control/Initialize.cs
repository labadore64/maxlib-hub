﻿namespace max.game
{

    public abstract partial class Game
    {
        /// <summary>
        /// Initializes the env variables.
        /// </summary>
        private void InitializeEnv()
        {
            Environment.Initialize();
        }
    }
}

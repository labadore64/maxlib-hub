﻿using System;
using max.process.arr.type;

namespace max.game
{
    public abstract partial class Game
    {
        /// <summary>
        /// Disposes and destroys the instance with the given UID.
        /// </summary>
        /// <param name="UID">UID</param>
        public void InstanceDispose(ulong UID)
        {
            Environment.Instances[UID].Dispose();
        }

        /// <summary>
        /// Disposes and destroys the instance.
        /// </summary>
        /// <param name="Instance">Instance</param>
        public void InstanceDispose(MaxObject Instance)
        {
            Instance.Dispose();
        }
    }
}

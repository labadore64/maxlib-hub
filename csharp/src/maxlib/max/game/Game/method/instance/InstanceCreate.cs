﻿using System;
using max.process.arr.type;

namespace max.game
{
    public abstract partial class Game
    {
        /// <summary>
        /// Creates an instance of the specified type.
        /// </summary>
        /// <typeparam>Object Type</typeparam>
        /// <returns>New instance</returns>
        public static T InstanceCreate<T>()
            where T : MaxObject
        {
            Type type = typeof(T);
            if (CurrentGame.Environment.MaxObjectTypes.ContainsKey(type))
            {
                T obj = (T)CurrentGame.Environment.MaxObjectTypes[type].InstanceCreateInternal();
                CurrentGame.Environment.StageCreatedInstance(obj);
                obj.Create();
                return obj;
            }

            // throw an exception on failure
            throw new Exception("The type " + type.FullName + " does not inherit MaxObject and cannot be instantiated as part of the Max environment.");

        }

        /// <summary>
        /// Creates an instance of the specified type.
        /// </summary>
        /// <param name="Type">Type</param>
        /// <returns>New instance</returns>
        public static MaxObject InstanceCreate(Type Type)
        {
            if (CurrentGame.Environment.MaxObjectTypes.ContainsKey(Type))
            {
                MaxObject obj = CurrentGame.Environment.MaxObjectTypes[Type].InstanceCreateInternal();
                CurrentGame.Environment.StageCreatedInstance(obj);
                obj.Create();
                return obj;
            }

            // throw an exception on failure
            throw new Exception("The type " + Type.FullName + " does not inherit MaxObject and cannot be instantiated as part of the Max environment.");

        }
    }
}

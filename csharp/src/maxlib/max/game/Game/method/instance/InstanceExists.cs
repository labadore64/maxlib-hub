﻿using System;
using max.process.arr.type;

namespace max.game
{
    public abstract partial class Game
    {
        /// <summary>
        /// Whether or not the instance associated with the UID exists in the environment.
        /// </summary>
        /// <param name="UID">UID</param>
        internal bool InstanceExists(ulong UID)
        {
            if (Environment.Instances.ContainsKey(UID))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Whether or not the instance exists in the environment. Also tests staged instances.
        /// </summary>
        /// <param name="Instance">Instance</param>
        internal bool InstanceExists(MaxObject Instance)
        {
            if (Environment.Instances.ContainsValue(Instance))
            {
                return true;
            }
            if (Environment.StagedInstancesCreated.Contains(Instance))
            {
                return true;
            }
            return false;
        }
    }
}

﻿using System;
using max.process.arr.type;

namespace max.game
{
    public abstract partial class Game
    {
        /// <summary>
        /// Creates an instance of a specified data type.
        /// </summary>
        /// <typeparam>Data Type</typeparam>
        /// <returns>New instance</returns>
        public static IDataType DatatypeCreate<T>()
        {
            return DatatypeCreate<T>(1);
        }

        /// <summary>
        /// Creates an instance of a specified data type.
        /// </summary>
        /// <param name="Length">Number of elements</param>
        /// <typeparam>Data Type</typeparam>
        /// <returns>New instance</returns>
        public static IDataType DatatypeCreate<T>(ulong Length)
        {
            Type Type = typeof(T);
            if (CurrentGame.Environment.MaxDataTypes.ContainsKey(Type))
            {
                IDataType obj = CurrentGame.Environment.MaxDataTypes[Type].InstanceCreate(Length);
                return obj;
            }

            // throw an exception on failure
            throw new Exception("The type " + Type.FullName + " does not inherit Type and cannot be instantiated as a Max Datatype.");

        }

        /// <summary>
        /// Creates an instance of a specified data type.
        /// </summary>
        /// <param name="Type">Datatype</param>
        /// <returns>New instance</returns>
        public static IDataType DatatypeCreate(Type Type)
        {
            return DatatypeCreate(Type,1);
        }

        /// <summary>
        /// Creates an instance of a specified data type.
        /// </summary>
        /// <param name="Type">Datatype</param>
        /// <param name="Length">Number of elements</param>
        /// <returns>New instance</returns>
        public static IDataType DatatypeCreate(Type Type, ulong Length)
        {
            if (CurrentGame.Environment.MaxDataTypes.ContainsKey(Type))
            {
                IDataType obj = CurrentGame.Environment.MaxDataTypes[Type].InstanceCreate(Length);
                return obj;
            }

            // throw an exception on failure
            throw new Exception("The type " + Type.FullName + " does not inherit Type and cannot be instantiated as a Max Datatype.");

        }
    }
}

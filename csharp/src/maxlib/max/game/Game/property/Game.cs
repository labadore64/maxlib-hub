﻿namespace max.game
{
    public abstract partial class Game
    {
        /// <summary>
        /// The current running game.
        /// </summary>
        /// <value>Game</value>
        public static Game CurrentGame { get; private set; }
    }
}

﻿using System;

namespace max.game
{

    public partial class Game
    {

        /// <summary>
        /// Calls the exit routine.
        /// </summary>
        internal GameState DoExit()
        {
            try
            {
                OnExit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n\n" + e.StackTrace + "\n\n" + e.Source);
                // exception thrown
                return GameState.GAME_EXIT_FAILURE;
            }

            // successful initialization
            return CurrentState;
        }

        /// <summary>
        /// The exit routine of your game.
        /// </summary>
        protected abstract void OnExit();

        /// <summary>
        /// Exits the game.
        /// </summary>
        public static void Exit()
        {
            CurrentGame.CurrentState = GameState.GAME_EXIT;
        }
    }
}

﻿using System;

namespace max.game
{

    public abstract partial class Game
    {
        /// <summary>
        /// Initializes the game.
        /// </summary>
        internal GameState Initialize()
        {
            try
            {
                //sets the current game to this one
                CurrentGame = this;

                InitializeMem();
                InitializeEnv();

                OnInitialize();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n\n" + e.StackTrace + "\n\n" + e.Source);
                // exception thrown
                return GameState.GAME_INIT_FAILURE;
            }

            // successful initialization
            return CurrentState;
        }

        /// <summary>
        /// The initialization of your game.
        /// </summary>
        protected abstract void OnInitialize();

    }
}

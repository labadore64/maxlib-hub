﻿using System;

namespace max.game
{

    public partial class Game
    {
        /// <summary>
        /// Updates the game state.
        /// </summary>
        /// <returns>Whether the game exits.</returns>
        internal GameState Update()
        {
            try
            {
                Environment.Update();
                OnUpdate();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n\n" + e.StackTrace + "\n\n" + e.Source);
                // exception thrown
                return GameState.GAME_MAIN_FAILURE;
            }

            // successful initialization
            return CurrentState;
        }

        /// <summary>
        /// Update the game's state here.
        /// </summary>
        protected abstract void OnUpdate();
    }
}

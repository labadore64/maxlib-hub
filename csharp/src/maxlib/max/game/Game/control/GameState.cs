﻿using System;

namespace max.game
{

    public partial class Game
    {

        /// <summary>
        /// Current gamestate.
        /// </summary>
        /// <value>Game state</value>
        protected GameState CurrentState { get; set; } = GameState.GAME_CONTINUE;
    }
}

﻿using System;

namespace max.game
{
    public partial class Game
    {
		/// <summary>
		/// Runs the game.
		/// </summary>
        public void Run()
        {
            CurrentState = Initialize();

            while (CurrentState == GameState.GAME_CONTINUE)
            {
                CurrentState = Update();
            }

            if(CurrentState == GameState.GAME_EXIT)
            {
                CurrentState = DoExit();
            }

            Console.WriteLine("Game exited with state: " + CurrentState.ToString());
        }
    }
}

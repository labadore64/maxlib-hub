﻿namespace max.game
{
    public abstract partial class Game
    {
        /// <summary>
        /// The Property index in the split array.
        /// </summary>
        /// <value>Index of property</value>
        internal const int SPLITUID_PROPERTY = 0;
        /// <summary>
        /// The Type in the split array.
        /// </summary>
        /// <value>Index of type</value>
        internal const int SPLITUID_TYPE = 1;
        /// <summary>
        /// The UID in the split array.
        /// </summary>
        /// <value>Index of UID</value>
        internal const int SPLITUID_ID = 2;
        /// <summary>
        /// The Complex value in the split array.
        /// </summary>
        /// <value>Index of complex value</value>
        internal const int SPLITUID_COMPLEX = 3;
    }
}

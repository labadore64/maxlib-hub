﻿namespace max.game
{
    public abstract partial class Game
    {
        /// <summary>
        /// Retrieves the Type from the Property ID.
        /// </summary>
        /// <returns>Type</returns>
        /// <param name="PropertyID">Property ID</param>
        static internal ulong GetPropertyType(ulong PropertyID)
        {
            return (PropertyID >> 24) & 0xFF;
        }
    }
}

﻿namespace max.game
{
    public abstract partial class Game
    {
        /// <summary>
        /// Returns if the Property ID represents a complex type.
        /// </summary>
        /// <returns>True/False</returns>
        /// <param name="PropertyID">Property ID</param>
        static internal ulong GetPropertyComplex(ulong PropertyID)
        {
            return (PropertyID >> 31) & 1;
        }
    }
}

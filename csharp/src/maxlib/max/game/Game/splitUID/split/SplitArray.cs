﻿namespace max.game
{
    public abstract partial class Game
    {
        /// <summary>
        /// Splits the property value into a 4-index array that includes
        /// the Unique, the type ID, complex flag, and the Property ID.
        /// </summary>
        /// <returns>Split properties</returns>
        /// <param name="PropertyID">Property ID.</param>
        static internal ulong[] SplitProperty(ulong PropertyID)
        {
            ulong[] Split = new ulong[4];

            Split[SPLITUID_PROPERTY] = PropertyID & 0xFFFFFF;
            Split[SPLITUID_TYPE] = (PropertyID >> 24) & 0xFF;
            Split[SPLITUID_ID] = (PropertyID >> 32) & 0xFFFFFFFF;
            Split[SPLITUID_COMPLEX] = (PropertyID >> 31) & 1;

            return Split;
        }
    }
}

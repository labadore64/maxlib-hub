﻿namespace max.game
{
    public abstract partial class Game
    {

        /// <summary>
        /// Retrieves the Unique from the Property ID.
        /// </summary>
        /// <returns>Unique</returns>
        /// <param name="PropertyID">Property ID</param>
        static internal ulong GetUnique(ulong PropertyID)
        {
            return (PropertyID >> 32) & 0xFFFFFFFF;
        }
    }
}

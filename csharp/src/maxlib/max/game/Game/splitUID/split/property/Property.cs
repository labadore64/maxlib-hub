﻿namespace max.game
{
    public abstract partial class Game
    {
        /// <summary>
        /// Retrieves the Property Index from the Property ID.
        /// </summary>
        /// <returns>PropertyIndex</returns>
        /// <param name="PropertyID">Property ID</param>
        static internal ulong GetPropertyIndex(ulong PropertyID)
        {
            return PropertyID & 0xFFFFFF;
        }
    }
}

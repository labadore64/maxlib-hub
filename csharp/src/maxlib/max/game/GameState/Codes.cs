﻿namespace max.game
{
    /// <summary>
    /// Represents different states of the game running.
    /// </summary>
    public enum GameState
    {
        /// <summary>
        /// Game continues running
        /// </summary>
        GAME_CONTINUE,
        /// <summary>
        /// Game exits gracefully
        /// </summary>
        GAME_EXIT,
        /// <summary>
        /// Game failed in the main loop
        /// </summary>
        GAME_MAIN_FAILURE,
        /// <summary>
        /// Game failed while exiting
        /// </summary>
        GAME_EXIT_FAILURE,
        /// <summary>
        /// Game failed while initializing
        /// </summary>
        GAME_INIT_FAILURE
    }
}

#!/bin/sh

. ./scripts/posix/realdir.sh

path="$(relative_path_from_to "$(dirname "$2")" "$1")"

RESULT=0
[ -L "$2" ] && rm -f "$2" && echo "Removed old symbolic link $2 ."
ln -s "${path}" "$2" && echo "Wrote symbolic link $2 -> ${path} ." || {
    RESULT="$?"
    echo "Failed to write symbolic link $2 -> ${path} !"
}

exit "${RESULT}"


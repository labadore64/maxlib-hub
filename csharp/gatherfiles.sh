#!/bin/sh
# gatherfiles.sh
# gather C# files recursively and spit them out

# TODO: make sure the user has dirname, readlink and find programs

CURRENTWD="`pwd`"
SCRIPT_DIR='.' # fallback
cd "$(dirname "$(readlink -f "$BASH_SOURCE")")" && {
    SCRIPT_DIR="`pwd`"
    cd "${CURRENTWD}"
} 

FIRST=
CURRENTWD="`pwd`"
cd "${SCRIPT_DIR}"
find "${SCRIPT_DIR}/$1" -name '*.cs' | while IFS= read -r part
do
    [ ! -z "$FIRST" ] && printf ' ' || {
        FIRST=no
    }
    printf '%s' "$part"
done
printf '\n'
cd "${CURRENTWD}"


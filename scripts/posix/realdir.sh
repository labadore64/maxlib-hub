#!/bin/sh

realpath() {
    canonicalize_path "$(resolve_symlinks "$1")"
}

resolve_symlinks() {
    _resolve_symlinks "$1"
}

_resolve_symlinks() {
    _assert_no_path_cycles "$@" || return

    local dir_context path
    path=$(readlink -- "$1")
    [ $? -eq 0 ] && {
        dir_context=$(dirname -- "$1")
        _resolve_symlinks "$(_prepend_dir_context_if_necessary "$dir_context" "$path")" "$@"
    } || {
        printf '%s\n' "$1"
    }
}

_prepend_dir_context_if_necessary() {
    [ "$1" = . ] && {
        printf '%s\n' "$2"
    } || {
        _prepend_path_if_relative "$1" "$2"
    }
}

_prepend_path_if_relative() {
    case "$2" in
        /* ) printf '%s\n' "$2" ;;
         * ) printf '%s\n' "$1/$2" ;;
    esac
}

_assert_no_path_cycles() {
    local target path

    target="$1"
    shift

    for path in "$@"; do
        [ "$path" = "$target" ] && {
            return 1
        }
    done
}

canonicalize_path() {
    [ -d "$1" ] && {
        _canonicalize_dir_path "$1"
    } || {
        _canonicalize_file_path "$1"
    }
}

_canonicalize_dir_path() {
    ( cd "$1" 2>/dev/null && pwd -P )
}

_canonicalize_file_path() {
    local dir file
    dir=$(dirname -- "$1")
    file=$(basename -- "$1")
    ( cd "$dir" 2>/dev/null && printf '%s/%s\n' "$(pwd -P)" "$file" )
}

readlink() {
    _has_command readlink && {
        _system_readlink "$@"
    } || {
        _emulated_readlink "$@"
    }
}

_has_command() {
    hash -- "$1" 2>/dev/null
}

_system_readlink() {
    command readlink "$@"
}

_emulated_readlink() {
    [ "$1" = -- ] && {
        shift
    }

    _gnu_stat_readlink "$@" || _bsd_stat_readlink "$@"
}

_gnu_stat_readlink() {
    local output
    output=$(stat -c %N -- "$1" 2>/dev/null) &&

    printf '%s\n' "$output" |
        sed "s/^‘[^’]*’ -> ‘\(.*\)’/\1/
             s/^'[^']*' -> '\(.*\)'/\1/"
    # FIXME: handle newlines
}

_bsd_stat_readlink() {
    stat -f %Y -- "$1" 2>/dev/null
}

relative_path_from_to() {
    local path1 path2 common path1common path2common base cmp halt up prefix i
    path1="$(canonicalize_path "$1")"
    path2="$(canonicalize_path "$2")"

    common=
    path1common="${path1}"
    path2common="${path2}"
    halt=
    while [ -z "${halt}" ]
    do
        base="$(printf '%s' "${path1common}" | sed 's|^\(/[^/]*\).*$|\1|')"
        cmp="$(printf '%s' "${path2common}" | sed 's|^\(/[^/]*\).*$|\1|')"
        [ "${base}" = "${cmp}" ] && {
            baselen="$(printf '%s' "${base}" | wc -m)"
            path1common="$(printf '%s' "${path1common}" | cut -c"$((baselen + 1))"-)"
            path2common="$(printf '%s' "${path2common}" | cut -c"$((baselen + 1))"-)"
            common="${common}${base}"
        } || {
            halt=yes
            path2common="$(printf '%s' "${path2common}" | cut -c2-)"
        }
    done

    up="$(printf '%s' "${path1common}" | grep -o "/" | wc -l)"
    prefix=

    i=0
    while [ "$i" -ne "$up" ]
    do
        prefix="../${prefix}"
        i="$((i + 1))"
    done

    printf '%s\n' "${prefix}${path2common}"
}

[ -z "$SCRIPT_DIR" ] && {
    SCRIPT_DIR='.' # fallback
    CWDCOPY="`pwd`"
    cd "$(dirname "$(resolve_symlinks "${BASH_SOURCE}")")" && {
        SCRIPT_DIR="$(resolve_symlinks "`pwd`")"
        cd "$CWDCOPY"
    } 
}


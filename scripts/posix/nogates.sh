#!/bin/sh
# silence the annoying microsoft copyright lines with every invocation of
# the C# compiler. hehe.
CSCPROGRAM="$1"
shift
"${CSCPROGRAM}" "$@" | sed '1,3d'

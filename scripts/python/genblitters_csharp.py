# blitting routines generator

import os, sys, datetime, importlib.util

if __name__ != '__main__':
    print("This program is not a module.", file=sys.stderr)
    sys.exit(1)

script_path = os.path.dirname(os.path.realpath(__file__))

spec = importlib.util.spec_from_file_location("csty", script_path+"/csharp_types.py")
csty = importlib.util.module_from_spec(spec)
spec.loader.exec_module(csty)

filename, namespace, classname, visibility = sys.argv[1:5]

print(f"""// This is the {namespace}.{classname} class.
//
// {filename} was automatically generated at
//
//   {str(datetime.datetime.utcnow())} (UTC)
//
// by scripts/python/genblit_csharp.py, which is a part of the Max Library
// Machine utility scripts. It provides (relatively) fast blitting routines for
// C# as a convenience for interfacing with the max kernel (mxker), and these
// also support punning during blits. Concrete methods are about twice as fast
// as generics on both Mono and .net, and this is owed to the fact that unlike
// C# generic methods, C# concrete methods can be compiled to native code. In
// particular, C# generics are this way because their mechanism is not to
// generate a set of concrete instances as is done for templates by C++
// compilers, but adjust implementation at runtime directly from the IL code.
//
// Since the entire purpose of these routines is to offer real time blitting
// performance, C# generics are unacceptable, and so they are implemented in
// generic form as formatting templates in a python program which is used to
// regenerate the file if they are changed for any reason. The bootstrap
// process is opinionated against invoking any languages other than C and
// shell scripts (only as a stop gap until the entire process is written in
// C), so this is done by the developer in the course of maintenance.

using System;

namespace {namespace} {{
    {visibility} static class {classname} {{""", end='')
print(f"""
        ///////////////////////////////////////////////////////
        // Punning To Circumvent Type Conversion Limitations //
        ///////////////////////////////////////////////////////

        {visibility} static double PunLongDouble(long li)
            {{ unsafe {{ return *((double *)&li); }} }}

        {visibility} static long PunDoubleLong(double d)
            {{ unsafe {{ return *((long *)&d); }} }}

        {visibility} static float PunIntFloat(int i)
            {{ unsafe {{ return *((float *)&i); }} }}

        {visibility} static int PunFloatInt(float f)
            {{ unsafe {{ return *((int *)&f); }} }}

        {visibility} static int PunBoolInt(bool b)
            {{ unsafe {{ return *((int *)&b); }} }}

        {visibility} static bool PunIntBool(int i)
            {{ unsafe {{ return *((bool *)&i); }} }}""")

print("""
        ///////////////////////////////////////
        // Pointer-Pointer Blitting Routines //
        ///////////////////////////////////////""")
for name1, ty1, size1 in zip(csty.nominal, csty.datatypes, csty.datasizes):
    for name2, ty2, size2 in zip(csty.nominal, csty.datatypes, csty.datasizes):
        if size1 == size2:
            print(f"""
        unsafe {visibility} static void Blit{name1}Ptr{name2}Ptr({ty2} *pdst, {ty1} *psrc, int n)
            {{ while (n-- > 0) *pdst++ = *{'('+ty2+' *)' if ty1 != ty2 else ''}psrc++; }}""")

print("""
        ///////////////////////////////////////
        // Address-Address Blitting Routines //
        ///////////////////////////////////////""")
for name1, ty1, size1 in zip(csty.nominal, csty.datatypes, csty.datasizes):
    for name2, ty2, size2 in zip(csty.nominal, csty.datatypes, csty.datasizes):
        if size1 == size2:
            print(f"""
        unsafe {visibility} static void Blit{name1}Addr{name2}Addr(IntPtr dstaddr, IntPtr srcaddr, int n)
        {{
            {ty2} *pdst = ({ty2} *)dstaddr;
            {ty1} *psrc = ({ty1} *)srcaddr;
            while (n-- > 0) *pdst++ = *{'('+ty2+' *)' if ty1 != ty2 else ''}psrc++;
        }}""")

print("""
        ///////////////////////////////////
        // Array-Array Blitting Routines //
        ///////////////////////////////////""")
for name1, ty1, size1 in zip(csty.nominal, csty.datatypes, csty.datasizes):
    for name2, ty2, size2 in zip(csty.nominal, csty.datatypes, csty.datasizes):
        if size1 == size2:
            print(f"""
        unsafe {visibility} static void Blit{name1}Arr{name2}Arr({ty2}[] dst, {ty1}[] src, int n)
        {{
            fixed ({ty2} *fdst = &dst[0])
            {{
                fixed ({ty1} *pfsrc = &src[0])
                {{
                    {ty2} *pdst = pfdst;
                    {ty1} *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *{'('+ty2+' *)' if ty1 != ty2 else ''}psrc++;
                }}
            }}
        }}

        unsafe {visibility} static void Blit{name1}Arr{name2}Arr({ty2}[] dst, int idst, {ty1}[] src, int n)
        {{
            fixed ({ty2} *fdst = &dst[0])
            {{
                fixed ({ty1} *pfsrc = &src[0])
                {{
                    {ty2} *pdst = pfdst + idst;
                    {ty1} *psrc = pfsrc;
                    while (n-- > 0) *pdst++ = *{'('+ty2+' *)' if ty1 != ty2 else ''}psrc++;
                }}
            }}
        }}

        unsafe {visibility} static void Blit{name1}Arr{name2}Arr({ty2}[] dst, {ty1}[] src, int isrc, int n)
        {{
            fixed ({ty2} *fdst = &dst[0])
            {{
                fixed ({ty1} *pfsrc = &src[0])
                {{
                    {ty2} *pdst = pfdst;
                    {ty1} *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *{'('+ty2+' *)' if ty1 != ty2 else ''}psrc++;
                }}
            }}
        }}

        unsafe {visibility} static void Blit{name1}Arr{name2}Arr({ty2}[] dst, int idst, {ty1}[] src, int isrc, int n)
        {{
            fixed ({ty2} *fdst = &dst[0])
            {{
                fixed ({ty1} *pfsrc = &src[0])
                {{
                    {ty2} *pdst = pfdst + idst;
                    {ty1} *psrc = pfsrc + isrc;
                    while (n-- > 0) *pdst++ = *{'('+ty2+' *)' if ty1 != ty2 else ''}psrc++;
                }}
            }}
        }}""")

print("""
        ///////////////////////////////////////
        // Address-Pointer Blitting Routines //
        ///////////////////////////////////////""")
for name1, ty1, size1 in zip(csty.nominal, csty.datatypes, csty.datasizes):
    for name2, ty2, size2 in zip(csty.nominal, csty.datatypes, csty.datasizes):
        if size1 == size2:
            print(f"""
        unsafe {visibility} static void Blit{name1}Addr{name2}Ptr({ty2} *pdst, IntPtr srcaddr, int n)
        {{
            {ty1} *psrc = ({ty1} *)srcaddr;
            while (n-- > 0) *pdst++ = *{'('+ty2+' *)' if ty1 != ty2 else ''}psrc++;
        }}

        unsafe {visibility} static void Blit{name1}Ptr{name2}Addr(IntPtr dstaddr, {ty1} *psrc, int n)
        {{
            {ty2} *pdst = ({ty2} *)dstaddr;
            while (n-- > 0) *pdst++ = *{'('+ty2+' *)' if ty1 != ty2 else ''}psrc++;
        }}""")

print("""
        /////////////////////////////////////
        // Array-Pointer Blitting Routines //
        /////////////////////////////////////""")
for name1, ty1, size1 in zip(csty.nominal, csty.datatypes, csty.datasizes):
    for name2, ty2, size2 in zip(csty.nominal, csty.datatypes, csty.datasizes):
        if size1 == size2:
            print(f"""
        unsafe {visibility} static void Blit{name1}Arr{name2}Ptr({ty2} *pdst, {ty1}[] src, int n)
        {{
            fixed ({ty1} *pfsrc = &src[0])
            {{
                {ty1} *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *{'('+ty2+' *)' if ty1 != ty2 else ''}psrc++;
            }}
        }}

        unsafe {visibility} static void Blit{name1}Arr{name2}Ptr({ty2} *pdst, {ty1}[] src, int isrc, int n)
        {{
            fixed ({ty1} *pfsrc = &src[0])
            {{
                {ty1} *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *{'('+ty2+' *)' if ty1 != ty2 else ''}psrc++;
            }}
        }}

        unsafe {visibility} static void Blit{name1}Ptr{name2}Arr({ty2}[] dst, {ty1} *psrc, int n)
        {{
            fixed ({ty2} *pfdst = &dst[0])
            {{
                {ty2} *pdst = pfdst;
                while (n-- > 0) *pdst++ = *{'('+ty2+' *)' if ty1 != ty2 else ''}psrc++;
            }}
        }}

        unsafe {visibility} static void Blit{name1}Ptr{name2}Arr({ty2}[] dst, int idst, {ty1} *psrc, int n)
        {{
            fixed ({ty2} *pfdst = &dst[0])
            {{
                {ty2} *pdst = pfdst + idst;
                while (n-- > 0) *pdst++ = *{'('+ty2+' *)' if ty1 != ty2 else ''}psrc++;
            }}
        }}""")

print("""
        /////////////////////////////////////
        // Array-Address Blitting Routines //
        /////////////////////////////////////""")
for name1, ty1, size1 in zip(csty.nominal, csty.datatypes, csty.datasizes):
    for name2, ty2, size2 in zip(csty.nominal, csty.datatypes, csty.datasizes):
        if size1 == size2:
            print(f"""
        unsafe {visibility} static void Blit{name1}Arr{name2}Ptr(IntPtr dstaddr, {ty1}[] src, int n)
        {{
            fixed ({ty1} *pfsrc = &src[0])
            {{
                {ty2} *pdst = ({ty2} *)dstaddr;
                {ty1} *psrc = pfsrc;
                while (n-- > 0) *pdst++ = *{'('+ty2+' *)' if ty1 != ty2 else ''}psrc++;
            }}
        }}

        unsafe {visibility} static void Blit{name1}Arr{name2}Ptr(IntPtr dstaddr, {ty1}[] src, int isrc, int n)
        {{
            fixed ({ty1} *pfsrc = &src[0])
            {{
                {ty2} *pdst = ({ty2} *)dstaddr;
                {ty1} *psrc = pfsrc + isrc;
                while (n-- > 0) *pdst++ = *{'('+ty2+' *)' if ty1 != ty2 else ''}psrc++;
            }}
        }}

        unsafe {visibility} static void Blit{name1}Ptr{name2}Arr({ty2}[] dst, IntPtr srcaddr, int n)
        {{
            fixed ({ty2} *pfdst = &dst[0])
            {{
                {ty2} *pdst = pfdst;
                {ty1} *psrc = ({ty1} *)srcaddr;
                while (n-- > 0) *pdst++ = *{'('+ty2+' *)' if ty1 != ty2 else ''}psrc++;
            }}
        }}

        unsafe {visibility} static void Blit{name1}Ptr{name2}Arr({ty2}[] dst, int idst, IntPtr srcaddr, int n)
        {{
            fixed ({ty2} *pfdst = &dst[0])
            {{
                {ty2} *pdst = pfdst + idst;
                {ty1} *psrc = ({ty1} *)srcaddr;
                while (n-- > 0) *pdst++ = *{'('+ty2+' *)' if ty1 != ty2 else ''}psrc++;
            }}
        }}""")

print("""    }
}
""")


nominal = [
    'Bool',
    'Byte', 'SByte',
    'Char', 'Short', 'UShort',
    'Int', 'UInt',
    'Long', 'ULong',
    'Float',
    'Double'
]

datatypes = [
    'bool',
    'byte', 'sbyte',
    'char', 'short', 'ushort',
    'int', 'uint',
    'long', 'ulong',
    'float',
    'double'
]

# only like sized types should be punned if we don't want to open up a whole
# nother can of worms re endianness. we can make the assumption about these
# type sizes because the C# specification requires that types have a fixed
# size, and we are not interested in C# bindings for platforms which do not
# have integer and floating point types for these sizes. if a platform not
# supporting these sizes is desired, a different set of language bindings
# should be used.
datasizes = [
    4,
    1, 1,
    2, 2, 2,
    4, 4,
    8, 8,
    4,
    8
]


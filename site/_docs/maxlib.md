---
title: Developer Docs
layout: apidocs
sidebar:
  nav: "docs"
---

# Max Library Machine Developer Docs

![Max Library Machine logo, a dog with a black scarf](/assets/images/maxlogo_big.png)

Max Library Machine is documented to allow developers and end users to understand, modify and reimplement Max as they see fit. 

Select one of the topics to the left to get started.
---
title: class ShortArray
toc: true
---

# class ShortArray
## Summary
This class represents a signed 16-bit array managed by the Max Environment.

<div class="tiny-text">syntax:</div>
```
public class ShortArray : Array<short>
```
<div class="tiny-text">namespace: <a href="/docs/apidocs/csharp/max.process.arr.type">max.process.arr.type</a></div>

## Constructors

### ShortArray(IntPtr Pointer, ulong Elements, ulong Alignment)

Instantiates an array of shorts with a given pointer, element count and alignment.

<div class="tiny-text">syntax:</div>
```
public ShortArray(IntPtr Pointer, ulong Elements, ulong Alignment) : base(Pointer, Elements, Alignment)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">IntPtr</td>
	<td class="params__name">Pointer</td>
	<td>Array pointer</td>
</tr><tr>
	<td class="params__type"></td>
	<td class="params__name">Count</td>
	<td>Number of elements</td>
</tr><tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Alignment</td>
	<td>Alignment</td>
</tr>
	</tbody>
</table>
### ShortArray(ulong Count)

Instantiates an array of shorts with a certain number of elements.

<div class="tiny-text">syntax:</div>
```
public ShortArray(ulong Count) : base(Count)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Count</td>
	<td>Number of elements</td>
</tr>
	</tbody>
</table>
### ShortArray(short[] Elements)

Instantiates an array of shorts with an array.

<div class="tiny-text">syntax:</div>
```
public ShortArray(short[] Elements) : base(Elements)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">short[]</td>
	<td class="params__name">Elements</td>
	<td>Elements</td>
</tr>
	</tbody>
</table>




## Methods

### CopyFromLength(short[] Array, int Length)

Copies a source array to this one, of a certain length of elements.

<div class="tiny-text">syntax:</div>
```
protected override void CopyFromLength(short[] Array, int Length)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">short[]</td>
	<td class="params__name">Array</td>
	<td>Source</td>
</tr><tr>
	<td class="params__type">int</td>
	<td class="params__name">Length</td>
	<td>Length</td>
</tr>
	</tbody>
</table>


### InstanceCreate(ulong Length)

Creates an instance of ShortArray of a specified length.

<div class="tiny-text">syntax:</div>
```
public override IDataType InstanceCreate(ulong Length)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Length</td>
	<td>Length</td>
</tr>
	</tbody>
</table>

 Returns: Instance *( override IDataType )*


## Operators

#### ShortArray

Implicitly converts a short array to an instance of ShortArray.

<div class="tiny-text">syntax:</div>
```
public static implicit operator ShortArray(short[] Value) => new ShortArray(Value);
```

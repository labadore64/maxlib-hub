---
title: namespace max.process.arr.type
---

# namespace max.process.arr.type

## Members

* **[IDataType](/docs/apidocs/csharp/max.process.arr.type.IDataType)** - An interface representing a data type in the Max environment.
* **[MaxProperty](/docs/apidocs/csharp/max.process.arr.type.MaxProperty)** - An attribute used on MaxObject properties to add them to the Max Environment.
* **[MaxObject](/docs/apidocs/csharp/max.process.arr.type.MaxObject)** - An abstract class that represents an object in the max environment.
* **[Array < T > ](/docs/apidocs/csharp/max.process.arr.type.ArrayT)** - This abstract class represents an array managed by the Max Environment.
* **[BoolArray](/docs/apidocs/csharp/max.process.arr.type.BoolArray)** - This class represents a boolean array managed by the Max Environment.
* **[ByteArray](/docs/apidocs/csharp/max.process.arr.type.ByteArray)** - This class represents a byte array managed by the Max Environment.
* **[CharArray](/docs/apidocs/csharp/max.process.arr.type.CharArray)** - This class represents a char array managed by the Max Environment. Note that chars are 16-bit wide.
* **[DoubleArray](/docs/apidocs/csharp/max.process.arr.type.DoubleArray)** - This class represents a double array managed by the Max Environment.
* **[FloatArray](/docs/apidocs/csharp/max.process.arr.type.FloatArray)** - This class represents a float array managed by the Max Environment.
* **[IntArray](/docs/apidocs/csharp/max.process.arr.type.IntArray)** - This class represents a signed 32-bit array managed by the Max Environment.
* **[LongArray](/docs/apidocs/csharp/max.process.arr.type.LongArray)** - This class represents a signed 64-bit array managed by the Max Environment.
* **[SByteArray](/docs/apidocs/csharp/max.process.arr.type.SByteArray)** - This class represents a signed byte array managed by the Max Environment.
* **[ShortArray](/docs/apidocs/csharp/max.process.arr.type.ShortArray)** - This class represents a signed 16-bit array managed by the Max Environment.
* **[UIntArray](/docs/apidocs/csharp/max.process.arr.type.UIntArray)** - This class represents an unsigned 32-bit array managed by the Max Environment.
* **[ULongArray](/docs/apidocs/csharp/max.process.arr.type.ULongArray)** - This class represents an unsigned 64-bit array managed by the Max Environment.
* **[UShortArray](/docs/apidocs/csharp/max.process.arr.type.UShortArray)** - This class represents an unsigned 16-bit array managed by the Max Environment.

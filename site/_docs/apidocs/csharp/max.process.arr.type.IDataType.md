---
title: interface IDataType
toc: true
---

# interface IDataType

An interface representing a data type in the Max environment.

<div class="tiny-text">syntax:</div>
```
public interface IDataType
```
<div class="tiny-text">namespace: <a href="/docs/apidocs/csharp/max.process.arr.type">max.process.arr.type</a></div>

## Properties

### DataPointer

Pointer to the data.

<div class="tiny-text">syntax:</div>
```
IntPtr DataPointer { get; }
```
### DataLength

Length of the data in array elements, as a ulong.

<div class="tiny-text">syntax:</div>
```
ulong DataLength { get; }
```
### Length

Length of the data in array elements.

<div class="tiny-text">syntax:</div>
```
int Length { get; }
```
### Alignment

Alignment of the data.

<div class="tiny-text">syntax:</div>
```
ulong Alignment { get; }
```


## Methods

### InstanceCreate(ulong length)

Creates an instance of this datatype, of a certain length.

<div class="tiny-text">syntax:</div>
```
IDataType InstanceCreate(ulong length);
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">ulong</td>
	<td class="params__name">length</td>
	<td>Length</td>
</tr>
	</tbody>
</table>

 Returns: Instance *(Type )*
### SetData(IDataType Data)

Sets the data in this datatype to be the same as a different datatype.

<div class="tiny-text">syntax:</div>
```
void SetData(IDataType Data);
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">IDataType</td>
	<td class="params__name">Data</td>
	<td>Data</td>
</tr>
	</tbody>
</table>



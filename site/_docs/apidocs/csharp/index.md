---
title: Max C# API Documentation
---

# Max C# API documentation

This API documentation explains all ``public`` and ``protected`` methods of the components in Max C#.

## Table of Contents

* [max](/docs/apidocs/csharp/max)
	* [max.game](/docs/apidocs/csharp/max.game)
		* [Game](/docs/apidocs/csharp/max.game.Game)
		* [GameState](/docs/apidocs/csharp/max.game.GameState)
	* [max.process](/docs/apidocs/csharp/max.process)
		* [max.process.env](/docs/apidocs/csharp/max.process.env)
			* [MaxEnvironment](/docs/apidocs/csharp/max.process.env.MaxEnvironment)
		* [max.process.arr](/docs/apidocs/csharp/max.process.arr)
			* [max.process.arr.type](/docs/apidocs/csharp/max.process.arr.type)
				* [IDataType](/docs/apidocs/csharp/max.process.arr.type.IDataType)
				* [MaxProperty](/docs/apidocs/csharp/max.process.arr.type.MaxProperty)
				* [MaxObject](/docs/apidocs/csharp/max.process.arr.type.MaxObject)
				* [Array < T > ](/docs/apidocs/csharp/max.process.arr.type.ArrayT)
				* [BoolArray](/docs/apidocs/csharp/max.process.arr.type.BoolArray)
				* [ByteArray](/docs/apidocs/csharp/max.process.arr.type.ByteArray)
				* [CharArray](/docs/apidocs/csharp/max.process.arr.type.CharArray)
				* [DoubleArray](/docs/apidocs/csharp/max.process.arr.type.DoubleArray)
				* [FloatArray](/docs/apidocs/csharp/max.process.arr.type.FloatArray)
				* [IntArray](/docs/apidocs/csharp/max.process.arr.type.IntArray)
				* [LongArray](/docs/apidocs/csharp/max.process.arr.type.LongArray)
				* [SByteArray](/docs/apidocs/csharp/max.process.arr.type.SByteArray)
				* [ShortArray](/docs/apidocs/csharp/max.process.arr.type.ShortArray)
				* [UIntArray](/docs/apidocs/csharp/max.process.arr.type.UIntArray)
				* [ULongArray](/docs/apidocs/csharp/max.process.arr.type.ULongArray)
				* [UShortArray](/docs/apidocs/csharp/max.process.arr.type.UShortArray)

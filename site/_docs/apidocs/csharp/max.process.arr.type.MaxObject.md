---
title: class MaxObject
toc: true
---

# class MaxObject
## Summary
An abstract class that represents an object in the max environment.

<div class="tiny-text">syntax:</div>
```
public abstract class MaxObject
```
<div class="tiny-text">namespace: <a href="/docs/apidocs/csharp/max.process.arr.type">max.process.arr.type</a></div>



## Properties

### Disposed

Whether or not this object has been disposed. If not disposed, it has not been removed from the Max Library Machine kernel's space yet.

<div class="tiny-text">syntax:</div>
```
public bool Disposed { get; private set; }
```
### Initialized

Whether or not this object has been initialized. If not initialized, it does not exist in the Max Library Machine kernel's space yet.

<div class="tiny-text">syntax:</div>
```
public bool Initialized { get; private set; }
```
### UID

The max UID value, used to identify this object in the kernel.

<div class="tiny-text">syntax:</div>
```
public ulong UID { get; private set; }
```


## Methods

### Create()

The object's creation method. The developer can override this method so that custom routines can be defined. If not overriden, this method does nothing.

<div class="tiny-text">syntax:</div>
```
public virtual void Create()
```




### Destroy()

The object's destruction method. The developer can override this method so that custom routines can be defined. The developer should release resources and destroy child objects with this method as well as general destruction methods. If not overriden, this method does nothing.

<div class="tiny-text">syntax:</div>
```
public virtual void Destroy()
```




### Dispose()

Disposes this MaxObject and prepares it to be destroyed in the kernel.

<div class="tiny-text">syntax:</div>
```
public void Dispose()
```




### InstanceCreate()

Creates a new empty instance of this object.

<div class="tiny-text">syntax:</div>
```
protected abstract MaxObject InstanceCreate();
```



 Returns: New instance *( abstract MaxObject )*
### AddProperty(string Name, IDataType Type)

Adds a new property to the object's property list.

<div class="tiny-text">syntax:</div>
```
protected void AddProperty(string Name, IDataType Type)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">string</td>
	<td class="params__name">Name</td>
	<td>Name</td>
</tr><tr>
	<td class="params__type">IDataType</td>
	<td class="params__name">Type</td>
	<td>Type</td>
</tr>
	</tbody>
</table>

 Returns: Property ID *( void )*
### get()

Gets a property's value.

<div class="tiny-text">syntax:</div>
```
protected IDataType get([System.Runtime.CompilerServices.CallerMemberName] string Name = "")
```



 Returns: Value *( IDataType )*
### set(IDataType Value)

Sets a property's value.

<div class="tiny-text">syntax:</div>
```
protected void set(IDataType Value, [System.Runtime.CompilerServices.CallerMemberName] string Name = "")
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">IDataType</td>
	<td class="params__name">Value</td>
	<td>Value</td>
</tr>
	</tbody>
</table>





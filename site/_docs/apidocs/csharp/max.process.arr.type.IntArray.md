---
title: class IntArray
toc: true
---

# class IntArray
## Summary
This class represents a signed 32-bit array managed by the Max Environment.

<div class="tiny-text">syntax:</div>
```
public class IntArray : Array<int>
```
<div class="tiny-text">namespace: <a href="/docs/apidocs/csharp/max.process.arr.type">max.process.arr.type</a></div>

## Constructors

### IntArray(IntPtr Pointer, ulong Count, ulong Alignment)

Instantiates an array of ints with a given pointer, element count and alignment.

<div class="tiny-text">syntax:</div>
```
public IntArray(IntPtr Pointer, ulong Count, ulong Alignment) : base(Pointer, Count, Alignment)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">IntPtr</td>
	<td class="params__name">Pointer</td>
	<td>Array pointer</td>
</tr><tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Count</td>
	<td>Number of elements</td>
</tr><tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Alignment</td>
	<td>Alignment</td>
</tr>
	</tbody>
</table>
### IntArray(ulong Count)

Instantiates an array of ints with a certain number of elements.

<div class="tiny-text">syntax:</div>
```
public IntArray(ulong Count) : base(Count)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Count</td>
	<td>Number of elements</td>
</tr>
	</tbody>
</table>
### IntArray(int[] Elements)

Instantiates an array of ints with an array.

<div class="tiny-text">syntax:</div>
```
public IntArray(int[] Elements) : base(Elements)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">int[]</td>
	<td class="params__name">Elements</td>
	<td>Elements</td>
</tr>
	</tbody>
</table>




## Methods

### CopyFromLength(int[] Array, int Length)

Copies a source array to this one, of a certain length of elements.

<div class="tiny-text">syntax:</div>
```
protected override void CopyFromLength(int[] Array, int Length)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">int[]</td>
	<td class="params__name">Array</td>
	<td>Source</td>
</tr><tr>
	<td class="params__type">int</td>
	<td class="params__name">Length</td>
	<td>Length</td>
</tr>
	</tbody>
</table>


### InstanceCreate(ulong Length)

Creates an instance of IntArray of a specified length.

<div class="tiny-text">syntax:</div>
```
public override IDataType InstanceCreate(ulong Length)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Length</td>
	<td>Length</td>
</tr>
	</tbody>
</table>

 Returns: Instance *( override IDataType )*


## Operators

#### IntArray

Implicitly converts a int array to an instance of IntArray.

<div class="tiny-text">syntax:</div>
```
public static implicit operator IntArray(int[] Value) => new IntArray(Value);
```

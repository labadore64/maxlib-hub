---
title: class MaxEnvironment
toc: true
---

# class MaxEnvironment
## Summary
This class represents the Environment Controller for the MaxEnvironment. This controller manages the various components of the MaxEnvironment and acts as a public interface outside of the library for the MaxEnvironment.

<div class="tiny-text">syntax:</div>
```
public class MaxEnvironment
```
<div class="tiny-text">namespace: <a href="/docs/apidocs/csharp/max.process.env">max.process.env</a></div>








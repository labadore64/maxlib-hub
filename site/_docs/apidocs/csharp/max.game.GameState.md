---
title: enum GameState
toc: true
---

# enum GameState

Represents different states of the game running.

<div class="tiny-text">syntax:</div>
```
public enum GameState
```
<div class="tiny-text">namespace: <a href="/docs/apidocs/csharp/max.game">max.game</a></div>
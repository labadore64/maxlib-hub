---
title: namespace max.process.env
---

# namespace max.process.env

## Members

* **[MaxEnvironment](/docs/apidocs/csharp/max.process.env.MaxEnvironment)** - This class represents the Environment Controller for the MaxEnvironment. This controller manages the various components of the MaxEnvironment and acts as a public interface outside of the library for the MaxEnvironment.

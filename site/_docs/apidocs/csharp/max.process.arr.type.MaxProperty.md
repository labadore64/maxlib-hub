---
title: class MaxProperty
toc: true
---

# class MaxProperty
## Summary
An attribute used on MaxObject properties to add them to the Max Environment.

<div class="tiny-text">syntax:</div>
```
public class MaxProperty : Attribute
```
<div class="tiny-text">namespace: <a href="/docs/apidocs/csharp/max.process.arr.type">max.process.arr.type</a></div>

## Constructors

### MaxProperty()

Initializes the property with the name of the calling member.

<div class="tiny-text">syntax:</div>
```
public MaxProperty([CallerMemberName] string propertyName = null)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">[CallerMemberName]</td>
	<td class="params__name">propertyName</td>
	<td>Calling member's name.</td>
</tr>
	</tbody>
</table>


## Properties

### Name

Name of the property

<div class="tiny-text">syntax:</div>
```
public string Name { get; private set; }
```





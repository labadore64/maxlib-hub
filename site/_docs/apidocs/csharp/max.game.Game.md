---
title: class Game
toc: true
---

# class Game
## Summary
A class that represents the entire game.

<div class="tiny-text">syntax:</div>
```
public abstract class Game
```
<div class="tiny-text">namespace: <a href="/docs/apidocs/csharp/max.game">max.game</a></div>



## Properties

### CurrentState

Current gamestate.

<div class="tiny-text">syntax:</div>
```
protected GameState CurrentState { get; set; }
```
### CurrentGame

The current running game.

<div class="tiny-text">syntax:</div>
```
public static Game CurrentGame { get; private set; }
```
### Environment

The Max environment.

<div class="tiny-text">syntax:</div>
```
public MaxEnvironment Environment { get; private set; }
```


## Methods

### OnExit()

The exit routine of your game.

<div class="tiny-text">syntax:</div>
```
protected abstract void OnExit();
```




### Exit()

Exits the game.

<div class="tiny-text">syntax:</div>
```
public static void Exit()
```




### OnInitialize()

The initialization of your game.

<div class="tiny-text">syntax:</div>
```
protected abstract void OnInitialize();
```




### Run()

Runs the game.

<div class="tiny-text">syntax:</div>
```
public void Run()
```




### OnUpdate()

Update the game's state here.

<div class="tiny-text">syntax:</div>
```
protected abstract void OnUpdate();
```




### DatatypeCreate<T>()

Creates an instance of a specified data type.

<div class="tiny-text">syntax:</div>
```
public static IDataType DatatypeCreate<T>()
```



 Returns: New instance *(  IDataType )*
### DatatypeCreate<T>(ulong Length)

Creates an instance of a specified data type.

<div class="tiny-text">syntax:</div>
```
public static IDataType DatatypeCreate<T>(ulong Length)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Length</td>
	<td>Number of elements</td>
</tr>
	</tbody>
</table>

 Returns: New instance *(  IDataType )*
### DatatypeCreate(Type Type)

Creates an instance of a specified data type.

<div class="tiny-text">syntax:</div>
```
public static IDataType DatatypeCreate(Type Type)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">Type</td>
	<td class="params__name">Type</td>
	<td>Datatype</td>
</tr>
	</tbody>
</table>

 Returns: New instance *(  IDataType )*
### DatatypeCreate(Type Type, ulong Length)

Creates an instance of a specified data type.

<div class="tiny-text">syntax:</div>
```
public static IDataType DatatypeCreate(Type Type, ulong Length)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">Type</td>
	<td class="params__name">Type</td>
	<td>Datatype</td>
</tr><tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Length</td>
	<td>Number of elements</td>
</tr>
	</tbody>
</table>

 Returns: New instance *(  IDataType )*
### InstanceCreate<T>()

Creates an instance of the specified type.

<div class="tiny-text">syntax:</div>
```
public static T InstanceCreate<T>() where T : MaxObject
```



 Returns: New instance *(  T )*
### InstanceCreate(Type Type)

Creates an instance of the specified type.

<div class="tiny-text">syntax:</div>
```
public static MaxObject InstanceCreate(Type Type)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">Type</td>
	<td class="params__name">Type</td>
	<td>Type</td>
</tr>
	</tbody>
</table>

 Returns: New instance *(  MaxObject )*
### InstanceDispose(ulong UID)

Disposes and destroys the instance with the given UID.

<div class="tiny-text">syntax:</div>
```
public void InstanceDispose(ulong UID)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">ulong</td>
	<td class="params__name">UID</td>
	<td>UID</td>
</tr>
	</tbody>
</table>


### InstanceDispose(MaxObject Instance)

Disposes and destroys the instance.

<div class="tiny-text">syntax:</div>
```
public void InstanceDispose(MaxObject Instance)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">MaxObject</td>
	<td class="params__name">Instance</td>
	<td>Instance</td>
</tr>
	</tbody>
</table>





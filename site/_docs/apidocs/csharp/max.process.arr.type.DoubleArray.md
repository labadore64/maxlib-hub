---
title: class DoubleArray
toc: true
---

# class DoubleArray
## Summary
This class represents a double array managed by the Max Environment.

<div class="tiny-text">syntax:</div>
```
public class DoubleArray : Array<double>
```
<div class="tiny-text">namespace: <a href="/docs/apidocs/csharp/max.process.arr.type">max.process.arr.type</a></div>

## Constructors

### DoubleArray(IntPtr Pointer, ulong Count, ulong Alignment)

Instantiates an array of doubles with a given pointer, element count and alignment.

<div class="tiny-text">syntax:</div>
```
public DoubleArray(IntPtr Pointer, ulong Count, ulong Alignment) : base(Pointer, Count, Alignment)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">IntPtr</td>
	<td class="params__name">Pointer</td>
	<td>Array pointer</td>
</tr><tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Count</td>
	<td>Number of elements</td>
</tr><tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Alignment</td>
	<td>Alignment</td>
</tr>
	</tbody>
</table>
### DoubleArray(ulong Count)

Instantiates an array of doubles with a certain number of elements.

<div class="tiny-text">syntax:</div>
```
public DoubleArray(ulong Count) : base(Count)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Count</td>
	<td>Number of elements</td>
</tr>
	</tbody>
</table>
### DoubleArray(double[] Elements)

Instantiates an array of doubles with an array.

<div class="tiny-text">syntax:</div>
```
public DoubleArray(double[] Elements) : base(Elements)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">double[]</td>
	<td class="params__name">Elements</td>
	<td>Elements</td>
</tr>
	</tbody>
</table>




## Methods

### CopyFromLength(double[] Array, int Length)

Copies a source array to this one, of a certain length of elements.

<div class="tiny-text">syntax:</div>
```
protected override void CopyFromLength(double[] Array, int Length)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">double[]</td>
	<td class="params__name">Array</td>
	<td>Source</td>
</tr><tr>
	<td class="params__type">int</td>
	<td class="params__name">Length</td>
	<td>Length</td>
</tr>
	</tbody>
</table>


### InstanceCreate(ulong Length)

Creates an instance of DoubleArray of a specified length.

<div class="tiny-text">syntax:</div>
```
public override IDataType InstanceCreate(ulong Length)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Length</td>
	<td>Length</td>
</tr>
	</tbody>
</table>

 Returns: Instance *( override IDataType )*


## Operators

#### DoubleArray

Implicitly converts a double array to an instance of DoubleArray.

<div class="tiny-text">syntax:</div>
```
public static implicit operator DoubleArray(double[] Value) => new DoubleArray(Value);
```

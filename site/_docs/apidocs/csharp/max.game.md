---
title: namespace max.game
---

# namespace max.game

## Members

* **[Game](/docs/apidocs/csharp/max.game.Game)** - A class that represents the entire game.
* **[GameState](/docs/apidocs/csharp/max.game.GameState)** - Represents different states of the game running.

---
title: namespace max.process
---

# namespace max.process

## Members

* [max.process.env](/docs/apidocs/csharp/max.process.env)
* [max.process.arr](/docs/apidocs/csharp/max.process.arr)

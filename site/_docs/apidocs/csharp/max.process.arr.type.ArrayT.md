---
title: class Array<T>
toc: true
---

# class Array<T>
## Summary
This abstract class represents an array managed by the Max Environment.

<div class="tiny-text">syntax:</div>
```
public abstract class Array<T> : IDataType where T : unmanaged
```
<div class="tiny-text">namespace: <a href="/docs/apidocs/csharp/max.process.arr.type">max.process.arr.type</a></div>

## Constructors

### Array<T>(IntPtr Pointer, ulong Count, ulong Alignment)

Initializes the array with a pointer, array length and alignment.

<div class="tiny-text">syntax:</div>
```
protected Array(IntPtr Pointer, ulong Count, ulong Alignment)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">IntPtr</td>
	<td class="params__name">Pointer</td>
	<td>Pointer</td>
</tr><tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Count</td>
	<td>Length</td>
</tr><tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Alignment</td>
	<td>Alignment</td>
</tr>
	</tbody>
</table>
### Array<T>(ulong Count)

Initializes an array of a specified length.

<div class="tiny-text">syntax:</div>
```
protected Array(ulong Count)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Count</td>
	<td>Count.</td>
</tr>
	</tbody>
</table>
### Array<T>()

Initializes an array with 0 length.

<div class="tiny-text">syntax:</div>
```
protected Array()
```


### Array<T>(T[] Elements)

Initializes an array with an array of elements.

<div class="tiny-text">syntax:</div>
```
protected Array(T[] Elements)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">T[]</td>
	<td class="params__name">Elements</td>
	<td>Elements</td>
</tr>
	</tbody>
</table>


## Properties

### ElementByteSize

The size of each element of the array in bytes.

<div class="tiny-text">syntax:</div>
```
public abstract ulong ElementByteSize { get; }
```
### ByteLength

Length of the array in bytes.

<div class="tiny-text">syntax:</div>
```
public abstract ulong ByteLength { get; }
```


## Methods

### CopyFromLength(T[] Array, int Length)

Copies a set number of values from one array to another.

<div class="tiny-text">syntax:</div>
```
protected abstract void CopyFromLength(T[] Array, int Length);
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">T[]</td>
	<td class="params__name">Array</td>
	<td>Array to copy</td>
</tr><tr>
	<td class="params__type">int</td>
	<td class="params__name">Length</td>
	<td>Number of elements</td>
</tr>
	</tbody>
</table>


### CopyFrom(T[] Array)

Copies values from one array to this one.

<div class="tiny-text">syntax:</div>
```
public void CopyFrom(T[] Array)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">T[]</td>
	<td class="params__name">Array</td>
	<td>Array</td>
</tr>
	</tbody>
</table>


### InstanceCreate(ulong Length)

Creates an instance of this datatype.

<div class="tiny-text">syntax:</div>
```
public abstract IDataType InstanceCreate(ulong Length);
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Length</td>
	<td>Length</td>
</tr>
	</tbody>
</table>

 Returns: Instance *( abstract IDataType )*
### getByte(int Index)

Gets the value at a certain index as a byte.

<div class="tiny-text">syntax:</div>
```
protected byte getByte(int Index)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">int</td>
	<td class="params__name">Index</td>
	<td>Index</td>
</tr>
	</tbody>
</table>

 Returns: Byte value *( byte )*
### getShort(int Index)

Gets the value at a certain index as a short.

<div class="tiny-text">syntax:</div>
```
protected short getShort(int Index)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">int</td>
	<td class="params__name">Index</td>
	<td>Index</td>
</tr>
	</tbody>
</table>

 Returns: Short value *( short )*
### getInt(int Index)

Gets the value at a certain index as an int.

<div class="tiny-text">syntax:</div>
```
protected int getInt(int Index)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">int</td>
	<td class="params__name">Index</td>
	<td>Index</td>
</tr>
	</tbody>
</table>

 Returns: Int value *( int )*
### getLong(int Index)

Gets the value at a certain index as a long.

<div class="tiny-text">syntax:</div>
```
protected long getLong(int Index)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">int</td>
	<td class="params__name">Index</td>
	<td>Index</td>
</tr>
	</tbody>
</table>

 Returns: Long value *( long )*
### getFloat(int Index)

Gets the value at a certain index as a float.

<div class="tiny-text">syntax:</div>
```
protected float getFloat(int Index)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">int</td>
	<td class="params__name">Index</td>
	<td>Index</td>
</tr>
	</tbody>
</table>

 Returns: Float value *( float )*
### getDouble(int Index)

Gets the value at a certain index as a double.

<div class="tiny-text">syntax:</div>
```
protected double getDouble(int Index)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">int</td>
	<td class="params__name">Index</td>
	<td>Index</td>
</tr>
	</tbody>
</table>

 Returns: Double value *( double )*
### getBool(int Index)

Gets the value at a certain index as a boolean.

<div class="tiny-text">syntax:</div>
```
protected bool getBool(int Index)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">int</td>
	<td class="params__name">Index</td>
	<td>Index</td>
</tr>
	</tbody>
</table>

 Returns: Bool value *( bool )*
### setByte(int Index, byte Value)

Sets the value at the given index as a byte.

<div class="tiny-text">syntax:</div>
```
protected void setByte(int Index, byte Value)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">int</td>
	<td class="params__name">Index</td>
	<td>Index</td>
</tr><tr>
	<td class="params__type">byte</td>
	<td class="params__name">Value</td>
	<td>Value</td>
</tr>
	</tbody>
</table>


### setShort(int Index, short Value)

Sets the value at the given index as a short.

<div class="tiny-text">syntax:</div>
```
protected void setShort(int Index, short Value)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">int</td>
	<td class="params__name">Index</td>
	<td>Index</td>
</tr><tr>
	<td class="params__type">short</td>
	<td class="params__name">Value</td>
	<td>Value</td>
</tr>
	</tbody>
</table>


### setInt(int Index, int Value)

Sets the value at the given index as an int.

<div class="tiny-text">syntax:</div>
```
protected void setInt(int Index, int Value)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">int</td>
	<td class="params__name">Index</td>
	<td>Index</td>
</tr><tr>
	<td class="params__type">int</td>
	<td class="params__name">Value</td>
	<td>Value</td>
</tr>
	</tbody>
</table>


### setLong(int Index, long Value)

Sets the value at the given index as a long.

<div class="tiny-text">syntax:</div>
```
protected void setLong(int Index, long Value)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">int</td>
	<td class="params__name">Index</td>
	<td>Index</td>
</tr><tr>
	<td class="params__type">long</td>
	<td class="params__name">Value</td>
	<td>Value</td>
</tr>
	</tbody>
</table>


### setFloat(int Index, float Value)

Sets the value at the given index as a float.

<div class="tiny-text">syntax:</div>
```
protected void setFloat(int Index, float Value)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">int</td>
	<td class="params__name">Index</td>
	<td>Index</td>
</tr><tr>
	<td class="params__type">float</td>
	<td class="params__name">Value</td>
	<td>Value</td>
</tr>
	</tbody>
</table>


### setDouble(int Index, double Value)

Sets the value at the given index as a double.

<div class="tiny-text">syntax:</div>
```
protected void setDouble(int Index, double Value)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">int</td>
	<td class="params__name">Index</td>
	<td>Index</td>
</tr><tr>
	<td class="params__type">double</td>
	<td class="params__name">Value</td>
	<td>Value</td>
</tr>
	</tbody>
</table>


### setBool(int Index, bool Value)

Sets the value at the given index as a bool.

<div class="tiny-text">syntax:</div>
```
protected void setBool(int Index, bool Value)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">int</td>
	<td class="params__name">Index</td>
	<td>Index</td>
</tr><tr>
	<td class="params__type">bool</td>
	<td class="params__name">Value</td>
	<td>Value</td>
</tr>
	</tbody>
</table>


### SetData(IDataType Data)

Copies the data from the passed DataType to this array.

<div class="tiny-text">syntax:</div>
```
public void SetData(IDataType Data)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">IDataType</td>
	<td class="params__name">Data</td>
	<td>Data</td>
</tr>
	</tbody>
</table>


### RePoint(IntPtr Pointer, ulong Count, ulong Alignment)

Repoints the array somewhere else, with new number of elements and alignment.

<div class="tiny-text">syntax:</div>
```
protected void RePoint(IntPtr Pointer, ulong Count, ulong Alignment)
```

#### Parameters

<table class="params__table">
	<thead>
		<tr>
			<th class="params__type">Type</th>
			<th class="params__name">Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
			<tr>
	<td class="params__type">IntPtr</td>
	<td class="params__name">Pointer</td>
	<td>Pointer</td>
</tr><tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Count</td>
	<td>Length</td>
</tr><tr>
	<td class="params__type">ulong</td>
	<td class="params__name">Alignment</td>
	<td>Alignment</td>
</tr>
	</tbody>
</table>





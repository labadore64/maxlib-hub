---
title: Max Library Machine C#
---
# Max Library Machine C#

Max Library Machine C# (also called *Max C#*) is a library (``maxlib.dll``) that creates an object environment for game development with ``mxker`` in C#.

## In-Depth Documentation

This documentation goes into depth about the internal structure of our implementation of Max Library Machine C# - forks should provide their own documentation.

* [game](../game)
* [process](../process)
* kernel
* UI
* [aux](../maux)

## API Documentation

The [API Documentation](/docs/apidocs/csharp/index/) provides details about the definitions of ``public`` and ``protected`` members of ``maxlib.dll``.

*Note: To be compatible across titles, these definitions should be consistent as a baseline standard across Max C# implementations.*

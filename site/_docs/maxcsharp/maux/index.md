---
permalink: /docs/maxcsharp/maux
title: max.aux
---

# max.aux

``max.aux`` represents auxillary classes and structs that are used throughout the Max Library Machine.

* [ReflectionUtil](ReflectionUtil) - Reflect on types.
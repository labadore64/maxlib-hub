---
toc: true
title: Reflection Utilities
---

# Reflection Utilities

These utilities allow you to use C# reflection to gather information on methods and types.

*Please note that reflection is computationally expensive and should be minimized as much as possible.*

## String Conversion

Types can be converted into a string with the method ``TypeToString()``, which will convert it to the following format:

```
[type], [assembly]
```

To retrieve the type back, use ``StringToType()``.

You can also retrieve the entire type name with ``FullTypeToString()``:

```
[namespace].[type], [assembly]
```

To retrieve the type back, use ``FullStringToType()``. To ensure there are no possible ambiguities, ``FullTypeToString()`` and ``FullStringToType()`` are preferred.

## Types

To get a list of types in the entire assembly that inherit a specific type, you can call ``GetTypesContainingType()``.

This can be useful for obtaining a list of types that inherit a ``InstanceCreate()`` method and using the list to create new instances through the method call instead of reflection.

## Methods

You can get a string representing a method from a ``MethodInfo`` object with ``MethodToString()`` or ``FullMethodToString()``, in the following format:

```
[type]:[method]
```

You can also get a string of a method from a ``Delegate``, with ``DelegateToString()`` or ``FullDelegateToString()``.

To get the method from a string, call ``StringToMethod()`` or ``FullStringToMethod()``.

Like with types, it is encouraged to use ``FullMethodToString()``, ``FullDelegateToString`` or ``FullStringToMethod()`` to avoid ambiguities.
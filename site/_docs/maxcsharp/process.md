---
title: max.process
---

# max.process

``max.process`` represents classes that help connect the UI, duplex and kernel components into a single abstract environment.

* [arr](arr)
* [env](env)

---
title: max.game
---

# max.game

``max.game`` represents the entire game.

* [Game](Game) - An abstract class that contains all the game functions a developer uses to interact with Max Library Machine.
* [Game Methods](method) - Methods in the ``Game`` class used to manage the state of the game.
	* [Control](method/control) - Methods for controlling the flow of the game.
	* [Instance Management](method/instance) - Methods for managing instances.
	* [Datatypes](method/datatype) - Methods for managing data types.

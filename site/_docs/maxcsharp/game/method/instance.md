---
toc: true
title: Instance Management
---

# Instance Management

Instance management methods allow you to create, destroy and check if instances exist in the Max Environment.

## Methods

### InstanceCreate()

To create an instance of a ``MaxObject`` class, use ``InstanceCreate()``.

```
// creates a new instance of TestObject
TestObject obj = Game.InstanceCreate<TestObject>();
```

This will create a new instance of your object and stage it in the Max Environment. When the Max Environment is updated, the instance is assigned a UID and pushed to the environment.

### InstanceDestroy()

Disposes the given instance and removes it from the Max Environment. You can pass the object or its UID.

```
// pass the instance
Game.InstanceDestroy(obj);

// pass the UID
Game.InstanceDestroy(obj.UID);
```

*Note: If you still reference the instance somewhere in your program it won't be picked up from the GC. It's just released from the Max Environment.*

### InstanceExists()

Checks if the instance exists in the Max Environment given its UID.

```
// checks if the instance exists
if(Game.InstanceExists(obj.UID)){
	// do stuff here
}
```

*Note: if the object is currently staged to be created, ``InstanceExists()`` will return ``false``.*
---
toc: true
title: Datatype Methods
---

# Datatype Methods

Datatype methods allow you to create Datatypes easily.

## Methods

### DatatypeCreate()

Create an instance of a given datatype.

```
// creates a bool array of length 10.
BoolArray array = Game.DatatypeCreate<BoolArray>(10);
```
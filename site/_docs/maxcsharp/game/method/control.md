---
toc: true
title: Control Methods
---

# Control Methods

Control methods help maintain the flow of the game and manage functions such as game initialization, updating and exiting.

## Methods

### Run()

Runs the game. It will keep running until an error occurs or if ``Game.Exit()`` is called.

```
// Runs the game
Game.Run();
```

### Initialize()

An internal method that runs when the game is first initialized. It sets ``CurrentGame`` to this running instance of the game, and initializes internal components before running the ``OnInitialize()`` routine. It returns the game state (``GAME_CONTINUE`` or a failure state).

### OnInitialize() 

A protected method called in ``Initialize()``, used for user-defined routines that occur when the game is initialized and should be overridden by the inheriting class.

```
protected override void OnInitialize()
{
    // do stuff here
}
```

### Update() 

An internal method that runs every time the game is updated. It updates the state of internal components and runs ``OnUpdate()``. It returns a game state (``GAME_CONTINUE`` or a failure state).

### OnUpdate() 

A protected method called in ``Update()``, used for user-defined routines that occur when the game is updated and should be overridden by the inheriting class.

```
protected override void OnUpdate()
{
    // do stuff here
}
```

### DoExit()

An internal method that runs when the game exits and calls ``OnExit()``. It returns a game state (``GAME_EXIT`` or a failure state).

### OnExit()

A protected method called in ``DoExit()``, used for user-defined routines that occur when the game exits and should be overridden by the inheriting class.

```
protected override void OnExit()
{
    // do stuff here
}
```

### Exit()

A static method that exits the game when called.

```
// Exits the game.
Game.Exit();
```
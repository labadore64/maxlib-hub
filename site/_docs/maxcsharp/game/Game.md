---
toc: true
---

# Game

``Game`` is an abstract class that represents the game's main interface for the library.

To use it, create your own class that inherits ``Game`` and implement ``OnInitialize()``, ``OnUpdate()`` and ``OnExit()``.

```
using max.game;

namespace mygame
{
    public class MyGame : Game
    {
        // put the initialize code here
        protected override void OnInitialize()
        {

        }

        // put the update code here
        protected override void OnUpdate()
        {

        }

        // put the exit code here
        protected override void OnExit()
        {

        }
    }
}
```

## Game State

The ``CurrentState`` property contains the current running ``GameState`` of the game. ``GameState`` is an enum that has several possible values:

* ``GAME_CONTINUE`` - The game is continuing normally.
* ``GAME_EXIT`` - The game exited gracefully.
* ``GAME_MAIN_FAILURE`` - The game failed in the main update loop.
* ``GAME_EXIT_FAILURE`` - The game failed when trying to exit.
* ``GAME_INIT_FAILURE`` - The game failed when trying to initialize.

Usually, while the game is running, ``CurrentState`` will be ``GAME_CONTINUE``.

## Running the Game

To run your game, create an instance of your ``Game`` class and call ``Run()``.

```
MyGame Game = new MyGame();
Game.Run();
```

Note that ``Game.Run()`` continues in a loop until ``Game.Exit()`` is called, or if a fatal error is thrown.

## Game Methods

``Game`` contains many methods to manage the state of the game. Refer to [Game Methods](../method) for more details.
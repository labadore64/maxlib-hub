---
title: Game Methods
---

# Game Methods

``Game`` contains many static methods for managing the current game. This section helps explain these methods.

* [Control](control) - Methods for controlling the flow of the game.
* [Instance Management](instance) - Methods for managing instances.
* [Datatypes](datatype) - Methods for managing data types.

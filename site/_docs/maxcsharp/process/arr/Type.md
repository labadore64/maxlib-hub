---
title: Types
toc: true
---

# Types

Types are managed data in the Max Library Machine. You can add types to your ``MaxObjects`` so they can be managed by Max. All types contained natively in Max are ``public``.

## Provided Types

The following types are provided natively by Max Library Machine:

* ``bool``
* ``byte``
* ``sbyte``
* ``short``
* ``ushort``
* ``int``
* ``uint``
* ``long``
* ``ulong``
* ``float``
* ``double``
* ``char``

You can define your own datatypes by implementing the ``IDataType`` interface.

## Interface

All types implement the ``IDataType``. This interface provides the following properties and methods:

* ``DataPointer`` - ``IntPtr`` pointing to the data in memory.
* ``Length`` - Length of the data in elements.
* ``DataLength`` - ``Length`` as a ``ulong``.
* ``Alignment`` - Alignment of the data.
* ``InstanceCreate()`` - Returns a new instance of this datatype.
* ``SetData()`` - Sets the data pointer and length of this instance the same as another datatype instance.

Max Library Machine scans for all classes that implement ``MaxObject`` on initialization, and any property given the ``MaxProperty`` attribute whose class implements ``IDataType`` will automatically configure its properties with Max Environment when an instance is constructed, as long as the inheriting class calls the default ``MaxObject`` constructor.

## Arrays

Arrays are special kinds of data types that inherit the abstract class ``Array`` and use indexers to access multiple elements.

You can read more about arrays [here](Arrays).

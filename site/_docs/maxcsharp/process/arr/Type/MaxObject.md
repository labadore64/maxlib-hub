---
toc: true
title: MaxObject
---

# MaxObject

``MaxObject`` is an abstract class that represents an object in the Max Environment.

## UID

``UID`` is the ``ulong`` unique ID assigned to the object by the kernel.

## Control

Control methods are called during certain points of the life cycle of the ``MaxObject``.

Some methods are ``internal`` and are called by ``Game`` and its life cycle.

* ``Initialize()`` - Initializes the object when its about to be pushed after being created.
* ``Dispose()`` - Disposes the object and releases it from the environment when it is about to be pushed after being destroyed. Also calls ``Destroy()``, which is overriden by the developer. Is also called when the instance's deconstructor is called.

While others are ``public virtual`` and can be overridden in the inheriting class.

* ``Create()`` - Routines that should run when the instance is created through ``Game.InstanceCreate()``.
* ``Destroy()`` - Routines that should run when the instance is destroyed.

Properties are used to check the state of the object through this flow.

* ``Initialized`` - Whether or not the object has called ``Initialize()`` yet.
* ``Disposed`` - Whether or not the object has called ``Dispose()`` yet.

## Max Property Management

Properties in ``MaxObjects`` are managed Max Properties. Use the ``MaxProperty`` attribute on any property that should be part of the Max Environment.

```
[MaxProperty]
public IntArray Values
{
    get => (IntArray)get();
    set => set(value);
}
```

Properties are stored in the ``Dictionary<string, IDataType> Properties`` field.

### Add

``AddProperty()`` is a ``protected`` method that adds a property to the property list.

### get/set

``get()`` and ``set()`` (notice they are lowercase) are methods that are used for the getters/setters with properties with the ``MaxProperty`` attribute. Both methods will pass ``System.Runtime.CompilerServices.CallerMemberName`` by default as the property's key.

``get()`` returns the ``IDataType`` stored at the property. It requires a cast.

``set()`` takes any ``IDataType`` and sets it to the property value in the object's property map.

### Property Prototypes

When the ``MaxObject`` templates are created when the Max Environment initializes, property prototypes for each object are created with ``InitializePropertyPrototypes()``. This method reflectively searches for all properties that contain the ``MaxProperty`` attribute and use the names stored for the attribute, based on the property's reference name, and populates the ``Prototypes`` array.

When the object is created with ``Game.InstanceCreate()``, it will create real properties based on the prototype template by calling ``AddPropertiesFromPrototypes()``.

Property prototypes are stored using the ``PrototypeProperty`` struct and stored in the field array ``Prototypes``.

## Create Instance

Any class that overrides ``MaxObject`` must define its own ``InstanceCreate()`` method which returns an instance of itself. This is used with ``Game.InstanceCreate()`` so that all instances created as part of the game are able to be managed by the game.

Simply return a new instance of this object.
```
protected override MaxObject InstanceCreate()
{
    return new TestObject();
}
```
*Remember to always use ``Game.InstanceCreate()`` when creating instances in your game, otherwise they will not function properly as part of the Max Environment.*
---
toc: true
title: MaxProperty
---

# MaxProperty

``MaxProperty`` is an attribute that can be assigned to properties in a class that inherits ``MaxObject`` to signify properties that are part of the Max Library Machine.

## Declaration

You can declare any object that implements ``IDataType`` as a ``MaxProperty``, for example arrays.

```
[MaxProperty]
public IntArray Values
{
    get => (IntArray)get();
    set => set(value);
}
```

## PrototypeProperty

``PrototypeProperty`` is an ``internal`` ``struct`` that contains a template of a ``MaxProperty``.

When the ``Game`` initializes, all classes that inherit ``MaxObject`` will have a template instance created and added to a list inside the ``Game``. This instance is generated with a prototype of the property map used by all instances of this object to interact with the Max Library Machine. Each time a new instance is created through ``Game.InstanceCreate()``, it will transform the prototype property map from the template instance into a series of properties that are linked with the Max Environment and Memory Management.
---
toc: true
title: MaxEnvironment
---

# MaxEnvironment

``MaxEnvironment`` is a class that represents objects and instances in the Max Environment.

## Type Templates

When ``Game`` initializes, ``MaxEnvironment`` will use reflection to collect information about certain types used in the Max Environment. 

* ``MaxObjectTypes`` - ``Dictionary<Type, MaxObject>`` that contains a set of template instances of every class that inherits ``MaxObject`` type in the loaded assemblies. Each template object creates an array of prototype properties when being generated.
* ``MaxDataTypes`` - ``Dictionary<Type, IDataType>`` that contains a set of template instances of every class that implements ``IDataType`` in the loaded assemblies.

When ``Game.InstanceCreate()`` or ``Game.CreateDatatype()`` is called, they will create an instance from the template by calling the template instance's ``InstanceCreate()`` method.

## MaxObject Instances

``MaxEnvironment`` contains and manages all the ``MaxObject`` instances currently loaded in the Max Environment.

You can access an instance directly with its ``UID`` using the indexer.

```
// gets the MaxObject with the UID of "test"
MaxObject testObject = Game.Env[test];
```

### Instance Management

There are several methods you can use to manage and test instances.

* ``InstanceCreate()`` - Creates an instance of a given type.
* ``InstanceDispose()`` - Disposes the instance provided.
* ``InstanceExists()`` - Checks to see if an instance exists in the MaxEnvironment. 

### Instance List

All ``MaxObject`` instances that are active in the Max Environment are assigned a ``UID`` value (Unique ID). These instances are stored in a ``Dictionary<ulong, MaxObject>``, ``Objects``.

When an object is first created with ``Game.InstanceCreate()``, the object is staged by calling ``StageCreatedObject()``. When the Max Environment calls ``Update()``, it will push the staged objects with ``PushCreatedObjects()``

Likewise, when an object's ``Dispose()`` method is called, the object is staged for destruction by calling ``StageDestroyedObject()``, and is removed from the object list on the Max Environment's next update by calling ``PushDestroyedObjects()``.
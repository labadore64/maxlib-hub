---
title: max.process.env
---

# max.process.env

``max.process.env`` represents the virtual environment for ``MaxObjects``.

* [MaxEnvironment](MaxEnvironment) - Represents the Max Environment.

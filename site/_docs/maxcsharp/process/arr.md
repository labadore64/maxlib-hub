---
title: max.process.arr
---

# max.process.arr

``max.process.arr`` manages arrays, and contains data types and objects.

* [ArrayManager](ArrayManager) - The class that manages arrays in memory.
* [MaxProperty](Type/MaxProperty) - Attribute for Max Properties.
* [Types](Type) - Types managed by the Max Library Machine.
	* [Arrays](Type/Arrays) - Array types.
	* [MaxObject](Type/MaxObject) - The base Max Library Object type.

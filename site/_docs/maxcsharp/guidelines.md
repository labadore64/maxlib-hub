---
title: Guidelines
toc: true
---

# Project Guidelines

This is a list of basic coding guidelines to help you both understand how the main Max Library Machine C# project is structured as well as helping guide developers on how to make clean contributions. 

## General Guidelines

### Minimum Design Requirements

While Max Library Machine C# is designed to minimize how many ``public`` and ``protected`` members to allow for the highest level of internal modularity, it does require these members to stay consistent between implementations of Max. Please refer to the definition of the [API Documentation](/docs/apidocs/csharp/index/) to guarentee baseline compatibility with Max games.

## Types

### Partial Types

All types should be marked ``partial``. ``partial`` is a type modifier that allows types to be split up into multiple files, and are assembled based on their namespace and type name. You can read more about ``partial`` types [here](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/types-and-structs/partial-types-and-methods).

On the project level, having all types made of many ``partial`` files allows individual files to represent various parts of class functionality, and allows easy swapping or removal of these files if necessary. This kind of project management allows for easy swapping of the internal type components before compilation.

Generally, individual .cs files should be relatively small and large files should be split up into smaller ones.

### Namespaces

Class namespaces are consistent with the component in mxker that the component either is interacting with or represents. The only exception is ``max.game``, which represents the direct interface between the developer's private C# code and the Max Library Machine, and ``max.aux``, which represents general utility functions that are subject to being replaced or moved.

### Directory Structure

Because all types are ``partial``, directories can be used to organize partial type files into functionality provided. Each type should have its own base directory.

Reserve the base directory for ``Global.cs``. This file should include the summary comment for the given type. Other directories should indicate what kind of functionality is managed in those files, and subdirectories can be used to help organize further.

As an example, here is the directory structure of ``max.game.Game``.

```
- max
  - game
    - Game
	  - control
	    - Exit.cs
		- GameState.cs 
		- Initialize.cs 
		- Run.cs 
		- Update.cs 
	  - method 
	    - [...]
	  - process 
	    - [...]
	  - property
	    - [...]
      Global.cs
```

## Commenting

Commenting throughout the source is highly encouraged to help new and unfamiliar developers understand how Max works.

### Comment on Separate Lines with a line divider

Comments should be on their own lines for readability in most cases.

```
int value = 0; // please don't do this...
// and not this either...
int number = 3;

// do this instead!
int base = 1;
```

### XML Documentation Comments

XML documentation comments are comments added above declarations to describe what they do or represent. You should try to add these comments on all members, so that Intellisense detects it and auto-generating documentation can build docs from the comments.

```
/// <summary>
/// Runs the game.
/// </summary>
public void Run()
```

You can read more about XML documentation [here](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/xmldoc/).
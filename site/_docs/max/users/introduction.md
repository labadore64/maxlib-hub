---
title: Introduction
toc: true
---

*Notice: these documentation pages are a work in progress and will change periodically as they are updated.*

# Introduction

Max Library Machine is a collection of small programs that work together to build a video game environment. When compiled and configured, it forms a game engine that allows the internal topology of the Max Library Machine components to be changed easily through modding.

To get started with Max Library Machine, [install](/docs/max/users/installation/) the libraries on your machine.

## Max as a Game Engine

Max Library Machine isn't like a lot of game engines where it compiles with a closed library that is distributed with the game. Instead, Max is installed directly to your computer, and games reference Max as a library.

Max Library Machine is split into two parts - mxker, which is a special program that manages most of the game's code, and an abstract implementation, which is a library that lets game developers write in their own environment and import mxker for use.

Because mxker is compatible with both Windows and Linux (and many other systems too!), Max can be installed on many machines. Because game developers use a wide variety of environments for developing their games, Max has abstract implementations which are libraries that allow developers to easily work with mxker. This allows for a high level of native cross-compatibility.

mxker is available completely [open source](/docs/max/meta/license/) and so can be migrated to a wide variety of environments, and many abstract implementations of Max Library Machine are also open source as well, which allows the game engine to be modified without changing the game itself.

mxker also allows for modules to be loaded dynamically at runtime. This allows for custom functionality to be added at runtime, and even completely modifying the presentation of the game. We plan on developing a wide range of accessibility and topology focused UI modules available for free for use with Max. Implementations can be developed for public or private purposes and can freely implement these modules.

All of these changes can be either applied globally to the entire machine, or just to specific Max-compatible titles. This means that Max creates an environment on your computer for Max-compatible games to work with, and that environment allows for high levels of customization of these games.

## How Max Works

This is a basic overview of Max Library Machine's basic functionality.

### Programs

In most implementations of Max Library Machine, there are two main programs used to create the game.

* mxker - The core of the engine, which acts as a kernel for the game. It also loads modules dynamically, coordinating the interactions between modules to customize the output of the game.
* Abstract Implementation - An abstract pass-through library to mxker that allows developers to easily manage its resources in other environments.

These programs work together to form the game engine.

mxker is the main kernel of Max Library Machine and manages how the game environment stages and presents objects both to the game and the user. It communicates with the abstract implementation to allow other environments to direct the game's functionality. In many cases it is compiled directly for your computer on installation.

Abstract implementations, like [Max C#](/docs/maxcsharp/index/), are designed to communicate from mxker to the game's external code. This way, developers can easily develop their own games directly in their own environment with Max without much advanced set up. These implementations are designed to have a small amount of ``public`` members so most of the internals can be changed for custom builds.

While a default installation of mxker and its various implementation libraries is provided through the Max file system environment, these defaults can be overridden for each game, enabling custom mxker and library implementations for individual games.

### Modding

Max Library Machine is [open source](/docs/max/meta/license/) and so all of the internal components can be modified as necessary. However, Max is natively built with strong modding capabilities that allow for high levels of extendability without changing the machine's core.

Max is designed with a customizable hierarchy of resource management, allowing users to easily override game defaults for game assets natively, and add additional functionality such as color-blind support or additional captions. If resources aren't located, it uses the Max installation as a default. Max can also load additional functionality through custom mxker modules.

### User Interface

Max Library Machine's most powerful modding capabilities are through its ability to completely modify the user interface of any game built using Max. Max loads its UI topologies from modules loaded into mxker.

Most applications build their UI as part of a static pipeline of the application, another part being the core logic that controls the state of the application. This forces all user interaction to be constructed by the developer and enforced through the structure of the application. In certain cases, such as game developers working within a commercial game engine, this process is abstracted - not only are the developer's choices enforced, but also the choices of the game engine's developers as well.

Max is instead designed with a direct, intentional separation between the logic layer of the game and its user interface - using a messaging system to direct the creation of user interfaces. These interfaces are built through a user interface module interpreting these messages and transforming it into an interactable environment for the player. This transformation impacts both how a player interprets the game environment and how they respond to it. When an interaction is recorded, a message is sent back to the game and progresses its state.

This allows Max to enable multiple topologies for playing games and other software in an open source environment modular while still preserving privately produced and distributed commercial code. These topologies can be developed independently of individual games, applied to any Max compatible game and even have custom builds of these topologies for specific titles.
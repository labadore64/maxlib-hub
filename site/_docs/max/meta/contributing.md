---
title: Contributing
---
*Notice: these documentation pages are a work in progress and will change periodically as they are updated.*

# Contributing

Max Library Machine is always open to new contributions...

## Directory Structure

The current directory structure of the project, with the common repository files removed, is like this:

```
- bin
- c 
- config
- csharp
- docs
- img
- lib
- lua-5.3
- public
- python
- res
- scripts
- site
- vs
Makefile
```

* Output
	* ``bin`` and ``lib`` contain compiled binaries and libraries respectively created by Max Library Machine.
* Source Code
	* ``c`` contains c files and source code.
	* ``csharp`` contains source code for Max C#.
	* ``python`` contains source code for Max Python.
	* ``config`` contains configuration files.
* Meta
	* ``docs`` contains documentation for the repository.
	* ``img`` contains images for the repository.
* Website
	* ``public`` contains live website files.
	* ``site`` contains files used to create the website.
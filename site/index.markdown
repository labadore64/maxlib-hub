---
title: "Max Library Machine"
layout: splash
permalink: /
date: 2020-05-12T11:48:41-04:00
header:
  overlay_image: /assets/images/unsplash-image-1.jpg
  actions:
    - label: "Learn More"
      url: "/docs/max/users/introduction"
excerpt: "A game engine designed for nearly unlimited customizable end user experience, built from the ground up."
feature_row:
  - title: "Easy to Use"
    excerpt: "Max Library Machine is a simple to use but powerful cross-platform game library that is compatible with many environments."
    url: "docs/max/users/installation"
    btn_label: "Get Started"
    btn_class: "btn--primary"
  - title: "High Customization"
    excerpt: "Max games have fully customizable user interfaces that allow for a wide range of end user experiences."
    url: "https://gitlab.com/labadore64/maxlib-hub"
    btn_label: "Read More"
    btn_class: "btn--primary"
  - title: "Completely Free"
    excerpt: "Max Library Machine is licensed under the MIT license and free to use and fork, and custom builds are compatible with most Max games."
    url: "/license"
    btn_label: "View License"
    btn_class: "btn--primary"
---

{% include feature_row id="intro" type="center" %}

{% include feature_row %}